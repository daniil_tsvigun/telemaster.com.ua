<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content calling">
        <div class="wrapper">
            <div class="main-block">
                <h1><?php echo LANG('label_call_master'); ?></h1>
                <div class="search-box">
                    <p>

                    </p>
                    <input id="region_search" placeholder="<?php echo LANG('label_enter_your_region'); ?>" type="text">
                    <ul>
                    </ul>
                    <div class="btn-block">
                        <a href="<?php echo site_url('calling'); ?>" class="btn-type2">
                            <p><?php echo LANG('label_find_master'); ?></p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/default/footer'); ?>