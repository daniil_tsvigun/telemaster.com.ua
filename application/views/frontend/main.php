<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
<div class="content main">
    <div class="wrapper">
        <div class="main-block">
            <div class="text">
                <h5><?php echo LANG('label_main_page_header'); ?></h5>
                <?php echo LANG('label_main_page_text'); ?>
                <?php //echo LANG('label_temp_page_text'); ?>
            </div>
            <div class="btn-block">
                <a href="<?php echo site_url('instructions'); ?>" class="btn-type1">
                    <span><?php echo LANG('label_setup_tuner_pre'); ?></span>
                    <p><?php echo LANG('label_setup_tuner'); ?></p>
                    <span><?php echo LANG('label_instr_video'); ?></span>
                </a>
                <a href="<?php echo site_url('calling'); ?>" class="btn-type2">
                    <p><?php echo LANG('label_call_master'); ?></p>
                </a>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/default/footer'); ?>