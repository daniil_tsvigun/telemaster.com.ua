<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content calling partner_calling">
        <div class="wrapper">
            <div class="main-block partners_padd-h1">
                <h1><?php echo LANG('label_call_partner'); ?></h1>

                <div class="search-box">
                    <p>
                        <?php echo LANG('label_provider_law'); ?>
                    </p>
                    <input id="region_search_partner" placeholder="<?php echo LANG('label_enter_region'); ?>" type="text">
                    <ul>
                    </ul>
                    <div class="btn-block">
                        <a href="<?php echo site_url('calling'); ?>" class="btn-type2">
                            <p><?php echo LANG('label_find_partner'); ?></p>
                        </a>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <p class="page_comment partners_cont"><?php echo LANG('label_provider_comment'); ?></p>
<?php $this->load->view('frontend/default/footer'); ?>