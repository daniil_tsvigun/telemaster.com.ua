<?php

class User_administrative extends User {

    public function __construct($source = null) {
        parent::__construct($source);

        $this->role_id = 2;
    }
}
class Administrative_model extends User_model {


    protected $table_name = 'administrative_users';

    public function __construct() {
        parent::__construct();
    }

}