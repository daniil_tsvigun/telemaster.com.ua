<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instructions extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();

    }

    public function index(){
        $this->load->view('frontend/static_instr'.SQL_LANG);
        // $data['instructions']=$this->instruction_model->get_all_instructions();
        // $this->load->view('frontend/instructions',$data);
    }

    public function model($model=false,$tuner=false){

        if($model===false){
            redirect(site_url('instructions'));
        } elseif($model=='slm'&&$tuner!==false){

            if(intval($tuner)===intval(0)){
                $data['model']='XFz8LGXezJU';
            } else{

                $instructions=$this->instruction_model->get_all_instructions();
                foreach($instructions as $instruction){

                    if(intval($instruction['id'])===intval($tuner)){

                        $data['model']=$instruction['slm_video'];
                        break;
                    }
                }

            }
            $this->load->view('frontend/templates/instruction',$data);
        } else {
            if(intval($model)===intval(0)){

                $data['model']='thOJqaOaBiM';
            } else{
                $instructions=$this->instruction_model->get_all_instructions();
                foreach($instructions as $instruction){
                    if(intval($instruction['id'])===intval($model)){
                        $data['model']=$instruction['video'];
                        break;
                    }
                }

            }
            $this->load->view('frontend/templates/instruction',$data);
        }
    }
}