<?php

class Page {

    public $alias;
    public $template_id;
    public $title_ru;
    public $content_ru;
    public $seo_title_ru;
    public $seo_keywords_ru	;
    public $seo_description_ru;
    public $title_ua;
    public $content_ua;
    public $seo_title_ua;
    public $seo_keywords_ua	;
    public $seo_description_ua;

    public function __construct($source = null) {
        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->template_id        = isset($source['template_id'])?        $source['template_id']       :1;
        $this->title_ru           = isset($source['title_ru'])?           $source['title_ru']          :'';
        $this->content_ru     = isset($source['content_ru'])?     $source['content_ru']    :'';
        $this->seo_title_ru       = isset($source['seo_title_ru'])?       $source['seo_title_ru']      :'';
        $this->seo_keywords_ru    = isset($source['seo_keywords_ru'])?    $source['seo_keywords_ru']   :'';
        $this->seo_description_ru = isset($source['seo_description_ru'])? $source['seo_description_ru']:'';
        $this->title_ua           = isset($source['title_ua'])?           $source['title_ua']          :'';
        $this->content_ua     = isset($source['content_ua'])?     $source['content_ua']    :'';
        $this->seo_title_ua       = isset($source['seo_title_ua'])?       $source['seo_title_ua']      :'';
        $this->seo_keywords_ua    = isset($source['seo_keywords_ua'])?    $source['seo_keywords_ua']   :'';
        $this->seo_description_ua = isset($source['seo_description_ua'])? $source['seo_description_ua']:'';
    }
}

class Page_model extends MY_Model {

    protected $table_name = 'pages';
    protected $t_templates = 'templates';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_all_page($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->table_name .'.*, title'. SQL_LANG .' as title')->from($this->table_name)->get()->result_array();

        return $result;
    }

    public function get_by_alias($alias, $only_active = false){
        if(!alias_validate($alias)){
            return false;
        }

        if($only_active){
            $this->db->where(array($this->table_name.'.active'=>1));
        }

        $res = $this->db->select($this->table_name .'.title'. SQL_LANG .' as title, '.
                                 $this->table_name .'.content'. SQL_LANG .' as content, '.
                                 $this->table_name .'.seo_title'. SQL_LANG .' as seo_title, '.
                                 $this->table_name .'.seo_description'. SQL_LANG .' as seo_description, '.
                                 $this->table_name .'.seo_keywords'. SQL_LANG .' as seo_keywords, '.
                                 $this->table_name .'.alias,'.
                                 $this->t_templates .'.alias as template'
                                 )
                        ->from($this->table_name)
                        ->join($this->t_templates,$this->t_templates .'.id ='. $this->table_name .'.template_id')
                        ->where(array($this->table_name.'.alias' => $alias))
                        ->get();

        return ($res->num_rows() === 1) ? $res->row_array() : false;

    }

}