<div id="header">
    <div class="container-fluid clearfix">
        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <a href="<?php echo site_url();?>/panel" title="TeleMaster Control panel" class="logo">
                    TeleMaster
<!--                    <img src="/assets/img/panel_logo.png" alt="TeleMaster" height="40px" />-->
                </a>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                <div class="top-menu-wrap">
                    <ul class="top-menu">

                        <li>
                            <a class="" href="#" title="Clear cache">
                                <i class="icon-trash"></i>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo site_url();?>" target="_blank" class="user-menu" title="Go to site">
                                <i class="icon-energy"></i>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo site_url();?>/panel/auth/logout" title="Logout">
                                <i class="icon-logout"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>