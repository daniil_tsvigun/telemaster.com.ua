<?php

class Picture {

    public $alias;
    public $title_ru;
    public $title_ua;
    public $master_id;
    public $images;
    public $preview_name;
    public $preview_type;
    public $categories;
    
    public function __construct($source = null) {
        $this->alias        = isset($source['alias'])        ? $source['alias']        : '';
        $this->master_id    = isset($source['master_id'])    ? $source['master_id']    : '';
        $this->title_ru     = isset($source['title_ru'])     ? $source['title_ru']     : '';
        $this->title_ua     = isset($source['title_ua'])     ? $source['title_ua']     : '';
        $this->preview_name = isset($source['preview_name']) ? $source['preview_name'] : '';
        $this->preview_type = isset($source['preview_type']) ? $source['preview_type'] : '';
        $this->images       = isset($source['images'])       ? $source['images']       : array();
        $this->categories   = isset($source['categories'])   ? $source['categories']   : array();

        if(isset($source['confirm_status'])){
            $this->confirm_status = $source['confirm_status'];
        }
        if(isset($source['products'])){
            $this->products = $source['products'];
        }
    }

}

class Picture_model extends MY_Model {

    protected $table_name = 'pictures';
    protected $t_tovars = 'tovars';
    protected $t_prints_images = 'pictures_images';
    protected $t_prints_preview = 'pictures_preview';
    protected $t_templates_tovar = 'templates_tovar';
    protected $t_prints_image_mockup='pictures_image_mockup';
    protected $t_filters_to_prints = 'filters_to_print';
    protected $t_filters_to_tovars = 'filters_to_tovars';
    protected $t_painter_users = 'painter_users';
    protected $t_tags = 'tags';
    protected $t_tag_to_print = 'tag_to_print';
    protected $t_refuse_message = 'refuse_message';
    protected $t_favorites = 'favorites';
    protected $t_likes = 'likes';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;
    private $print_image = array(
        'a' => array('x' => 300, 'y' => 300, 'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
        
    );
    private $print_preview = array(
        'preview' => array('x' => 320, 'y' => 320, 'metod' => 4, 'options' => array('bg_color' => '255,255,255'))
    );
    private $print_mockup = array(
        'a' => array(false, false, 'metod' => 4, 'options' => array('bg_color' => '255,255,255'))
    );

    public function __construct() {
        parent::__construct();
        $this->load->library('FolderWork');
        $this->load->library('UploadImage');
    }

    public function get_all_templates(){
        $result = $this->db->select($this->t_templates_tovar . '.*')->from($this->t_templates_tovar)->get()->result_array();
        return $result;
    }
    
    public function get_all_picture($pagination = false) {

        if ($pagination) {
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $_sort = str_replace("-", " ", $pagination['sort']);
            $_sort = str_replace("title", "title" . SQL_LANG, $_sort);
            $this->db->order_by($_sort);
        }
        $result = $this->db->select($this->table_name . '.*, title' . SQL_LANG . ' as title')->from($this->table_name)->get()->result_array();
        return $result;
    }

    public function get_picture_catalog_by_author($author_id,$filters){

        $_print = array();
        if($filters){
            $this->db->select('tAll.print_id');

            foreach($filters as $filter_group){
                $_dat[] = '(SELECT print_id FROM '. $this->t_filters_to_prints .'  WHERE filter_id ='. implode(" OR filter_id =",$filter_group).')';
            }
            $this->db->from('(' . implode(' UNION ALL ', $_dat) . ') as tAll');
            $this->db->group_by('tAll.print_id');
            $this->db->having('COUNT(tAll.print_id) >= '.count($_dat));
            $res = $this->db->get();
            if($res->num_rows() > 0){
                foreach($res->result_array() as $item){
                    $_print[] = $item['print_id'];
                }
            }
            $res = $this->db->select('id')->from($this->table_name)->where($this->table_name.'.active = 1 AND '. $this->table_name.'.master_id = '. $author_id .'')->where_in('id',$_print)->get();
            if($res->num_rows() > 0){
                foreach($res->result_array() as $item){
                    $_print[] = $item['id'];
                }
            }
            return $_print;
        }else{
            $this->db->select($this->table_name.'.id');
            $this->db->from($this->table_name);
            $this->db->where($this->table_name.'.active = 1 AND '. $this->table_name.'.master_id = '. $author_id .'');
            $res = $this->db->get();

            if($res->num_rows() > 0){
                foreach($res->result_array() as $item){
                    $_print[] = $item['id'];

                }
                return $_print;
            }
        }

        // echo $this->db->last_query();
        return $_print;
    }
    public function get_picture_catalog_by_user($user,$filters){

        $_print = array();
        if($filters && isset($filters[21])){
            $_filters = $filters[21];
            $this->db->select($this->t_likes.'.print_id');
            $this->db->from($this->t_likes);
            $this->db->where('user_id = '. $user->id .' AND role_id = '. $user->role_id);
            $this->db->where('print_id IN(SELECT print_id FROM '. $this->t_filters_to_prints .' WHERE filter_id IN('. implode(",",$_filters).'))');
            $res = $this->db->get();

            if($res->num_rows() > 0){
                foreach($res->result_array() as $item){
                    $_print[] = $item['print_id'];

                }
                return $_print;
            }
        }else{
            $this->db->select($this->t_likes.'.print_id');
            $this->db->from($this->t_likes);
            $this->db->where('user_id = '. $user->id .' AND role_id = '. $user->role_id);
            $res = $this->db->get();

            if($res->num_rows() > 0){
                foreach($res->result_array() as $item){
                    $_print[] = $item['print_id'];

                }
                return $_print;
            }
        }

        // echo $this->db->last_query();
        return $_print;
    }

    public function get_catalog($pint_ids,$pagination = false) {

        if(!$pint_ids){
            return false;
        }

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($this->table_name .'.'. $_sort);
        }

        $this->db->select($this->table_name.'.*,'.
            $this->table_name.'.title'. SQL_LANG .' as title,'.
            $this->t_painter_users.'.alias as author_alias,'.
            $this->t_painter_users.'.name as author_name,'.
            $this->t_painter_users.'.id as author_id,
            (SELECT COUNT('. $this->t_favorites .'.print_id) FROM '. $this->t_favorites .' WHERE '. $this->t_favorites .'.print_id = '. $this->table_name .'.id) as likes'

        );
        $this->db->from($this->table_name);
        $this->db->where($this->table_name.'.id IN('. implode(",",$pint_ids).')');
        $this->db->join($this->t_painter_users,$this->t_painter_users.'.id = '. $this->table_name .'.master_id','left');
        $res = $this->db->get();

        if($res->num_rows() > 0){
            return $res->result_array();
        }else{
            return false;
        }

    }

    public function get_picture_by_template($template_id) {

        $result = $this->db->select($this->table_name . '.*, title' . SQL_LANG . ' as title,'.
                                    $this->t_prints_images.'.name as photo_name,'.
                                    $this->t_prints_images.'.type as photo_type'
                            )
                           ->from($this->table_name)
                           ->join($this->t_prints_images,$this->t_prints_images.'.print_id = '. $this->table_name.'.id AND '. $this->t_prints_images .'.active = 1 AND '. $this->t_prints_images .'.template_id ='.$template_id)
                           ->get()->result_array();
        return $result;
    }

    public function create($source, $source_class) {

        $data = new $source_class($source);

        $_images = $data->images;
        $_categories = $data->categories;
        unset($data->images);
        unset($data->categories);
        unset($data->preview_name);
        unset($data->preview_type);

        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();

        if (count($_images) > 0) {
            $this->save_images_info($_images,$_id);
        }
        
        if (isset($_FILES) && !empty($_FILES)) {
            $this->save_photos($_FILES,$_id);
        }
        $this->save_print_categories($_categories,$_id);
        return $_id;
    }

    public function create_by_painter($source, $source_class,$user) {
        $source['master_id'] = $user->id;
        $source['title_ru'] = $source['title'];
        $source['title_ua'] = $source['title'];
        $source['alias'] = make_unic_alias(translit($source['title']));
        $source['products'] = json_encode($source['products']);
        $data = new $source_class($source);

        $_categories=$data->categories;
        unset($data->images);
        unset($data->categories);
        unset($data->preview_name);
        unset($data->preview_type);

        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        $this->save_base64_preview($source['preview'],$_id);
        $this->save_photos_painter($source,$_id);
        $this->save_print_tags($source['tags'],$_id);
        $this->save_print_categories(array('21'=>$_categories),$_id);
    }

    public function get_mockup_by_id($picture_id=false){
        if(!$picture_id){
            return array();
        }
        $result = $this->db->get_where($this->t_prints_image_mockup,array('picture_id'=>$picture_id))->result_array();
        return $result;
    }

    public function update($source, $source_id, $source_class) {
        $_data = $this->get_by_id($source_id);

        if ($_data) {
            if(isset($source['products'])){
                $source['products'] = json_encode($source['products']);
            }

            $data = new $source_class($source);

            $_images = $data->images;

            $_categories=$data->categories;
            unset($data->images);
            unset($data->categories);
            unset($data->preview_name);
            unset($data->preview_type);

            $this->db->update($this->table_name, $data, ['id' => $source_id]);
            if (count($_images) > 0) {
                $this->save_images_info($_images, $source_id);
            }
            if (isset($_FILES) && !empty($_FILES)) {

                $this->save_photos($_FILES, $source_id);
            }

            $this->save_print_categories($_categories,$source_id);

            return true;
        }
        return false;
    }

    public function get_picture_filters($picture_id){
        $res = $this->db->get_where($this->t_filters_to_prints,array('print_id'=> $picture_id));
        if($res->num_rows() > 0){
            return $res->result_array();
        }else{
            return false;
        }
    }

    private function save_print_categories($filters, $print_id){

        $_data = $this->db->where(array('print_id'=>$print_id))->get($this->t_filters_to_prints)->result_array();
        $_old_filters = array();
        if(count($_data) > 0){
            foreach($_data as $item){
                $_old_filters[$item['filter_id']] =  $item['filter_id'];
            }
        }

        $_no_new_filters = ($_old_filters == $filters)?true:false;

        if($_no_new_filters){
            return;
        }

        foreach($filters as $filter_group){
            foreach($filter_group as $filter_id ){
                $data[$filter_id]['filter_id'] = $filter_id;
                $data[$filter_id]['print_id'] = $print_id;
            }
        }

        $this->db->where(array('print_id'=>$print_id))->delete($this->t_filters_to_prints);
        $this->db->insert_batch($this->t_filters_to_prints,$data);
        $this->update_tovar_filters($filters,$print_id);
    }

    private function save_print_tags($tags,$print_id){

        $data = array();
        foreach($tags as $tag){
            if(!name_validate($tag)){
                continue;
            }
            $_teg_id = $this->save_tag($tag);
            $data[$_teg_id]['tag_id'] = $_teg_id;
            $data[$_teg_id]['print_id'] = $print_id;
        }

        if(count($data) >0){
            $this->db->insert_batch($this->t_tag_to_print,$data);
        }
    }

    private function save_tag($tag){
        $res = $this->db->get_where($this->t_tags,array('name'=>$tag))->row();
        if($res){
            return $res->id;
        }else{
            $this->db->insert($this->t_tags,array('name'=>$tag));
            return $this->db->insert_id();
        }
    }

    public function get_refuse_message(){
        $_res = $this->db->select('*, message'. SQL_LANG .' as message')
            ->from($this->t_refuse_message)
            ->where(array('active'=>1))
            ->order_by('sort')
            ->get()->result_array();

        foreach($_res as $item){
            $data[$item['id']] = $item['message'];
        }

        return $data;
    }

    private function update_tovar_filters($filters,$print_id){

        $data = $this->db->select('DISTINCT tovar_id',false)
            ->from($this->t_filters_to_tovars)
            ->where(array('picture_id'=>$print_id))
            ->get()->result_array();
        if(!$data){
            return;
        }

        $this->db->delete($this->t_filters_to_tovars,array('picture_id'=>$print_id));

        foreach($data as $item){
            $_insert_data = array();
            foreach($filters as  $val){
                $_insert_data[key($val)]['tovar_id'] = $item['tovar_id'];
                $_insert_data[key($val)]['picture_id'] = $print_id;
                $_insert_data[key($val)]['filter_id'] = key($val);
                $_insert_data[key($val)]['product_id'] = 0;
            }

            $this->db->insert_batch($this->t_filters_to_tovars,$_insert_data);
        }
    }

    private function save_images_info($data, $print_id) {

        $res = $this->db->where(array('print_id' => $print_id))->get($this->t_prints_images)->result_array();
        $update_data = array();
        $delete_data = array();
        foreach ($res as $image_info) {
            if (isset($data[$image_info['id']])) {
                $update_data[] = array(
                    'id' => $image_info['id'],
                    'print_id' => $print_id,
                    'template_id' => isset($data[$image_info['id']]['template_id']) ? $data[$image_info['id']]['template_id'] : '',
                );
            } else {
                unlink('images/prints/' . $print_id . '/' . $image_info['name']. '.' .$image_info['type'] );
                $delete_data[] = $image_info['id'];
            }
        }

        if (count($update_data) > 0) {
            
            $this->db->update_batch($this->t_prints_images, $update_data, 'id');
        }
        if (count($delete_data) > 0) {
            
            $this->db->where_in('id', $delete_data)->delete($this->t_prints_images);
        }
    }

    private function save_base64_preview($preview,$print_id){
        $img_name = str_replace('e', 'a', substr(sha1(time() . $print_id), 0, 5));
        $_url = 'images/prints/'.$print_id.'/preview';

        $img = str_replace('data:image/jpeg;base64,', '', $preview);
        $img = str_replace(' ', '+', $img);
        $img = base64_decode($img);
        $FolderWork = new FolderWork($_url);
        $FolderWork->chek_and_make_path();
        $FolderWork->clear();
        $fpng = fopen($_url.'/'.$img_name.'.jpg', "w");
        fwrite($fpng,$img);
        fclose($fpng);

        $this->db->where('id',$print_id);
        $this->db->update($this->table_name, array(
            'preview_name' => $img_name,
            'preview_type' => 'jpg',
        ));

    }

    private function save_photos_painter($data,$print_id){
        $_templates = get_templates_data();
        $_files = $data['files'];
        $_products = $data['products'];
        foreach($_templates as  $template_data){
            if(
             !isset($_files[$template_data['id']])
             || !isset($_products[$template_data['id']])
             || empty($_files[$template_data['id']])
             || empty($_products[$template_data['id']])
            ){
                continue;
            }

            if($template_data['id'] == 1){
                $_file_tif = $_files[$template_data['id']].'/'. $template_data['id'] .'.tif';
                $img_name = str_replace('e', 'a', substr(sha1(time() . $template_data['id']), 0, 5));
                $_url = 'images/prints/' . $print_id . '/' . 'mockup';
                $FolderWork = new FolderWork($_url);
                $FolderWork->chek_and_make_path();
                $FolderWork->clear();
                $moving = copy($_file_tif, $_url . '/' . $img_name.'.tif');
                if ($moving) {
                    $this->db->insert($this->t_prints_image_mockup, array(
                        'picture_id' => $print_id,
                        'name' => $img_name,
                        'type' => 'tif',
                    ));
                }
            }
            $_file = $_files[$template_data['id']].'/'. $template_data['id'] .'.jpg';
            $img_name = str_replace('e', 'a', substr(sha1(time() . $template_data['id']), 0, 5));
            $_url = 'images/prints/' . $print_id . '/' ;
            $FolderWork = new FolderWork($_url);
            $FolderWork->chek_and_make_path();
            $_movented = copy($_file, $_url . $img_name.'.jpg');

            if ($_movented){
                $this->db->insert($this->t_prints_images, array(
                    'print_id' => $print_id,
                    'template_id' => $template_data['id'],
                    'name' => $img_name,
                    'type' => 'jpg',
                ));
            }
        }

        $FolderWork = new FolderWork('uploads/tmp/'. $_SESSION['print_upload']['print_upload_path']);
        $FolderWork->del();
    }

    private function save_photos($data, $print_id) {

        $_templates = get_templates_data();

        foreach ($data as $up_key => $up_file) {

            if (empty($up_file['name'])) {
                continue;
            }

            if ($up_key == 'preview') {

                $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));
                $_url = 'images/prints/'.$print_id.'/preview';
                $FolderWork = new FolderWork($_url);
                $FolderWork->chek_and_make_path();
                $FolderWork->clear();
                $UploadImage = new UploadImage;
                $error_msg = '';
                $UploadImage->do_action($up_file, '/' . $FolderWork->path . '/' , $this->print_preview['preview']['x'], $this->print_preview['preview']['y'], 10000, $img_name, 90, $this->print_preview['preview']['metod']);
                if(is_array($this->print_preview['preview']['options'])){
                    $res = $UploadImage->addOptions($this->print_preview['preview']['options']);
                    if ($res !== true) {
                        $error_msg = 'Ошибка конфигурации постера: ' . $res;
                        break;
                    }
                    $res = $UploadImage->save();
                    if ($res !== true) {
                        $error_msg = 'Ошибка загрузки постера: ' . $res;
                        break;
                    }
                }
                if (empty($error_msg)) {
                    $this->db->where('id',$print_id);
                    $this->db->update($this->table_name, array(
                        'preview_name' => $img_name,
                        'preview_type' => $UploadImage->format,
                    ));
                }
            } elseif($up_key == 'mockup'){
                if(!empty($up_file['name'][key($up_file['name'])])) {
                    $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));
                    $_url = 'images/prints/' . $print_id . '/' . 'mockup';
                    $FolderWork = new FolderWork($_url);
                    $FolderWork->chek_and_make_path();
                    $FolderWork->clear();
                    $file_info = new SplFileInfo($up_file['name'][key($up_file['name'])]);
                    $moving = move_uploaded_file($up_file['tmp_name'][key($up_file['tmp_name'])], $_url . '/' . $img_name.'.'.$file_info->getExtension());
                    if ($moving) {


                        $element_exists=$this->db->get_where($this->t_prints_image_mockup,array('picture_id'=>key($up_file['name'])))->result();

                        if(empty($element_exists)){

                            $this->db->insert($this->t_prints_image_mockup, array(
                                'picture_id' => key($up_file['name']),
                                'name' => $img_name,
                                'type' => $file_info->getExtension(),
                            ));
                        } else {
                            $this->db->where('picture_id',key($up_file['name']));
                            $this->db->update($this->t_prints_image_mockup, array(
                                'picture_id' => key($up_file['name']),
                                'name' => $img_name,
                                'type' => $file_info->getExtension(),
                            ));
                        }

                    }
                }
            } else {

                $_img_atributs = get_img_params($up_file);
                if($_img_atributs['error']){
                    add_system_message('danger', $_img_atributs['message'], '');
                    continue;
                }elseif($up_file['type'] != "image/pjpeg" && $up_file['type'] != "image/jpeg" && $up_file['type'] != "image/jpg"){
                    add_system_message('danger', LANG('error_file_format_jpg') , '');
                    continue;
                }

                $_template_id = $this->_check_sizes($_img_atributs['size_x'],$_img_atributs['size_y'],$_templates);
                if(!$_template_id){
                    add_system_message('danger', LANG('error_file_size_exist'),'');
                    continue;
                }

                $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));
                $_url = 'images/prints/' . $print_id . '/' ;
                $FolderWork = new FolderWork($_url);
                $FolderWork->chek_and_make_path();

                $_movented = move_uploaded_file($up_file["tmp_name"], $_url . $img_name.'.jpg');

                if ($_movented) {
                    $this->db->insert($this->t_prints_images, array(
                        'print_id' => $print_id,
                        'template_id' => $_template_id,
                        'name' => $img_name,
                        'type' => 'jpg',
                    ));
                }
            }


        }
    }


    private function _check_sizes($size_x,$size_y,$template_info){
        var_dump($template_info);
        $exist = false;
        foreach($template_info as $data){
            if($size_x == $data['image_size_x'] && $size_y == $data['image_size_y']){
                $exist = $data['id'];
            }
        }

        return $exist;
    }

    public function delete($alias,$user){
        $_res = $this->get_by_alias($alias);
        if(!$_res || $_res['master_id'] != $user->id ){
            return false;
        }

        $this->db->where(array('id'=>$_res['id']))->update($this->table_name,array('active'=>0));
        $this->db->where(array('picture_id'=>$_res['id']))->update($this->t_tovars,array('active'=>0));

        return true;
    }

    public function update_title($alias,$title_ua,$user){
        $_res = $this->get_by_alias($alias);
        if(!$_res || $_res['master_id'] != $user->id ){
            return false;
        }
        $_data = array(
            'title_ua'=>$title_ua,
            'title_ru'=>$title_ua,
            'alias'=>make_unic_alias(translit($title_ua))
        );

        $this->db->where(array('id'=>$_res['id']))->update($this->table_name,$_data);
        return $_data['alias'];
    }

}
