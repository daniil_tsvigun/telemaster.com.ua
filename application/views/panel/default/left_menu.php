<?php  if(count($left_menu) > 0): ?>
    <ul class="navigation">
    <?php foreach($left_menu as $main_level):?>
        <?php if(!isset($main_level['submenu'])){
            continue;
        }?>
        <li class="<?php echo($active == $main_level['alias'])?'active':'';?>">
            <a href="#">
                <i class="icon-<?php echo $main_level['icon'];?>"></i><?php echo $main_level['title'];?>
            </a>
            <ul>
            <?php foreach($main_level['submenu'] as $submenu):?>
                <li>
                    <a class="" href="<?php echo $submenu['url'];?>">
                        <?php echo $submenu['title']?>
                    </a>
                </li>
            <?php endforeach;?>
            </ul>
        </li>
    <?php endforeach;?>
    </ul>
<?php endif; ?>

