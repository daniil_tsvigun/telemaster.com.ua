<!DOCTYPE html>
<html>
<head>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tele Master</title>
    <link href="/assets/lib/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="/assets/lib/chosen/1.4.2/chosen.min.css" rel="stylesheet"> -->
    <!-- <link href="/assets/lib/font-awesome/4.3.0/font-awesome.min.css" rel="stylesheet"> -->
    <link href="/assets/lib/animate.css/3.3.0/animate.min.css" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('favicon.ico'); ?>" />
    <link rel="icon" type="image/x-icon" href="<?php echo base_url('favicon.ico'); ?>" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css?v=1.3.6"/>
    <script type="text/javascript">
        var site_url = "<?php echo site_url();?>";
    </script>

</head>
<body>
<script>
 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
 
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a
,m)
 
})(window,document,'script','https://www.google-analytics.com/analytics.js',
'ga');

  ga('create', 'UA-88207792-1', 'auto');
  ga('send', 'pageview');

</script>

<div id="vk_api_transport"></div>