<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();
 
    }

    public function index($category = false){

        $search = $this->input->get('search');
        if(!text_validate($search) || !$search){
            $search = '';
        }

        $data['current_alias'] = $category;
        $data['current_link'] = site_url('/search/'.$data['current_alias']);

        if($category == false){
            $category_id = 0;
        }else{
            $_category_data = $this->category_model->get_by_alias($category);
            if($_category_data == false){
                redirect(site_url('/search/'.$data['current_alias']));
            }else{
                $category_id = $_category_data['id'];
            }
        }

        $data['categories'] = sub_tree_from_arr($this->category_model->get_all_categories(false,true));

        $pagination_data = get_pagination_data(DEFAULT_CATALOG_PAGINATION_COUNT);
        $data['sort'] = $pagination_data['sort'];
        $_tovars_data = $this->search_model->get_search_tovars($category_id,$search);
        $data['tovars'] = $this->tovar_model->get_catalog($_tovars_data,$pagination_data);
        $data['pagination'] = pagination_catalog($_tovars_data,$pagination_data['page'],$pagination_data['count'],make_link_by($data['current_link'],array('page')));

        $this->load->view('frontend/search',$data);
    }


}