<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller
{

    public $user = '';

    public function __construct()
    {
        parent::__construct();
        $this->user = get_user();

    }

    public function index()
    {
        $this->load->view('frontend/static_faq' . SQL_LANG);
    }
}