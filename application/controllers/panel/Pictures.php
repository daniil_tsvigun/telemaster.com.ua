<?php

class Pictures extends MY_Controller{
    
    public $user = '';
    protected $base_url = 'panel/pictures';
    protected $home_url = 'panel/pictures/';
    protected $config_info;
    private $source_models = array(
        'picture'=>'Picture', 
    );
    
    public function __construct(){
        
        parent::__construct();
        $this->user = get_user(true);

        if(!has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel/auth/login');
        }

        if(!has_permission('list_prints_permissions',$this->user)){
            redirect(site_url() .'/panel/');
        }

        $this->config_info = new Config_info('pictures');
        $this->lang_exist = $this->config->config['lang_uri_abbr'];
    }
    
    public function index()
    {
    }
    
    public function picture(){
        $this->_watch('picture');
    }
    
    private function _watch($type = 'picture'){
        
        $_model = $type .'_model';
        $_table = $type.'s';
        $data['data_type'] = $type;
        $data['page_title'] = LANG('page_title_print_list');
        $data['base_link'] = $this->base_url;
        $data['base_lang_link'] = site_url() .'/'.$this->base_url;
        $data['current_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['create_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
        $data['remove_link'] = site_url() .'/'. $this->base_url .'/remove/'. $type;
        $data['edit_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type;
        $data['content'] = 'panel/default/default_list';
        $data['display_fields'] = $this->config_info->display_table_fields[$type];
        $pagination_data = get_pagination_data();
        $data['sort'] = $pagination_data['sort'];
        $data['table_data'] = $this->$_model->{'get_all_'. $type}($pagination_data);
        if(count($data['table_data']) == 0 && $pagination_data['page'] >1){
            $_link = make_link_by($data['current_link'],array('page'));
            $action_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
            redirect($action_link .'page='.($pagination_data['page'] - 1));
        }
        $data['pagination'] = pagination($_table,array(),$pagination_data['page'],$pagination_data['count'],make_link_by($data['current_link'],array('page')));
        $this->load->view('panel/main_layout',$data);
    }
    
    public function create($type = 'picture',$close = false){

        redirect_has_no_permission($this->user,'create_print_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $data = new $_source_model();
        $this->_editor($data,'create',$type,$close);

    }

    public function edit($type = 'picture',$id = false,$close = false){

        redirect_has_no_permission($this->user,'edit_print_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type]; 
        $data =  new $_source_model($this->{$type .'_model'}->get_by_id($id)); 
        if(!$data){
            add_system_message("danger", LANG("ntf_npt_print_no_exist_title"), LANG("ntf_npt_print_no_exist"));
            redirect($this->base_url .'/menu/'. $type);
        } 
        $this->_editor($data,$id,$type,$close);
    }

    private function _editor($source,$data_id,$type,$close){
        $_model = $type .'_model';
        $_source_model = $this->source_models[$type];
        $data['page_title'] = LANG('page_title_print_edit');
        if($data_id === 'create'){
            $data['save_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/create/'. $type .'/close';
        }else{
            $data['save_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'. $data_id;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'.$data_id .'/close';
        }
        $data['model'] = $_model;
        $data['close_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['content'] = $this->base_url .'/'. $type .'_edit';
        
        $this->form_validation->set_rules($this->config_info->validation_rules[$type]);
        $this->form_validation->data_id = $data_id;
        $data['refuse_messages'] = $this->picture_model->get_refuse_message();
        $data['products_list'] = $this->product_model->get_all_product_by_template();
        if ($this->form_validation->run() === true) {
            
            if($data_id ==='create'){

                $_id = $this->$_model->create($this->input->post(),$_source_model);
                add_system_message('success', LANG('ntf_print_created_title'), LANG('ntf_print_created'));
            }else{
                $this->$_model->update($this->input->post(), $data_id,$_source_model);
                add_system_message('success', LANG('ntf_print_edited_title'), LANG('ntf_print_edited'));
                if($this->input->post('confirm_status') && $this->input->post('confirm_status') != 'new'){

                    $this->change('confirm_status',$data_id,$this->input->post('confirm_status'));
                    $_message_data = array(
                        'user_id'=>$this->input->post('master_id'),
                        'user_role_id'=>'4',
                    );

                    if($this->input->post('confirm_status') == 'confirm'){
                        if($this->input->post('tovar_generate')){
                            $_products = $this->input->post('products');
                            if(count($_products) > 0){
                                $this->tovar_model->create_tovars($_products,$data_id);
                                add_system_message('success', LANG('ntf_tovars_created_title'), LANG('ntf_tovars_created'));
                            }
                        }
                        $_message_data ['template'] = 'print_confirm';
                    }elseif($this->input->post('confirm_status') == 'refuse'){
                        $_message_data ['template'] = 'print_refuse';
                        $_message_data ['message'] = ($this->input->post('refuse_message'))?$this->input->post('refuse_message'):$data['refuse_messages'][$this->input->post('refuse_message_id')];
                    }

                    add_system_message('success', LANG('ntf_picture_confirm_status_'. $this->input->post('confirm_status') .'_title'), LANG('ntf_picture_confirm_status_'. $this->input->post('confirm_status') .''));
                    $this->sender_model->message($_message_data);
                }

            }
            $data['data'] =  new $_source_model($this->input->post());
            $data['data']->updated = true;
            $data['products_to_generate'] = isset($data['data']->products)?$data['data']->products:array();
            if($close){
                redirect($data['close_link']);
            }elseif($data_id === 'create' && isset($_id) && $_id){
                redirect($this->home_url .'edit/'. $_id);
            }

        } else {
            if (count($this->input->post()) > 0) {
                $data['data'] = new $_source_model($this->input->post());
                $data['products_to_generate'] = isset($data['data']->products)?$data['data']->products:array();
            } else {
                $data['data'] = $source;
                $data['products_to_generate'] = json_decode($data['data']->products,true);
            }
            $data['data']->updated = false;
        }

        $data['data']->id = $data_id;
        $data['data']->tovar_generate = isset($_POST['tovar_generate'])?$_POST['tovar_generate']:1;
        $data['user_id']=$this->user->id;
        $data['p_category']=$this->filter_model->get_print_filter_ids($data_id);
        $data['print_categories']=$this->filter_model->get_filters_and_groups('21',true);
        $data['data']->preview_name=$this->{$type .'_model'}->get_by_id($data_id)['preview_name'];
        $data['data']->preview_type=$this->{$type .'_model'}->get_by_id($data_id)['preview_type'];
        //$data['image_template'] = ($data_id == 'create')? 'other':$this->category_model->get_template_by_category($data['data']->category_id)->template;
        //$data['image_template_id'] = $this->category_model->get_template_by_category($data['data']->category_id)->id;
        $data['print_images'] = $this->print_image_model->get_image_by_print(($data_id == 'create')?false:$data['data']->id);
        //$data['print_preview'] = $this->print_image_model->get_preview_by_print();
        $data['lang_exist'] =$this->lang_exist;
        $all_categories = $this->category_model->get_all_categories(false,true);
        $data['categories_tree'] = level_tree_from_arr($all_categories);
        //$data['templates'] = $this->panel_model->get_all_templates();

        $data['tovar_templates'] = get_templates_data();
        $data['author_list'] = $this->painter_model->get_all_users();
        $this->load->view('panel/main_layout',$data);
    }
} 