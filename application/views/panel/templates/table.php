<div id="table-content" class="table-overflow">
    <table id="data-table-articles" class="table table-striped table-bordered table-checks">
        <thead>
            <tr>
                <?php foreach($display_fields as $field_title => $field_config):?>
                    <?php if($field_title == 'remove_checkbox'){?>
                        <th width="25px"><input type="checkbox" name="checkRow" class="styled"/></th>
                    <?php }elseif($field_config['sortable'] == 'yes'){?>
                        <?php
                            $_link = make_link_by($current_link,array('sort'));
                            $sort_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
                        ?>
                        <th>
                            <a href="<?php echo $sort_link?>sort=<?php echo ($sort == $field_title.'-asc')?$field_title.'-desc':$field_title.'-asc';?>">
                                <img src="assets/img/arrow_<?php echo ($sort == $field_title.'-desc')?'up':'down'?>.png" alt="" class="arrow_<?php echo ($sort == $field_title.'-desc')?'up':'down'?>">
                                <?php echo LANG('table_'.$field_title)?>
                            </a>
                        </th>
                    <?php }else{?>
                        <th><?php echo LANG('table_'.$field_title)?></th>
                    <?php }?>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
        
        <?php foreach($table_data as $item):?>
            <tr>
                <?php foreach($display_fields as $field_title => $field_config):?>
                    <?php if($field_title == 'remove_checkbox'){?>
                        <td><input type="checkbox" name="items[<?php echo $item['id']?>]" class="styled" value="<?php echo $item['id']?>"/></td>
                    <?php }elseif($field_config['type'] == 'edit_link'){?>
                        <td><a href="<?php echo $edit_link;?>/<?php echo $item['id'];?>"><?php echo $item[$field_title];?></a></td>
                    <?php }elseif($field_config['type'] == 'radio'){?>
                        <td>
                            <input 	type="hidden"   name="changearchive" value="0" checked="checked">
                            <input 	type="checkbox" name="changearchive" class="switcher checked" value="1"
                                      data-name="<?php echo $field_title;?>"
                                      data-type="<?php echo $data_type ?>"
                                      data-url="<?php echo $base_lang_link .'/change' ?>"
                                      data-entity_id="<?php echo $item['id'];?>"
                                      data-style="default"
                                      data-selected="<?php echo($item[$field_title] == 1)? 'true':'false';?>"
                                      data-disabled="false">
                        </td>
                    <?php }elseif($field_config['type'] == 'text_type'){?>
                        <td><?php echo LANG('label_table_type_'. $item[$field_title]);?></td>
                    <?php }elseif($field_config['type'] == 'pay_status'){?>
                        <td><?php echo ($item[$field_title] == 1)? LANG('label_pay_status_active'):LANG('label_pay_status_no_active');?></td>
                    <?php }elseif($field_config['type'] == 'date'){?>
                        <td><?php echo date('d-m-Y H:i',strtotime($item[$field_title]));?></td>
                    <?php }elseif($field_config['type'] == 'text_type_boolean'){?>
                        <td><?php echo LANG('label_table_boolean_'. $item[$field_title]);?></td>
                    <?php }elseif($field_config['type'] == 'text_confirm_status'){?>
                        <td><?php echo LANG('label_confirm_status_now_'. $item[$field_title]);?></td>
                    <?php }elseif($field_config['type'] == 'link_order'){?>
                        <td><a href="<?php echo site_url('/panel/orders/edit/order/'. $item['order_id']);?>"><?php echo $item[$field_title];?></a></td>
                    <?php }elseif($field_config['type'] == 'social_link'){?>
                        <?php 
                            
                            if(strlen ($item[$field_title])>11){
                                $social='FB';
                                $slink='https://www.facebook.com/'.$item[$field_title];
                            } else {
                                $social='VK';
                                $slink='https://vk.com/id'.$item[$field_title];
                            }
                        ?>
                        <td><a href="<?php echo $slink; ?>"><?php echo $social;?></a></td>
                    <?php }elseif($field_config['type'] == 'link_painter'){?>
                        <td><a href="<?php echo site_url('/panel/users/edit/painter/'. $item['painter_id']);?>"><?php echo $item[$field_title];?></a></td>
                    <?php }else{?>
                        <td><?php echo $item[$field_title];?></td>
                    <?php }?>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="20">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div id="sort">
                                <span class="r show-on-page">
                                    <?php
                                        $_link = make_link_by($current_link,array('count','page'));
                                        $count_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
                                    ?>
                                    <?php echo LANG('table_show');?>:
                                    <a href="<?php echo $count_link?>count=10">10</a>
                                    <a href="<?php echo $count_link?>count=25">25</a>
                                    <a href="<?php echo $count_link?>count=50">50</a>
                                    <a href="<?php echo $count_link?>count=100">100</a>
                                  </span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <?php echo $pagination;?>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>

    </table>
</div>