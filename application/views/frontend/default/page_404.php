<?php
$this->load->view('frontend/default/header');
$this->load->view('frontend/default/nav_bar');
?>
<div class="container no_data">
    <div class="content">
        <div class="wrapper">
            <div class="cloud"><?php echo LANG('label_404'); ?><span>404?</span></div>
            <div class="willy"></div>
        </div>

    </div>
</div>
<?php $this->load->view('frontend/default/footer');?>

</body>
</html>