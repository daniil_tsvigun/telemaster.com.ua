<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php $this->load->view('panel/default/system_message');?>
            <div class="row">
                <?php if(count($dashboard_menu) > 0):?>
                    <?php foreach($dashboard_menu as $item):?>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 dash">
                            <a class="" href="<?php echo $item['url']?>" title="<?php echo $item['title']?>"><?php echo $item['title']?><i class="icon-<?php echo $item['icon']?>"></i></a>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>

            <script type="text/javascript">
                function playSound(soundfile) {
                    $("#dummy").html("<embed src=\""+soundfile+"\" hidden=\"true\" autostart=\"true\" loop=\"false\" />");
                }
            </script>
        </div>
    </div>
</div>