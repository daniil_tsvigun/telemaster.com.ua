<?php

class Product {

    public $alias;
    public $category_id;
    public $title_ru;
    public $description_ru;
    public $seo_title_ru;
    public $seo_keywords_ru	;
    public $seo_description_ru;
    public $title_ua;
    public $description_ua;
    public $seo_title_ua;
    public $seo_keywords_ua	;
    public $seo_description_ua;

    public function __construct($source = null) {
        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->article            = isset($source['article'])?            $source['article']           :'';
        $this->price              = isset($source['price'])?              $source['price']             :'';
        $this->price_sale         = isset($source['price_sale'])?         $source['price_sale']        :'';
        $this->category_id        = isset($source['category_id'])?        $source['category_id']       :0;
        $this->flag_new           = isset($source['flag_new'])?           $source['flag_new']          :0;
        $this->flag_action        = isset($source['flag_action'])?        $source['flag_action']       :0;
        $this->flag_sale          = isset($source['flag_sale'])?          $source['flag_sale']         :0;
        $this->title_ru           = isset($source['title_ru'])?           $source['title_ru']          :'';
        $this->print_size_x       = isset($source['print_size_x'])?       $source['print_size_x']      :'';
        $this->print_size_y       = isset($source['print_size_y'])?       $source['print_size_y']      :'';
        $this->print_offset_x     = isset($source['print_offset_x'])?     $source['print_offset_x']    :'';
        $this->print_offset_y     = isset($source['print_offset_y'])?     $source['print_offset_y']    :'';
        $this->description_ru     = isset($source['description_ru'])?     $source['description_ru']    :'';
        $this->seo_title_ru       = isset($source['seo_title_ru'])?       $source['seo_title_ru']      :'';
        $this->seo_keywords_ru    = isset($source['seo_keywords_ru'])?    $source['seo_keywords_ru']   :'';
        $this->seo_description_ru = isset($source['seo_description_ru'])? $source['seo_description_ru']:'';
        $this->title_ua           = isset($source['title_ua'])?           $source['title_ua']          :'';
        $this->description_ua     = isset($source['description_ua'])?     $source['description_ua']    :'';
        $this->seo_title_ua       = isset($source['seo_title_ua'])?       $source['seo_title_ua']      :'';
        $this->seo_keywords_ua    = isset($source['seo_keywords_ua'])?    $source['seo_keywords_ua']   :'';
        $this->seo_description_ua = isset($source['seo_description_ua'])? $source['seo_description_ua']:'';
        $this->filters            = isset($source['filters'])?            $source['filters']           :array();
        $this->images             = isset($source['images'])?             $source['images']            :array();
        $this->icon               = isset($source['icon'])?               $source['icon']              :'';
        $this->other0             = isset($source['other0'])?             $source['other0']            :'';
        $this->other1             = isset($source['other1'])?             $source['other1']            :'';
        $this->other2             = isset($source['other2'])?             $source['other2']            :'';

    }
}

class Product_model extends MY_Model {

    protected $table_name = 'products';
    protected $t_filters_to_products = 'filters_to_products';
    protected $t_filters_to_tovars = 'filters_to_tovars';
    protected $t_products_images = 'products_images';
    protected $t_products_images_data = 'products_images_data';
    protected $t_categories = 'categories';
    protected $t_products_gender_associated = 'products_gender_associated';
    protected $t_product_pay_limits = 'product_pay_limits';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;
    private $image_cutter_sizes = array(
        'a' => array('x' => DEFAULT_MAX_PHOTO_SIZE_X, 'y' => DEFAULT_MAX_PHOTO_SIZE_Y, 'metod' => 3, 'options' => array('bg_color' => '255,255,255')),
        'b' => array('x' => 500, 'y' => 500,  'metod' => 3, 'options' => array('bg_color' => '255,255,255')),
        'd' => array('x' => 160, 'y' => 160,  'metod' => 3, 'options' => array('bg_color' => '255,255,255')),

    );


    public function __construct() {
        parent::__construct();
        $this->load->library('FolderWork');
        $this->load->library('UploadImage');
    }

    public function get_all_product($pagination = false,$only_active =false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($this->table_name .'.'.$_sort);
        }

        if($only_active){
            $this->db->where($this->table_name.'.active = 1');
        }

        $result = $this->db->select($this->table_name .'.*,'.$this->table_name .'.title'. SQL_LANG .' as title,'.
                                    $this->t_products_images .'.name as photo_name,'.
                                    $this->t_products_images .'.type as photo_type,'.
                                    $this->t_categories .'.template_id'
                    )
                    ->from($this->table_name)
                    ->join($this->t_products_images,$this->t_products_images.'.product_id = '.$this->table_name.'.id','left')
                    ->join($this->t_categories,$this->t_categories .'.id = '. $this->table_name .'.category_id','left')
                    ->group_by($this->table_name .'.id')
                    ->get()->result_array();

        return $result;
    }

    public function get_all_product_by_template(){
        $_data = $this->get_all_product(false,true);
        if(count($_data) == 0){
            return array();
        }

        foreach($_data as $item){
            $data[$item['template_id']][$item['id']] = $item;
        }

        return $data;
    }

    public function create($source,$source_class){
        $data = new $source_class($source);
        $_filters = $data->filters;
        unset($data->filters);
        $_images  = $data->images;
        unset($data->images);
        unset($data->other0);
        unset($data->other1);
        unset($data->other2);
        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        if(count($_filters) > 0){
            $this->save_filters($_filters,$_id);
        }
        if(count($_images) > 0){
            $this->save_images_info($_images,$_id);
        }

        if(isset($_FILES) && !empty($_FILES)){
           $this->save_photos($_FILES,$_id);
        }
//        if(isset($_FILES['photo']) && !empty($_FILES['photo'])){
//            $this->save_photos($_FILES['photo'],$_id);
//        }
        if(isset($source['gender'])){
            $this->set_gender_association($source['gender'],$_id);
        }

        if(isset($source['pay'])){
            $this->save_pay_limits($source['pay'],$_id);
        }

        return $_id;
    }

    public function update($source, $source_id,$source_class) {

        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            
            $_filters = $data->filters;  
            unset($data->filters);
            $_images  = $data->images;
            unset($data->images);
            unset($data->other0);
            unset($data->other1);
            unset($data->other2);
            $this->db->update($this->table_name, $data, ['id' => $source_id]);
            if(count($_filters) > 0){
                $this->save_filters($_filters,$source_id);
            }

            if(count($_images) > 0){

                $this->save_images_info($_images,$source_id);
            }

            if(isset($_FILES) && !empty($_FILES)){
                $this->save_photos($_FILES,$source_id);
            }

            if(isset($source['gender'])){
                $this->set_gender_association($source['gender'],$source_id);
            }

            if(isset($source['pay'])){
                $this->save_pay_limits($source['pay'],$source_id);
            }

            return true;
        }
        return false;
    }

    public function get_gender_association($product_id){
        //уточнить у Данила зачем это ?
        if($product_id=='create'){
            $this->db->select('column_name');
            $this->db->limit(10);
            $this->db->from('information_schema.columns');
            $this->db->where('table_name =', $this->t_products_gender_associated);
            $query = $this->db->get()->result_array();
            $res=array();
            foreach($query as $q){
                $res[$q['column_name']]='0';
            }
        } else {
            $res = $this->db->limit(1)->where('male =' . $product_id . ' OR female = ' . $product_id . ' OR kid = ' . $product_id)->get($this->t_products_gender_associated)->row_array();
        }
        return $res;
    }

    private function set_gender_association($source,$product_id){

        $this->db->where('male ='. $product_id .' OR female = '. $product_id .' OR kid ='. $product_id)->delete($this->t_products_gender_associated);
        $this->db->insert($this->t_products_gender_associated, array('male'=>$source['male'],'female'=>$source['female'],'kid'=>$source['kid'],));

    }

    public function get_product_filters($product_id){
        $res = $this->db->get_where($this->t_filters_to_products,array('product_id'=> $product_id));
        if($res->num_rows() > 0){
            return $res->result_array();
        }else{
            return false;
        }
    }


    private function save_filters($filters,$product_id){

        $_data = $this->db->where(array('product_id'=>$product_id))->get($this->t_filters_to_products)->result_array();
        $_old_filters = array();
        if(count($_data) >0){
            foreach($_data as $item){
                $_old_filters[$item['filter_id']] =  $item['filter_id'];
            }
        }

        $_no_new_filters = ($_old_filters == $filters)?true:false;

        if($_no_new_filters){
            return;
        }

        foreach($filters as $filter_id){
            $data[$filter_id]['filter_id'] = $filter_id;
            $data[$filter_id]['product_id'] = $product_id;
        }
        
        $this->db->where(array('product_id'=>$product_id))->delete($this->t_filters_to_products);
        $this->db->insert_batch($this->t_filters_to_products,$data);
        $this->update_tovar_filters($filters,$product_id);

    }

    private function update_tovar_filters($filters,$product_id){

        $data = $this->db->select('DISTINCT tovar_id',false)
                         ->from($this->t_filters_to_tovars)
                         ->where(array('product_id'=>$product_id))
                         ->get()->result_array();
        if(!$data){
            return;
        }

        $this->db->delete($this->t_filters_to_tovars,array('product_id'=>$product_id));

        foreach($data as $item){
            $_insert_data = array();
            foreach($filters as $filter_id){
                $_insert_data[$filter_id]['tovar_id'] = $item['tovar_id'];
                $_insert_data[$filter_id]['picture_id'] = 0;
                $_insert_data[$filter_id]['filter_id'] = $filter_id;
                $_insert_data[$filter_id]['product_id'] = $product_id;
            }
            $this->db->insert_batch($this->t_filters_to_tovars,$_insert_data);
        }
    }

    private function save_images_info($data,$product_id){
        
        $res = $this->db->where(array('product_id'=>$product_id))->get($this->t_products_images)->result_array();
        $update_data = array();
        $delete_data = array();
        foreach($res as $image_info){
            if(isset($data[$image_info['id']])){
                $update_data[] = array(
                    'id'=>$image_info['id'],
                    'product_id'=>$product_id,
                    'main'=>isset($data[$image_info['id']]['main'])?1:0,
                    'sort'=>isset($data[$image_info['id']]['sort'])?$data[$image_info['id']]['sort']:'',
                    'data'=>isset($data[$image_info['id']])?json_encode($data[$image_info['id']]):''
                );
            }else{
                $_url = 'images/products/'. $product_id .'/'.$image_info['name'];
                $FolderWork = new FolderWork($_url);
                $FolderWork->chek_and_make_path();
                $FolderWork->clear();
                $FolderWork->del();
                $delete_data[] = $image_info['id'];
            }
        }

        if(count($update_data) > 0){
            $this->db->update_batch($this->t_products_images,$update_data,'id');
        }
        if(count($delete_data) > 0){
            $this->db->where_in('id',$delete_data)->delete($this->t_products_images);
        }

    }

    private function save_photos($data,$product_id){

        foreach($data as $up_key => $up_file){

            if(empty($up_file['name'])){
                continue;
            }

            //типы фотографий
            //фото "цветов" товара
            if(strpos($up_key,'photo') !== false){
                $_img_atributs = get_img_params($up_file);
                if($_img_atributs['error']){
                    add_system_message('danger', $_img_atributs['message'], '');
                    continue;
                }elseif($_img_atributs['size_x'] != DEFAULT_MAX_PHOTO_SIZE_X || $_img_atributs['size_y'] != DEFAULT_MAX_PHOTO_SIZE_Y){
                    add_system_message('danger', LANG('error_product_photo_size') . DEFAULT_MAX_PHOTO_SIZE_X .'x'. DEFAULT_MAX_PHOTO_SIZE_Y , '');
                    continue;
                }elseif($up_file['type'] != "image/png"){
                    add_system_message('danger', LANG('error_product_photo_format_png') , '');
                    continue;
                }

                $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));
                $_url = 'images/products/'. $product_id .'/';
                $FolderWork = new FolderWork($_url);
                $FolderWork->chek_and_make_path();

                $_movented = move_uploaded_file($up_file["tmp_name"], $_url . $img_name.'.png');

                if($_movented){
                    $this->db->insert($this->t_products_images, array(
                        'product_id'=>$product_id,
                        'name'=>$img_name,
                        'type'=>'png',
                    ));
                }
            //фото миниатюры товара
            }elseif(strpos($up_key,'icon') !== false){

                $_img_atributs = get_img_params($up_file);
                if($_img_atributs['error']){
                    add_system_message('danger', $_img_atributs['message'], '');
                    continue;
                }elseif($_img_atributs['size_x'] != DEFAULT_PRODUCT_MINIATURE_SIZE_X || $_img_atributs['size_y'] != DEFAULT_PRODUCT_MINIATURE_SIZE_Y){
                    add_system_message('danger', LANG('error_icon_size') . DEFAULT_PRODUCT_MINIATURE_SIZE_X .'x'. DEFAULT_PRODUCT_MINIATURE_SIZE_Y , '');
                    continue;
                }elseif($up_file['type'] != "image/png"){
                    add_system_message('danger', LANG('error_icon_format_png') , '');
                    continue;
                }

                $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));
                $_url = 'images/products/'. $product_id .'/icon/';
                $FolderWork = new FolderWork($_url);
                $FolderWork->chek_and_make_path();
                $FolderWork->clear();
                $_movented = move_uploaded_file($up_file["tmp_name"], $_url . $img_name.'.png');

                if($_movented){
                    $this->db->update($this->table_name, array('icon'=>$img_name.'.png'),array('id'=>$product_id));
                }
            } elseif(strpos($up_key,'other') !== false){
                $image_id=preg_replace('/other\d_/','',$up_key);
                $image_type=preg_replace('/_(.*)/','',$up_key);
                $_img_atributs = get_img_params($up_file);
                if($_img_atributs['error']){
                    add_system_message('danger', $_img_atributs['message'], '');
                    continue;
                }elseif($_img_atributs['size_x'] != DEFAULT_MAX_PHOTO_SIZE_X || $_img_atributs['size_y'] != DEFAULT_MAX_PHOTO_SIZE_Y){
                    add_system_message('danger', LANG('error_product_photo_size') . DEFAULT_MAX_PHOTO_SIZE_X .'x'. DEFAULT_MAX_PHOTO_SIZE_Y , '');
                    continue;
                }elseif($up_file['type'] != "image/png"){
                    add_system_message('danger', LANG('error_product_photo_format_png') , '');
                    continue;
                }

                $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));
                $_url = 'images/products/'. $product_id .'/other_img/'.$img_name;
                $FolderWork = new FolderWork($_url);
                $FolderWork->chek_and_make_path();
                $UploadImage = new UploadImage;

                foreach($this->image_cutter_sizes as $key=>$val){
                    $UploadImage->do_action($up_file ,'/'. $FolderWork->path .'/', $val['x'], $val['y'], 10000, $key.'_'.$img_name, 90, $val['metod']);
                    $UploadImage->addOptions($val['options']);
                    $_movented=$UploadImage->save();
                }

                if($_movented){
                    $this->db->update($this->t_products_images, array($image_type =>$img_name.'.png'),array('id'=>$image_id));
                }

            }
        }
    }


    public function get_product_list_to_gender($product_category){

        $_category_data= $this->category_model->get_by_id($product_category);
        if(!$_category_data){
            return false;
        }

        $products = $this->db->select($this->table_name .'.*,'. $this->table_name. '.title'. SQL_LANG .' as title' )
                             ->from($this->table_name)
                             ->where($this->table_name.'.category_id IN(SELECT id FROM '. $this->t_categories.' WHERE parent_id = '. $_category_data['parent_id'] .')')
                             ->get()->result_array();
        if(!$products){
            return false;
        }
        return $products;
    }

    private function save_pay_limits($pay,$product_id){

        $this->db->where('product_id  ='. $product_id)->delete($this->t_product_pay_limits);
        $_data = array();
        foreach($pay as $key => $item){
            if(empty($item['bound']) || empty($item['percent'])){
                continue;
            }
            $_data[$key]['bound'] = $item['bound'];
            $_data[$key]['percent'] = $item['percent'];
            $_data[$key]['product_id'] = $product_id;
        }
        if(count($_data) > 0){
            $this->db->insert_batch($this->t_product_pay_limits, $_data);
        }
    }

    public function get_pay_limits($product_id){
        $res = $this->db->get_where($this->t_product_pay_limits,array('product_id'=>$product_id))->result_array();
        return $res;
    }
}