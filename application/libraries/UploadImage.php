<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UploadImage {

    public $width = 800;
    public $height = 600;
    public $path = '/images/';
    public $name = 'name_img';
    public $size = 30720000; // 300Kb (300b * 1024 = 307200)
    public $quality = 80;
    public $file;
    private $_format;
    public $save_format = false;
    public $type = array(
        "image/gif" => "gif",
        "image/pjpeg" => "jpg",
        "image/jpeg" => "jpg",
        "image/jpg" => "jpg",
        "image/png" => "png"
    );

    public function do_action($file, $path = null, $width = null, $height = null, $size = null, $name = null, $quality = null, $metod = 1, $g_align = null, $v_align = null, $bg_color = null, $wmark = 'yes') {
	
		$this->mark_b = 700;
		$this->mark_s = 500;
		$this->mark_m = 200;
		
		$this->fformat = '';
		$this->bg_image = '';
		
		$this->file = $file;
		$this->metod = $metod;
		$this->wmark = $wmark;
		$this->start_metod = false;
        if ($path != null)
            $this->path = $path;
        if ($width != null)
            $this->width = $width;
        if ($height != null)
            $this->height = $height;
        if ($size != null)
            $this->size = $size * 1024;
        if ($name != null)
            $this->name = $name;
        if ($quality != null)
            $this->quality = $quality;
        if ($g_align != null)
            $this->g_align = $g_align;
		if ($v_align != null)
            $this->v_align = $v_align;
        if ($bg_color != null)
            $this->bg_color = $bg_color;

    }
	
	//получить тип файла
	public function getType() {
	
		return $this->_format;
	}
	
	//внесение дополнительных настроек обработки изображения
	public function addOptions($option) {
		
		if(!is_array($option))
			return 'Переменная опций не массив.';
		
		foreach($option as $key => $val){
		
			$this->{$key} = $val;
		}
	
		return true;
	}
	

	//загрузить и сохранить изображение
    public function save(){

		if(!is_array($this->file))
			return 'Не передан файл для обработки';
	
        switch ($this->file['error']) {
            case UPLOAD_ERR_OK: //(= 0)
				//return 'Файл получен полностью, и его можно найт во временном каталоге сервера';
                break;
            case UPLOAD_ERR_INI_SIZE: //(= 1)
				return 'Превышен максимальный размер файла, который загружается на сервер. Лимит ' . get_cfg_var('upload_max_filesize');
                break;
            case UPLOAD_ERR_FORM_SIZE: //(= 2)
                return 'Превышен размер, задаваемый в необязательном поле формы с именем MAX_FILE_SIZE';
                break;
            case UPLOAD_ERR_PARTIAL: //(= 3)
                return 'В результате обрыва соединения файл не был докачан до конца';
                break;
            case UPLOAD_ERR_NO_FILE: //(= 4)
                return 'Пользователь не выбрал файл в браузере';
                break;
        }

        if($this->file['size'] > $this->size){
            return 'Превышен максимальный размер файла ' . round($this->size / 1024) . 'Kb!';
        }
		if (!array_key_exists(strtolower($this->file['type']), $this->type)) {
			return 'Тип файла не соответствует графическим файлам gif, jpg и png!';
		}
		if (!preg_match('/^(.+)\.(jpg|jpeg|png|gif)$/si', $this->file['name'])) {
			return 'Расширение файла не соответствует графическим файлам gif, jpg и png!';
		}
		
		if(getimagesize($this->file['tmp_name']) === false)
			return 'Невозожно обработать файл, возможно файл поврежден.';
		
		switch (strtolower(preg_replace('/^(.+)\.(jpg|jpeg|png|gif)$/si', '$2', $this->file['name']))) {
			case 'jpg':
			case 'jpeg':
				$img = imagecreatefromjpeg($this->file['tmp_name']); // Создаем временный файл
				$this->_format = "jpg";
				break;
			case 'png':
				$img = imagecreatefrompng($this->file['tmp_name']); // Создаем временный файл
				$this->_format = "png";
				break;
			case 'gif':
				$img = imagecreatefromgif($this->file['tmp_name']); // Создаем временный файл
				$this->_format = "gif";
				break;
		}
		
		// Определяем его линейный размер по горизонтали (ширина)
		$w_src = imagesx($img);
		// Определяем его линейный размер по вертикали (высота)
		$h_src = imagesy($img);
		// Полный путь к файлу (включая и его имя)
		$path = $_SERVER['DOCUMENT_ROOT'] . $this->path . $this->name . "." . $this->_format; 
	
		//Обработка изображения с учетом метода обработки
		if($this->metod == 1){
			if ($w_src > $this->width || $h_src > $this->height) {  // Проверяем не равна ли уже исходная ширина/высота необходимой нам
				if ($w_src > $h_src) { // Если изображение горизонтальное
					$ratio = $w_src / $this->width; // Считаем соотношение пропорций
					$w_dest = $this->width; // Конечная ширина будет равна максимальной
					$h_dest = round($h_src / $ratio); // Считаем конечную высоту
				} elseif ($h_src > $w_src) { // Если изображение вертикальное
					$ratio = $h_src / $this->height; // Считаем соотношение пропорций
					$h_dest = $this->height; // Конечная высота будет равна максимальной
					$w_dest = round($w_src / $ratio); // Считаем конечную ширину
				} else { // Если изображение квадратное
					$w_dest = $this->height; // Подставляем максимальные значения высоты
					$h_dest = $this->height;
				}
			$dest = imagecreatetruecolor($w_dest, $h_dest); // Создаем пустое изображение
			
			//изменение фонового цвета
			$dest = $this->bg_color_make($dest);

			//определения метода работы с прозрачностями
			$this->alphablending($dest);
			
			// Преобразуем исходное изображение в конечное (с новыми размерами).
			imagecopyresampled($dest, $img, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
			}		
		}
		
		if($this->metod == 2){
			if ($w_src != $h_src) {// Если изображение не  квадратное
				$ratio = $h_src / $w_src; // Считаем соотношение пропорций
				$test_visota = $this->width * $ratio;//узнаем высоту из нужной ширины учитывая пропорцию
				
				if($test_visota > $this->height){//узнаем, что использовать за основу - ширину или высоту
					$h_dest = $this->height; // Конечная высота будет равна максимальной
					$w_dest = round($h_dest / $ratio); // Считаем конечную ширину
				}else{
					$w_dest = $this->width; // Конечная ширина будет равна максимальной
					$h_dest = round($w_dest * $ratio); // Считаем конечную высоту
				}
			} else {// Если изображение квадратное
				if($this->height < $this->width){//узнаем, что использовать за основу - ширину или высоту
					$w_dest = $this->height; // Конечная высота будет равна максимальной
					$h_dest = $this->height;
				}else{
					$h_dest = $this->width; // Конечная ширина будет равна максимальной
					$w_dest = $this->width;
				}			
			}
		
			$dest = imagecreatetruecolor($w_dest, $h_dest); // Создаем пустое изображение
			
			//изменение фонового цвета
			$dest = $this->bg_color_make($dest);
			
			//определения метода работы с прозрачностями
			$this->alphablending($dest);
			
			// Преобразуем исходное изображение в конечное (с новыми размерами).
			imagecopyresampled($dest, $img, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);	
		}
		
		if($this->metod == 3){
			if ($w_src != $h_src) {// Если изображение не  квадратное
				$ratio = $h_src / $w_src; // Считаем соотношение пропорций
				$test_visota = $this->width * $ratio;//узнаем высоту из нужной ширины учитывая пропорцию
				
				if($test_visota > $this->height){//узнаем, что использовать за основу - ширину или высоту
					$w_dest = $this->width; // Конечная ширина будет равна максимальной
					$h_dest = round($w_dest * $ratio); // Считаем конечную высоту
				}else{
					$h_dest = $this->height; // Конечная высота будет равна максимальной
					$w_dest = round($h_dest / $ratio); // Считаем конечную ширину
				}
			} else {// Если изображение квадратное
				if($this->height > $this->width){//узнаем, что использовать за основу - ширину или высоту
					$w_dest = $this->height; // Конечная высота будет равна максимальной
					$h_dest = $this->height;
				}else{
					$h_dest = $this->width; // Конечная ширина будет равна максимальной
					$w_dest = $this->width;
				}			
			}
            $c_x = $c_y = 0;
			$dest = imagecreatetruecolor($this->width, $this->height); // Создаем пустое изображение с нужными размерами
			//определяем нужное значение cдвига для центровки изображения
			if($this->width < $w_dest) $c_x = ($this->width - $w_dest) / 2;
			if($this->height < $h_dest) $c_y = ($this->height - $h_dest) / 2;
			
			//изменение фонового цвета
			$dest = $this->bg_color_make($dest);
			
			//определения метода работы с прозрачностями
			$this->alphablending($dest);
			
			// Преобразуем исходное изображение в конечное (с новыми размерами).
			imagecopyresampled($dest, $img, $c_x, $c_y, 0, 0, $w_dest, $h_dest, $w_src, $h_src);	
		}
		
		if($this->metod == 4){
			if ($w_src > $this->width || $h_src > $this->height) {  // Проверяем не равна ли уже исходная ширина/высота необходимой нам
			
				if ($w_src != $h_src) {// Если изображение не  квадратное
					$ratio = $h_src / $w_src; // Считаем соотношение пропорций
					$test_visota = $this->width * $ratio;//узнаем высоту из нужной ширины учитывая пропорцию
					
					if ($test_visota < $this->height){//узнаем, что использовать за основу - ширину или высоту
						$w_dest = $this->width; // Конечная ширина будет равна максимальной
						$h_dest = round($this->width * $ratio); // Считаем конечную высоту
					}else{ // Если изображение вертикальное
						$h_dest = $this->height; // Конечная высота будет равна максимальной
						$w_dest = round($this->height / $ratio); // Считаем конечную ширину
					} 
				}else{ // Если изображение квадратное
					if($this->width <= $this->height){
						$w_dest = $this->width; // Подставляем максимальные значения ширины
						$h_dest = $this->width;
					}else{
						$w_dest = $this->height; // Подставляем максимальные значения высоты
						$h_dest = $this->height;
					}
				}

			$dest = imagecreatetruecolor($w_dest, $h_dest); // Создаем пустое изображение
			
			//изменение фонового цвета
			$dest = $this->bg_color_make($dest);
			
			//изменение фонового цвета
			$dest = $this->add_bacgraund_img($dest);

			//определения метода работы с прозрачностями
			$this->alphablending($dest);
			
			// Преобразуем исходное изображение в конечное (с новыми размерами).
			imagecopyresampled($dest, $img, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
			}		
		}		
		
		if($this->metod == 5){
            $s_ver = $s_gor = 0;
			if ($w_src > $this->width || $h_src > $this->height || $this->start_metod) {  // Проверяем не равна ли уже исходная ширина/высота необходимой нам
			
				if($w_src > $this->width || $h_src > $this->height){
				
					if ($w_src != $h_src) {// Если изображение не  квадратное
						$ratio = $h_src / $w_src; // Считаем соотношение пропорций
						$test_visota = $this->width * $ratio;//узнаем высоту из нужной ширины учитывая пропорцию
						
						if ($test_visota < $this->height){//узнаем, что использовать за основу - ширину или высоту
							$w_dest = $this->width; // Конечная ширина будет равна максимальной
							$h_dest = round($this->width * $ratio); // Считаем конечную высоту
						}else{ // Если изображение вертикальное
							$h_dest = $this->height; // Конечная высота будет равна максимальной
							$w_dest = round($this->height / $ratio); // Считаем конечную ширину
						} 
					}else{ // Если изображение квадратное
						if($this->width >= $this->height){
							$w_dest = $this->width; // Подставляем максимальные значения ширины
							$h_dest = $this->width;
						}else{
							$w_dest = $this->height; // Подставляем максимальные значения высоты
							$h_dest = $this->height;
						}
					}
				}else{

					$w_dest = $w_src;
					$h_dest = $h_src;
				}
				
				//просчитываем позиционирование по горизонтали
				if($this->width > $w_dest){
                    if(isset($this->g_align)){
                        switch($this->g_align){
                            case "left":
                                $s_gor = 0;
                                break;
                            case "center":
                                $s_gor = round(($this->width - $w_dest) / 2);
                                break;
                            case "right":
                                $s_gor = ($this->width - $w_dest);
                                break;
                            default:
                                $s_gor = round(($this->width - $w_dest) / 2);
                        }
                    }else{
                        $s_gor = round(($this->width - $w_dest) / 2);
                    }
				}
			
				//просчитываем позиционирование по вертикале
				if($this->height > $h_dest){
                    if(isset($this->v_align)){
                        switch($this->v_align){
                            case "top":
                                $s_ver = 0;
                                break;
                            case "middle":
                                $s_ver = round(($this->height - $h_dest) / 2);
                                break;
                            case "bottom":
                                $s_ver = ($this->height - $h_dest);
                                break;
                            default:
                                $s_ver = round(($this->height - $h_dest) / 2);
                        }
                    }else{
                        $s_ver = round(($this->height - $h_dest) / 2);
                    }
				}
                $s_ver = $s_gor = 0;
				// Создаем пустое изображение !!!требуемого!!! размера
				$dest = imagecreatetruecolor($this->width, $this->height);
				//делаем его прозрачным
				$transp = imagecolorallocatealpha($dest, 0, 0, 0, 127);
				imagefill($dest, 0, 0, $transp);

				//изменение фонового цвета
				$dest = $this->bg_color_make($dest);
				
				//изменение фонового цвета
				$dest = $this->add_bacgraund_img($dest);
				
				//определения метода работы с прозрачностями
				$this->alphablending($dest);

				// Преобразуем исходное изображение в конечное (с новыми размерами) и позиционируем его на новом, с нужными размерами, изображении.
				imagecopyresampled($dest, $img, $s_gor, $s_ver, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
			}		
		}


		
		// Проверяем не существует ли уже файл
		if(file_exists($path))
			return 'Файл с текущем имененм уже существует.';
		
		//сохраняем изображение
		$img = isset($dest) ? $dest : $img;

		//наносим водяной знак
		if($this->wmark == 'yes'){
		
			//наносим произвольный водяной знак на картику
			if(isset($this->wmark_file)){
		
				if(file_exists('img/'. $this->wmark_file)){
					$play_btn = imagecreatefrompng('img/'. $this->wmark_file);
					$w2 = imagesx($play_btn);
					$h2 = imagesy($play_btn);
					imagecopy($img, $play_btn, 0, 0, 0, 0, $w2, $h2);
				}
				
			}else{
			
				if(imagesx($img) > $this->mark_b){
				
					if(file_exists("img/znak_g.png")){
						//горизонтальный знак
						$play_btn = imagecreatefrompng("img/znak_g.png");
						$w  = imagesx($img);
						$h  = imagesy($img);
						$w2 = imagesx($play_btn);
						$h2 = imagesy($play_btn);
						
						//позиционируем водяной знак
						if(isset($this->mark_position_x)){
						
							if(is_int($this->mark_position_x)){
							
								if($this->mark_position_x > 0){
								
									$wpos_x = ($w-$w2) - $this->mark_position_x;
								}else{
								
									$wpos_x = $this->mark_position_x;
								}
								
							}else{
							
								switch($this->mark_position_x){
									case "top":
										$wpos_x = round(($w-$w2) / 2);
										break;
										
									case "center":
										$wpos_x = round(($w-$w2) / 2);
										break;
										
									case "bottom":
										$wpos_x = round(($w-$w2) / 2);
										break;
									default:
										$wpos_x = round(($w-$w2) / 2);
								}
							}
						}else{
							$wpos_x = ($w-$w2) - 10;
						}
						
						if(isset($this->mark_position_y)){
						
							if(is_int($this->mark_position_y)){
							
								if($this->mark_position_y > 0){
								
									$wpos_y = ($h-$h2) - $this->mark_position_y;
								}else{
								
									$wpos_y = $this->mark_position_y;
								}
								
							}else{
							
								switch($this->mark_position_y){
									case "top":
										$wpos_y = round(($h-$h2) / 2);
										break;
										
									case "middle":
										$wpos_y = round(($h-$h2) / 2);
										break;
										
									case "bottom":
										$wpos_y = round(($h-$h2) / 2);
										break;
									default:
										$wpos_y = round(($h-$h2) / 2);
								}
							}
						}else{
							$wpos_y = ($h-$h2) - 10;
						}
						
						imagecopy($img, $play_btn, $wpos_x, $wpos_y, 0, 0, $w2, $h2);
					}
					
				}elseif(imagesx($img) > $this->mark_s){
				
					if(file_exists("img/znak_v.png")){
						//вертикальный знак
						$play_btn = imagecreatefrompng("img/znak_v.png");
						$w  = imagesx($img);
						$h  = imagesy($img);
						$w2 = imagesx($play_btn);
						$h2 = imagesy($play_btn);
						imagecopy($img, $play_btn, ($w-$w2)-10, ($h-$h2)-10, 0, 0, $w2, $h2);
					}
					
				}elseif(imagesx($img) > $this->mark_m){
					if(file_exists("img/znak_mini.png")){
						//вертикальный знак
						$play_btn = imagecreatefrompng("img/znak_mini.png");
						$w  = imagesx($img);
						$h  = imagesy($img);
						$w2 = imagesx($play_btn);
						$h2 = imagesy($play_btn);
						imagecopy($img, $play_btn, ($w-$w2)-10, ($h-$h2)-10, 0, 0, $w2, $h2);
					}
				}
			}
		}
		
		//наносим декор на слайдер
		if($this->wmark == 'slider' && file_exists("img/znak_slider.png")){
			$play_btn = imagecreatefrompng("img/znak_slider.png");
			$w2 = imagesx($play_btn);
			$h2 = imagesy($play_btn);
			imagecopy($img, $play_btn, 0, 0, 0, 0, $w2, $h2);
		}
		
		//наносим декор на постер
		if($this->wmark == 'poster' && file_exists("img/znak_poster.png")){
			$play_btn = imagecreatefrompng("img/znak_poster.png");
			$w2 = imagesx($play_btn);
			$h2 = imagesy($play_btn);
			imagecopy($img, $play_btn, 0, 0, 0, 0, $w2, $h2);
		}
		
		//записываем файл на диск
		if(!empty($this->fformat)){
			$f_format = $this->fformat;
			$path = substr_replace($path, $this->fformat, -3, 3);
		}else{
			$f_format = $this->_format;
		}
		$this->format = $f_format;
		switch ($f_format) {
			case 'jpg':
				$res = imagejpeg($img, $path, $this->quality);
				break;

			case 'png':
				imagesavealpha($img, true);
				$res = imagepng($img, $path);
				break;

			case 'gif':
				imagesavealpha($img, true);
				$res = imagegif($img, $path);
				break;
		}

        if($this->save_format != false && $this->save_format != $this->format){
            $_function = 'image'. $this->save_format;
            $_function(imagecreatefromstring(file_get_contents($path)), $_SERVER['DOCUMENT_ROOT'] . $this->path . $this->name . "." . $this->save_format);
            @unlink($path);
        }

		//удаляем картинки из памяти
		if($img)
			@imagedestroy($img);
		if(isset($dest))
			@imagedestroy($dest);
		if(isset($play_btn))
			@imagedestroy($play_btn);
			
		return true;
    }
	
	//изменение фонового цвета
	private function bg_color_make($dest){

		if(empty($this->bg_color))
			return $dest;
	
		$piec = explode(",", $this->bg_color);
		$fill_color = ImagecolorAllocate($dest, $piec[0], $piec[1], $piec[2]);
		ImageFill($dest, 0, 0, $fill_color); // заливаем изображение цветом	

		return $dest;
	}
	
	//наносим фоновое изображение
	function add_bacgraund_img($dest){
	
		if(empty($this->bg_image))
			return $dest;
			
		if(!file_exists("img/". $this->bg_image))
			return $dest;
			
		switch (strtolower(preg_replace('/^(.+)\.(jpg|jpeg|png|gif)$/si', '$2', $this->bg_image))) {
			case 'jpg':
			case 'jpeg':
				$bg_img = imagecreatefromjpeg("img/". $this->bg_image);
				break;
			case 'png':
				$bg_img = imagecreatefrompng("img/". $this->bg_image);
				break;
			case 'gif':
				$bg_img = imagecreatefromgif("img/". $this->bg_image);
				break;
		}			
			
		$w2 = imagesx($bg_img);
		$h2 = imagesy($bg_img);
		imagecopy($dest, $bg_img, 0, 0, 0, 0, $w2, $h2);			
	
		return $dest;
	}	
	
	//определения метода работы с прозрачностями
	private function alphablending($dest){

		if($this->_format == 'png' || $this->_format == 'gif' || $this->fformat == 'png' || $this->fformat == 'gif'){
			if($this->fformat != 'jpg')
				imagealphablending($dest, false);
		}

		return;
	}
	
}

?>