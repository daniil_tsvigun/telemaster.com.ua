<!DOCTYPE html>
<html>
<head>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tele Master</title>
    <link href="/assets/lib/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/lib/animate.css/3.3.0/animate.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css?v=1.3.6"/>
    <script type="text/javascript">
        var site_url = "http://www.telemaster.com.ua/ua";
    </script>
</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
            Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a
        ,m)

    })(window,document,'script','https://www.google-analytics.com/analytics.js',
        'ga');

    ga('create', 'UA-88207792-1', 'auto');
    ga('send', 'pageview');

</script>

<div id="vk_api_transport"></div><div data-id="mobile_btn" class="lang-menu">
    <ul>
        <li class="current"><a href="http://www.telemaster.com.ua/ua/reports">Укр</a></li>
        <li class=""><a href="http://www.telemaster.com.ua/ru/reports">Рус</a></li>
    </ul>
</div>
<div data-id="mobile_btn" class="master ">
    <a class="" href="http://www.telemaster.com.ua/ua/registration">Реєстрація майстра</a>
</div>
<section class="nav_bar" >
    <div class="wrapper">
        <div class="menu">
            <a href="http://www.telemaster.com.ua/ua" class="logo-anchor"></a>
            <ul class="main-menu slideInDown animated">

                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua">Головна</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua/calling">Знайти майстра</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua/partners">Знайти провайдера</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua/instructions">Інструкції</a>
                </li>
                <!--                <li>-->
                <!--                    <a class="--><!--" href="--><!--">--><!--</a>-->
                <!--                </li>-->
                <li class="master ">
                    <a class="" href="http://www.telemaster.com.ua/ua/registration">Реєстрація майстра</a>
                </li>
            </ul>
            <div id="menu-btn"></div>
            <div class="lang-menu">
                <ul>

                    <li class="current"><a href="http://www.telemaster.com.ua/ua/reports">Укр</a></li>
                    <li class=""><a href="http://www.telemaster.com.ua/ru/reports">Рус</a></li>
                </ul>
            </div>
            <div class="master ">
                <a class="" href="http://www.telemaster.com.ua/ua/registration">Реєстрація майстра</a>
            </div>
        </div>

    </div>
</section>    <div data-alias="reports" class="content single-instruction">
    <div class="wrapper">
        <div class="main-block">

            <h2>Правила модерації відгуків</h2>
            <div data-first="" class="text">
                <div>
                    <p>Ми цінуємо відгуки про роботу Майстрів і вважаємо важливим дати можливість нашим відвідувачам поділитися своєю думкою про Майстра і про роботу нашого ресурсу. Для того, щоб Сайт залишався надійним джерелом релевантної інформації, просимо вас перед публікацією відкликання переконатися, що при його складанні дотримуються наступні правила.</p>
                    <p><strong>Прийнятність для перегляду відвідувачами будь-якого віку </strong></p>
                    <p>Нашим Сайтом користуються телеглядачі різного віку. Ми маємо намір підтримувати безпечну і доброзичливу атмосферу, яка підходить для спілкування відвідувачів будь-якого віку. Ми не публікуємо відгуки з лайкою або вульгарними виразами. Також відхиляються відгуки з нестриманим висловлюваннями сексуального характеру, гнівними репліками або упередженими зауваженнями, погрозами і особистими образами, що суперечать визнаним моральним і етичним нормам, що порушують чинне міжнародне законодавство і / або законодавство України, що порушують права третіх осіб, а також Правила користування Сайтом.</p>
                    <p><strong>Некомерційний характер відгуків</strong></p>
                    <p>Головне завдання відгуків - поділитися враженням про Майстра і допомогти іншим відвідувачам Сайту у виборі Майстра. У відгуках про Майстрів просимо обговорювати послуги, що надаються Майстрами, їх якість, швидкість виконання і загальне враження про Майстра. Ми не дозволяємо публікувати під виглядом відгуків рекламу послуг, товарів або комерційних підприємств. Відгуки, пропоновані в обмін на особисту вигоду, наприклад подарунки, послуги або гроші, видаляються.</p>
                    <p>Ми також не приймаємо відгуки з посиланнями на зовнішні ресурси, будь-який спам, повідомлення поза тематикою Сайту, заклики додаватися в друзі, посилання на зовнішні ресурси, сайти, групи в соціальних мережах незалежно від тематики.</p>
                    <p>Якщо у відвідувача Сайту є конкретні пропозиції щодо поліпшення роботи майстра або скарга на конкретного Майстри, він може адресувати повідомлення на адресу електронної пошти Лінії підтримки майстрів - так ми зможемо найбільш оперативно відреагувати.&nbsp;</p>
                    <p><strong>Легкість для читання</strong></p>
                    <p>Зробіть свій відгук максимально зрозумілим для будь-якого користувача Сайту: використовуйте алфавіт, відповідний для вашої мови, не використовуйте занадто багато ВЕЛИКИХ ЛІТЕР і сленгових слів.</p>
                    <p>Ваші коментарі повинні містити загальновживані слова та вислови, доступні для розуміння кожному. Повідомлення, в яких використовуються словесні вирази, що характерні тільки для вашої місцевості, а також двозначні фрази, зміст яких буде неясний або незрозумілий відвідувачам цього сайту, опубліковані не будуть.</p>
                    <p>Ми залишаємо за собою право видаляти певні матеріали у будь-який момент (в тому числі і після публікації) без наведення причини. Відгуки, що розміщуються на Сайті, є приватними суб'єктивними думками. Відгуки відображають думку відвідувачів Сайту. Ми не приєднуємося до думки авторів відгуків. Відповідно до нашої політики конфіденційності, ми не надаємо особистої контактної інформації, за винятком тієї, яка опублікована на Сайті.</p>                    </div>
            </div>

        </div>
    </div>
</div>
<div class="hidden rate popup fadeIn animated">
    <div class="wrapper">
        <div class="wrap">
            <div class="box">
                <div class="close"></div>
                <div class="rate">
                    <div class="levels" data-choosen="null">
                        <ul>
                            <li class="" data-level_id="0" id="level0"><div></div></li>
                            <li class="" data-level_id="1" id="level1"><div></div></li>
                            <li class="" data-level_id="2" id="level2"><div></div></li>
                            <li class="" data-level_id="3" id="level3"><div></div></li>
                            <li class="" data-level_id="4" id="level4"><div></div></li>
                        </ul>
                    </div>
                    <div class="btn-box">
                        <div id="vote" class="btn btn-type3">Проголосувати</div>
                    </div>
                    <div class="auth-box">
                        <div class="fb_auth"></div>
                        <div class="vk_auth"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="wrapper">
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <a href="http://www.telemaster.com.ua/ua/about">Про нас</a>
            <a href="http://www.telemaster.com.ua/ua/faq">Питання та відповіді</a>
            <a href="http://www.telemaster.com.ua/ua/rules">Правила користування сайтом</a>
            <a href="http://www.telemaster.com.ua/ua/reports">Правила модерації відгуків</a>
            <a href="http://www.telemaster.com.ua/ua/polices">Політика конфіденційності</a>

        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 center">
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <p>Адміністратор сайту: </p>
            <p>info@telemaster.com.ua</p>
            <div class="btns special">
                <div id="youtube_btn">
                    <a target="_blank" href="https://www.youtube.com/channel/UCUrsf864ut8p5UETF4Ym6Hw">&nbsp;</a>
                </div>
            </div>
        </div>
    </div>
    <div class="rights">
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <div class="logo"></div>
            </div>
            <div class="center col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <p>© 2016 All rights reserved</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p>
                    <a href="http://lemon.ua/">Створення сайту  Lemon.ua</a>
                </p>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/assets/js/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/lib/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/lib/animate.css/3.3.0/animate.min.css" ></script>