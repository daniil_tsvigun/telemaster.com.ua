<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calling extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();

    }    

    public function index(){
        $this->load->view('frontend/calling_master');
    }

    public function region($region=false){
        if($region==false){
            redirect(site_url('calling'));
        } else {
            $data=array();
            $regions['id']=$region;
            $data['regions']=$this->region_model->get_region_by_id($region);
            $data['masters']=$this->master_model->get_masters_by_region($regions);
            for($i=0; $i<count($data['masters']); $i++){
                $data['masters'][$i]->comments=$this->comment_model->get_master_comments($data['masters'][$i]->id);
            }
            $this->load->view('frontend/calling_region',$data);
        }

    }

    public function master($master_id=false){
        if($master_id==false||!text_validate($master_id)){
            redirect(site_url('calling'));
        } else {
            $data=array();

            $data['master']=$this->master_model->get_master_by_id($master_id);
            
                $set_crimea=false;
                foreach($data['master']->regions as $key=>$reg){
                    if($reg->area_name_ru=='Крым'){
                        if(!$set_crimea){
                            $set_crimea=true;
                            $data['master']->regions[$key]->district_place_ru='Крым';
                            $data['master']->regions[$key]->district_place_ua='Крим';
                            $data['master']->regions[$key]->district_id=0;
                            $data['master']->regions[$key]->area_name_ru=NULL;
                            $data['master']->regions[$key]->area_name_ua=NULL;
                            $data['master']->regions[$key]->type=0;
                        } else {
                            unset($data['master']->regions[$key]);
                        }
                        
                    }
                }

            
            echo '<div class="hidden">';

            echo '</div>';
            $data['master']->comments=$this->comment_model->get_master_comments($master_id);
            $this->load->view('frontend/master',$data);
        }
    }



    //ajax functions

    public function ajax_search_regions(){
        $keyword=$this->input->post('keyword');
        $result = array('status'=>'error');
        if( !text_validate($keyword)){
            echo json_encode($result);
            return;
        }
        $status = $this->region_model->search_region_by_name($keyword);
        if($status){

            $result = array(
                'status'=>'success',
                'data'=>$status,
                'lang'=>SQL_LANG,
                'label_obl'=>LANG('label_obl'),
                'label_district'=>LANG('label_district'),
            );
        }
        echo json_encode($result);
        return;
    }

    public function ajax_set_rate(){
        $data=$this->input->post('data');
        $result = array('status'=>'error');
        $status = $this->master_model->set_master_rate($data['master_id'],$data['user_id'],$data['rate']);
        if($status){

            $result = array(
                'status'=>'success',
                'data'=>$status,
                'message'=>LANG('label_success_voted')
            );
        } else {
            $result['message']=LANG('label_already_vote');
        }
        echo json_encode($result);
        return;
    }
}