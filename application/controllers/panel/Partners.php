<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends MY_Controller {

    public $user = '';
    protected $base_url = 'panel/partners';
    protected $home_url = 'panel/partners/';
    protected $config_info;
    private $source_models = array(
        'partner'=>'Partner',
    );

    public function __construct(){
        parent::__construct();
        $this->load->library('FolderWork');
        $this->load->library('UploadImage');

        $this->user = get_user(true);

        if(!has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel/auth/login');
        }


        $this->config_info = new Config_info('partners');
        $this->lang_exist = $this->config->config['lang_uri_abbr'];
    }


    public function index()
    {
    }

    public function partner(){

        $this->_watch('partner');
    }

    private function _watch($type = 'partner'){

        $_model = $type .'_model';
        $_table = $type.'s';
        $data['data_type'] = $type;
        $data['page_title'] = LANG('page_title_'. $type .'_list');
        $data['base_link'] = $this->base_url;
        $data['base_lang_link'] = site_url() .'/'.$this->base_url;
        $data['current_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['create_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
        $data['remove_link'] = site_url() .'/'. $this->base_url .'/remove/'. $type;
        $data['edit_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type;
        $data['content'] = 'panel/default/default_list';
        $data['display_fields'] = $this->config_info->display_table_fields[$type];
        $pagination_data = get_pagination_data();
        $pagination_data['id']=(isset($_GET['id']))? $_GET['id']:false;
        $pagination_data['name']=(isset($_GET['master_id']))? $_GET['master_id']:false;
        $pagination_data['company']=(isset($_GET['surname']))? $_GET['surname']:false;
        $pagination_data['phone']=(isset($_GET['phone']))? $_GET['phone']:false;
        $pagination_data['active']=(isset($_GET['active']))? $_GET['active']:false;
        $data['sort'] = $pagination_data['sort'];
        $data['table_data'] = $this->$_model->{'get_all_'. $_table}($pagination_data);
        if(count($data['table_data']) == 0 && $pagination_data['page'] >1){
            $_link = make_link_by($data['current_link'],array('page'));
            $action_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
            redirect($action_link .'page='.($pagination_data['page'] - 1));
        }
        $data['pagination'] = pagination($_table,array(),$pagination_data['page'],$pagination_data['count'],make_link_by($data['current_link'],array('page')));
        $this->load->view('panel/main_layout',$data);
    }

    public function create($type = 'master',$close = false){

        //redirect_has_no_permission($this->user,'create_'. $type .'_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $data = new $_source_model();
        $this->_editor($data,'create',$type,$close);
    }


    public function edit($type = 'partner',$id = false,$close = false){

        //redirect_has_no_permission($this->user,'edit_'. $type .'_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $data =  new $_source_model($this->{$type .'_model'}->get_by_id($id));

        $this->db->from('partners_to_regions');
        $this->db->where('partner_id', $id);
        $regions=$this->db->get()->result_array();

        foreach($regions as $key=>$item){
            $regions[$key]=$item['region_id'];
        }
        $data->regions=$this->region_model->get_regions_by_ids_from_admterobj($regions);
        //$data->test_r=$this->region_model->get_regions_by_ids_from_admterobj($regions);

        if(!$data){
            add_system_message("danger", LANG("ntf_npt_'. $type .'_no_exist_title"), LANG("ntf_npt_'. $type .'_no_exist"));
            redirect($this->base_url .'/menu/'. $type);
        }

        $this->_editor($data,$id,$type,$close);
    }
    private function _editor($source,$data_id,$type,$close){

        $_model = $type .'_model';
        $_source_model = $this->source_models[$type];
        $data['page_title'] = LANG('page_title_'. $type .'_edit');
        if($data_id === 'create'){
            $data['save_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/create/'. $type .'/close';
        }else{
            $data['save_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'. $data_id;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'.$data_id .'/close';
        }
        $data['areas']=$this->region_model->get_all_areas();

        /* Removeing AR Crimea */
        foreach($data['areas'] as $key=>$area){
            if($area['id']==01){
               // unset($data['areas'][$key]);
            }
        }
        $data['close_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['content'] = $this->base_url .'/'. $type .'_edit';
        $this->form_validation->set_rules($this->config_info->validation_rules[$type]);
        $this->form_validation->data_id = $data_id;
        if ($this->form_validation->run() === true) {
            if($data_id ==='create'){
                $data_id = $this->$_model->create_partner($this->input->post());
                add_system_message('success', LANG('ntf_'. $type .'_created_title'), LANG('ntf_'. $type .'_created'));
            }else{

                $this->$_model->partner_update($this->input->post(), $data_id,$_source_model);
                add_system_message('success', LANG('ntf_'. $type .'_edited_title'), LANG('ntf_'. $type .'_edited'));
            }
            $data['data'] =  new $_source_model($this->{$type .'_model'}->get_by_id($data_id));
            if($close || $data_id === 'create'){
                redirect($data['close_link']);
            }
        } else {
            if (count($this->input->post()) > 0) {
                $data['data'] = new $_source_model($this->input->post());
            } else {
                $data['data'] = $source;
            }
        }

        $this->db->from('partners_to_regions');
        $this->db->where('partner_id', $data_id);
        $regions=$this->db->get()->result_array();
        foreach($regions as $key=>$item){
            $regions[$key]=$item['region_id'];
        }
        $data['data']->regions=$this->region_model->get_regions_by_ids_from_admterobj($regions);

        $data['test_r']=$this->region_model->get_regions_by_ids_from_admterobj($regions);
        $data['data']->id = $data_id;
        $data['lang_exist'] =$this->lang_exist;
        $this->load->view('panel/main_layout',$data);

    }



}