<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                    <?php $this->load->view('panel/templates/action_button');?>
                    <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                    <div id="general" class="tab-pane active" role="tabpanel">
                        <div class="row">
                            <div class="col-md-4">
                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                                <div class="widget-body">
                                    <p>
                                        <span><? echo LANG ('label_order');?></span>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$orders,'name'=>'order_id','selected'=>array($data->order_id),'params' => array('no_all'=>'yes','title_field'=>'id')));?>
                                    </p>
                                    <p>
                                        <span><? echo LANG ('label_painter');?></span>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$painters,'name'=>'painter_id','selected'=>array($data->painter_id),'params' => array('no_all'=>'yes','title_field'=>'name')));?>
                                    </p>
                                    <p>
                                        <span><? echo LANG ('label_tovar');?></span>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$tovars,'name'=>'tovar_id','selected'=>array($data->tovar_id),'params' => array('no_all'=>'yes')));?>
                                    </p>
                                    <p>
                                        <span><? echo LANG ('label_value');?></span>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->value;?>" name="value">
                                    </p>
                                    <p>
                                        <span class="head_status"><? echo LANG ('label_pay_debit_status');?></span>
                                        <input 	type="hidden"   name="status" value="0" checked="checked">
                                        <input 	type="checkbox" name="status" class="switcher checked" value="1"
                                                  data-name="active"
                                                  data-entity_id="1"
                                                  data-style="default"
                                                  data-selected="<?php echo($data->status == 1)? 'true':'false';?>"
                                                  data-disabled="false">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>