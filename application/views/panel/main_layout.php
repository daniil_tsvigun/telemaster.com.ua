<?php $this->load->view('panel/default/meta');?>
<body class="dashboard-">
    <div class="wrapper">

        <?php $this->load->view('panel/default/header');?>

        <div class="clearfix"></div>

        <div class="content-wrapper">

        <div id="sidebar">
            <div class="sidebar-inner">
               <?php panel_left_menu();?>
            </div>
        </div>

        <div id="content">
            <div class="content-inner">
                <?php if(isset($content) && $content){$this->load->view($content);}?>
            </div>
        </div>

    </div>
    <?php $this->load->view('panel/default/footer');?>

</body>
</html>