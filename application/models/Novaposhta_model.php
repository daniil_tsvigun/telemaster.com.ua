<?php
/**
 * @package     Artlook_CI
 * @version     1.0
 * @license     http://mit-license.org/
 * @author      Tapakan https://github.com/Tapakan
 * @coder       Alexander Oganov <t_tapak@yahoo.com>
 */

/**
 * Class NovaposhtaEntity
 */
class Novaposhta_entity
{
    public function __construct(array $source = [])
    {
        foreach ($source as $key => $value) {
            $this->{$key} = $value;
        }
    }
}

/**
 * Class Novaposhta_model
 */
class Novaposhta_model extends MY_Model
{
    /**
     * @var string
     */
    protected $table_name = 'shipping_novaposhta_cities';

    /**
     * @param $source
     * @param $source_class
     */
    public function create($source, $source_class = 'Novaposhta_entity')
    {
        return parent::create($source, $source_class);
    }

    /**
     * @param array  $source
     * @param int    $source_id
     * @param string $source_class
     *
     * @return bool
     */
    public function update($source, $source_id, $source_class = 'Novaposhta_entity')
    {
        return parent::update($source, $source_id, $source_class);
    }

    /**
     * @param string $area Area Ref
     *
     * @return mixed
     */
    public function get_cities_by_area($area)
    {
        return $this->db
            ->select('*')
            ->from($this->table_name)
            ->where(['Area' => $area])
            ->get()
            ->result_array();
    }

    /**
     * @param string $ref City UUID
     *
     * @return mixed
     */
    public function get_city_by_ref($ref)
    {
        return $this->db
            ->select('*')
            ->from($this->table_name)
            ->where(['Ref' => $ref])
            ->get()
            ->result_row();
    }
}