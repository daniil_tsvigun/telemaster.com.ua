<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basic extends CI_Controller {


    private $url_segments_arr;

    public function __construct(){
        parent::__construct();
        $this->url_segments_arr = isset($this->uri->segments)? $this->uri->segments:array();
        //$this->output->enable_profiler(TRUE);
    }

    //точка входа
    public function frontend(){

        if(count($this->url_segments_arr) == 0){

            $this->index();
            return;
        }

        if(count($this->url_segments_arr) == 1||(count($this->url_segments_arr) == 2&&$this->url_segments_arr[2]=='parameters')){
            $_page = $this->page_model->get_by_alias($this->url_segments_arr[1],true);
            if(isset($this->url_segments_arr[2])){
                if($this->url_segments_arr[2]=='parameters'){
                    $_page['first_active']=true;
                } else {
                    $_page['first_active']=false;
                }
            }

            if($_page){
                $this->page($_page);
                return;
            }

            $this->page(array('alias'=>'404'));
            return;
        }

        return;

    }

    public function index(){
        $this->load->view('frontend/main');
    }

    public function page($data){

        if($data['alias'] == '404'){
            $this->load->view('frontend/default/page_404');
            return;
        }

        $this->load->view('frontend/templates/'. $data['template'],$data);

    }

}
