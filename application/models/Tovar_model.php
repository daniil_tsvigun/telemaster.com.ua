<?php

class Tovar {


    public $alias;
    public $category_id;
    public $product_id;
    public $picture_id;
    public $seo_title_ru;
    public $seo_keywords_ru;
    public $seo_description_ru;
    public $seo_title_ua;
    public $seo_keywords_ua;
    public $seo_description_ua;

    public function __construct($source = null) {

        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->title_ru           = isset($source['title_ru'])?           $source['title_ru']          :'';
        $this->title_ua           = isset($source['title_ua'])?           $source['title_ua']          :'';
        $this->category_id        = isset($source['category_id'])?        $source['category_id']       :0;
        $this->product_id         = isset($source['product_id'])?         $source['product_id']        :0;
        $this->picture_id         = isset($source['picture_id'])?         $source['picture_id']        :0;
        $this->seo_title_ru       = isset($source['seo_title_ru'])?       $source['seo_title_ru']      :'';
        $this->seo_keywords_ru    = isset($source['seo_keywords_ru'])?    $source['seo_keywords_ru']   :'';
        $this->seo_description_ru = isset($source['seo_description_ru'])? $source['seo_description_ru']:'';
        $this->seo_title_ua       = isset($source['seo_title_ua'])?       $source['seo_title_ua']      :'';
        $this->seo_keywords_ua    = isset($source['seo_keywords_ua'])?    $source['seo_keywords_ua']   :'';
        $this->seo_description_ua = isset($source['seo_description_ua'])? $source['seo_description_ua']:'';

    }
}

class Tovar_model extends MY_Model {

    protected $table_name = 'tovars';
    protected $t_products = 'products';
    protected $t_pictures = 'pictures';
    protected $t_tovars_images = 'tovars_images';
    protected $t_products_images = 'products_images';
    protected $t_pictures_images = 'pictures_images';
    protected $t_categories = 'categories';
    protected $t_pictures_to_filters = 'filters_to_print';
    protected $t_products_to_filters = 'filters_to_products';
    protected $t_filters_to_tovars = 'filters_to_tovars';
    protected $t_painter_users = 'painter_users';
    protected $t_favorites = 'favorites';
    protected $t_likes = 'likes';
    protected $t_tags = 'tags';
    protected $t_tag_to_print = 'tag_to_print';
    protected $templates = 'templates_tovar';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;
    private $tovar_image = array(
        'a' => array('x' => DEFAULT_MAX_PHOTO_SIZE_X, 'y' => DEFAULT_MAX_PHOTO_SIZE_Y, 'metod' => 3, 'options' => array('bg_color' => '255,255,255')),
        'b' => array('x' => 500, 'y' => 500,  'metod' => 3, 'options' => array('bg_color' => '255,255,255')),
        'c' => array('x' => 320, 'y' => 320,  'metod' => 3, 'options' => array('bg_color' => '255,255,255')),
        'd' => array('x' => 160, 'y' => 160,  'metod' => 3, 'options' => array('bg_color' => '255,255,255')),

    );

    public function __construct() {
        parent::__construct();
    }

    public function get_all_tovar($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title",$this->t_products .".title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->table_name .'.*,'.
                            $this->t_products .'.title'. SQL_LANG .' as title,'.
                            $this->t_categories .'.title'. SQL_LANG .' as category_name,'.
                            $this->t_categories .'.template_id'
                            )
                           ->from($this->table_name)
                           ->join($this->t_products,$this->t_products .'.id = '. $this->table_name .'.product_id','left')
                           ->join($this->t_pictures,$this->t_pictures .'.id = '. $this->table_name .'.picture_id','left')
                           ->join($this->t_categories,$this->t_categories .'.id = '. $this->table_name .'.category_id','left')
                           ->get()->result_array();

        return $result;
    }

    public function get_catalog_tovars($category_id,$filters){
        $categories_ids = get_all_child(array(0=>$category_id),'categories');
        $_tovar = array();
        if($filters){
            $this->db->select('tAll.tovar_id');

            foreach($filters as $filter_group){
                $_dat[] = '(SELECT tovar_id FROM '. $this->t_filters_to_tovars .'  WHERE filter_id ='. implode(" OR filter_id =",$filter_group).')';
            }
            $this->db->from('(' . implode(' UNION ALL ', $_dat) . ') as tAll');
            $this->db->group_by('tAll.tovar_id');
            $this->db->having('COUNT(tAll.tovar_id) >= '.count($_dat));
            $res = $this->db->get();
            if($res->num_rows() > 0){
                foreach($res->result_array() as $item){
                    $_tovar[] = $item['tovar_id'];
                }
            }
            return $_tovar;
        }else{
            $this->db->select($this->table_name.'.id');
            $this->db->from($this->table_name);
            $this->db->where($this->table_name.'.active = 1 AND '. $this->table_name.'.category_id IN('. implode(",",$categories_ids).')');
            $res = $this->db->get();

            if($res->num_rows() > 0){
                foreach($res->result_array() as $item){
                    $_tovar[] = $item['id'];

                }
                return $_tovar;
            }
        }

       // echo $this->db->last_query();
        return $_tovar;
    }



    public function get_catalog($tovar_ids,$pagination = false) {

        if(!$tovar_ids){
            return false;
        }

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($this->table_name .'.'. $_sort);
        }

        $this->db->select($this->table_name.'.id,'.
                          $this->table_name.'.alias,'.
                          $this->table_name.'.title'. SQL_LANG .' as title,'.
                          $this->t_products.'.price,'.
                          $this->t_products.'.price_sale,'.
                          $this->t_painter_users.'.name as author_name,'.
                          $this->t_painter_users.'.id as author_id,'.
                          $this->t_painter_users.'.alias as author_alias,'.
                          $this->t_tovars_images.'.name as photo_name,'.
                          $this->t_tovars_images.'.type as photo_type,
                          (SELECT COUNT('. $this->t_likes .'.tovar_id) FROM '. $this->t_likes .' WHERE '. $this->t_likes .'.tovar_id = '. $this->table_name .'.id) as likes'
        );
        $this->db->from($this->table_name);
        $this->db->where($this->table_name.'.id IN('. implode(",",$tovar_ids).')');


        $this->db->join($this->t_products,$this->t_products.'.id = '. $this->table_name .'.product_id','left');
        $this->db->join($this->t_pictures,$this->t_pictures.'.id = '. $this->table_name .'.picture_id','left');
        $this->db->join($this->t_painter_users,$this->t_painter_users.'.id = '. $this->t_pictures .'.master_id','left');
        $this->db->join($this->t_tovars_images,$this->t_tovars_images.'.tovar_id = '. $this->table_name .'.id AND '.$this->t_tovars_images.'.product_image_id = (SELECT id FROM '. $this->t_products_images .' WHERE '. $this->t_products_images .'.product_id = '. $this->table_name .'.product_id ORDER BY '. $this->t_products_images .'.main desc, '. $this->t_products_images .'.sort asc LIMIT 1 )','left');
        $_res = $this->db->get();


        if($_res->num_rows() > 0){
            return $_res->result_array();
        }else{
            return false;
        }
    }

    public function get_tovar_templates(){

        $result = $this->db->select($this->templates .'.*, title'. SQL_LANG .' as title')->from($this->templates)->get()->result_array();
        return $result;

    }

    public function get_by_alias($alias, $only_active = false){
        if(!alias_validate($alias)){
            return false;
        }

        if($only_active){
            $this->db->where(array($this->table_name.'.active'=>1));
        }

        $res = $this->db->select($this->table_name .'.id as tovar_id, '.
                                 $this->table_name .'.alias as tovar_alias, '.
                                 $this->table_name .'.product_id, '.
                                 $this->table_name .'.picture_id, '.
                                 $this->table_name .'.category_id, '.
                                 $this->table_name.'.title'. SQL_LANG .' as tovar_title,'.
                                 $this->table_name.'.seo_title'. SQL_LANG .' as tovar_seo_title,'.
                                 $this->table_name.'.seo_keywords'. SQL_LANG .' as tovar_seo_keywords,'.
                                 $this->table_name.'.seo_description'. SQL_LANG .' as tovar_seo_description,'.
                                 $this->t_products .'.price,'.
                                 $this->t_products .'.price_sale,'.
                                 $this->t_products .'.description'. SQL_LANG .' as product_description,'.
                                 $this->t_products .'.seo_title'. SQL_LANG .' as product_seo_title,'.
                                 $this->t_products .'.seo_keywords'. SQL_LANG .' as product_seo_keywords,'.
                                 $this->t_products .'.seo_description'. SQL_LANG .' as product_seo_description,'.
                                 $this->t_pictures .'.master_id,'.
                                 $this->t_painter_users.'.name as author_name,'.
                                 $this->t_painter_users.'.alias as author_alias,'.
                                 $this->t_painter_users.'.id as author_id,
                                 (SELECT COUNT('. $this->t_likes .'.tovar_id) FROM '. $this->t_likes .' WHERE '. $this->t_likes .'.tovar_id = '. $this->table_name .'.id) as likes'
                                )
                        ->from($this->table_name)
                        ->join($this->t_products,$this->t_products .'.id ='. $this->table_name .'.product_id')
                        ->join($this->t_pictures,$this->t_pictures .'.id ='. $this->table_name .'.picture_id')
                        ->join($this->t_painter_users,$this->t_painter_users.'.id = '. $this->t_pictures .'.master_id','left')
                        ->where(array($this->table_name.'.alias' => $alias))
                        ->get();


        $result = ($res->num_rows() === 1) ? $res->row_array() : false;
        if(!$result){
            return false;
        }
        $result['template']  = $this->category_model->get_template_by_category($result['category_id'])->template;

        $this->db->select($this->t_tags.'.*');
        $this->db->from($this->t_tags);
        $this->db->where('id IN(SELECT tag_id FROM '. $this->t_tag_to_print .' WHERE print_id = '. $result['picture_id'] .')');
        $_res = $this->db->get();

        $result['tags'] = ($_res->num_rows() > 0)?$_res->result_array():array();
        return $result;

    }

    public function get_gender_associated_tovars($product_id,$picture_id){
        $_products = $this->product_model->get_gender_association($product_id);
        if(!$_products){
            return false;
        }
        $_res = $this->db->select('title'. SQL_LANG .' as title, alias,product_id,id')
                         ->from($this->table_name)
                         ->where('picture_id = '. $picture_id .' AND product_id IN('. implode(",",$_products) .')')
                         ->get()->result_array();

        if(!$_res){
            return false;
        }
        $data = array();
        foreach($_res as $product){
            switch($product['product_id']){
                case $_products['male']:
                    $data['male'] = $product;
                    break;
                case $_products['female']:
                    $data['female'] = $product;
                    break;
                case $_products['kid']:
                    $data['kid'] = $product;
                    break;
            }
        }

        return $data;
    }

    public function create($source,$source_class){
        $data = new $source_class($source);

        $data = $this->_generate_parametrs($data);

        $this->db->insert($this->table_name, $data);
        $data->id = $this->db->insert_id();
        $this->make_tovar_image((array)$data);
        $this->add_filters_to_tovar($data);

    }

    public function create_tovars($products,$print_id){

        foreach($products as $template_id => $product_by_template){
            $_products = $this->db->where_in('id',$product_by_template)->where(array('active'=>1))->get($this->t_products)->result_array();
            if(count($_products) == 0){
                continue;
            }
            foreach($_products as $product){
                $_source = array(
                    'product_id'        =>$product['id'],
                    'picture_id'        =>$print_id,
                    'category_id'       =>$product['category_id'],
                    'seo_title_ru'      =>$product['seo_title_ru'],
                    'seo_keywords_ru'   =>$product['seo_keywords_ru'],
                    'seo_description_ru'=>$product['seo_description_ru'],
                    'seo_title_ua'      =>$product['seo_title_ua'],
                    'seo_keywords_ua'   =>$product['seo_keywords_ua'],
                    'seo_description_ua'=>$product['seo_description_ua'],
                );
                $this->create($_source,'Tovar');
            }
        }
    }

    public function update($source, $source_id,$source_class) {
        $_data = $this->get_by_id($source_id);
        if ($_data){
            $data = new $source_class($source);

            /*
            if($data->alias == 'create'){
                $data = $this->_generate_parametrs($data);
            }
            */

            $this->db->update($this->table_name, $data, ['id' => $source_id]);
            $data->id = $source_id;
            $this->add_filters_to_tovar($data);
            return true;
        }
        return false;
    }

    private function add_filters_to_tovar($data){
        $product_filters = $this->product_model->get_product_filters($data->product_id);
        $picture_filters = $this->picture_model->get_picture_filters($data->picture_id);
        $this->db->where('tovar_id = '. $data->id)->delete($this->t_filters_to_tovars);

        if($product_filters){
            foreach($product_filters as $item){
                $filter_data_product[$item['filter_id']]['filter_id'] = $item['filter_id'];
                $filter_data_product[$item['filter_id']]['picture_id'] = 0;
                $filter_data_product[$item['filter_id']]['product_id'] = $data->product_id;
                $filter_data_product[$item['filter_id']]['tovar_id'] = $data->id;
            }
            $this->db->insert_batch($this->t_filters_to_tovars,$filter_data_product);
        }
        if($picture_filters){
            foreach($picture_filters as $item){
                $filter_data_picture[$item['filter_id']]['filter_id'] = $item['filter_id'];
                $filter_data_picture[$item['filter_id']]['picture_id'] = $data->picture_id;
                $filter_data_picture[$item['filter_id']]['product_id'] = 0;
                $filter_data_picture[$item['filter_id']]['tovar_id'] = $data->id;
            }
            $this->db->insert_batch($this->t_filters_to_tovars,$filter_data_picture);
        }
    }

    private function _generate_parametrs($data){
        $product_data = $this->product_model->get_by_id($data->product_id);
        $picture_data = $this->picture_model->get_by_id($data->picture_id);

        $data->alias = make_unic_alias($product_data['alias'].' '. $picture_data['alias']);
        $data->title_ru = make_unic_title($product_data['title_ru'].' '. $picture_data['title_ru']);
        $data->title_ua = make_unic_title($product_data['title_ua'].' '. $picture_data['title_ua']);
        return $data;
    }

    public function make_tovar_image($tovar_data){

        $product_data = $this->product_model->get_by_id($tovar_data['product_id']);
        $product_template_id = $this->db->select('template_id')->from($this->t_categories)->where(array('id'=> $product_data['category_id']))->get()->row()->template_id;
        $picture_img_data = $this->db->select('name,type')->from($this->t_pictures_images)->where(array('print_id'=> $tovar_data['picture_id'],'template_id'=>$product_template_id))->limit(1)->get()->row_array();
        $product_images = $this->product_image_model->get_image_by_product($tovar_data['product_id']);

        if(!$picture_img_data || !$product_images){
            return false;
        }

        $print_img = "images/prints/". $tovar_data['picture_id'] ."/". $picture_img_data['name'] .".". $picture_img_data['type'];
        $print_palitra = make_template_print($product_data['print_size_y'],$product_data['print_size_x'],$print_img);
        //save print to file 800x800
        print_to_palitra($print_palitra,$product_data['print_size_y'],$product_data['print_size_x'],$product_data['print_offset_x'],$product_data['print_offset_y']);

        $FolderWork = new FolderWork('images/tovars/'. $tovar_data['id']);
        $FolderWork->del();

        foreach($product_images as $img_data){

            $product_img = "images/products/". $tovar_data['product_id'] ."/". $img_data['name'] .".". $img_data['type']; //передаем скрипту изображение, на которое нужно что-то наложить, в формате .jpg

            //make tovar
            $_file = print_to_product($print_palitra,$product_img);

            $img_name = str_replace('e', 'a', substr(sha1(time() . $img_data['id']), 0, 5));
            $_url = 'images/tovars/'. $tovar_data['id'] .'/'.$img_name;
            $FolderWork = new FolderWork($_url);
            $FolderWork->chek_and_make_path();
            $FolderWork->clear();
            $UploadImage = new UploadImage;
            $_img_file = array(
                'name'=>'file.jpg',
                'type'=>"image/jpeg",
                'tmp_name'=>$_file,
                'error'=>0,
                'size'=>filesize($_file),
            );

          foreach($this->tovar_image as $key=>$val){
              $UploadImage->do_action($_img_file ,'/'. $FolderWork->path .'/', $val['x'], $val['y'], 10000, $key.'_'.$img_name, 90, $val['metod']);
              $UploadImage->addOptions($val['options']);
              $UploadImage->save();
          }

          $insert_data[] = array(
              'tovar_id'=>$tovar_data['id'],
              'name'=>$img_name,
              'type'=>$UploadImage->format,
              'product_image_id'=>$img_data['id'],

          );

           unlink($_file);

        }
        $this->db->where(array('tovar_id'=>$tovar_data['id']))->delete($this->t_tovars_images);
        $this->db->insert_batch($this->t_tovars_images,$insert_data);
        unlink($print_palitra);

        return true;
    }

    public function get_tovar_images($tovar_id){

        $res = $this->db->select(
            $this->t_tovars_images .'.*, '.
            $this->t_products_images .'.main,'.
            $this->t_products_images .'.sort,'.
            $this->t_products_images .'.data,'.
            $this->t_products_images .'.other0,'.
            $this->t_products_images .'.other1,'.
            $this->t_products_images .'.other2,'
        )
            ->from($this->t_tovars_images)
            ->join($this->t_products_images,$this->t_products_images .'.id ='. $this->t_tovars_images .'.product_image_id')
            ->where(array($this->t_tovars_images.'.tovar_id' => $tovar_id))
            ->order_by($this->t_products_images .'.main desc, '. $this->t_products_images .'.sort asc')
            ->get();

        return ($res->num_rows() > 0) ? $res->result_array() : false;

    }

    public function get_tovars_by_picture($picture_id,$count){
        $res = $this->db->select('id as tovar_id, alias as tovar_alias,(SELECT name FROM '. $this->t_tovars_images .' WHERE tovar_id = '. $this->table_name .'.id LIMIT 1) as photo_name')
                        ->from($this->table_name)
                        ->where('picture_id = '.$picture_id)
                        ->limit($count)
                        ->order_by('id desc')
                        ->get()->result_array();
        return $res;
    }

    public function get_painter_tovars($category_id,$user){
        $categories_ids = get_all_child(array(0=>$category_id),'categories');
        $_res = $this->db->select($this->table_name .'.*')
            ->from($this->table_name)
            ->where($this->table_name .'.category_id IN('. implode(",",$categories_ids) .')')
            ->where($this->table_name .'.active = 1')
            ->where($this->t_pictures .'.master_id = '. $user->id)
            ->join($this->t_pictures,$this->t_pictures.'.id = '. $this->table_name .'.picture_id')
            ->get();

        $_tovar = array();
        if($_res->num_rows() == 0){
            return $_tovar;
        }

        foreach($_res->result_array() as $item){
            $_tovar[] = $item['id'];

        }
        return $_tovar;
    }

    public function get_tovars_by_artist($author_id,$category_id,$count){
        $categories_ids = get_all_child(array(0=>$category_id),'categories');
        $_res = $this->db->select(
                                  $this->table_name.'.alias as tovar_alias,'.
                                  $this->table_name.'.id as tovar_id,'.
                                  '(SELECT name FROM '. $this->t_tovars_images .' WHERE tovar_id = '. $this->table_name.'.id LIMIT 1) as photo_name'

        )
            ->from($this->table_name)
            ->where($this->table_name .'.category_id IN('. implode(",",$categories_ids) .')')
            ->where($this->table_name .'.active = 1')
            ->where($this->t_pictures .'.master_id = '. $author_id)
            ->join($this->t_pictures,$this->t_pictures.'.id = '. $this->table_name .'.picture_id')
            ->order_by($this->table_name.'.id desc')
            ->limit($count)
            ->get();

        $_tovar = array();
        if($_res->num_rows() == 0){
            return $_tovar;
        }

        foreach($_res->result_array() as $item){
            $_tovar[$item['tovar_id']] = $item;
 
        }
        return $_tovar;
    }
}