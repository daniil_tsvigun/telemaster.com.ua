<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                <div class="row">
                    <div class="col-md-8 menu_select_box">
                        <?php $this->load->view('panel/templates/select_box');?>
                    </div>
                    <div class="col-md-4">
                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_title');?> </h5>
                        <input type="text" placeholder="" class="input" value="<?php echo $data->title;?>" name="title"><br>

                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                        <div class="widget-body">
                            <p><span><? echo LANG ('label_sort');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->sort;?>" name="sort"> </p>
                            <p><span><? echo LANG ('label_menu');?></span>
                                <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$menu_list,'name'=>'menu_id','selected'=> array($data->menu_id),'params' => array('no_all'=>'yes')));?>
                            </p>
                            <p><span><? echo LANG ('label_parent_menu_item');?></span>
                                <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$menu_items,'name'=>'parent_id','selected'=> array($data->parent_id),'params' => array(),'current'=>$data->id));?>
                            </p>
                        </div>
                    </div>
                </div>

                <input name="item_id" value="<?php echo $data->item_id;?>" type="hidden">
                <input name="item_type" value="<?php echo $data->item_type;?>" type="hidden">
            </form>
        </div>
    </div>
</div>
<script>
    (function ($, undefined) {
        $(document).ready(function () {

            $('.entity_group li').click(function(){
                $('.entity_group li').removeClass('selected');
                $(this).addClass('selected');
                var item_type = $(this).attr('data-item_type');
                var item_id = $(this).attr('data-item_id');

                $('#cform input[name="item_type"]').val(item_type);
                $('#cform input[name="item_id"]').val(item_id);
            });

            $('.menu_select_box .entity_group li').click(function(){
                $('input[name="title"]').val($(this).text());
            });
        });
    })(jQuery);
</script>