<h5 class="widget-name"><i class="icon-tasks"></i><?php echo LANG('label_filters'); ?></h5>
<div class="row" id="filters" style="margin-top:15px;">
    <div class="col-md-12">
        <?php if(count($filters) >0): ?>

            <?php foreach($filters as $group_title => $filters_data): ?>

                <p>
                    <span class="small-widget-name"><? echo $group_title;?></span>
                </p>
                <p data-group-title="<?php echo $group_title; ?>" class="small-input-content">
                    <?php foreach($filters_data as $filter): ?>
                        <label class="small-input-block">

                            <input <?php echo (in_array($filter['id'], $product_filters))?'checked="checked"':''; ?>type="checkbox" placeholder="" class="input" value="<?php echo $filter['id']; ?>" name="filters[<?php echo $filter['id']; ?>]">
                            <?php echo $filter['title']; ?>
                        </label>
                    <?php endforeach; ?>
                </p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<h5 class="widget-name"><i class="icon-tasks"></i><?php echo LANG('label_photo_parametrs'); ?></h5>
<div class="row" id="filters" style="margin-top:15px;">
    <div class="col-md-2">
        <p><span><? echo LANG ('label_print_size_x');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->print_size_x;?>" name="print_size_x"> </p>
    </div>
    <div class="col-md-2">
        <p><span><? echo LANG ('label_print_size_y');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->print_size_y;?>" name="print_size_y"> </p>
    </div>
    <div class="col-md-2">
        <p><span><? echo LANG ('label_print_offset_x');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->print_offset_x;?>" name="print_offset_x"> </p>
    </div>
    <div class="col-md-2">
        <p><span><? echo LANG ('label_print_offset_y');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->print_offset_y;?>" name="print_offset_y"> </p>
    </div>
</div>
<h5 class="widget-name"><i class="icon-picture"></i><?php echo LANG('label_photo'); ?></h5>
<div class="row" style="margin-top:15px;">
    <div class="col-md-2"><?php echo LANG('label_photo'); ?></div>
    <div class="col-md-3">Доп фото</div>
    <div class="col-md-1"><?php echo LANG('label_title'); ?></div>
    <div class="col-md-2"><?php echo LANG('label_model_type'); ?></div>
    <div class="col-md-1"><?php echo LANG('label_sort'); ?></div>
    <div class="col-md-1"><?php echo LANG('label_main_photo'); ?></div>
    <div class="col-md-2"><?php echo LANG('label_do'); ?></div>
</div>
<input name="images" value="" type="hidden">
<div class="media sortable ui-sortable" id="product-medias">
    <?php if(count($product_images) > 0): ?>
        <?php foreach($product_images as $image): ?>
            <div class="row stripped ui-sortable-handle">
                <div class="col-md-2" data-id="images[<?php echo $image['id']; ?>]" data-product_id="images[<?php echo $image['product_id']; ?>]">
                    <img src="/images/products/<?php echo $image['product_id']; ?>/<?php echo $image['name']; ?>.<?php echo $image['type']; ?>" style="display: block;margin: 0 auto;max-height:120px;">

                </div>
                <div class="col-md-3">
                    <div class="input-file">

                        <label for="other_img_0_<?php echo $image['id']; ?>">
                            <img data-img-id="other_img_0_<?php echo $image['id']; ?>" src="<?php echo isset($image['other0'])?'/images/products/'.$image["product_id"]."/other_img/". str_replace('.png','',$image['other0']) ."/"."d_".$image["other0"]:'http://placehold.it/95x95'; ?>" alt="no imgage">
                        </label>
                        <input id="other_img_0_<?php echo $image['id']; ?>" style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" multiple="" name="other0_<?php echo $image['id']; ?>" type="file" />
                    </div>
                    <div class="input-file">
                        <label for="other_img_1_<?php echo $image['id']; ?>">
                            <img data-img-id="other_img_1_<?php echo $image['id']; ?>" src="<?php echo isset($image['other1'])?'/images/products/'.$image["product_id"]."/other_img/". str_replace('.png','',$image['other1'])."/" ."d_".$image["other1"]:'http://placehold.it/95x95'; ?>" alt="no img">
                        </label>
                        <input id="other_img_1_<?php echo $image['id']; ?>" style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" multiple="" name="other1_<?php echo $image['id']; ?>" type="file" />
                    </div>
                    <div class="input-file">
                        <label for="other_img_2_<?php echo $image['id']; ?>">
                            <img data-img-id="other_img_2_<?php echo $image['id']; ?>" src="<?php echo isset($image['other2'])?'/images/products/'.$image["product_id"]."/other_img/". str_replace('.png','',$image['other2'])."/" ."d_".$image["other2"]:'http://placehold.it/95x95'; ?>" alt="no img">
                        </label>
                        <input id="other_img_2_<?php echo $image['id']; ?>" style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" multiple="" name="other2_<?php echo $image['id']; ?>" type="file" />
                    </div>
                </div>
                <div class="col-md-1">
                    <p><span><? echo LANG ('label_title');?> (<? echo LANG ('label_ru');?>)</span><input type="text" placeholder="" class="input" value="<?php echo isset($image['data']['title_ru'])?$image['data']['title_ru']:''; ?>" name="images[<?php echo $image['id']; ?>][title_ru]"/> </p>
                    <p><span><? echo LANG ('label_title');?> (<? echo LANG ('label_ua');?>)</span><input type="text" placeholder="" class="input" value="<?php echo isset($image['data']['title_ua'])?$image['data']['title_ua']:''; ?>" name="images[<?php echo $image['id']; ?>][title_ua]"/> </p>
                </div>
                <div class="col-md-2">
                    <div class="radio-select color-select">
                        <?php foreach($all_filters as $s_filter): ?>
                            <?php if($s_filter['parent_id']=='6'): ?>
                                <label class=" small-input-block">
                                    <input id="<?php echo $s_filter['id']; ?>" name="images[<?php echo $image['id']; ?>][phone_model]" class="input" <?php
                                    if(isset($image['data']['phone_model'])){
                                        if($image['data']['phone_model']==$s_filter['id']){
                                            echo 'checked';
                                        }
                                    }
                                    ?> value="<?php echo $s_filter['id']; ?>" type="radio">
                                    <?php echo $s_filter['title']; ?>
                                </label>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="col-md-1">
                    <input name="images[<?php echo $image['id']; ?>][sort]" class="sort" value="<?php echo isset($image['data']['sort'])?$image['data']['sort']:''; ?>" type="text">
                </div>
                <div class="col-md-1 setmaindiv">

                    <div class="checker"><input name="images[<?php echo $image['id']; ?>][main]" class="styled setmain" value="1" <?php echo isset($image['data']['main'])?'checked':''; ?> type="checkbox"></div>
                </div>

                <div class="col-md-2">
                    <a href="#" class="del" data-id="images[<?php echo $image['id']; ?>]" data-product_id="images[<?php echo $image['product_id']; ?>]"><i class="icon-trash"></i></a>
                </div>
            </div>
        <? endforeach;?>
    <? endif;?>
</div>
<div class="media">
    <div class="row stripped" id="product-medias-new">
        <div class="col-md-2 upload_img_conteiner">
            <label class="upload_image_label btn green label-input-file" style="padding: 38px 57px;">
                <input style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" class="upload_image_button" multiple="" name="photo_0" title="Upload images" type="file">
                <span><?php echo LANG('label_upload_photo'); ?></span>
            </label>
        </div>
    </div>
</div>
<div id="cropbox_wrap"></div>

<style>
    .upload_img_conteiner{
        height: 100px;
        width: 235px;
    }
    .upload_img_conteiner img{
        display: block;
        margin: 0 auto;
        max-height: 100%;
        max-width: 100%;
    }
    #product-medias label.small-input-block input[type='radio']{
        float: left;
        display: inline;
        width: 15px;
    }

    .media div.input-file {
        float: left;
        margin: 0 2px;
    }
    .media div.input-file img{
        height:95px;
        width:95px;
        cursor: pointer;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        var iter = 1;
        if (window.File && window.FileList && window.FileReader) {
            $('body').on('change', ".upload_image_button", function (e) {
                Make_preview(e, $(this).closest('.upload_image_label'));
                iter++;
            });
        }
        if (window.File && window.FileList && window.FileReader) {
            $('body').on('change', ".input-file>input[type='file']", function (e) {
                Make_my_preview(e, $('img[data-img-id="'+ $(this).attr('id') +'"]'));
            });
        }
        function Make_my_preview(input, obj) {
            var reader = new FileReader();
            var files = input.target.files || input.dataTransfer.files;
            reader.readAsDataURL(files[0]);
            reader.onload = function (e) {
                obj.attr('src', e.target.result);
            }
        }
        function Make_preview(input, obj) {
            var reader = new FileReader();
            var files = input.target.files || input.dataTransfer.files;
            if (obj.prev('.upload_image').length > 0) {
                obj.prev('.upload_image').remove();
            } else {
                var str = '<div class="col-md-2 upload_img_conteiner" >';
                str += '<label class="upload_image_label btn green label-input-file" style="padding: 38px 57px;">';
                str += '<input style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" class="upload_image_button" multiple="" name="photo_' + iter + '" title="Upload images" type="file">';
                str += '<span><?php echo LANG('label_upload_photo'); ?></span>';
                str += '</label>';
                str += '</div>';
                obj.closest('.upload_img_conteiner').after(str);
            }
            reader.readAsDataURL(files[0]);
            reader.onload = function (e) {
                obj.before('<image class="upload_image">');
                obj.prev('.upload_image').attr('src', e.target.result);
                obj.hide();
            }
        }
        $('body').on('click', '.upload_image', function () {
            $(this).siblings('.upload_image_label').trigger('click');
        });
        $('#media>#filters input').click(function(e){
            var filter=$(this).val();
            if($(this).is(':checked')){
                return false;
            } else {
                $(this).prop('checked',false);
                $('#product-medias label input[value='+filter+']').each(function(){
                    $(this).prop('checked',false);
                });
            }
        });
        $('#product-medias label input').click(function(){
            var filter=$(this).val();
            var count=0;
            var filter_arr=[];
            $('#product-medias label input:checked').each(function(i){
                filter_arr.push($(this).attr('id'));
            });
            $('#media>#filters input').prop('checked',false);
            for(var i=0; i<filter_arr.length; i++){
                $('#media>#filters input[value='+filter_arr[i]+']').prop('checked',true);
            }
        });
        $('.del').click(function(){
            $('[data-bb-handler="danger"]').click(function(){
                var filter_arr=[];
                setTimeout(function(){
                    $('#product-medias label input:checked').each(function(i){
                        filter_arr.push($(this).attr('id'));
                    });
                    console.log(filter_arr);
                    $('#media>#filters input').prop('checked',false);
                    for(var i=0; i<filter_arr.length; i++){
                        $('#media>#filters input[value='+filter_arr[i]+']').prop('checked',true);
                    }
                },1000);
            });
        });
        $('[task="save"],[task="saveandclose"]').off();
        $('[task="save"],[task="saveandclose"]').click(function(e){
            e.preventDefault();
            var trigger=true;
            var err_msg=[];
            var all_colors=[];
            $('#product-medias>div').each(function(k){
                $(this).find('.radio-select').each(function(){
                    var colors_arr=[];
                    $(this).find('input:checked').each(function(){
                        colors_arr.push($(this).attr('id'));
                    });
                    if(colors_arr.length>1){
                        trigger=false;
                        err_msg.push('Выбрано более одного цвета для одного изображения!');
                    }
                    if(colors_arr.length==0){
                        trigger=false;
                        err_msg.push('Не выбрано цвета для '+(k+1)+'-го изображения!<br/>');
                    }
                    if(all_colors.indexOf(colors_arr[0])==-1){
                        all_colors.push(colors_arr[0]);
                    } else {
                        trigger=false;
                        err_msg.push('Цвета должны быть уникальными!');
                    }
                });
            });
            if(!trigger){
                for(var i=0; i<err_msg.length; i++){
                    error_message_clear();
                    error_message_push(err_msg);
                }
            } else {
                $('#cform input#tab').val(window.location.hash);
                $("#cform").attr("action", $(this).attr('href'));
                $('#cform').submit();
            }
        });
    });
</script>