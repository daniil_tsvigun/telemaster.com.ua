<?php

$lang['error_wrong_combination_user_password']	= "Помилка, така комбінація логіна і пароля не знайдена.";
$lang['School']                               = "Школа";
$lang['hello']                               = "Вітаємо";

$lang['timetable']                               = "Розклад";
$lang['lesson']                               = "Урок";
$lang['journal']                               = "Журнал";
$lang['help']                               = "Допомога";
$lang['diary']                               = "Щоденник";

$lang['month_01']                               = "Січень";
$lang['month_02']                               = "Лютий";
$lang['month_03']                               = "Березень";
$lang['month_04']                               = "Квітень";
$lang['month_05']                               = "Травень";
$lang['month_06']                               = "Червень";
$lang['month_07']                               = "Липень";
$lang['month_08']                               = "Серпень";
$lang['month_09']                               = "Вересень";
$lang['month_10']                               = "Жовтень";
$lang['month_11']                               = "Листовад";
$lang['month_12']                               = "Грудень";

$lang['Jan']                               = "Січень";
$lang['Feb']                               = "Лютий";
$lang['Mar']                               = "Березень";
$lang['Apr']                               = "Квітень";
$lang['May']                               = "Травень";
$lang['Jun']                               = "Червень";
$lang['Jul']                               = "Липень";
$lang['Aug']                               = "Серпень";
$lang['Sep']                               = "Вересень";
$lang['Oct']                               = "Жовтень";
$lang['Nov']                               = "Листопад";
$lang['Dec']                               = "Грудень";

$lang['day_of_week_1']                     ="Понеділок";
$lang['day_of_week_2']                     ="Вівторок";
$lang['day_of_week_3']                     ="Середа";
$lang['day_of_week_4']                     ="Четвер";
$lang['day_of_week_5']                     ="П'ятниця";
$lang['day_of_week_6']                     ="Субота";
$lang['day_of_week_0']                     ="Неділя";

$lang['monday']                            ="Понеділок";
$lang['tuesday']                           ="Вівторок";
$lang['wednesday']                         ="Середа";
$lang['thursday']                          ="Четвер";
$lang['friday']                            ="П'ятниця";
$lang['saturday']                          ="Субота";
$lang['sunday']                            ="Неділя";

//login
$lang['login_user']	                        = "Користувач";
$lang['login_new']	                        = "Змінити";
$lang['login_hello']	                        = "Вхід до адмін панелі";
$lang['login_login']	                        = "Логін";
$lang['login_password']	                        = "Пароль";
$lang['login_password_rep']	                    = "Повторіть пароль";
$lang['login_password_replace']	                = "Зміна пароля для авторізації";
$lang['login_foget_password']	                = "Забули пароль";
$lang['login_reg']	                            = "Реєстрація";
$lang['login_enter']	                        = "Вхід";
$lang['login_title']	                        = "Школа № 106 | Адмінпанель";

$lang['added_file']	                        = "Закріплений файл";
$lang['add_file']	                        = "Закріпити файл";
$lang['cancel']	                        = "Відміна";
$lang['save']	                        = "Зберегти";
$lang['close']	                        = "Закрити";
$lang['hv']	                        = "хв";
$lang['no']	                        = "Ні";
$lang['yes']	                        = "Так";
$lang['download']	                        = "Завантажити";
$lang['delete']	                        = "Видалити";
$lang['exit']	                        = "Вихід";
$lang['send']	                        = "Відправити";


$lang['homework_for_this_lesson']	                        = "Домашнє завдання на данний урок";

$lang['lastes']	                        = "Залишок";
$lang['predmet']	                        = "Предмет";
$lang['select_another_predmet']	                        = "Вибрати інший предмет";
$lang['class']	                        = "Клас";
$lang['group']	                        = "Група";
$lang['select_another_class']	                        = "Вибрати інший клас";
$lang['homework_to_lesson']	                        = "Домашнє завдання, на урок";
$lang['you_really_wont_change_lesson']	                        = "Зберегти зміни в данном уроці?";
$lang['you_really_wont_change_journal']	                        = "Зберегти зміни в журналі?";
$lang['finish_lesson']	                        = "Завершити урок";
$lang['progress_on']	                        = "Враховувати оцінки";
$lang['homework_for_this_lesson']	                        = "Домашнє завдання на данний урок";
$lang['homework']	                        = "Домашнє завдання";
$lang['homework_is']	                        = "є д/з";

$lang['lesson_by_predmet']	                        = "Урок по предмету";
$lang['in']	                        = "в";
$lang['success_save']	                        = "класі успішно збережений";

$lang['name']	                        = "Ім'я";
$lang['family']	                        = "Прізвище";
$lang['attent']	                        = "Присутність";
$lang['progress']	                        = "Оцінка";
$lang['comment']	                        = "Коментар";
$lang['diary_desc']	                        = "Запис у щоденник";
$lang['history']	                        = "Історія";
$lang['user_is']	                        = "Користувач";
$lang['message']	                        = "Повідомлення";
$lang['message_private']	                        = "особисте";
$lang['message_all']	                        = "загальне";
$lang['by_teacher']	                        = "Від вчителя";
$lang['message_send']	                        = "Ваше повідомлення відправлено";
$lang['homework_empty']	                        = "відсутнє";

$lang['date']	                        = "Дата";
$lang['time']	                        = "Час";
$lang['header']	                        = "Заголовок";
$lang['month']	                        = "Місяць";
$lang['save_edit']	                        = "зберегти зміни";
$lang['add_lesson']	                        = "додати урок";

$lang['help_send_message']	                        = "Форма зворотнього зв'язку";
$lang['input_help_text']	                        = "введіть ваш запит";
$lang['input_help_forback_contakt']	                = "Данні для зворотніого звязку";
$lang['input_help_forback_title']	                = "Як з вами зв'язатися?";
$lang['input_help_forback_text']	                        = "Номер мобільного телефона";

$lang['timetable_today']	                        = "Розклад на сьогодні";
$lang['my_timetable']	                        = "Мій розклад";
$lang['all_timetable']	                        = "Загальний розклад";

$lang['datatable_empty']	                        = "Таблиця пуста";
$lang['datatable_view']	                        = "Відображено";
$lang['datatable_from']	                        = "з";
$lang['datatable_to']	                        = "по";
$lang['datatable_froms']	                        = "з";
$lang['datatable_messages']	                        = "повідомлень";
$lang['datatable_empty']	                        = "повідомлення відсутні";
$lang['datatable_search']	                        = "Пошук";
$lang['datatable_on_page']	                        = "На сторінці";

$lang['diary_desc']	                        = "Запис в щоденнику";
$lang['homework_was']	                        = "Домашнє завдання на цей урок було";
$lang['homework_is']	                        = "Задане на уроці домашнє завдання";
$lang['edit_lesson']	                        = "Ви хочете перейти до редагування данного урока?";

$lang['progres_temm']	                        = "оцінка за тематичне заняття";
$lang['progres_off']	                        = "оцінка відключена(не враховується)";
$lang['progres_norm']	                        = "звичайні оцінки";

//Админка
$lang['copy_timetable_success']	                        = "Розклад успішно скопійовано";
$lang['hello_text']	                        = "Привітальний текст  ";

$lang['edit']	                        = "Редагувати";
$lang['create']	                        = "Створити";
$lang['remove']	                        = "Видалити";
$lang['add']	                        = "Додати";
$lang['save_close']	                    = "Зберегти і закрити";
$lang['save']	                        = "Зберегти";
$lang['menu']	                        = "Меню";
$lang['no_select']	                    = "не вибрано";

/* permissions group title*/
$lang['permission_title_service']	    = "Базові";
$lang['permission_title_roles']	        = "Ролі";
$lang['permission_title_users']	        = "Користувачі";
$lang['permission_title_categories']    = "Категорії";
$lang['permission_title_filters']       = "Фільтри";
$lang['permission_title_menus']	        = "Меню";
$lang['permission_title_media']	        = "Меіа";
$lang['permission_title_pages']	        = "Сторінки";
$lang['permission_title_news']	        = "Новини";
$lang['permission_title_blog']	        = "Блог";
$lang['permission_title_orders']	    = "Замовлення";
$lang['permission_title_products']	    = "Продукти";
$lang['permission_title_prints']	    = "Принти";
$lang['permission_title_tovars']	    = "Товари";
$lang['permission_title_orders']	    = "Замовлення";
$lang['permission_title_promos']	    = "Промо коди";
$lang['permission_title_frontend']	    = "Сайт";
$lang['permission_title_comments']	    = "Коментарі";
$lang['permission_title_finances']	    = "Фінанси";



$lang['search']							= "Пошук";

/* End of file content_lang.php */
