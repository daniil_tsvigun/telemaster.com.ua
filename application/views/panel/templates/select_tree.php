<select <? echo (isset($params['multiple']))? 'multiple="multiple"':'';?> style=" <? echo (isset($params['multiple']))? 'height:300px;':'max-height: 100px;';?>" name="<?php echo $name;?>">

    <?php if(!isset($params['no_all'])):?><option value ="0"><?php echo LANG('label_no_parent');?></option><?php endif;?>
    <?php $title_field = (!isset($params['title_field']))?'title':$params['title_field'];?>

        <?php foreach($select_data as $key => $item):?>
        <option <?php if(isset($current)):echo ($current == $item['id'])? 'disabled="disabled"':'';endif;?> value ="<?php echo $item['id']?>" <?php echo (in_array($item['id'],$selected))? 'selected="selected"':'';?>><?php echo (isset($item['level']))?make_tree_separator($item['level']):'';?><?php echo $item[$title_field]?></option>
    <?php endforeach;?>
</select>