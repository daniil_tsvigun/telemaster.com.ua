<!DOCTYPE html>
<html>
<head>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tele Master</title>
    <link href="/assets/lib/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/lib/animate.css/3.3.0/animate.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css?v=1.3.6"/>
    <script type="text/javascript">
        var site_url = "http://www.telemaster.com.ua/ru";
    </script>
</head>
<body>
<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
            Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a
        ,m)

    })(window,document,'script','https://www.google-analytics.com/analytics.js',
        'ga');

    ga('create', 'UA-88207792-1', 'auto');
    ga('send', 'pageview');

</script>

<div id="vk_api_transport"></div><div data-id="mobile_btn" class="lang-menu">
    <ul>
        <li class=""><a href="http://www.telemaster.com.ua/ua/reports">Укр</a></li>
        <li class="current"><a href="http://www.telemaster.com.ua/ru/reports">Рус</a></li>
    </ul>
</div>
<div data-id="mobile_btn" class="master ">
    <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
</div>
<section class="nav_bar" >
    <div class="wrapper">
        <div class="menu">
            <a href="http://www.telemaster.com.ua/ru" class="logo-anchor"></a>
            <ul class="main-menu slideInDown animated">

                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru">Главная</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/calling">Найти мастера</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/partners">Найти провайдера</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/instructions">Инструкции</a>
                </li>
                <!--                <li>-->
                <!--                    <a class="--><!--" href="--><!--">--><!--</a>-->
                <!--                </li>-->
                <li class="master ">
                    <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
                </li>
            </ul>
            <div id="menu-btn"></div>
            <div class="lang-menu">
                <ul>

                    <li class=""><a href="http://www.telemaster.com.ua/ua/reports">Укр</a></li>
                    <li class="current"><a href="http://www.telemaster.com.ua/ru/reports">Рус</a></li>
                </ul>
            </div>
            <div class="master ">
                <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
            </div>
        </div>

    </div>
</section>    <div data-alias="reports" class="content single-instruction">
    <div class="wrapper">
        <div class="main-block">

            <h2>Правила модерации отзывов</h2>
            <div data-first="" class="text">
                <div>
                    <p>Мы ценим отзывы о работе Мастеров и считаем важным дать возможность нашим посетителям поделиться своим мнением о Мастере и о работе нашего ресурса.&nbsp; Для того, чтобы Сайт оставался надежным источником релевантной информации, просим вас перед публикацией отзыва убедиться, что при его составлении соблюдаются следующие правила.</p>
                    <p><strong>Уместность для просмотра посетителями любого возраста </strong></p>
                    <p>Нашим Сайтом пользуются телезрители разного возраста.&nbsp; Мы намерены поддерживать безопасную и доброжелатеьную атмосферу, подходящую для общения посетителей любого возраста. Мы не публикуем отзывы с ругательствами или вульгарными выражениями. Также отклоняются отзывы с несдержанными высказываниями сексуального характера, гневными репликами или предвзятыми замечаниями, угрозами и личными оскорблениями, противоречащие моральным и этическим нормам, нарушающие действующее международное законодательство и/или законодательство Украины, нарушающие права третьих лиц, а также Правила пользования Сайтом.</p>
                    <p><strong>Некоммерческий характер отзывов</strong></p>
                    <p>Главная задача отзывов &ndash; поделиться впечатлением о Мастере и помочь другим посетителям Сайта в выборе Мастера.&nbsp; В отзывах о Мастерах просим обсуждать услуги, предоставляемые Мастерами, их качество, быстроту выполнения и общее впечатление о Мастере. Мы не разрешаем публиковать под видом отзывов &nbsp;рекламу услуг, товаров или коммерческих предприятий. Отзывы, предлагаемые в обмен на личную выгоду, например подарки, услуги или деньги, удаляются.</p>
                    <p>Мы также не принимаем отзывы со ссылками на внешние ресурсы, любой спам, сообщения вне тематики Сайта, призывы добавляться в друзья, ссылки на внешние ресурсы, сайты, группы в социальных сетях независимо от тематики.</p>
                    <p>Если у посетителя Сайта есть конкретные предложения по улучшению работы мастера или жалоба на конкретного Мастера, он может адресовать сообщение на адрес электронной почты Линии поддержки мастеров &ndash; так мы сможем наиболее оперативно отреагировать.&nbsp;</p>
                    <p><strong>Удобочитаемость</strong></p>
                    <p>Сделайте свой отзыв максимально понятным для других посетителей Сайта: используйте алфавит, подходящий для вашего языка, не используйте слишком много ЗАГЛАВНЫХ БУКВ и сленговых слов.</p>
                    <p>Ваши комментарии должны содержать общеупотребительные слова и выражения, доступные для понимания каждому. Сообщения, в которых используются слова и выражения, характерные только для вашей местности, а также двусмысленные фразы, смысл которых будет неясен или непонятен посетителям этого сайта, опубликованы не будут.</p>
                    <p>Мы оставляем за собой право удалять определенные материалы в любой момент (в том числе и после публикации) без указания причины. Отзывы, размещаемые на&nbsp;Сайте, являются частными субъективными мнениями. Отзывы отражают мнение посетителей Сайта. Мы не&nbsp;присоединяемся к&nbsp;мнению авторов отзывов. В соответствии с нашей политикой конфиденциальности, мы не предоставляем личной контактной информации, за исключением той, которая опубликована на Сайте.</p>                    </div>
            </div>

        </div>
    </div>
</div>
<div class="hidden rate popup fadeIn animated">
    <div class="wrapper">
        <div class="wrap">
            <div class="box">
                <div class="close"></div>
                <div class="rate">
                    <div class="levels" data-choosen="null">
                        <ul>
                            <li class="" data-level_id="0" id="level0"><div></div></li>
                            <li class="" data-level_id="1" id="level1"><div></div></li>
                            <li class="" data-level_id="2" id="level2"><div></div></li>
                            <li class="" data-level_id="3" id="level3"><div></div></li>
                            <li class="" data-level_id="4" id="level4"><div></div></li>
                        </ul>
                    </div>
                    <div class="btn-box">
                        <div id="vote" class="btn btn-type3">Проголосовать</div>
                    </div>
                    <div class="auth-box">
                        <div class="fb_auth"></div>
                        <div class="vk_auth"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="wrapper">
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <a href="http://www.telemaster.com.ua/ru/about">О нас</a>
            <a href="http://www.telemaster.com.ua/ru/faq">Вопросы и ответы</a>
            <a href="http://www.telemaster.com.ua/ru/rules">Правила пользования сайтом</a>
            <a href="http://www.telemaster.com.ua/ru/reports">Правила модерации отзывов</a>
            <a href="http://www.telemaster.com.ua/ru/polices">Политика конфиденциальности</a>

        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 center">
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <p>Администратор сайта: </p>
            <p>info@telemaster.com.ua</p>
            <div class="btns special">
                <div id="youtube_btn">
                    <a target="_blank" href="https://www.youtube.com/channel/UCUrsf864ut8p5UETF4Ym6Hw">&nbsp;</a>
                </div>
            </div>
        </div>
    </div>
    <div class="rights">
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <div class="logo"></div>
            </div>
            <div class="center col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <p>© 2016 All rights reserved</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p>
                    <a href="http://lemon.ua/">Создание сайта  Lemon.ua</a>
                </p>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/assets/js/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/lib/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/lib/animate.css/3.3.0/animate.min.css" ></script>