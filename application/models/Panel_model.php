<?php

class Panel_model extends CI_Model {

    protected $panel_menu_table = 'panel_adminmenu';

    public function __construct() {
        parent::__construct();
        
    }

    public function left_menu(){
        $result = array();
        $menu_data = $this->get_menu_data();
        if(!$menu_data)
            return $result;

        foreach($menu_data as $item){

            if(!has_permission($item['permissions'],$this->user)){
                continue;
            }

            if($item['parent_id'] == 0){
                $result[$item['id']] = array(
                    'title'=>$item['title'. SQL_LANG],
                    'alias'=>$item['alias'],
                    'icon'=>$item['icon'],
                );
            }else{

              //if(!isset($result[$item['parent_id']]) || !has_permission($item['permissions'],$this->user)){
                if(!isset($result[$item['parent_id']])){
                    continue;
                }

                $result[$item['parent_id']]['submenu'][$item['id']] = array(
                    'title'=>$item['title'. SQL_LANG],
                    'alias'=>$item['alias'],
                    'icon'=>$item['icon'],
                    'url'=> site_url().'/panel/'. $result[$item['parent_id']]['alias'] .'/'. $item['alias'],
                );
            }

        }

        return $result;
    }
    public function dashboard(){
        $result = array();
        $dashboard_data = $this->get_menu_data(true,true);
        if(!$dashboard_data)
            return $result;

        foreach($dashboard_data as $item){

            if(!has_permission($item['permissions'],$this->user)){
                continue;
            }

            if($item['parent_id'] == 0){
                $_parents[$item['id']] = array(
                    'title'=>$item['title'. SQL_LANG],
                    'alias'=>$item['alias'],
                    'icon'=>$item['icon'],
                );
            }else{

                if(!isset($_parents[$item['parent_id']])){
                    continue;
                }

                $result[$item['id']] = array(
                    'title'=>$item['title'. SQL_LANG],
                    'icon'=>$item['icon'],
                    'url'=> site_url().'/panel/'. $_parents[$item['parent_id']]['alias'] .'/'. $item['alias'],
                );
            }

        }

        return $result;
    }

    public function get_menu_data($only_active = true,$only_dashboard = false){

        if($only_active){
            $this->db->where(array('active'=>1));
        }

        if($only_dashboard){
            $this->db->where('(show_dashboard = 1 OR parent_id = 0)');
        }

        $this->db->order_by('parent_id, position');
        $res = $this->db->get($this->panel_menu_table);
        return ($res->num_rows() > 0)? $res->result_array():null;
    }

    public function get_all_templates() {

         $this->db->order_by('title'. SQL_LANG);

        $result = $this->db->select('templates.*, title'. SQL_LANG .' as title')->from('templates')->get()->result_array();

        return $result;
    }

}