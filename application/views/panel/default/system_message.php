<?php
$system_message = get_system_messages();
if(count($system_message) > 0):?>
    <?php foreach($system_message as $message):?>
    <div class="alert alert-<?php echo $message['type'];?>" style="margin-top: 16px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo $message['title'];?></strong> <?php echo $message['text'];?>
    </div>
    <?php endforeach;?>
<?php endif;?>