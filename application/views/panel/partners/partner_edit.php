<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                    <?php $this->load->view('panel/templates/action_button');?>
                    <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="general" href="#general" aria-expanded="true"><?php echo LANG('label_tab_general')?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active" role="tabpanel">
                            <div class="row">
                                <div class="col-md-8 inside_tab">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                            <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="data_<?php echo $lang_key;?>" href="#data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                            <?php $_first = false;
                                        endforeach;?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php if(false): ?>
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                            <div id="data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                                <?php if($lang_key=='ru'):
                                                    $sufix='';
                                                else :
                                                    $sufix='_'.$lang_key;
                                                endif; ?>
                                                <label for="">Yfpdf</label>
                                                <input type="text" placeholder="" class="input" value="<?php echo $data->{'name_'. $lang_key};?>" name="name<?php echo $sufix;?>">

                                            </div>

                                            <?php $_first = false;
                                        endforeach;?>
                                        <?php endif; ?>
                                        <label for="">Name</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->name;?>" name="name">
                                        <label for="">Company</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->company;?>" name="company">
                                        <label for="">Phone</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->phone;?>" name="phone">
                                        <label for="">Link</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->link;?>" name="link">
                                        <input type="hidden" name="active" value="<?php echo $data->active; ?>">
                                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_regions');?></h5>
                                        <input id="current_regions" value="" type="hidden">

                                        <div class="region-block">
                                            <div class="areas">
                                                <input name="areas[]" type="hidden">
                                                <p><?php echo LANG('label_enter_regions'); ?></p>
                                                <p><?php echo LANG('label_delivery_area'); ?></p>
                                                <?php
                                                if(isset($data->test_r)){
                                                    foreach($data->test_r as $r){
                                                        echo $r->district_place_ua.', ';
                                                    }
                                                }

                                                ?>
                                                <ul>
                                                    <?php

                                                    if(isset($areas)):
                                                        foreach($areas as $area):
                                                            $active='';
                                                            foreach($data->regions as $item){
//                                                                if($item->{'area_name'.SQL_LANG}==$area['area_name'.SQL_LANG]){
//                                                                    $active='checked';
//                                                                }
                                                                if($item->Oblast_KOATUU==$area['KOATUU']){
                                                                    $active='checked';
                                                                }
                                                            }
                                                            ?>

                                                            <li>
                                                                <input <?php echo $active; ?> class="<?php echo $active; ?>" data-area_id="<?php echo $area['id']; ?>" name="areas[<?php echo $area['KOATUU']; ?>]" type="checkbox">
                                                                <label ><?php echo $area['area_name'.SQL_LANG]; ?></label>
                                                            </li>
                                                        <?php endforeach;
                                                    endif;
                                                    ?>
                                                </ul>
                                            </div>

                                            <div class="districts">
                                                <p><?php echo LANG('label_delivery_district'); ?></p>
                                                <div>
                                                    <span style="font-size: 1.16em; padding-right: 10px; font-weight: 600;" >Все</span><input type="checkbox" name="all">
                                                </div>
                                                <ul>

                                                </ul>
                                            </div>
                                            <div class="third_part">
                                                <p><?php echo LANG('label_delivery_district'); ?></p>
                                                <div>
                                                    <span style="font-size: 1.16em; padding-right: 10px; font-weight: 600;" >Все</span><input type="checkbox" name="all">
                                                </div>
                                                <ul>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                                    <div class="widget-body">

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
            </form>
        </div>
    </div>
</div>
<style>
    .master_avatar{
        position: relative;
    }
    .master_avatar input{
        opacity: 0;
        position: absolute;
        top: 0;
        width: 100%;
        height: 100%;
        cursor: pointer;
    }
    .region-block>div>p{
        display: block;
        font-size: 1.5em;
        font-weight: 200;
        color: #898a8a;
        margin: 0;
    }
    .region-block ul{
        padding: 0;
        list-style-position: inside;
        -moz-column-count: 3;
        -webkit-column-count: 3;
        column-count: 3;
        min-height: 150px;
    }
    .region-block ul>li{
        line-height: 30px;
    }
    .region-block input[type="checkbox"]{
        margin: 4px 0 0;
        line-height: normal;
        float: left;
        margin-right: 13px;
        width: 19px;
        height: 19px;
    }
    .form-wrapper label{
        display: block;
        font-size: 1.5em;
        font-weight: 200;
        color: initial;
        margin: 0;
    }
</style>
<script>
    var site_url = "<?php echo site_url();?>";
    $('input[name="all"]').change(function(){
        if($(this).prop('checked')==true){
            $('.districts input[type="checkbox"]').prop('checked',true);
        } else {
            $('.districts input[type="checkbox"]').prop('checked',false);
        }
    });
    $('.region-block .areas input[type="checkbox"]').click(function(){
        var areas=[];
        var current=[];

        $('.districts li').each(function(){
            if($(this).find('input').prop('checked')==true){
                current.push($(this).find('input').attr('data-koatuu'));
            }
        });
        $('.region-block .areas input[type="checkbox"]').each(function(){
            if($(this).prop('checked')==true){
                areas.push($(this).attr('data-area_id'));
            } else {
                $('.region-block .districts ul>li').remove();
            }
        });


        get_districts(areas,current);

    });



    $(document).ready(function(){
        var active=[];
        var current=[];
        var regions=<?php echo json_encode($data->regions);  ?>;

        for(var k=0;k<regions.length;k++){
            current.push(regions[k].district_id);
        }
        $('.region-block ul>li input').each(function(){
            if($(this).prop('checked')==true){
                active.push($(this).attr('data-area_id'));
            }

        });
        get_districts(active,current);
        setTimeout(function(){
            for(var i=0;i<regions.length; i++){

                if(regions[i].District_KOATUU){

                    $('.districts input[data-koatuu='+regions[i].District_KOATUU+']').click();
                    $('input[data-koatuu='+regions[i].district_id+']').click();

                }



            }

            setTimeout(function(){
                for(var i=0;i<regions.length; i++){
                    if(regions[i].District_KOATUU){
                        $('input[data-koatuu='+regions[i].district_id+']').click();
                    }
                }
            },1500);
        },1500);



    });


    function add_district_listner(){
        //$('.region-block .districts input[type="checkbox"]').off();
        $('.region-block .districts input[type="checkbox"]').click(function(){

            var areas=[];
            var current=[];

            $('.third_part li').each(function(){
                if($(this).find('input').prop('checked')==true){
                    current.push($(this).find('input').attr('data-koatuu'));
                }
            });
            $('.region-block .districts input[type="checkbox"]').each(function(){
                if($(this).prop('checked')==true){
                    areas.push($(this).attr('data-koatuu'));
                } else {
                    $('.region-block .third_part ul>li').remove();
                }
            });


            get_in_district(areas,current);
        });
    }

    function get_districts(data,current){

        $.ajax({
            url: site_url +'/registration/ajax_get_districts',
            type: 'POST',
            data: {"data":data},
            success: function (result){

                var obj = $.parseJSON(result);
                var list= $('.region-block>div.districts ul');
                list.find('li').remove();
                if(obj.status == 'success'){

                    for(var i in obj.data){
                        if(obj.data[i].district_id!=45){
                            var prop='';
                            for(var j=0; j<current.length; j++){
                                if(current[j]==obj.data[i].KOATUU){
                                    prop='checked';
                                }
                            }
                            switch(obj.data[i].type){

                                case "6":
                                    var reg_type='м. ';
                                    break;
                                case "7":
                                    var reg_type='cмт. ';
                                    break;
                                default:
                                    var reg_type='';
                                    break;
                            }
                            list.append('' +
                                '<li>' +
                                '   <input data-koatuu="'+obj.data[i].KOATUU+'" name="regions['+obj.data[i].KOATUU+']" type="checkbox" '+ prop +'>' +
                                '   <label for="">'+reg_type+obj.data[i]["district_place"+obj.lang]+'</label>' +
                                '</li>');
                        }
                    }
                    setTimeout(function(){
                        add_district_listner();
                    },1000);

                }else{

                }

            }
        });
    }
    function get_in_district(data,current){

        $.ajax({
            url: site_url +'/registration/ajax_get_by_district_koatuu',
            type: 'POST',
            data: {"data":data},
            success: function (result){

                var obj = $.parseJSON(result);
                var list= $('.region-block>div.third_part ul');
                list.find('li').remove();
                if(obj.status == 'success'){
                    //console.log(obj);
                    for(var i in obj.data){
                        //if(obj.data[i].district_id!=45){
                            var prop='';
                            for(var j=0; j<current.length; j++){
                                if(current[j]==obj.data[i].KOATUU){
                                    prop='checked';
                                }
                            }

                            switch(obj.data[i].Type){
                                case "6":
                                    var reg_type='м. ';
                                    break;
                                case "7":
                                    var reg_type='cмт. ';
                                    break;
                                case "8":
                                    var reg_type='с. ';
                                    break;
                                default:
                                    var reg_type='';
                                    break;
                            }

                            list.append('' +
                                '<li>' +
                                '   <input data-koatuu="'+obj.data[i].KOATUU+'" name="regions['+obj.data[i].District_KOATUU+']['+obj.data[i].KOATUU+']" type="checkbox" '+ prop +'>' +
                                '   <label for="">'+reg_type+obj.data[i].UkrName+'</label>' +
                                '</li>');
                        //}
                    }
                }else{

                }
            }
        });
    }
</script>