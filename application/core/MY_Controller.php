<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->user = get_user(true);
    }


    public function change(){

        $id = ($this->input->post('id'))? (int)$this->input->post('id'):false;
        $changes = ($this->input->post('changes'))? (int)$this->input->post('changes'):0;
        $name = ($this->input->post('name'))? $this->input->post('name'):false;
        $type = ($this->input->post('type'))? $this->input->post('type'):false;

        if(!$id || !$name || !$type || !has_permission('changes_'. $type .'_permissions',$this->user)){
            return;
        }

        $_model = $type .'_model';
        $this->$_model->change($name,$id,$changes);

    }

    public function remove($type = false){
        
        redirect_has_no_permission($this->user,'remove_'. $type. '_permissions',$this->base_url .'/'.$type);

        $remove = ($this->input->post('items'))? $this->input->post('items'):false;

        if(!$remove){
            add_system_message('danger', LANG('ntf_error_'. $type. '_remove_no_users_title'), LANG('ntf_error_'. $type. '_remove'));
            redirect($this->base_url .'/'. $type);
        }

        if(!$type){
            add_system_message('danger', LANG('ntf_error_'. $type. '_remove_title'), LANG('ntf_error_'. $type. '_remove'));
            redirect($this->base_url);
        }

        $_model = $type .'_model';
        $this->$_model->remove($remove);
        add_system_message('success', LANG('ntf_'. $type. '_remove_title'), LANG('ntf_'. $type. '_remove'));
        redirect($this->base_url .'/'. $type);
    }

    public function ajax(){

    }

}