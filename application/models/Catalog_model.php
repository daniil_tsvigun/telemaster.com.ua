<?php

class Catalog_model extends CI_Model {

    protected $table_name = 'tovars';
    protected $t_products= 'products';
    protected $t_pictures = 'pictures';
    protected $t_filters = 'filters';

    public function __construct() {
        parent::__construct();

    }

    public function get_catalog_tovars($category_data,$filters){

        $this->db->select($this->table_name.'.*'

                          );
        $this->db->from($this->table_name);
        if($filters){
            $this->db->where(array($this->table_name.'.active' => 1,$this->t_products.'.category_id' => $category_data['id']));
        }else{
            $this->db->where(array($this->table_name.'.active' => 1,$this->t_products.'.category_id' => $category_data['id']));
        }

        $this->db->join($this->t_products,$this->t_products.'.id = '. $this->table_name .'.product_id','left');
        $this->db->join($this->t_pictures,$this->t_pictures.'.id = '. $this->table_name .'.picture_id','left');
        $res = $this->db->get();

        $_tovars_count = $res->num_rows();
        if($_tovars_count > 0){
            $_SESSION['catalog']['tovar_count'] = $_tovars_count;
            return $res->result_array();
        }else{
            return false;
        }

    }

}
