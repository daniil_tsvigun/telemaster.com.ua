<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FolderWork{

	public $result, $error;
	
	function __construct($path =''){
	
		//инициализируем массив путь - адрес папки пример: ('../img/slider/'. $id) !не закрывать слешем!
		$this->path = $path;
		$this->work_path[] = $path;
		
	}

	//Получить список файлов директории
	public function get_list($path = false){
	
		if(!$path)
			$path = $this->path;
	
		if(!file_exists($path))
			return false;

		$files = scandir($path);
		
		if(count($files) < 3)
			return false;
			
		array_shift($files);
		array_shift($files);
		
		return $files;
	}
	
	//удалить директорию
	public function del($del_dir = false) {
	
		if($del_dir)
			$this->work_path[] = $del_dir;
	
		//путь текущей директории
		$cur_link = implode('/', $this->work_path);

		if(!file_exists($cur_link))
			return;
		
		//проверяем наличие файлов в папке
		$files = $this->get_list($cur_link);
		
		//если файлы есть
		if($files){
			//пытаемся их удалить
			foreach($files as $val){
				//если текущее имя - папка
				if(is_dir($cur_link . '/' .$val)){
					//если папка не пуста
					if($this->get_list($cur_link . '/' .$val)){
						//увеличиваем глубину папок и пытаемся удалить папку и ее содержимое
						$this->work_path[] = $val;
						$this->del();
						//после удаления уменьшаем глубину папок
						array_pop($this->work_path);
					}else{
						//если папка пуста, удаляем ее (если не удалось ее удалить, останавливаемся)
						if(!@rmdir($cur_link . '/' .$val)){
							$this->error[] = 'Не удалось удалить директорию: '. $cur_link . '/' .$val;
							break;
						}
					}
				}else{
					//если текущее имя - файл, удаляем его (если не удалось его удалить, останавливаемся)
					if(!@unlink($cur_link . '/' .$val)){
						$this->error[] = 'Не удалось удалить файл: '. $cur_link . '/' .$val;
						break;
					}
				}
			}
		}
		
		//если папка пуста или очищена от файлов, пытаемся ее удалить
		if(!@rmdir($cur_link)){
			$this->error[] = 'Не удалось удалить директорию: '. $cur_link;
		}
                
                
                if(isset($del_path)){
                    if($del_path){
                        array_pop($this->work_path);
                    }
                }
		if(empty($this->error)){
			return true;
		}else{
			return false;
		}
	}

	//очистка папки
	public function clear(){
	
		//проверяем наличие файлов в папке
		$files = $this->get_list($this->path);

		if(!$files) return true;
		
		//пытаемся их удалить
		foreach($files as $val){
			//если текущее имя - папка, удаляем ее
			if(is_dir($this->path .'/'. $val)){
				$this->del($val);
				
			}else{
				//если текущее имя - файл, удаляем его (если не удалось его удалить, останавливаемся)
				if(!@unlink($this->path .'/'. $val)){
					$this->error[] = 'Не удалось удалить файл: '. $this->path . '/' .$val;
					return false;
				}
			}
		}
	
		return true;
	}
	
	//очистка папки от файлов
	public function clear_files(){
	
		//проверяем наличие файлов в папке
		$files = $this->get_list($this->path);

		if(!$files) return true;
		
		//пытаемся их удалить
		foreach($files as $val){
			//если текущее имя - папка, пропускаем
			if(is_dir($this->path .'/'. $val)){
				continue;
				
			}else{
				//если текущее имя - файл, удаляем его (если не удалось его удалить, останавливаемся)
				if(!@unlink($this->path .'/'. $val)){
					$this->error[] = 'Не удалось удалить файл: '. $this->path . '/' .$val;
					return false;
				}
			}
		}
	
		return true;
	}
	
	//проверка пути и создание при необходимости папок
	public function chek_and_make_path(){

		$names = explode("/", $this->path);
		
		$real_path = '';

		//проверяем и создаем путь
		foreach($names AS $val){
			
			if(!empty($real_path))
				$real_path .= '/';
			
			$real_path .= $val;
			
			if(!file_exists($real_path)){
				if(!@mkdir($real_path)){
					return false;
				}
			}
		}
			
		return true;
	}
}
?>