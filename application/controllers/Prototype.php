<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prototype extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();
        define('UPLOAD_DIR', 'images/test/');

    }

    public function index(){

        $data['new_file'] = '';
        if($this->input->post('form_img')){
            $data['new_file'] = $this->save_base64_img($this->input->post('form_img'));
        }

        $img_file = 'images/GM.jpg';

        $imgData = base64_encode(file_get_contents($img_file));

        $data['src'] = 'data: '.mime_content_type($img_file).';base64,'.$imgData;


        $this->load->view('frontend/test/test_canvas',$data);
    }

    public function _save_base64_img($base64_img){



        $img = $base64_img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $new_image = imagecreatefromstring(base64_decode($img));
        $w=imagesx($new_image);
        $h=imagesy($new_image);
        imageAlphaBlending($new_image, false);
        imageSaveAlpha($new_image, true);
        $data = array('image'=>$new_image,'width'=>$w,'height'=>$h);

        $dest = imagecreate($w, $h);   // Создаём пустую картинку размером $x на $y
        $dst1_x=$dst1_y=0;

        imagecopy ($dest, $data['image'], $dst1_x, $dst1_y, 0, 0, imagesx($data['image']), imagesy($data['image']));

        $file = UPLOAD_DIR . uniqid() . '.png';
        imagePng($dest, $file);

        return $file;
    }
    public function save_base64_img($base64_img){

        $image = explode('base64,',$base64_img);
        $file = UPLOAD_DIR . uniqid() . '.png';
        file_put_contents($file, base64_decode($image[1]));
       return $file;
    }

}
