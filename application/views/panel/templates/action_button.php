<div class="crumbs row" id="action_buttons_list">
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <ul class="panel-breadcrumb">
            <li>
                <a href="<?php site_url();?>/panel"><?php echo $page_title;?></a>
            </li>
        </ul>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
        <div class="alt-buttons-wrap">

            <ul class="alt-buttons">
                <?php if($this->uri->segment(3)=='master'): ?>
                    <li style="width: 80px; margin-right: 5px;" >
                        <input value="<?php echo $this->input->get('id'); ?>" class="search_input" id='filter_id' type="text" placeholder="ID" name="filter_id">
                    </li>
                    <li style="width: 80px; margin-right: 5px;">
                        <input value="<?php echo $this->input->get('master_id'); ?>" class="search_input" id='filter_master_id' type="text" placeholder="ID master" name="filter_master_id">
                    </li>
                    <li style="width: 80px; margin-right: 5px;" >
                        <select class="search_input" id='filter_active' name="filter_active">
                            <option <?php if($this->input->get('active')==false): echo 'selected'; endif; ?> value="">All</option>
                            <option <?php if($this->input->get('active')=='1'): echo 'selected'; endif; ?> value="1">Active</option>
                            <option <?php if($this->input->get('active')=='0'): echo 'selected'; endif; ?> value="0">Non-active</option>
                            <option <?php if($this->input->get('active')=='2'): echo 'selected'; endif; ?> value="2">Banned</option>
                        </select>
                    </li>
                    <li style="width: 100px; margin-right: 5px;">
                        <input value="<?php echo $this->input->get('surname'); ?>" class="search_input" id='filter_surname' type="text" placeholder="Прізвище" name="filter_surname">
                    </li>
                    <li style="width: 180px;">
                        <input value="<?php echo $this->input->get('phone'); ?>" class="search_input" id='filter_phone' type="text" placeholder="Номер телефону" name="filter_phone">
                    </li>
                    <li>
                        <?php 
                            $_link = make_link_by($current_link,array(''));
                            $sort_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
                        ?>
                        <a id="search_input_btn" title="Search" class="btn green" href="<?php echo $sort_link; ?>">
                            
                            <span><?php echo LANG('search');?></span>
                        </a>
                    </li>
                    <script>
                        var s_b=$('#search_input_btn');
                        var link='<?php echo site_url("/panel/masters/master/?"); ?>';
                        
                        function set_link(link,id='',master_id='',surname='',phone='',active=''){
                            
                            link+='id='+id.trim()+'&master_id='+master_id.trim()+'&surname='+surname.trim()+'&phone='+phone.trim()+'&active='+active.trim();
                            s_b.attr('href',link);
                                                    
                        }
                        $('.search_input').on("input",function(){
                            
                            set_link(link,$('#filter_id').val(),$('#filter_master_id').val(),$('#filter_surname').val(),$('#filter_phone').val(),$('#filter_active').val());
                        });

                    
                    </script>
                <?php endif; ?>
                <?php if(isset($create_link)):?>
                    <li>
                        <a title="Add" class="btn green" href="<?php echo $create_link?>">
                            <i class="icon-plus"></i>
                            <span><?php echo LANG('create');?></span>
                        </a>
                    </li>
                <?php endif;?>

                <?php if(isset($remove_link)):?>
                    <li>
                        <a title="Remove" task="remove" href="<?php echo $remove_link;?>" class="cform btn green">
                            <i class="icon-remove"></i>
                            <span><?php echo LANG('remove');?></span>
                        </a>
                    </li>
                <?php endif;?>

                <?php if(isset($save_link)):?>
                    <li>
                        <a class="cform btn green" href="<?php echo $save_link;?>" task="save" title="Save">
                            <i class="icon-ok"></i>
                            <span><?php echo LANG('save');?></span>
                        </a>
                    </li>
                <?php endif;?>

                <?php if(isset($save_close_link)):?>
                    <li>
                        <a class="cform btn green" href="<?php echo $save_close_link;?>" task="saveandclose" title="Save &amp; close">
                            <i class="icon-ok"></i>
                            <span><?php echo LANG('save_close');?></span>
                        </a>
                    </li>
                <?php endif;?>

                <?php if(isset($close_link)):?>
                    <li>
                        <a class=" btn green" href="<?php echo $close_link;?>" task="close" title="Close">
                            <i class="icon-stop"></i>
                            <span><?php echo LANG('close');?></span>
                        </a>
                    </li>
                <?php endif;?>

            </ul>
        </div>
    </div>
</div>