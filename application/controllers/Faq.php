<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();

    }

    public function index(){
        //$data['title']='Питання та відповіді';
        $this->load->view('frontend/static_faq' . SQL_LANG);
        //$this->load->view('frontend/templates/content',$data);
    }

}