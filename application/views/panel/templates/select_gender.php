<select name="gender[<?php echo $gender_name;?>]">
    <option value ="0"><?php echo LANG('label_no_select');?></option>
    <?php if($product_list_to_gender):?>
        <?php foreach($product_list_to_gender as $key => $item):?>
            <option <?php if($item['id'] == $data->id){ echo 'style="font-weight:bold;"';}?> value ="<?php echo $item['id']?>" <?php echo $item['id'] == $selected? 'selected="selected"':'';?>><?php echo $item['title'];?></option>
        <?php endforeach;?>
    <?php endif;?>
</select>