<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                <div class="row">
                    <div class="col-md-8">
                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_title');?></h5>
                        <input type="text" placeholder="" class="input" value="<?php echo $data->title;?>" name="title"><br>
                    </div>
                    <div class="col-md-4">
                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                        <div class="widget-body">
                            <p><span><? echo LANG ('label_alias');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->alias;?>" name="alias"> </p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>