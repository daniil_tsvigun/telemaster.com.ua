<?php

class Master
{
    public function __construct($source = null)
    {

        $this->id = isset($source['id']) ? $source['id'] : '';
        $this->id_master = isset($source['id_master']) ? $source['id_master'] : '';
        $this->name = isset($source['name']) ? $source['name'] : '';
        $this->surname = isset($source['surname']) ? $source['surname'] : '';
        $this->name_ua = isset($source['name_ua']) ? $source['name_ua'] : '';
        $this->surname_ua = isset($source['surname_ua']) ? $source['surname_ua'] : '';
        $this->inn = isset($source['inn']) ? $source['inn'] : '';
        $this->passport = isset($source['passport']) ? $source['passport'] : '';
        $this->passport_data = isset($source['passport_data']) ? $source['passport_data'] : '';
        $this->email = isset($source['email']) ? $source['email'] : '';
        $this->phone1 = isset($source['phone1']) ? $source['phone1'] : '';
        $this->phone2 = isset($source['phone2']) ? $source['phone2'] : '';
        $this->phone3 = isset($source['phone3']) ? $source['phone3'] : '';
        $this->phone4 = isset($source['phone4']) ? $source['phone4'] : '';
        $this->regions = isset($source['regions']) ? $source['regions'] : '';
        $this->rate = isset($source['rate']) ? $source['rate'] : '';
        $this->rate_amount = isset($source['rate_amount']) ? $source['rate_amount'] : '';
        $this->image = isset($source['image']) ? $source['image'] : '';
        $this->active = isset($source['active']) ? $source['active'] : 0;
    }
}

class Master_model extends MY_Model
{
    private $image_cutter_sizes = array(
        //'a' => array('x' => NULL, 'y' => NULL, 'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
        'b' => array('x' => 136, 'y' => 120, 'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
        'd' => array('x' => 348, 'y' => 234, 'metod' => 4, 'options' => array('bg_color' => '255,255,255')),

    );

    protected $kyiv_districts=array(
        '8036100000',
        '8036300000',
        '8036400000',
        '8036600000',
        '8038000000',
        '8038200000',
        '8038500000',
        '8038600000',
        '8038900000',
        '8039100000'
    );
    protected $sev_districts=array(
        '8536900000',
    );
    protected $crimea_districts=array(
        '110100000',
        '110300000',
        '110600000',
        '110900000',
        '111200000',
        '111300000',
        '111400000',
        '111500000',
        '111600000',
        '111700000',
        '111900000',
        '120400000',
        '120700000',
        '121100000',
        '121600000',
        '122000000',
        '122300000',
        '122700000',
        '123100000',
        '123500000',
        '123900000',
        '124300000',
        '124700000',
        '125200000',
        '125600000'
    );
    protected $table_participated_users = 'participated_users';
    protected $table_name = 'masters';
    protected $table_masters_to_regions = 'masters_to_regions';
    protected $table_master_images = 'master_images';
    protected $table_masters_to_refuses = 'masters_to_refuses';

    public function __construct()
    {

        parent::__construct();
    }

     public function get_master_by_email($email='')
    {
        if($email==''){
            $master_info=array();
        } else {
            $master_info = $this->db->get_where($this->table_name, array('email' => $email))->result_array();
        }
        return $master_info;
    }

    public function get_master_by_phone($phone)
    {
        $this->db->from($this->table_name);
        $this->db->where(array('phone1' => $phone));
        $this->db->or_where(array('phone2' => $phone));
        $this->db->or_where(array('phone3' => $phone));
        $this->db->or_where(array('phone4' => $phone));
        $info=$this->db->get()->result_array();
        return $info;
    }

    public function get_refuses(){
        $info=$this->db->get('refuses')->result_array();
        return $info;
    }

    public function get_refuse_by_id($refuse_id){
        $info=$this->db->where(array('id'=>$refuse_id))->get('refuses')->row_array();
        return $info;
    }

    /**
     * @return string
     */
    public function get_refuses_by_master($master_id)
    {
        $info=$this->db->where(array('id_master'=>$master_id))->get($this->table_masters_to_refuses)->row_array();
        return $info;
    }

    public function get_master_by_id($id, $pagination = false)
    {

        if ($pagination) {
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $this->db->order_by(str_replace("-", " ", $pagination['sort']));
        }
        $master_info = $this->db->get_where($this->table_name, array('id' => $id))->row_array();
        $this->db->from($this->table_masters_to_regions);
        $this->db->where('master_id', $master_info['id']);
        $regions=$this->db->get()->result_array();
        foreach($regions as $key=>$item){
            $regions[$key]=$item['region_id'];
        }
        $master_info['regions']=$this->region_model->get_regions_by_ids($regions);

//        foreach ($regions as $key => $region) {
//            $master_info['regions'][$key] = $this->region_model->get_region_by_id($region['region_id']);
//        }
        //var_dump($master_info);
        $master_info['image'] = $this->get_master_image_by_id($master_info['id']);
        return new Master($master_info);
    }

    public function get_all_masters($pagination = false)
    {   
        
        if ($pagination) {
            
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $this->db->order_by(str_replace("-", " ", $pagination['sort']));
            if($pagination['id']!=false&&!empty($pagination['id'])){
                $this->db->where('id',$pagination['id']);
            }
            if($pagination['master_id']!=false&&!empty($pagination['master_id'])){
                $this->db->where('id_master',$pagination['master_id']);
            }
            if($pagination['surname']!=false&&!empty($pagination['surname'])){
                $this->db->like('surname',$pagination['surname'],'after');
            }
            
            if($pagination['active']!==false&&$pagination['active']!=''){
                
                $this->db->where('active',intval($pagination['active']));
            }
            if($pagination['phone']!=false&&!empty($pagination['phone'])){
                $this->db->like('phone1', $pagination['phone']);
                $this->db->or_like('phone2', $pagination['phone']);
                $this->db->or_like('phone3', $pagination['phone']);
                $this->db->or_like('phone4', $pagination['phone']); 
                // $where='phone1='.intval($pagination['phone']).' OR phone2='.intval($pagination['phone']).' OR phone3='.intval($pagination['phone']).' OR phone4='.intval($pagination['phone']);
                // $this->db->where($where);
            }
        }

        $this->db->order_by('active', "asc");
        //var_dump($this->db->get_compiled_select());
        $result = $this->db->get($this->table_name)->result_array();
        return $result;
    }

    /**
     * @param $master_id
     * @param $user_id
     * @param $rate
     * @return float
     */
    public function set_master_rate($master_id, $user_id, $rate)
    {

        $user = $this->db->get_where($this->table_participated_users, array('user_id' => $user_id, 'master_id' => $master_id))->num_rows();

        if ($user != 0) {
            return false;
        } else {
            $rate = ($rate + 1) * 0.2;
            $master = $this->db->get_where($this->table_name, array('id' => $master_id))->row_array();
            $final_rate_amount = $master['rate_amount'] + 1;
            $final_rate = round(($master['rate'] * $master['rate_amount'] + $rate) / $final_rate_amount, 2);
            $data = array(
                'rate' => $final_rate,
                'rate_amount' => $final_rate_amount
            );
            $this->db->where('id', $master_id);
            $this->db->update($this->table_name, $data);
            $this->db->insert($this->table_participated_users, array('id' => NULL, 'user_id' => $user_id, 'master_id' => $master_id));
            return $final_rate;
        }
    }

    public function get_masters_by_region($region_data, $pagination = false)
    {
        if ($pagination) {
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $this->db->order_by(str_replace("-", " ", $pagination['sort']));
        }
        $this->db->distinct();
        $this->db->select(
            $this->table_name . '.id,' .
            $this->table_name . '.id_master,' .
            $this->table_name . '.name,' .
            $this->table_name . '.surname,' .
            $this->table_name . '.inn,' .
            $this->table_name . '.email,' .
            $this->table_name . '.phone1,' .
            $this->table_name . '.phone2,' .
            $this->table_name . '.phone3,' .
            $this->table_name . '.phone4,' .
            $this->table_name . '.rate,' .
            $this->table_master_images . '.img_name,' .
            $this->table_master_images . '.img_type,' .
            $this->table_name . '.active,'.
            $this->table_name . '.name_ua,' .
            $this->table_name . '.surname_ua' 
        );
        $this->db->from($this->table_name);
        $this->db->join($this->table_masters_to_regions, $this->table_name . ".id=" . $this->table_masters_to_regions . '.master_id', 'inner');
        $this->db->join($this->table_master_images, $this->table_master_images . '.master_id=' . $this->table_name . '.id', 'left');
        $this->db->where($this->table_masters_to_regions . '.region_id', intval($region_data['id']));
        $master_info = $this->db->get()->result_array();
        $result = array();
            
        foreach ($master_info as $master) {
            $master['image'] = array('img_name' => $master['img_name'], 'img_type' => $master['img_type']);
            unset($master['img_name']);
            unset($master['img_type']);
            unset($master['master_id']);
            $result[] = new Master($master);
        }

        return $result;
    }


    public function get_masters_by_region_test($region_data, $pagination = false)
    {
        if ($pagination) {
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $this->db->order_by(str_replace("-", " ", $pagination['sort']));
        }
        $this->db->distinct();
        $this->db->select(
            $this->table_name . '.id,' .
            $this->table_name . '.id_master,' .
            $this->table_name . '.name,' .
            $this->table_name . '.surname,' .
            $this->table_name . '.inn,' .
            $this->table_name . '.email,' .
            $this->table_name . '.phone1,' .
            $this->table_name . '.phone2,' .
            $this->table_name . '.phone3,' .
            $this->table_name . '.phone4,' .
            $this->table_name . '.rate,' .
            $this->table_master_images . '.img_name,' .
            $this->table_master_images . '.img_type,' .
            $this->table_name . '.active'
        );
        $this->db->from($this->table_name);
        $this->db->join($this->table_masters_to_regions, $this->table_name . ".id=" . $this->table_masters_to_regions . '.master_id', 'inner');
        $this->db->join($this->table_master_images, $this->table_master_images . '.master_id=' . $this->table_name . '.id', 'left');
        $this->db->where($this->table_masters_to_regions . '.region_id', intval($region_data['id']));
        $master_info = $this->db->get()->result_array();
        $result = array();
            
        foreach ($master_info as $master) {
            $master['image'] = array('img_name' => $master['img_name'], 'img_type' => $master['img_type']);
            unset($master['img_name']);
            unset($master['img_type']);
            unset($master['master_id']);
            $result[] = new Master($master);
        }

        return $result;
    }


    public function get_master_image_by_id($master_id)
    {
        $image = $this->db->get_where($this->table_master_images, array('master_id' => $master_id))->row_array();
        return $image;
    }

    public function master_update($source, $source_id,$source_class){
        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            $regions=$data->regions;
            unset($data->regions);
            unset($data->image);
            $data->id = $source_id;
            $this->db->update($this->table_name, $data, ['id' => $source_id]);

            if (isset($_FILES) && !empty($_FILES)) {
                $this->save_photos($_FILES, $data->id);
            }

            // if(isset($regions)&&$regions!=''&&!empty($regions)){
            //     $source['id']=$data->id;
            //     $source['regions']=$regions;
            //     $this->update_locations($source);
            // }
            $source['id']=$data->id;
            $source['regions']=$regions;
            $this->update_locations($source);

            if (isset($source["master_refuses"]) && $source["master_refuses"] !=0 ){
                $this->db->where(array('id_master'=>$source_id))->delete($this->table_masters_to_refuses);
                $this->db->insert($this->table_masters_to_refuses,array('id_master'=>$source_id,'id_refuses'=>$source["master_refuses"],'comment'=>$source['refuses_comment']));
                $this->db->update($this->table_name,array('active'=>2),['id' => $source_id]);

                if(isset($source["refuses_send_email"]) && $source["refuses_send_email"] == 'yes' ){
                    $message=$this->get_refuse_by_id($source["master_refuses"]);
                    $_data['to_email']=$source['email'];
                    $_data['template']='refuse_message';
                    $_data['subject'] = 'Відмова в реєстрації';
                    $_data['message'] = array('refuse_mess'=>$message['name_ua'],'name'=>$source['name'] .''. $source['surname']);
                   $this->sender_model->message($_data);

                }
            }
            
            if($this->uri->segment(1)=='panel'){
                $source['action']='update';
                $log_data=json_encode($source);
                log_problems($log_data);
            }
            return true;
        }
        return false;
    }

    public function create_master($source)
    {
        if (isset($source['index1']) && isset($source['phone1']) && $source['phone1'] != '' && $source['index1'] != '') {
            $prefix='+38';
            if(isset($source['prefix1'])&&$source['prefix1']!=''){
                $prefix=$source['prefix1'];
            }
            $source['phone1'] = $prefix . $source['index1'] . $source['phone1'];
        } else {
            unset($source['phone1']);
        }
        if (isset($source['index2']) && isset($source['phone2']) && $source['phone2'] != '' && $source['index2'] != '') {
            $prefix='+38';
            if(isset($source['prefix2'])&&$source['prefix2']!=''){
                $prefix=$source['prefix2'];
            }
            $source['phone2'] = $prefix . $source['index2'] . $source['phone2'];
        } else {
            unset($source['phone2']);
        }
        if (isset($source['index3']) && isset($source['phone3']) && $source['phone3'] != '' && $source['index3'] != '') {
            $prefix='+38';
            if(isset($source['prefix3'])&&$source['prefix3']!=''){
                $prefix=$source['prefix3'];
            }
            $source['phone3'] = $prefix . $source['index3'] . $source['phone3'];
        } else {
            unset($source['phone3']);
        }
        if (isset($source['index4']) && isset($source['phone4']) && $source['phone4'] != '' && $source['index4'] != '') {
            $prefix='+38';
            if(isset($source['prefix4'])&&$source['prefix4']!=''){
                $prefix=$source['prefix4'];
            }
            $source['phone4'] = $prefix . $source['index4'] . $source['phone4'];
        } else {
            unset($source['phone4']);
        }
        $data = new Master($source);
        $image=$data->image;
        unset($data->image);
        unset($data->id);
        $regions=$data->regions;
        unset($data->regions);

        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        if($this->uri->segment(1)=='panel'){
            $source['action']='create';
            $source['inserted_id']=$_id;
            $log_data=json_encode($source);
            log_problems($log_data);
        }
        //$_id=$source['id'];
        if (isset($_id) && $_id != NULL && $_id != '' && isset($source['regions'])) {
        	$flag=false;
            $crim_flag=false;
            $sev_flag=false;
            foreach ($source['regions'] as $key => $active) {

                if ($active == 'on') {
                    if(in_array(intval($key),$this->kyiv_districts)){
                        $flag=true;
                    }
                    if(in_array(intval($key),$this->crimea_districts)){
                        $crim_flag=true;
                    }
                    if(in_array(intval($key),$this->sev_districts)){
                        $sev_flag=true;
                    }
                        $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => $key));
                }
            }

            if($flag){
                $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => '8000000000'));
            }
            if($sev_flag){
                $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => '8500000000'));
            }
            if($crim_flag){
                $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => '0100000000'));
            }
        }
        if (isset($_FILES) && !empty($_FILES)) {

            $this->save_photos($_FILES, $_id);
        }
        return $_id;
    }

    public function rotate_image($filename=false,$degree){
        if(!$filename){
            return false;
        } else {
            $file_info = new SplFileInfo($filename);
            if($degree=='right'){
                $degrees = -90;
            } else {
                $degrees = 90;
            }
            if(strtolower($file_info->getExtension())=='jpg'||strtolower($file_info->getExtension())=='jpeg'){
                $source = imagecreatefromjpeg($filename);
                $rotate = imagerotate($source, $degrees, 0);
                imagejpeg($rotate,$filename);
            } elseif(strtolower($file_info->getExtension())=='png') {
                $source = imagecreatefrompng($filename);
                $rotate = imagerotate($source, $degrees, 0);
                imagepng($rotate,$filename);
            }
        }
    }

    public function save_photos($data, $master_id){

        foreach ($data as $up_key => $up_file) {

            if (empty($up_file['name'])) {
                continue;
            }

            $image_id = preg_replace('/other\d_/', '', $up_key);
            $image_type = preg_replace('/_(.*)/', '', $up_key);
            $_img_atributs = get_img_params($up_file);
            if ($_img_atributs['error']) {
                add_system_message('danger', $_img_atributs['message'], '');
                continue;
            }

            $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));

            $_url = 'images/avatars/' . $master_id . '/' . $img_name;
            $FolderWork = new FolderWork($_url);
            $FolderWork->chek_and_make_path();
            $UploadImage = new UploadImage;

            foreach ($this->image_cutter_sizes as $key => $val) {
                $UploadImage->do_action($up_file, '/' . $FolderWork->path . '/', $val['x'], $val['y'], 10000, $key . '_' . $img_name, 90, $val['metod']);
                $UploadImage->addOptions($val['options']);

                $_movented = $UploadImage->save();

            }
            if ($_movented) {

//                $image1 = imagecreatefromjpeg($_url.'/b_'.$img_name);
//                $image2 = imagecreatefromjpeg($_url.'/b_'.$img_name);
//                var_dump($image1);
//                var_dump($image2);
//
//                $exif = exif_read_data ($up_file['tmp_name'],0,true);
//
//                switch ($exif['IFD0']['Orientation']) {
//                    // Поворот на 180 градусов
//                    case 3: {
//                        //$image = imagerotate($image,180,0);
//                        break;
//                    }
//                    // Поворот вправо на 90 градусов
//                    case 6: {
//                        //$image = imagerotate($image,-90,0);
//                        break;
//                    }
//                    // Поворот влево на 90 градусов
//                    case 8: {
//                        //$image = imagerotate($image,90,0);
//                        break;
//                    }
//                }
                $fname = $up_file['name'];
                $file_info = new SplFileInfo($fname);

                $this->db->where('master_id',$master_id);
                $q = $this->db->get($this->table_master_images);
                if ( $q->num_rows() > 0 )
                {
                    $this->db->where('master_id',$master_id);
                    $this->db->update($this->table_master_images, array(
                        'img_name'=>$img_name,
                        'img_type'=>strtolower($file_info->getExtension()),
                    ));
                } else {
                    $this->db->insert($this->table_master_images, array(
                        'master_id'=>$master_id,
                        'img_name'=>$img_name,
                        'img_type'=>strtolower($file_info->getExtension()),
                    ));
                }

                //$this->db->update($this->table_master_images, array($image_type => $img_name . '.png'), array('id' => $image_id));
            }


        }
    }

    public function update_locations($source){
        
        $_id=$source['id'];
        if (isset($_id) && $_id != NULL && $_id != '') {
            $this->db->delete($this->table_masters_to_regions, array('master_id' => $_id));
            $flag=false;
            $sev_flag=false;
            $crim_flag=false;
            if (isset($source['regions'])&&$source['regions']!='') {
                foreach ($source['regions'] as $key => $active) {
                    if ($active == 'on') {
                        if(in_array(intval($key),$this->kyiv_districts)){
                            $flag=true;
                        }
                        if(in_array(intval($key),$this->crimea_districts)){
                            $crim_flag=true;
                        }
                        if(in_array(intval($key),$this->sev_districts)){
                            $sev_flag=true;
                        }
                        $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => $key));
                    }
                }
                if($flag){
                    $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => '8000000000'));
                }
                if($crim_flag){
                    $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => '0100000000'));
                }
                if($sev_flag){
                    $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => '8500000000'));
                }
            }
        }
    }

    public function update_master($source)
    {
        $_id = $source['masters'];
        if (isset($_id) && $_id != NULL && $_id != '') {
            if (isset($source['regions'])) {
                foreach ($source['regions'] as $key => $active) {
                    if ($active == 'on') {
                        $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => $key));
                    }
                }
            } elseif (isset($source['areas'])) {
                foreach ($source['areas'] as $key => $active) {
                    if ($active == 'on') {
                        $this->db->insert($this->table_masters_to_regions, array('master_id' => $_id, 'region_id' => $key));
                    }
                }
            }
        }
    }

    public function remove_master_image($id=0){
        if($id==0){
            return false;
        } else {
            $this->db->delete($this->table_master_images,array('master_id'=>$id));
            $_url = 'images/avatars/'.$id;
            $FolderWork = new FolderWork($_url);
            $FolderWork->chek_and_make_path();
            $FolderWork->clear();
            $FolderWork->del();
            return true;
        }
    }

    public function get_master_between_dates($date_start=false,$date_end=false)
    {
        $this->db->from('masters');
        if($date_start!=false){
            $this->db->where('masters.date >=',$date_start);
        } 
        if($date_end!=false){
            $this->db->where('masters.date <=',$date_end);
            if($date_start==false){
                $this->db->or_where('masters.date is NULL');
            }
        }
        $this->db->order_by('date', "desc");
        $masters=$this->db->get()->result_array();
        return $masters;
    }

    public function get_comments_between_dates($date_start=false,$date_end=false)
    {
        $this->db->from('comments');
        if($date_start!=false){
            $this->db->where('comments.date >=',$date_start);
        }
        if($date_end!=false){
            $this->db->where('comments.date <=',$date_end);
        }
        
        $masters=$this->db->get()->result_array();
        return $masters;
    }

    public function remove($remove){
        $this->db->where_in('master_id',$remove)->delete($this->table_masters_to_regions);
        $this->db->where_in('id',$remove)->delete($this->table_name);
    }
}