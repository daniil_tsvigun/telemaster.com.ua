<?php

class Region_model extends MY_Model {

    protected $table_districts_to_area='districts_to_area';
    protected $table_area='areas';
    protected $table_districts='districts';

    //new
    protected $table_admter_level1='admter_level1';
    protected $table_admter_level2='admter_level2';
    protected $table_admter_level3='admter_level3';
    protected $table_admter_level4='admter_level4';
    protected $table_admter_object='admter_object';

    protected $table_type='type';


    public function __construct() {
        parent::__construct();
    }

    public function get_all_areas_v2(){
        $this->db->from($this->table_area);
        $this->db->order_by('area_name'.SQL_LANG,"asc");
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_all_areas(){
        $this->db->select('
        admter_level1.level1_id as id,
        admter_level1.level1_name_ua as area_name_ua,
        admter_level1.level1_name_ru as area_name_ru,
        admter_level1.level1_name_en as area_name_en,
        KOATUU,
        type
        ');
        $this->db->from($this->table_admter_level1);
        //$this->db->where('admter_level1.level1_id !=',85);
        $this->db->order_by('level1_name'.SQL_LANG,"asc");

        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_by_district_koatuu($areas_id=array()){
        $this->db->select('*');
        $this->db->from($this->table_admter_object);
        foreach($areas_id as $area_id){
            $this->db->or_where('District_KOATUU',$area_id);
        }
        $this->db->order_by('RusName',"asc");
        $this->db->where('(Type =6 OR Type=7 OR Type=8)');
        //$result = $this->db->get_compiled_select();
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_regions_by_ids_from_admterobj($ids=array(),$pagination=false){
        if(!empty($ids)){
            if($pagination){
                $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
                $this->db->order_by(str_replace("-"," ",$pagination['sort']));
            }

            $this->db->select(
                $this->table_admter_object.'.KOATUU as district_id,
            RusName as district_place_ru,
            UkrName as district_place_ua,
            EngName as district_place_en,
            Oblast_KOATUU,
            District_KOATUU,
            '.$this->table_admter_object.'.Type
            ');
            $this->db->from($this->table_admter_object);

            foreach($ids as $id){
                $this->db->or_where(array($this->table_admter_object.'.KOATUU'=>intval($id)));
            }
            $region_info=$this->db->get()->result_object();

        } else {
            $region_info=array();
        }

        return $region_info;
    }

    public function get_districts_by_area_id($areas_id=array()){
        $this->db->select('
        level2_id as id,
        level2_id as district_id,
        level2_name_ru as district_place_ru,
        level2_name_ua as district_place_ua,
        level2_name_en as district_place_en,'.$this->table_admter_level2.'.KOATUU,
        '.$this->table_admter_level2.'.type');
        $this->db->from($this->table_admter_level2);
        $this->db->join($this->table_admter_level1,$this->table_admter_level2.'.level2_level1_id='.$this->table_admter_level1.'.level1_id','inner');
        $this->db->order_by('level2_name'.SQL_LANG,"asc");
        foreach($areas_id as $area_id){
            $this->db->or_where('level2_level1_id',$area_id);
        }
        //$this->db->where($this->table_admter_level2.'.type=','3');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_districts_by_area_id_2($areas_id=array()){
//        $this->db->select('
//        level2_id as id,
//        level2_id as district_id,
//        level2_name_ru as district_place_ru,
//        level2_name_ua as district_place_ua,
//        level2_name_en as district_place_en,'.$this->table_admter_level2.'.KOATUU,
//        '.$this->table_admter_level2.'.type');
        $this->db->from($this->table_admter_object);
        if(SQL_LANG=='_ua'){
            $this->db->order_by('UkrName',"asc");
        } elseif(SQL_LANG=='_ru'){
            $this->db->order_by('RusName',"asc");
        } else{
            $this->db->order_by('EngName',"asc");
        }
        foreach($areas_id as $area_id){
            $this->db->or_where('Oblast_KOATUU',$area_id);
        }

        $this->db->where('('.$this->table_admter_object.'.Type=3 OR '.$this->table_admter_object.'.Type=4 OR '.$this->table_admter_object.'.Type=6 )');
        $this->db->where('District_KOATUU',NULL);
        $this->db->limit(40);
        $result = $this->db->get()->result_array();
        return $result;
    }


    public function get_all_regions($pagination=false){
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }
        $this->db->select('
        districts_to_area.id,
        districts.id as district_id,
        areas.id as area_id,
        districts.district_place_ru,
        areas.area_name_ru,
        districts.district_place_ua,
        areas.area_name_ua,
        districts.district_place_en,
        areas.area_name_en');
        $this->db->from($this->table_districts_to_area);
        $this->db->join($this->table_districts,$this->table_districts.'.id='.$this->table_districts_to_area.'.district_id','inner');
        $this->db->join($this->table_area,$this->table_area.'.id='.$this->table_districts_to_area.'.area_id','inner');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_region_by_id_v2($id,$pagination=false){
        $this->db->order_by('district_place'.SQL_LANG,"asc");
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }
        $this->db->select('
        districts_to_area.id,
        districts.id as district_id,
        areas.id as area_id,
        districts.district_place_ru,
        areas.area_name_ru,
        districts.district_place_ua,
        areas.area_name_ua,
        districts.district_place_en,
        areas.area_name_en');
        $this->db->from($this->table_districts_to_area);
        $this->db->join($this->table_districts,$this->table_districts.'.id='.$this->table_districts_to_area.'.district_id','inner');
        $this->db->join($this->table_area,$this->table_area.'.id='.$this->table_districts_to_area.'.area_id','inner');
        $this->db->where(array($this->table_districts_to_area.'.id'=>$id));
        $region_info=$this->db->get()->row();
        return $region_info;
    }

    public function get_region_by_id($id,$pagination=false){
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }
        $this->db->select(
        $this->table_admter_level2.'.KOATUU as district_id,
        level2_name_ru as district_place_ru,
        level2_name_ua as district_place_ua,
        level2_name_en as district_place_en,
        level1_name_ua as area_name_ua,
        level1_name_ru as area_name_ru,
        level1_name_en as area_name_en,
        '.$this->table_admter_level2.'.type
        ');

        $this->db->from($this->table_admter_level2);
        $this->db->join($this->table_admter_level1,$this->table_admter_level2.'.level2_level1_id='.$this->table_admter_level1.'.level1_id','inner');
        $this->db->where(array($this->table_admter_level2.'.KOATUU'=>$id));
        $region_info=$this->db->get()->row();

        return $region_info;
    }

    public function get_regions_by_ids($ids=array(),$pagination=false){
        if(!empty($ids)){
            if($pagination){
                $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
                $this->db->order_by(str_replace("-"," ",$pagination['sort']));
            }

            $this->db->select(
                $this->table_admter_level2.'.KOATUU as district_id,
            level2_name_ru as district_place_ru,
            level2_name_ua as district_place_ua,
            level2_name_en as district_place_en,
            level1_name_ua as area_name_ua,
            level1_name_ru as area_name_ru,
            level1_name_en as area_name_en,
            '.$this->table_admter_level2.'.type
            ');
            $this->db->from($this->table_admter_level2);
            $this->db->join($this->table_admter_level1,$this->table_admter_level2.'.level2_level1_id='.$this->table_admter_level1.'.level1_id','inner');
            foreach($ids as $id){
                $this->db->or_where(array($this->table_admter_level2.'.KOATUU'=>intval($id)));
            }
            $region_info=$this->db->get()->result_object();
            //var_dump($this->db->last_query());
        } else {
            $region_info=array();
        }
        return $region_info;
    }

    public function search_region_by_name_v2($name,$pagination=false){
        $this->db->order_by('district_place'.SQL_LANG,"asc");
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }
        $this->db->select('
        districts_to_area.id,
        districts.id as district_id,
        areas.id as area_id,
        districts.district_place_ru,
        areas.area_name_ru,
        districts.district_place_ua,
        areas.area_name_ua,
        districts.district_place_en,
        areas.area_name_en');
        $this->db->from($this->table_districts_to_area);
        $this->db->join($this->table_area,$this->table_area.'.id='.$this->table_districts_to_area.'.area_id','inner');
        $this->db->join($this->table_districts,$this->table_districts.'.id='.$this->table_districts_to_area.'.district_id','inner');
        $this->db->like('district_place'.SQL_LANG, $name,'after');
        $this->db->or_like('area_name'.SQL_LANG, $name,'after');
        $this->db->limit(10);
        $result=$this->db->get()->result_array();
        return $result;
    }

    public function search_region_by_name_old($name,$pagination=false){
        $this->db->order_by('level2_name'.SQL_LANG,"asc");
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }

        $this->db->from($this->table_admter_level1);
        $this->db->join($this->table_admter_level2,$this->table_admter_level1.'.level1_id='.$this->table_admter_level2.'.level2_level1_id','left');
        //$this->db->join($this->table_admter_level3,$this->table_admter_level2.'.level2_id='.$this->table_admter_level3.'.level3_level2_id','left');
        //$this->db->join($this->table_admter_level4,$this->table_admter_level2.'.level2_id='.$this->table_admter_level4.'.level4_level2_id','left');

//        $this->db->where($this->table_admter_level2.'.type !=','3');
//        $this->db->where($this->table_admter_level3.'.type !=','4');
//        $this->db->where($this->table_admter_level3.'.type !=','5');
        //$this->db->where($this->table_admter_level4.'.type !=','5');

        //$this->db->like('level3_name'.SQL_LANG, $name,'after');

        $this->db->limit(10);
        $result=$this->db->get()->result_array();
        return $result;
    }

    public function search_region_by_name($name,$pagination=false){
        if(SQL_LANG=='_ua'){
            $this->db->order_by('UkrName',"asc");
            $this->db->like('UkrName', $name,'after');
            $this->db->or_like('RusName', $name,'after');
        } elseif(SQL_LANG=='_ru'){
            $this->db->order_by('RusName',"asc");
            $this->db->like('RusName', $name,'after');
            $this->db->or_like('UkrName', $name,'after');
        } else{
            $this->db->order_by('EngName',"asc");
            $this->db->like('EngName', $name,'after');
        }
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }

        $this->db->select('
            ID,
            ParentID,
            '. $this->table_admter_object .'.KOATUU,
            UkrName as object_name_ua,
            RusName as object_name_ru,
            EngName as object_name_en,
            District_KOATUU,
            Oblast_KOATUU,
            '. $this->table_admter_object .'.Type as object_type,
            AdminCenterID,
            level2_name_ua,
            level2_name_ru,
            level2_name_en,
            level1_name_ua,
            level1_name_ru,
            level1_name_en,
            '. $this->table_admter_level2 .'.type as level2_type,
        ');
        $this->db->from($this->table_admter_object);
        $this->db->join($this->table_admter_level1,$this->table_admter_level1.'.KOATUU='.$this->table_admter_object.'.Oblast_KOATUU','left');
        $this->db->join($this->table_admter_level2,$this->table_admter_level2.'.KOATUU='.$this->table_admter_object.'.District_KOATUU','left');
        //$this->db->join($this->table_admter_level3,$this->table_admter_level2.'.level2_id='.$this->table_admter_level3.'.level3_level2_id','left');
        //$this->db->join($this->table_admter_level4,$this->table_admter_level2.'.level2_id='.$this->table_admter_level4.'.level4_level2_id','left');
        // $this->db->where($this->table_admter_object.'.type !=','2');
        $this->db->where($this->table_admter_object.'.type !=','3');
        $this->db->where($this->table_admter_object.'.type !=','4');
        $this->db->where($this->table_admter_object.'.type !=','5');
        $this->db->limit(50);
        $result=$this->db->get()->result_array();
        return $result;
    }
}