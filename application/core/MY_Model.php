<?php

class MY_Model extends CI_Model {

	protected $table_name = '';
	protected $fields = [];
	protected $date_fields = [];
	protected $date_format = 'd.m.y H:i';
	protected $obj = null;

	public function __construct() {
		parent::__construct();
	}

    public function get_by_id($source_id){

        $res = $this->db->get_where($this->table_name,array('id'=>(int)$source_id));
        return ($res->num_rows() === 1) ? $res->row_array() : null;
    }

    public function get_by_alias($alias,$only_active=false){

        if(!alias_validate($alias)){
            return false;
        }

        if($only_active){
            $this->db->where(array('active'=>1));
        }

        $res = $this->db->get_where($this->table_name,array('alias'=>$alias));
        return ($res->num_rows() === 1) ? $res->row_array() : false;
    }

    public function create($source,$source_class){
        $data = new $source_class($source);
        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        return $_id;
    }

    public function update($source, $source_id,$source_class) {
        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            $data->id = $source_id;
            $this->db->update($this->table_name, $data, ['id' => $source_id]);
            return true;
        }
        return false;
    }

    public function change($name,$id,$changes){

        $this->db->where(array('id'=>$id))->update($this->table_name,array($name => $changes));
    }

    public function remove($remove){

        $this->db->where_in('id',$remove)->delete($this->table_name);
    }
}
