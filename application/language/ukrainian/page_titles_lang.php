<?php
$lang['page_title_control_panel'] = 'Панель керування';

$lang['page_title_roles_list'] = 'Ролі';
$lang['page_title_roles_edit'] = 'Редагування ролі';

$lang['page_title_users_administrative_list'] = 'Адміністратори';
$lang['page_title_users_administrative_edit'] = 'Редагування адміністратор';
$lang['page_title_users_buyer_list'] = 'Покупці';
$lang['page_title_users_buyer_edit'] = 'Редагування Покупець';
$lang['page_title_users_painter_list'] = 'Художники';
$lang['page_title_users_painter_edit'] = 'Редагування художник';

$lang['page_title_categories_list'] = 'Категорії товарів';
$lang['page_title_category_edit'] = 'Редагування категорії товарів';

$lang['page_title_filter_list'] = 'Фільтри/Характеристики товарів';
$lang['page_title_filter_edit'] = 'Редагування фільтрів/характеристик товарів';

$lang['page_title_filter_group_list'] = 'Групи фільтрів/фарактеристик товарів';
$lang['page_title_filter_group_edit'] = 'Редагування груп фільтрів/фарактеристик товарів';

$lang['page_title_menu_list'] = 'Меню';
$lang['page_title_menu_edit'] = 'Редагування меню';

$lang['page_title_menu_item_list'] = 'Пункти меню';
$lang['page_title_menu_item_edit'] = 'Редагування пунктів меню';

$lang['page_title_filemanager'] = 'Файловий менеджер';

$lang['page_title_blog_list'] = 'Записи';
$lang['page_title_blog_edit'] = 'Редагування запису';

$lang['page_title_page_list'] = 'Сторінки';
$lang['page_title_page_edit'] = 'Редагування сторінки';

$lang['page_title_tovar_list'] = 'Товари';
$lang['page_title_tovar_edit'] = 'Редагування товара';

$lang['page_title_order_list'] = 'Замовлення';
$lang['page_title_order_edit'] = 'Редагування замовлення';

$lang['page_title_product_list'] = 'Продукти (те що зберігається і сінхронізується з 1С)';
$lang['page_title_product_edit'] = 'Редагування продукта';

$lang['page_title_print_list'] = 'Принти';
$lang['page_title_print_edit'] = 'Редагування принта';

$lang['page_title_promo_list'] = 'Промо коды';
$lang['page_title_promo_edit'] = 'Редагування промо кода';

$lang['page_title_comment_list'] = 'Коментарі';
$lang['page_title_comment_edit'] = 'Редагування коментаря';

$lang['page_title_debit_list'] = 'Нарахування майстрам за продані товари з принтами';
$lang['page_title_debit_edit'] = 'Редагування нарахування майстру';

$lang['page_title_credit_list'] = 'Списання з рахунків майстрів';
$lang['page_title_credit_edit'] = 'Редагування спмсання';


$lang['page_title_'] = '';
$lang['page_title_'] = '';
$lang['page_title_'] = '';
$lang['page_title_partner_list'] = 'Провайдери';
$lang['page_title_partner_edit'] = 'Редагування провайдера';	