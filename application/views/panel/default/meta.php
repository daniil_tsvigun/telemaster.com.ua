<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
    <base href="http://www.telemaster.com.ua/" />
    <meta property="og:url" content="http://www.telemaster.com.ua/panel"/>
    <meta name="language" content="en_GB"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="google-site-verification" content="hKBgqXxU3jq1k0gSW5iUowze0kWy-DJNYLT3SjNqln8" />
    <link rel="shortcut icon" href="http://www.telemaster.com.ua/viasat.ico" type="image/x-icon" />
    <title><?php echo (isset($page_title))? $page_title:'Viasat';?></title>
    <meta property="og:title" content="Viasat Masters | dashboard"/>
    <meta name="title" content="Viasat Masters | dashboard" />
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.telemaster.com.ua/assets/img/logo-artlook.png"/>
    <link rel="image_src" href="http://www.telemaster.com.ua/assets/img/logo-artlook.png" />

    <!--fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7CPT+Sans+Narrow%7CSource+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--end fonts-->


    <!--stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.3.0/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/elfinder/css/elfinder.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/datetimepicker/jquery.datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/switcher/switcher.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/jQRangeSlider/css/iThing-min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.2.0/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/panel.css">
    <!--end stylesheets-->


    <!--scripts-->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script type="text/javascript" src="/assets/lib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.2.0/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="/assets/lib/elfinder/js/elfinder.min.js"></script>
    <script type="text/javascript" src="/assets/lib/elfinder/js/i18n/elfinder.ru.js"></script>
    <script type="text/javascript" src="/assets/lib/datetimepicker/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.0.8/jquery.browser.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/history.js/1.8/bundled-uncompressed/html4+html5/jquery.history.js"></script>
    <script type="text/javascript" src="/assets/lib/jQRangeSlider/jQDateRangeSlider-min.js"></script>
    <script type="text/javascript" src="/assets/lib/switcher/switcher.min.js"></script>
    <script type="text/javascript" src="/assets/js/helper.js"></script>
    <script type="text/javascript" src="/assets/js/panel.js"></script>
    <script type="text/javascript">
        var lang 		    = "<?php echo LANG; ?>";
        var lang_interface  = "<?php echo LANG; ?>";
        var lang_code 		= "<?php echo LANG; ?>";
        var base_url 		= "http://www.telemaster.com.ua/";
        var lang_url 		= "http://www.telemaster.com.ua/<?php echo LANG; ?>/";
        var current_url		= "http://www.telemaster.com.ua/<?php echo LANG; ?>/panel";
        var login 		    = "true";
        var sitename 		= "Telemaster";
        var coords_lat 		= 0;
        var coords_lng 		= 0;
        var default_el 		= "map_canvas";
    </script>
    <!--end scripts-->

</head>