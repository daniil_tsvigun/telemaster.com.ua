<?php
class Permission {
	var $id;
	var $name;
}

class Permission_model extends CI_Model {

	protected $table_name = 'permissions';

	public function __construct() {
		parent::__construct();
	}

	public function get_permission_by_name($name) {
		$q = $this->db->get_where($this->table_name, ['name' => $name]);
		if ($q->num_rows() !== 1) {
			error_log($name);
		}
		return ($q->num_rows() === 1) ? $q->row(): null;
	}

	public function get_permission_by_id($id) {

		$q = $this->db->get_where($this->table_name, ['id' => $id]);
		return ($q->num_rows() === 1) ? $q->row(): null;
	}

	public function get_all_permissions() {
		$q = $this->db->order_by('name')->get($this->table_name)->result_array();
		$result = [];
		foreach ($q as $permission) {
			$result[$permission->id] = $permission;
			$result[$permission->id]['description'] = $permission['description'. SQL_LANG];
		}
		return $result;
	}
    public function get_all_permissions_by_groups() {
		$q = $this->db->order_by('name')->get($this->table_name)->result_array();
		$result = [];
		foreach ($q as $permission) {
			$result[$permission['group']][$permission['id']] = $permission;
			$result[$permission['group']][$permission['id']]['description'] = $permission['description'. SQL_LANG];
		}
		return $result;
	}


}
