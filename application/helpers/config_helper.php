<?php

class Config_info {

    public $display_table_fields = array(
        'roles'=>array(
            'id'=>array(
                'sortable'=>'yes',
                'type'=>'text',
            ),
            'title'=>array(
                'sortable'=>'yes',
                'type'=>'edit_link',
            ),
        ),
        'users'=>array(
            'remove_checkbox'=>array(
                'sortable'=>'no',
                'type'=>'remove_checkbox',
            ),
            'id'=>array(
                'sortable'=>'yes',
                'type'=>'text',
            ),
            'name'=>array(
                'sortable'=>'yes',
                'type'=>'edit_link',
            ),
            'active'=>array(
                'sortable'=>'no',
                'type'=>'radio',
            ),
        ),
        'categories'=>array(
            'remove_checkbox'=>array(
                'sortable'=>'no',
                'type'=>'remove_checkbox',
            ),
            'id'=>array(
                'sortable'=>'yes',
                'type'=>'text',
            ),
            'title'=>array(
                'sortable'=>'yes',
                'type'=>'edit_link',
            ),
            'parent_category'=>array(
                'sortable'=>'yes',
                'type'=>'text',
            ),
            'active'=>array(
                'sortable'=>'no',
                'type'=>'radio',
            ),
        ),
        'filters'=>array(
            'filter'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'filter_group'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'title'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'alias'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'active'=>array(
                    'sortable'=>'no',
                    'type'=>'radio',
                ),
            ),
            'filter_group'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'title'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'alias'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'active'=>array(
                    'sortable'=>'no',
                    'type'=>'radio',
                ),
            ),
        ),
        'menus'=>array(
            'menu'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'title'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'alias'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'active'=>array(
                    'sortable'=>'no',
                    'type'=>'radio',
                ),
            ),
            'menu_item'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'title'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'menu'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'active'=>array(
                    'sortable'=>'no',
                    'type'=>'radio',
                ),
            ),
        ),
        'media' => array(),
        'pages' => array(
            'page'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'title'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'active'=>array(
                    'sortable'=>'no',
                    'type'=>'radio',
                ),
            ),
        ),
        'pictures' => array(
            'picture'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'title'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'confirm_status'=>array(
                    'sortable'=>'yes',
                    'type'=>'text_confirm_status',
                ),
                'active'=>array(
                    'sortable'=>'no',
                    'type'=>'radio',
                ),
            ),
        ),
        'products' => array(
            'product'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'title'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'alias'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'active'=>array(
                    'sortable'=>'no',
                    'type'=>'radio',
                ),
            ),
        ),
        'partners' => array(
            'partner'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'name'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'company'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'link'=>array(
                    'sortable'=>'yes',
                    'type'=>'link',
                ),
                'phone'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'active'=>array(
                    'sortable'=>'yes',
                    'type'=>'radio',
                ),
            ),
        ),
        'masters' => array(
            'master'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'surname'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'id_master'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'email'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'active'=>array(
                    'sortable'=>'yes',
                    'type'=>'radio',
                ),
            ),
        ),
        'comments' => array(
            'comment'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'text'=>array(
                    'sortable'=>'no',
                    'type'=>'text',
                ),
                'author'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'user_id'=>array(
                    'sortable'=>'no',
                    'type'=>'social_link',
                ),
                'master'=>array(
                    'sortable'=>'yes',
                    'type'=>'text',
                ),
                'date'=>array(
                    'sortable'=>'yes',
                    'type'=>'date',
                ),
                'active'=>array(
                    'sortable'=>'yes',
                    'type'=>'radio',
                ),
            ),
        ),
        'finances' => array(
            'credit'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'painter_name'=>array(
                    'sortable'=>'no',
                    'type'=>'link_painter',
                ),
                'admin_name'=>array(
                    'sortable'=>'no',
                    'type'=>'link_admin',
                ),
                'value'=>array(
                    'sortable'=>'no',
                    'type'=>'edit_link',
                ),
                'date'=>array(
                    'sortable'=>'no',
                    'type'=>'date',
                ),
                'pay_status'=>array(
                    'sortable'=>'no',
                    'type'=>'pay_status',
                ),

            ),
            'debit'=>array(
                'remove_checkbox'=>array(
                    'sortable'=>'no',
                    'type'=>'remove_checkbox',
                ),
                'id'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'order_id'=>array(
                    'sortable'=>'yes',
                    'type'=>'link_order',
                ),
                'tovar_title'=>array(
                    'sortable'=>'yes',
                    'type'=>'link_tovar',
                ),
                'painter_name'=>array(
                    'sortable'=>'yes',
                    'type'=>'link_painter',
                ),
                'value'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit',
                ),
                'date'=>array(
                    'sortable'=>'yes',
                    'type'=>'edit_link',
                ),
                'pay_status'=>array(
                    'sortable'=>'no',
                    'type'=>'pay_status',
                ),
            ),
        ),

    );

    public $validation_rules = array(
        'users' => array(
            'administrative' => array(
                array(
                    'field' => 'name',
                    'label' => 'lang:label_name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'email',
                    'label' => 'lang:label_email',
                    'rules' => array('required','callback__is_email','is_unique[buyer_users.email]','is_unique[painter_users.email]','is_unique[administrative_users.email]'),
                ),
                array(
                    'field' => 'mobile_phone',
                    'label' => 'lang:label_mobile_phone',
                    'rules' => array('required','callback__is_phone','is_unique[administrative_users.mobile_phone]','is_unique[painter_users.mobile_phone]','is_unique[administrative_users.mobile_phone]'),
                ),
            ),
            'buyer' => array(
                array(
                    'field' => 'name',
                    'label' => 'lang:label_name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'email',
                    'label' => 'lang:label_email',
                    'rules' => array('required','callback__is_email','is_unique[buyer_users.email]','is_unique[painter_users.email]','is_unique[administrative_users.email]'),
                ),
                array(
                    'field' => 'mobile_phone',
                    'label' => 'lang:label_mobile_phone',
                    'rules' => array('required','callback__is_phone','is_unique[administrative_users.mobile_phone]','is_unique[painter_users.mobile_phone]','is_unique[administrative_users.mobile_phone]'),
                ),
            ),
            'painter' => array(
                array(
                    'field' => 'name',
                    'label' => 'lang:label_name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'email',
                    'label' => 'lang:label_email',
                    'rules' => array('required','callback__is_email','is_unique[buyer_users.email]','is_unique[painter_users.email]','is_unique[administrative_users.email]'),
                ),
                array(
                    'field' => 'mobile_phone',
                    'label' => 'lang:label_mobile_phone',
                    'rules' => array('required','callback__is_phone','is_unique[administrative_users.mobile_phone]','is_unique[painter_users.mobile_phone]','is_unique[administrative_users.mobile_phone]'),
                ),
            ),
        ),
        'roles' => array(),
        'categories' => array(
            array(
                'field' => 'alias',
                'label' => 'lang:label_alias',
                'rules' => array('required','is_unique[filters.alias]')
            ),
            array(
                'field' => 'title_ua',
                'label' => 'lang:label_title',
                'rules' => array('required','is_unique[filters.title_ua]')
            ),
            array(
                'field' => 'title_ru',
                'label' => 'lang:label_title',
                'rules' => array('required','is_unique[filters.title_ru]')
            ),
            array(
                'field' => 'description_ua',
                'label' => 'lang:label_description',
                'rules' => 'min_length[10]'
            ),
            array(
                'field' => 'description_ru',
                'label' => 'lang:label_description',
                'rules' => 'min_length[10]'
            ),
        ),
        'filters' => array(
            'filter' => array(
                array(
                    'field' => 'title_ru',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[filters.title_ru]')
                ),
                array(
                    'field' => 'title_ua',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[filters.title_ua]')
                ),
                array(
                    'field' => 'alias',
                    'label' => 'lang:label_alias',
                    'rules' => array('required','is_unique[filters.alias]')
                ),

            ),
            'filter_group' => array(
                array(
                    'field' => 'title_ru',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[filters.title_ru]')
                ),
                array(
                    'field' => 'title_ua',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[filters.title_ua]')
                ),
                array(
                    'field' => 'alias',
                    'label' => 'lang:label_alias',
                    'rules' => array('required','is_unique[filters.alias]')
                ),
                array(
                    'field' => 'category[]',
                    'label' => 'lang:label_parent_filter_category',
                    'rules' => 'required',
                ),

            ),
        ),
        'menus' => array(
            'menu' => array(
                array(
                    'field' => 'alias',
                    'label' => 'lang:label_alias',
                    'rules' => array('required','is_unique[menus.alias]')
                ),
                array(
                    'field' => 'title',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[menus.title]')
                ),


            ),
            'menu_item' => array(
                array(
                    'field' => 'title',
                    'label' => 'lang:label_title',
                    'rules' => array('required')
                ),
            ),
        ),
        'media' => array(),
        'pages' => array(
            'page'=>array(
                array(
                    'field' => 'alias',
                    'label' => 'lang:label_alias',
                    'rules' => array('required','is_unique[pages.alias]')
                ),
                array(
                    'field' => 'title_ua',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[pages.title_ua]')
                ),
                array(
                    'field' => 'title_ru',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[pages.title_ru]')
                ),
                array(
                    'field' => 'content_ua',
                    'label' => 'lang:label_description',
                    'rules' => 'min_length[10]'
                ),
                array(
                    'field' => 'content_ru',
                    'label' => 'lang:label_description',
                    'rules' => 'min_length[10]'
                ),
            ),
        ),
        'pictures' => array(
            'picture'=>array(
                array(
                    'field' => 'alias',
                    'label' => 'lang:label_alias',
                    'rules' => array('required','is_unique[pictures.alias]')
                ),
                array(
                    'field' => 'title_ua',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[pictures.title_ua]')
                ),
                array(
                    'field' => 'title_ru',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[pictures.title_ru]')
                ),
            ),
        ),
        'blogs' => array(
            'blog'=>array(
                array(
                    'field' => 'alias',
                    'label' => 'lang:label_alias',
                    'rules' => array('required','is_unique[blogs.alias]')
                ),
                array(
                    'field' => 'title_ua',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[blogs.title_ua]')
                ),
                array(
                    'field' => 'title_ru',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[blogs.title_ru]')
                ),
                array(
                    'field' => 'subtitle_ua',
                    'label' => 'lang:label_subtitle',
                    'rules' => 'min_length[10]'
                ),
                array(
                    'field' => 'subtitle_ru',
                    'label' => 'lang:label_subtitle',
                    'rules' => 'min_length[10]'
                ),
                array(
                    'field' => 'description_ua',
                    'label' => 'lang:label_description',
                    'rules' => 'min_length[10]'
                ),
                array(
                    'field' => 'description_ru',
                    'label' => 'lang:label_description',
                    'rules' => 'min_length[10]'
                ),
            ),
        ),
        'products' => array(
            'product'=>array(
                array(
                    'field' => 'alias',
                    'label' => 'lang:label_alias',
                    'rules' => array('required','is_unique[products.alias]')
                ),
                array(
                    'field' => 'article',
                    'label' => 'lang:label_article',
                    'rules' => array('required','is_unique[products.article]')
                ),
                array(
                    'field' => 'title_ua',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[products.title_ua]')
                ),
                array(
                    'field' => 'title_ru',
                    'label' => 'lang:label_title',
                    'rules' => array('required','is_unique[products.title_ru]')
                ),
                array(
                    'field' => 'description_ua',
                    'label' => 'lang:label_description',
                    'rules' => 'min_length[10]'
                ),
                array(
                    'field' => 'description_ru',
                    'label' => 'lang:label_description',
                    'rules' => 'min_length[10]'
                ),
            ),
        ),
        'masters' => array(
            'master'=>array(
                array(
                    'field' => 'email',
                    'label' => 'lang:label_email',
                    'rules' => array('required')
                ),
                array(
                    'field' => 'id_master',
                    'label' => 'lang:label_id',
                    //'rules' => array('required','is_unique[masters.id_master]')
                ),
                array(
                    'field' => 'name',
                    'label' => 'lang:label_name',
                    //'rules' => array('is_unique[masters.name]')
                ),
                array(
                    'field' => 'surname',
                    'label' => 'lang:label_surname',
                    //'rules' => array('is_unique[masters.surname]')
                ),

            ),
        ),
        'partners' => array(
            'partner'=>array(

                array(
                    'field' => 'name',
                    'label' => 'lang:label_name',
                    'rules' => array('required')
                ),

            ),
        ),
        'comments' => array(
            'comment'=>array(
                array(
                    'field' => 'text',
                    'label' => 'lang:label_text',
                    'rules' => array('required')
                )
            )
        ),

    );

    public function __construct($type) {
        $this->display_table_fields = $this->display_table_fields[$type];
        $this->validation_rules = $this->validation_rules[$type];
    }
}