<?php

class Like {

    public $tovar_id;
    public $print_id;
    public $user_id;
    public $role_id;

    public function __construct($source = null) {
        $this->tovar_id     = isset($source['tovar_id'])?  $source['tovar_id'] :0;
        $this->print_id     = isset($source['print_id'])?  $source['print_id'] :0;
        $this->user_id      = isset($source['user_id'])?   $source['user_id']  :0;
        $this->role_id      = isset($source['role_id'])?   $source['role_id']  :0;

    }
}

class Like_model extends MY_Model {

    protected $table_name = 'likes';
    protected $t_tovars = 'tovars';
    protected $t_pictures = 'pictures';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_liked_by_user($user){
        $data = array(
            'likes'=>0,
            'tovars'=>array(),
            'prints'=>array()
        );

        $res = $this->db->get_where($this->table_name,array('user_id'=> $user->id, 'role_id'=> $user->role_id));
        if($res->num_rows()>0){
            foreach($res->result_array() as $item){
                $data['tovars'][] = $item['tovar_id'];
                $data['prints'][] = $item['print_id'];
            }
            $data['likes'] = count($data['tovars']);
        }

        return $data;

    }

    public function get_like_for_user($user){

        if($user->role_id != 4){
            return 0;
        }

        $res = $this->db->select('COUNT(*) as count')
                        ->from($this->table_name)
                        ->where('print_id IN(SELECT id FROM '. $this->t_pictures .' WHERE master_id = '. $user->id .')')
                        ->get();

        $count =($res->num_rows()>0)? $res->row()->count:0;

        return $count;

    }

    public function get_like_tovars($category_id,$user){
        $categories_ids = get_all_child(array(0=>$category_id),'categories');
        $_res = $this->db->select($this->table_name .'.tovar_id as id')
            ->from($this->table_name)
            ->where($this->table_name .'.user_id ='. $user->id .' AND '. $this->table_name .'.role_id ='. $user->role_id)
            ->where($this->t_tovars .'.category_id IN('. implode(",",$categories_ids) .')')
            ->join($this->t_tovars,$this->t_tovars.'.id = '. $this->table_name .'.tovar_id')
            ->get();

        $_tovar = array();
        if($_res->num_rows() == 0){
            return $_tovar;
        }

        foreach($_res->result_array() as $item){
            $_tovar[] = $item['id'];

        }

        return $_tovar;
    }

    public function set_like_by_tovar($source){

        $_res = $this->tovar_model->get_by_id($source['tovar_id']);
        if(!$_res){
            return false;
        }
        $source['print_id'] = $_res['picture_id'];
        $data = new Like($source);
        $_res = $this->db->get_where($this->table_name,array('tovar_id'=>$data->tovar_id,'user_id'=>$data->user_id,'role_id'=>$data->role_id));
        if($_res->num_rows() > 0){
            return false;
        }
        $this->db->insert($this->table_name,$data);
        return true;
    }

    public function del_like_by_tovar($source){
        $data = new Like($source);
        $this->db->where(array('user_id'=>$data->user_id,'role_id'=>$data->role_id,'tovar_id'=>$data->tovar_id))->delete($this->table_name);
    }


}