<?php
/**
 * New Post library.
 *
 * @version     1.0
 * @license     http://mit-license.org/
 * @author      Tapakan https://github.com/Tapakan
 * @coder       Alexander Oganov <t_tapak@yahoo.com>
 */

/**
 * Class New_Post
 */
class MeestExpress
{
    /**
     * Login
     *
     * @var string
     */
    private $user;

    /**
     * Password
     *
     * @var string
     */
    private $password;

    /**
     * Client UUID
     *
     * @var string
     */
    private $uuid;

    /**
     * Default language
     *
     * @var string
     */
    private $language;

    const UKRAINE_UUID     = 'C35B6195-4EA3-11DE-8591-001D600938F8';
    CONST API_URI          = 'http://api1c.meest-group.com/services/1C_Query.php';
    const API_URI_FORMING  = 'http://api1c.meest-group.com/services/1C_Document.php';
    const DEFAULT_LANGUAGE = 'UA';

    /**
     * New_Post constructor.
     *
     * @param array $options Default options
     *
     * @throws InvalidArgumentException
     */
    public function __construct(array $options)
    {
        if (!array_key_exists('user', $options) || !array_key_exists('password', $options) || !array_key_exists('uuid', $options)) {
            throw new InvalidArgumentException('Parameters user, password, uuid is required');
        }

        if (!array_key_exists('language', $options)) {
            $options['language'] = self::DEFAULT_LANGUAGE;
        }

        $this->user     = $options['user'];
        $this->password = $options['password'];
        $this->uuid     = $options['uuid']; // Test uuid '8458f0b0-930f-11e2-a91e-003048d2b473';
        $this->language = $options['language'];
    }

    /**
     * Return array of regions
     *
     * @return array
     */
    public function get_area_list()
    {
        $list   = [
            '' => LANG('cart_shipping_meestexpress_region_default_value')
        ];
        $result = $this->generate('Region', 'Countryuuid = "' . self::UKRAINE_UUID . '"');
        $result = $this->call($result);
        if (!empty($result)) {
            array_shift($result); // Remove Crimea
            foreach ($result as $item) {
                $list[$item['uuid']] = $item['DescriptionUA'];
            }
        }

        return $list;
    }

    /**
     * @param $area
     *
     * @return array
     */
    public function get_district_list($area)
    {
        $list   = [
            '' => LANG('cart_shipping_meestexpress_district_default_value')
        ];
        $result = $this->generate('District', 'Regionuuid = "' . $area . '"');
        $result = $this->call($result);

        if (!empty($result)) {
            foreach ($result as $item) {
                $list[$item['uuid']] = $item['DescriptionUA'];
            }
        }

        return $list;
    }

    /**
     * Return city list
     *
     * @param string $district District UUID
     *
     * @return array
     */
    public function get_city_list($district)
    {
        $list = [
            '' => LANG('cart_shipping_meestexpress_city_default_value')
        ];

        $result = $this->generate('City', 'Districtuuid = "' . $district . '"');
        $result = $this->call($result);

        if (!empty($result)) {
            foreach ($result as $item) {
                $list[$item['uuid']] = $item['DescriptionUA'];
            }
        }

        return $list;
    }

    /**
     * Return warehouses for city
     *
     * @param  string $city City UUID
     *
     * @return array
     */
    public function get_warehouse_list($city)
    {
        $list   = [
            '' => LANG('cart_shipping_meestexpress_warehouse_default_value')
        ];
        $result = $this->generate('Branch', 'Cityuuid = "' . $city . '"');
        $result = $this->call($result);

        if (!empty($result)) {
            foreach ($result as $item) {
                $list[$item['UUID']] = $item['DescriptionUA'];
            }
        }

        return $list;
    }

    /**
     * @param $recipient
     * @param $type
     * @param $weight
     * @param $cost
     *
     * @return mixed
     */
    public function get_price($recipient, $type, $weight, $cost)
    {
        $request
            = '<CalculateShipment>
                            <ClientUID>' . $this->uuid . '</ClientUID>
                            <SenderService>0</SenderService>
                            <SenderBranch_UID>D14CF17E-4269-11E4-81A4-003048D2B473</SenderBranch_UID>
                            <SenderCity_UID/>
                            <ReceiverService>' . $type . '</ReceiverService>
                            <ReceiverBranch_UID/>
                            <ReceiverCity_UID>' . $recipient . '</ReceiverCity_UID>
                            <COD>300</COD>
                             
                            <Conditions_items>
                                <ContitionsName></ContitionsName>
                            </Conditions_items>
                            <Places_items>
                                <SendingFormat>PAX</SendingFormat>
                                <Quantity>1</Quantity>
                                <Weight>' . $weight . '</Weight>
                                <Length>50</Length>
                                <Width>50</Width>
                                <Height>80</Height>
                                <Packaging/>
                                <Insurance>' . $cost . '</Insurance>
                            </Places_items>
                </CalculateShipment>';
        $xml
            = '<?xml version="1.0" encoding="UTF-8"?>
                <param>
                    <login>' . $this->user . '</login>
                    <function>CalculateShipments</function>
                <sign>' . $this->getHash('CalculateShipments' . $request) . '</sign>
                <request>
                ' . $request . '
                </request>
                </param>';

        $result = $this->call($xml, self::API_URI_FORMING);

        return $result;
    }

    /**
     * @param        $function
     * @param string $where
     * @param string $order
     * @param string $additional
     *
     * @return string
     */
    private function generate($function, $where = '', $order = '', $additional = '')
    {
        $sign = $this->getHash($function . $where . $order);

        $xml
            = '<?xml version="1.0" encoding="utf-8"?>
                    <param>
                        <login>' . $this->user . '</login>
                        <function>' . $function . '</function>
                        <where>' . $where . '</where>
                        <order>' . $order . '</order>
                        <sign>' . $sign . '</sign>
                        ' . $additional . '
                    </param>';

        return $xml;
    }

    /**
     * @param  string $data
     *
     * @return string
     */
    private function getHash($data)
    {
        return md5($this->user . $this->password . $data);
    }

    /**
     * @param  array $data
     *
     * @param string $uri
     *
     * @return mixed
     */
    private function call($data, $uri = self::API_URI)
    {
        $result = [];
        $ch     = curl_init($uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/xml']);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);
        curl_close($ch);

        $response = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
        $response = json_decode(json_encode($response), true);

        if (isset($response['result_table']) && !empty($response['result_table'])) {
            $result = $response['result_table']['items'];
        }

        return $result;
    }
}
