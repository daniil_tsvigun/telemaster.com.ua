<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                <div class="row">
                    <div class="col-md-4">
                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                        <p>
                            <span><?php echo LANG('label_name');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->name;?>" name="name">
                        </p>
                        <p>
                            <span><?php echo LANG('label_profile_config_user_url');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->alias;?>" name="alias">
                        </p>
                        <p><span><?php echo LANG('label_profile_config_user_city');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->city;?>" name="city">
                        </p>
                        <p><span><?php echo LANG('label_profile_config_user_country');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->country;?>" name="country">
                        </p>
                        <p>
                            <span><?php echo LANG('label_email');?>/<?php echo LANG('label_login');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->email;?>" name="email">
                        </p>
                        <p><span><?php echo LANG('label_mobile_phone');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->mobile_phone;?>" name="mobile_phone">
                        </p>
                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_password');?></h5>
                        <p><?php echo LANG('label_enter_password');?></p>
                        <p>
                            <span><?php echo LANG('label_password');?></span>
                            <input type="password" placeholder="" class="input-block-level" value="" name="password">
                        </p>
                        <p>
                            <span><?php echo LANG('label_password_repeat');?></span>
                            <input type="password" placeholder="" class="input-block-level" value="" name="password_repeat">
                        </p>
                    </div>
                    <div class="col-md-8">
                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_social_links');?></h5>
                        <p>
                            <span><?php echo LANG('label_social_vk');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->vk_link;?>" name="vk_link">
                        </p>
                        <p>
                            <span><?php echo LANG('label_social_fb');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->fb_link;?>" name="fb_lnk">
                        </p>
                        <p>
                            <span><?php echo LANG('label_social_instagram');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->instagram_link;?>" name="instagram_link">
                        </p>
                        <p>
                            <span><?php echo LANG('label_social_tw');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->tw_link;?>" name="tw_link">
                        </p>
                        <p>
                            <span><?php echo LANG('label_social_behance');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->behance_link;?>" name="behance_link">
                        </p>
                        <p>
                            <span><?php echo LANG('label_social_description');?></span>
                            <input type="text" placeholder="" class="input-block-level" value="<?php echo $user->description;?>" name="description">
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>