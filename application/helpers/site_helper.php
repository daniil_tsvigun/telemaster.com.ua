<?php

if ( ! function_exists('make_month_period')) {
    function make_month_period($month){
        $time = strtotime($month);
        $data['start'] = date('Y-m',$time).'-01 00:00:00';
        $data['end'] = date('Y-m',$time) .'-'. date('t',$time) .' 23:59:59';
        return $data;
    }
}

if ( ! function_exists('get_filters')) {
    function get_filters(){
        $filters = (isset($_SESSION['all_filters']))?$_SESSION['all_filters']:false;

        if($filters !== false){
            return $filters;
        }

        $CI =& get_instance();
        $filters = $CI->filter_model->get_filters();
        $_SESSION['all_filters'] = $filters;
        return $filters;

    }
}

if ( ! function_exists('tree_from_arr')) {
    function tree_from_arr($arr, $parent_id = 0){

        $data = array();
        foreach ($arr as $row){

            if ($row['parent_id'] == $parent_id){

                $data[$row['id']] = $row;
                $data[$row['id']]['title'] = isset($row['title'. SQL_LANG])?$row['title'. SQL_LANG]:$row['title'] ;
                $data[$row['id']]['sub'] = tree_from_arr($arr, $row['id']);

            }
        }

        return $data;
    }
}

if ( ! function_exists('level_tree_from_arr')) {
    function level_tree_from_arr($arr, $parent_id = 0,$data=array(),$level=1){
        foreach ($arr as $row){

            if ($row['parent_id'] == $parent_id){

                $data[$row['id']] = $row;
                $data[$row['id']]['title'] = isset($row['title'. SQL_LANG])?$row['title'. SQL_LANG]:$row['title'] ;
                $data[$row['id']]['level'] = $level;
                $data = $data + level_tree_from_arr($arr, $row['id'],$data,$level+1);

            }
        }

        return $data;
    }
}if
( ! function_exists('sub_tree_from_arr')) {
    function sub_tree_from_arr($arr, $parent_id = 0){
        $data=array();
        foreach ($arr as $row){
            if ($row['parent_id'] == $parent_id){
                $data[$row['alias']] = $row;
                $data[$row['alias']]['title'] = isset($row['title'. SQL_LANG])?$row['title'. SQL_LANG]:$row['title'] ;
                $data[$row['alias']]['sub'] =  sub_tree_from_arr($arr, $row['id']);
            }
        }

        return $data;
    }
}

if ( ! function_exists('make_tree_separator')) {
    function make_tree_separator($level){

        if($level == 1){
            $str = '';
        }else{
            $str =  str_repeat('| ', ($level -1)) .'-- ';
        }

        return $str;
    }
}

if ( ! function_exists('alias_validate')) {
    function alias_validate($alias){

        if( ! preg_match( '/^[a-zA-Z][a-zA-Z0-9-_\.]{0,200}$/', $alias ) ) {
            return false;
        }

        return true;
    }
}

if ( ! function_exists('name_validate')) {
    function name_validate($name){

        if( ! preg_match( '/^[a-zA-Zа-яА-ЯёЁєЄїЇіІэЭ0-9-_ \.]{0,200}$/u', $name ) ) {
            return false;
        }

        return true;
    }
}
if ( ! function_exists('text_validate')) {
    function text_validate($text){

        if( ! preg_match( '/^[a-zA-Zа-яА-ЯёЁєЄїЇіІэЭ0-9-_ \.]{1,1024}$/u', $text ) ) {
            return false;
        }

        return true;
    }
}

if ( ! function_exists('log_problems')) {
    function log_problems($problem){

        $lines =  date("m-d H:i") .' '. $_SERVER['REMOTE_ADDR'] .' '. $problem .' '. PHP_EOL;
        $handle = fopen(PROBLEM_LOG_FILE, 'ab');
        fwrite($handle, $lines);
        fclose($handle);

        return true;
    }
}

if ( ! function_exists('get_menu')) {
    function get_menu($name){
        $ci =& get_instance();

        $_menu = $ci->menu_model->get_menu_tree($name);
        return ($_menu)?refactor_menu($_menu):false;

    }
}

if ( ! function_exists('refactor_menu')) {
     function refactor_menu($data){
        $result = array();
        foreach($data as $item){
            if($item['parent_id'] == 0){
                $result[$item['id']] = isset($result[$item['id']])?array_merge($result[$item['id']],$item):$item;
            }else{
                $result[$item['parent_id']]['sub'][$item['id']] = $item;
            }
        }

        return $result;
    }
}

if ( ! function_exists('pagination_catalog')) {
    function pagination_catalog($tovar_ids,$page, $items_on_page, $href){

        if(!$tovar_ids){
            return '';
        }

        $CI =& get_instance();
        $CI->db->where_in('id', $tovar_ids);

        $num_rows = $CI->db->get('tovars')->num_rows();
        if($num_rows <= $items_on_page)
            return '';

        return $CI->load->view('frontend/default/pagination',array('pages'=>ceil($num_rows/$items_on_page),'page'=>$page,'page_link'=>$href),true);
    }
}

if ( ! function_exists('pagination_follow')) {
    function pagination_follow($user,$follow = 'following',$page, $items_on_page, $href){

        $CI =& get_instance();
        if($follow == 'following'){
            $CI->db->where(array('follower_id'=>$user->id, 'follower_role' => $user->role_id));
        }else{
            $CI->db->where(array('user_id'=>$user->id));
        }

        $num_rows = $CI->db->get('followers')->num_rows();
        if($num_rows <= $items_on_page)
            return '';

        return $CI->load->view('frontend/default/pagination',array('pages'=>ceil($num_rows/$items_on_page),'page'=>$page,'page_link'=>$href),true);
    }
}

if ( ! function_exists('pagination_pictures')) {
    function pagination_pictures($print_ids,$page, $items_on_page, $href){

        if(!$print_ids){
            return '';
        }

        $CI =& get_instance();
        $CI->db->where_in('id', $print_ids);

        $num_rows = $CI->db->get('pictures')->num_rows();
        if($num_rows <= $items_on_page)
            return '';

        return $CI->load->view('frontend/default/pagination',array('pages'=>ceil($num_rows/$items_on_page),'page'=>$page,'page_link'=>$href),true);
    }
}

if ( ! function_exists('get_tovar_templates_data')) {
    function get_templates_data(){
        $CI =& get_instance();

        $res = $CI->db->get('templates_tovar')->result_array();

        if(!$res){
            return array();
        }

        foreach($res as $item){
            $data[$item['id']] = $item;
        }
        return $data;
    }
}

if ( ! function_exists('get_templates_data_alias')) {
    function get_templates_data_alias(){
        $CI =& get_instance();

        $res = $CI->db->get('templates_tovar')->result_array();

        if(!$res){
            return array();
        }

        foreach($res as $item){
            $data[$item['template']] = $item;
        }
        return $data;
    }
}

if ( ! function_exists('make_unic_alias')) {
    function make_unic_alias($str){
        $str = $str .' '. str_replace('e', 'a', substr(sha1(time()), 0, 5));
        $str = trim($str);
        $str = strtolower($str);
        $str = str_replace(" ","-",$str);
        $str = str_replace("_","-",$str);
        return $str;

    }
}
if ( ! function_exists('make_unic_title')) {
    function make_unic_title($str){
        $str = trim($str);
        $str = str_replace("_","-",$str);
        return $str;
    }
}
if ( ! function_exists('get_all_child')) {
    function get_all_child($parent_ids,$table){
        $CI =& get_instance();
        $res = $CI->db->select('id')->from('categories')->where_in('parent_id',$parent_ids)->get()->result_array();
        if(!$res){
            return $parent_ids;
        }
        foreach($res as $ids){
            $new_prent_ids[] = $ids['id'];
        }

        return array_merge($parent_ids,get_all_child($new_prent_ids,$table));
    }
}


if ( ! function_exists('get_header_account')) {
    function get_header_account($user_data,$only_data = false){
        if($user_data->role_id == 1){
            return '';
        }
        $CI =& get_instance();
        if($user_data->role_id != 3 && $user_data->role_id == 4){
            $_str_id = strval($user_data->id);
            $_position = intval($_str_id[0]);
            if($_position != 1){
                $_avatar_id = $_position;
                $_bg_id = ceil($_position/2);
            }else{
                $_position = (isset($_str_id[1]))?intval($_str_id[0].$_str_id[1]):intval($_str_id[0].'0');
                if($_position > 12){
                    $_avatar_id = $_position -12;
                    $_bg_id = ceil(($_position - 12)/2);
                }else{
                    $_avatar_id = $_position;
                    $_bg_id = ceil($_position/2);
                }
            }
            $_path = ($user_data->role_id == 3)?'buyers':'painters';

            if(file_exists('images/'. $_path .'/'. $user_data->id .'/bg/'. $user_data->id .'.png')){
                 $user_data->bg  = '/images/'. $_path .'/'. $user_data->id .'/bg/'. $user_data->id .'.png';
            }else{
                $user_data->bg = '/images/default/bg/'. $_bg_id .'.jpg';
            }
            if(file_exists('images/'. $_path .'/'. $user_data->id .'/avatar/'. $user_data->id .'.png')){
                 $user_data->avatar  = '/images/'. $_path .'/'. $user_data->id .'/avatar/'. $user_data->id .'.png';
            }else{
                $user_data->avatar = '/images/default/avatar/'. $_avatar_id .'.jpg';
            }
        }else{
            $user_data->bg = '/images/default/bg/'. ceil(date('d')/5) .'.jpg';
            $user_data->avatar = '/images/default/avatar/'. ceil(date('d')/3) .'.jpg';
        }

        if($only_data){
            return $user_data;
        }

        return $CI->load->view('frontend/default/header_account',array('user_data'=>$user_data),true);
    }
}

if (!function_exists('translit')){
    function translit($str){
        $tr = array("Ї" => "I", "Є" => "E", "І" => "I", "ї" => "i", "є" => "e", "і" => "i", " " => "-", "," => "", "." => "",  "`" => "", "(" => "", ")" => "", "'" => "-", "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D", "Е" => "E", "Ё" => "E", "Ж" => "J", "З" => "z", "?" => "I", "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SC", "Ъ" => "", "Ы" => "Y", "Ь" => "", "Э" => "E", "Ю" => "U", "Я" => "YA", "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "j", "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "sc", "ъ" => "y", "ы" => "y", "ь" => "", "э" => "e", "ю" => "u", "я" => "ya");
        return strtr(trim($str), $tr);
    }
}