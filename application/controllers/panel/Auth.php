<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user(true);

    }

    public function index()
    {

    }

    public function login(){

        if(has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel');
        }

        $data['email'] = ($this->input->post('email'))? $this->input->post('email'):'';
        $data['password'] = ($this->input->post('password'))?$this->input->post('password'):'';


        if($data['email'] && $data['password']){

            if(!data_is_email($data['email'])){
                add_system_message("danger", LANG("ntf_npt_auth_no_combo_title"), LANG("ntf_npt_auth_no_combo_exist"));
            }else{
                $user = auth_user($data['email'],$data['password']);
                if($user){
                    $_SESSION['user_admin'] = $user;
                    redirect(site_url(). '/panel');
                }
            }
        }

        $data['auth_message'] = get_system_messages();

        $this->load->view('panel/default/login',$data);

    }

    public function logout(){
        unset($_SESSION['user']);
        redirect(site_url() .'/panel/auth/login');
    }

}
