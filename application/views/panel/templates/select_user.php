<input type="hidden" name="role_id" value = "<?php echo $data->role_id;?>"/>
<select  name="<?php echo $name;?>">
    <?php foreach($select_data as $title => $user_group):?>
        <optgroup label="<?php echo LANG('label_user_'. $title);?>">
            <?php foreach($user_group as  $item):?>
                <option value ="<?php echo $item['id']?>" <?php echo ($item['id']== $selected && $item['role_id'] == $data->role_id)? 'selected="selected"':'';?>><?php echo $item['email']?></option>
            <?php endforeach;?>
        </optgroup>
    <?php endforeach;?>
</select>