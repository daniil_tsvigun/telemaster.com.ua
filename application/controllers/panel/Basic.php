<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basic extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user(true);

        if(!has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel/auth/login');
        }

    }

    public function index()
    {

        $data['page_title'] = 'Dashboard';

        $data['dashboard_menu'] = $this->panel_model->dashboard();
        $data['content'] = 'panel/dashboard';
        $this->load->view('panel/main_layout',$data);
    }


}
