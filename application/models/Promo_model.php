<?php

class Promo {

    public $title;
    public $word;
    public $value;
    public $type;
    public $start_date;
    public $end_date	;
    public $one_time;

    public function __construct($source = null) {
        $this->title        = isset($source['title'])?       $source['title']       :'';
        $this->word         = isset($source['word'])?        $source['word']        :'';
        $this->value        = isset($source['value'])?       $source['value']       :'';
        $this->type         = isset($source['type'])?        $source['type']        :'';
        $this->start_date   = isset($source['start_date'])?  $source['start_date']  :'';
        $this->end_date     = isset($source['end_date'])?    $source['end_date']    :'';
        $this->one_time     = isset($source['one_time'])?    $source['one_time']    :1;
        $this->category     = isset($source['category'])?    $source['category']    :array();

    }
}

class Promo_model extends MY_Model {

    protected $table_name = 'promos';
    protected $t_promos_to_categories = 'promos_to_categories';
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;
    public $promo_type;
    public $onetime_list = null;

    public function __construct() {
        parent::__construct();
        $this->promo_type = array(
            array('id'=>'percent','title'=> LANG('label_percent')),
            array('id'=>'cashe','title'=> LANG('label_cashe'))
        );
        $this->onetime_list = array(
            array('id'=>'0','title'=> LANG('label_multi_time')),
            array('id'=>'1','title'=> LANG('label_one_time'))
        );
    }

    public function get_by_id($source_id){

        $res = $this->db->get_where($this->table_name,array('id'=>$source_id));

        if($res->num_rows() !== 1)
            return null;

        $data = $res->row_array();
        $res_category = $this->get_category_to_promo($data['id']);
        if(count($res_category) > 0){
            foreach($res_category as $item){
                $data['category'][$item['category_id']] = $item['category_id'];
            }
        }else{
            $data['category'] = array();
        }

        return $data;

    }

    public function get_all_promo($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->table_name .'.*')
            ->from($this->table_name)
            ->get()->result_array();

        return $result;
    }

    public function get_promo_by_word($word){
        if(!alias_validate($word)){
            return false;
        }
        $this->db->where("start_date < '". date('Y-m-d H:i:s') ."' AND end_date > '". date('Y-m-d H:i:s') ."'");
        $res = $this->db->get_where($this->table_name,array('active'=>1,'word'=>$word))->row_array();

        if(!$res){
            return false;
        }

        $res['categories'] = $this->get_promo_categories($res['id']);
        return $res;
    }

    public function get_promo_categories($promo_id){
        $res = $this->db->get_where($this->t_promos_to_categories,array('promo_id'=>$promo_id))->result_array();
        if(!$res){
            return array();
        }

        foreach($res as $item){
            $data[$item['category_id']] = $item['category_id'];
        }
        return $data;
    }

    public function get_category_to_promo($promo_id){

        $res = $this->db->get_where($this->t_promos_to_categories,array('promo_id'=>$promo_id));
        return ($res->num_rows()> 0) ? $res->result_array() : null;
    }

    public function create($source,$source_class){
        $data = new $source_class($source);
        $_category = $data->category;
        unset($data->category);
        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        if(count($_category) > 0){
            foreach($_category as $category_id){
                $_arr[$category_id]['category_id'] = $category_id;
                $_arr[$category_id]['promo_id'] = $_id;
            }
            $this->db->insert_batch($this->t_promos_to_categories,$_arr);
        }
    }

    public function update($source, $source_id,$source_class) {
        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            $_category = $data->category;
            unset($data->category);
            $this->db->update($this->table_name, $data, ['id' => $source_id]);

            $this->db->where(array('promo_id'=>$source_id))->delete($this->t_promos_to_categories);
            if(count($_category) > 0){
                foreach($_category as $category_id){
                    $_arr[$category_id]['category_id'] = $category_id;
                    $_arr[$category_id]['promo_id'] = $source_id;
                }

                $this->db->insert_batch($this->t_promos_to_categories,$_arr);
            }
            return true;
        }
        return false;
    }
}