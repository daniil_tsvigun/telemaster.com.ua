<?php

class Blog {

    public $alias;
    public $category_id;
    public $template_id;
    public $author_name;
    public $author_id;
    public $title_ru;
    public $subtitle_ru;
    public $description_ru;
    public $seo_title_ru;
    public $seo_keywords_ru	;
    public $seo_description_ru;
    public $title_ua;
    public $subtitle_ua;
    public $description_ua;
    public $seo_title_ua;
    public $seo_keywords_ua	;
    public $seo_description_ua;
    public $images;

    public function __construct($source = null) {
        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->category_id        = isset($source['category_id'])?        $source['category_id']       :0;
        $this->template_id        = isset($source['template_id'])?        $source['template_id']       :1;
        $this->author_name        = isset($source['author_name'])?        $source['author_name']       :0;
        $this->author_id          = isset($source['author_id'])?          $source['author_id']         :0;
        $this->title_ru           = isset($source['title_ru'])?           $source['title_ru']          :'';
        $this->subtitle_ru        = isset($source['subtitle_ru'])?        $source['subtitle_ru']       :'';
        $this->description_ru     = isset($source['description_ru'])?     $source['description_ru']    :'';
        $this->seo_title_ru       = isset($source['seo_title_ru'])?       $source['seo_title_ru']      :'';
        $this->seo_keywords_ru    = isset($source['seo_keywords_ru'])?    $source['seo_keywords_ru']   :'';
        $this->seo_description_ru = isset($source['seo_description_ru'])? $source['seo_description_ru']:'';
        $this->title_ua           = isset($source['title_ua'])?           $source['title_ua']          :'';
        $this->subtitle_ua        = isset($source['subtitle_ua'])?        $source['subtitle_ua']       :'';
        $this->description_ua     = isset($source['description_ua'])?     $source['description_ua']    :'';
        $this->seo_title_ua       = isset($source['seo_title_ua'])?       $source['seo_title_ua']      :'';
        $this->seo_keywords_ua    = isset($source['seo_keywords_ua'])?    $source['seo_keywords_ua']   :'';
        $this->seo_description_ua = isset($source['seo_description_ua'])? $source['seo_description_ua']:'';
        $this->images             = isset($source['images'])?             $source['images']            :array();
        
    }
}

class Blog_model extends MY_Model {

    protected $table_name = 'blogs';
    protected $fields = [];
    protected $templates = 'templates_blog';
    protected $t_blog_categories = 'blog_categories';
    protected $t_blogs_images = 'blogs_images';
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;
    private $blog_image = array(
        'orig'  => array('x'=>null,  'y'=>null,   'metod' => 1, 'options' => array('bg_color' => '255,255,255')),
        'type1' => array('x' => 295, 'y' => 570,  'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
        'type2' => array('x' => 375, 'y' => 275,  'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
        'type3' => array('x' => 325, 'y' => 220,  'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
        'type4' => array('x' => 325, 'y' => 330,  'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
        'type5' => array('x' => 360, 'y' => 240,  'metod' => 4, 'options' => array('bg_color' => '255,255,255')),
    );
    public function __construct() {
        parent::__construct();
        $this->load->library('FolderWork');
        $this->load->library('UploadImage');
    }

    public function get_all_blog($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }
        
        $result = $this->db->select($this->table_name .'.*, title'. SQL_LANG .' as title')->from($this->table_name)->get()->result_array();
        
        $res=array();
        for($i=0;$i<count($result);$i++){
            $author_id=$result[$i]['author_id'];
            $author_name = $this->db->select('name')->where(array('id'=>$author_id))->get('administrative_users')->result_array();
            //$this->db->where(array('blog_id'=>$result[0]['id']))->get('blogs_images')->result_array());

            $results=array(array_merge($result[$i],$author_name[0]));
            $results[$i]['images']=$this->db->where(array('blog_id'=>$result[0]['id']))->get('blogs_images')->result_array();
            array_push($res,$results[0]);
        }
       
        return $res;
    }


    public function get_blog_templates(){
        $result = $this->db->select($this->templates .'.*, title'. SQL_LANG .' as title')->from($this->templates)->get()->result_array();
        return $result;
    }

    public function get_blog_categories(){
        $result = $this->db->select($this->t_blog_categories .'.*, title'. SQL_LANG .' as title')->from($this->t_blog_categories)->get()->result_array();
        return $result;
    }

    public function create($source,$source_class){
        $data = new $source_class($source);
        unset($data->filters);
        $_images  = $data->images;
        unset($data->images);
        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        if(count($_images) > 0){
            $this->save_images_info($_images,$_id);
        }

        if(isset($_FILES) && !empty($_FILES)){
            $this->save_photos($_FILES,$_id);
        }
    }

    public function update($source, $source_id,$source_class) {
        //$_data=$this->blog_image_model->get_image_by_blog($source_id);
        $_data = $this->get_by_id($source_id);

        if ($_data) {
            
            //$_images=$this->blog_image_model->get_image_by_blog($_data['id']);
            
            $data = new $source_class($source);
            
            //$_images  = $data->images;  
            $_images=$this->blog_image_model->get_image_by_blog($source_id);
            unset($data->images);
            $this->db->update($this->table_name, $data, ['id' => $source_id]);
            
            if(count($_images) > 0){
                $this->save_images_info($_images,$source_id);
            }
            
            if(isset($_FILES) && !empty($_FILES)){
                if($data->template_id!=$_data['template_id']){
                    $t_change_state=true;
                } else{
                    $t_change_state=false;
                } 
                $this->save_photos($_FILES,$source_id,$source['template_id'],$t_change_state);
            }

            return true;
        }
        return false;
    }

    private function save_images_info($data,$blog_id){
        
        $res = $this->db->where(array('blog_id'=>$blog_id))->get($this->t_blogs_images)->result_array();
        
        $update_data = array();
        $delete_data = array();
        
        foreach($res as $image_info){
                
                if(in_array($image_info['id'],$data[0])){
                    $update_data[] = array(
                        'id'=>$image_info['id'],
                        'blog_id'=>$blog_id ,
                        //'title_ru'=>isset($data[$image_info['id']['title_ru']])?$data[$image_info['id']['title_ru']]:'',
                        //'title_ua'=>isset($data[$image_info['id']['title_ua']])?$data[$image_info['id']['title_ua']]:'',
                        //'main'=>isset($data[$image_info['id']['title_ru']])?1:0,
                        //'sort'=>isset($data[$image_info['id']['sort']])?$data[$image_info['id']['sort']]:'',
                    );
                }else{
                    $_url = 'images/blogs/'. $blog_id .'/'.$image_info['name'];
                    $FolderWork = new FolderWork($_url);
                    $FolderWork->chek_and_make_path();
                    $FolderWork->clear();
                    $FolderWork->del();
                    $delete_data[] = $image_info['id'];
                }
            }
           
        if(count($update_data) > 0){
            //var_dump($update_data);
            //$this->db->update_batch($this->t_blogs_images,$update_data);
        }
        
        if(count($delete_data) > 0){
            
            $this->db->where_in('id',$delete_data)->delete($this->t_blogs_images);
        }

    }

    private function save_photos($data,$blog_id,$template=false,$t_change_state=false){

        if($t_change_state){
            
        }
        $blog_image_templated=$this->blog_image;
        
        foreach($blog_image_templated as $key=>$val){
            if($key!='type'.$template&&$key!='orig'){
                unset($blog_image_templated[$key]);
            }
        }
        
        foreach($data as $up_key => $up_file){
            if(empty($up_file['name'])){
                continue;
            }

            $img_name = str_replace('e', 'a', substr(sha1(time() . $up_key), 0, 5));
            $_url = 'images/blogs/'. $blog_id .'/'.$img_name;
            
            $FolderWork = new FolderWork($_url);
            $FolderWork->chek_and_make_path();
            $FolderWork->clear();
            $UploadImage = new UploadImage;
            $error_msg = '';
            $UploadImage->do_action($up_file,'/'.$FolderWork->path .'/',NULL,NULL,NULL,'original_'.$img_name,90,1);
            
            foreach($this->blog_image as $key=>$val){
                $UploadImage->do_action($up_file ,'/'. $FolderWork->path .'/', $val['x'], $val['y'], 10000, $key.'_'.$img_name, 90, $val['metod']);
                
                if(is_array($val['options'])){

                    $res = $UploadImage->addOptions($val['options']);
                    if($res !== true){
                        $error_msg =  'Ошибка конфигурации постера: '. $res;
                        break;
                    }
                }

                $res = $UploadImage->save();
                if($res !== true){
                    $error_msg = 'Ошибка загрузки постера: '. $res;
                    break;
                }
            }
            if(empty($error_msg)){
                $this->db->insert($this->t_blogs_images, array(
                    'blog_id'=>$blog_id,
                    'name'=>$img_name,
                    'type'=>$UploadImage->format,
                ));


            }
        }


    }

}