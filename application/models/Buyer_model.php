<?php
class User_buyer extends User {
    public function __construct($source = null) {
        parent::__construct($source);
        $this->vk_link        = isset($source['vk_link'])       ?$source['vk_link']        :'';
        $this->instagram_link = isset($source['instagram_link'])?$source['instagram_link'] :'';
        $this->behance_link   = isset($source['behance_link'])  ?$source['behance_link']   :'';
        $this->fb_link        = isset($source['fb_link'])       ?$source['fb_link']        :'';
        $this->tw_link        = isset($source['tw_link'])       ?$source['tw_link']        :'';
        $this->description    = isset($source['description'])   ?$source['description']    :'';
        $this->country        = isset($source['country'])       ?$source['country']        :'';
        $this->city           = isset($source['city'])          ?$source['city']           :'';
        $this->role_id = 3;
    }
}
class Buyer_model extends User_model {

    protected $table_name = 'buyer_users';
    protected $save_path = 'buyers';

    public function __construct() {
        parent::__construct();
    }

}