<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">

                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                 <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <h5 class="widget-name"><i class="icon-picture"></i><?php echo $page_title;?></h5>
                        <div id="file-manager" class="well" style="height:850px !important;min-height:750px !important;"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>