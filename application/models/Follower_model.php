<?php

class Follower {

    public $user_id;
    public $follower_id;
    public $follower_role;


    public function __construct($source = null) {
        $this->user_id            = isset($source['user_id'])?         $source['user_id']       :0;
        $this->follower_id        = isset($source['follower_id'])?     $source['follower_id']   :0;
        $this->follower_role      = isset($source['follower_role'])?   $source['follower_role'] :0;

    }
}

class Follower_model extends MY_Model {

    protected $table_name = 'followers';
    protected $t_buyer_users = 'buyer_users';
    protected $t_painter_users = 'painter_users';
    protected $t_pictures = 'pictures';
    protected $t_likes = 'likes';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_follow_by_user($user){
        $data = array(
            'followers'=>0,
            'following'=>0,
            'users'=>array()
        );

        $_res_following = $this->db->select('user_id')->from($this->table_name)->where(array('follower_id'=> $user->id,'follower_role'=> $user->role_id))->get()->result();
        if($_res_following){
            foreach($_res_following as $item){
                $data['users'][$item->user_id] = $item->user_id;
            }
            $data['following'] = count($data['users']);
        }
        $_res_followers = $this->db->select('COUNT(follower_id) as followers')->from($this->table_name)->where('user_id ='. $user->id)->get()->row();
        if($_res_followers){
            $data['followers'] = $_res_followers->followers;
        }

        return $data;

    }

    public function get_following_by_user($user,$pagination = false){

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $this->db->order_by($_sort);
        }

        $_res = $this->db->select(
            'pain.*,
            (SELECT GROUP_CONCAT('. $this->t_pictures .'.id,\'|\', '. $this->t_pictures .'.preview_name ,\'|\', '. $this->t_pictures .'.preview_type ,\';\') FROM '. $this->t_pictures .' WHERE '. $this->t_pictures .'.master_id = pain.id LIMIT 3) AS preview,
            (SELECT COUNT(user_id) FROM '. $this->table_name .' WHERE follower_id = pain.id AND follower_role = pain.role_id) as following,
            (SELECT COUNT(follower_id) FROM '. $this->table_name .' WHERE user_id = pain.id) as followers,
            (SELECT COUNT(print_id) FROM '. $this->t_likes .' WHERE print_id IN(SELECT id FROM '. $this->t_pictures .' WHERE master_id = pain.id)) as likes',false

            )
            ->from($this->t_painter_users .' as pain')
            ->where('pain.id IN(SELECT user_id FROM '. $this->table_name .' WHERE follower_id = '. $user->id .' AND follower_role = '. $user->role_id .')')
            ->get()->result_array();

        return $_res;
    }

    public function get_followers_by_user($user,$pagination = false){
return array();
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $this->db->order_by($_sort);
        }

        $_res = $this->db->select(
            'pain.*,
            (SELECT COUNT(user_id) FROM '. $this->table_name .' WHERE follower_id = pain.id AND follower_role = pain.role_id) as following,
            (SELECT COUNT(follower_id) FROM '. $this->table_name .' WHERE user_id = pain.id) as followers,
            (SELECT COUNT(print_id) FROM '. $this->t_likes .' WHERE print_id IN(SELECT id FROM '. $this->t_pictures .' WHERE master_id = pain.id)) as likes',false

        )
            ->from($this->t_painter_users .' as pain')
            ->where('pain.id IN(SELECT user_id FROM '. $this->table_name .' WHERE follower_id = '. $user->id .' AND follower_role = '. $user->role_id .')')
            ->get()->result_array();

        return $_res;
    }


    public function set_follower($source){
        $data = new Follower($source);
        $this->db->insert($this->table_name, $data);
    }

    public function del_follower($source){
        $data = new Follower($source);
        $this->db->where(array('user_id'=>$data->user_id,'follower_id'=>$data->follower_id,'follower_role'=>$data->follower_role,))->delete($this->table_name);
    }

}