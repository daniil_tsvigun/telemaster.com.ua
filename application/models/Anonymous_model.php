<?php

class Anonymous_model extends User_model {

    protected $table_name = '';

    public function __construct() {
        parent::__construct();
    }

    //переопределяем метод родителя
    public function get_user($user_data,$model = false){

        $user_info['permissions'] = $this->role_model->get_role_permissions_by_id($user_data['role_id']);
        $user_info['approved'] = false;
        return new User($user_info);
    }

}