<?php

class Debit{
    public function __construct($source = null) {
        $this->order_id     = isset($source['order_id'])    ?$source['order_id']     :0;
        $this->tovar_id     = isset($source['tovar_id'])    ?$source['tovar_id']     :0;
        $this->painter_id   = isset($source['painter_id'])  ?$source['painter_id']   :0;
        $this->value        = isset($source['value'])       ?$source['value']        :0;
        $this->status       = isset($source['status'])      ?$source['status']       :0;
    }
}
class Credit{
    public function __construct($source = null) {
        $this->admin_id     = isset($source['admin_id'])    ?$source['admin_id']     :0;
        $this->painter_id   = isset($source['painter_id'])  ?$source['painter_id']   :0;
        $this->value        = isset($source['value'])       ?$source['value']        :0;
        $this->status       = isset($source['status'])      ?$source['status']       :0;
    }
}

class Payment_statistic_model extends CI_Model {

    protected $t_painter_credit = 'painter_credit';
    protected $t_painter_debit = 'painter_debit';
    protected $t_tovars = 'tovars';
    protected $t_pictures = 'pictures';
    protected $t_products = 'products';
    protected $t_painter_users = 'painter_users';
    protected $t_buyer_users = 'buyer_users';
    protected $t_administrative_users = 'administrative_users';
    protected $t_product_pay_limits = 'product_pay_limits';

    public function __construct() {
        parent::__construct();

    }

    public function get_debit_by_id($source_id){
        $res = $this->db->get_where($this->t_painter_debit,array('id'=>(int)$source_id));
        return ($res->num_rows() === 1) ? $res->row_array() : null;
    }

    public function get_credit_by_id($source_id){
        $res = $this->db->get_where($this->t_painter_credit,array('id'=>(int)$source_id));
        return ($res->num_rows() === 1) ? $res->row_array() : null;
    }


    public function create($source,$source_class,$type){
        $_table = ($type == 'debit')? $this->t_painter_debit:$this->t_painter_credit;
        $data = new $source_class($source);
        $this->db->insert($_table, $data);
        $_id = $this->db->insert_id();
        return $_id;
    }

    public function update($source, $source_id,$source_class,$type) {
        $_method = 'get_'. $type .'_by_id';
        $_data = $this->$_method($source_id);
        $_table = ($type == 'debit')? $this->t_painter_debit:$this->t_painter_credit;
        if ($_data) {
            $data = new $source_class($source);
            $data->id = $source_id;
            $this->db->update($_table, $data, ['id' => $source_id]);
            return true;
        }
        return false;
    }


    public function get_all_credit($pagination = false){

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->t_painter_credit .'.*,'. $this->t_painter_credit .'.status as pay_status,'.$this->t_painter_users.'.name as painter_name,'.$this->t_administrative_users.'.name as admin_name')
            ->from($this->t_painter_credit)
            ->join($this->t_administrative_users,$this->t_administrative_users.'.id = '. $this->t_painter_credit .'.admin_id','left')
            ->join($this->t_painter_users,$this->t_painter_users.'.id = '. $this->t_painter_credit .'.painter_id','left')
            ->get()
            ->result_array();

        return $result;
    }

    public function get_all_debit($pagination = false){

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->t_painter_debit .'.*,'. $this->t_painter_debit .'.status as pay_status,'.$this->t_tovars.'.title'. SQL_LANG.' as tovar_title,'.$this->t_painter_users.'.name as painter_name')
            ->from($this->t_painter_debit)
            ->join($this->t_tovars,$this->t_tovars.'.id = '. $this->t_painter_debit .'.tovar_id','left')
            ->join($this->t_painter_users,$this->t_painter_users.'.id = '. $this->t_painter_debit .'.painter_id','left')
            ->get()
            ->result_array();

        return $result;
    }

    public function get_first_debit_action($painter_id){
        $res = $this->db->order_by('date asc')->limit(1)->get_where($this->t_painter_debit,array('painter_id'=>$painter_id))->row();
        return $res;
    }

    public function get_painter_statistic_period($user){
        $date['end'] = date('Y-m-d');
        $_start = $this->get_first_debit_action($user->id);
        $date['start'] = ($_start)? date('Y-m-d',strtotime($_start->date)):$date['end'];
        return $date;
    }

    public function make_payment_debit($order_id,$payment_tovars){

        $_tovars = $this->get_painters_by_tovar($payment_tovars['tovars']);
        if($_tovars == false){
            log_problems('Ошибка при начислении средств мастеру по заказу id '. $order_id);
            return;
        }

        foreach($payment_tovars['tovars'] as $key => $cart_tovar){
            $data[$key]['order_id'] = $order_id;
            $data[$key]['tovar_id'] = $_tovars[$cart_tovar]['tovar_id'];
            $data[$key]['painter_id'] = $_tovars[$cart_tovar]['master_id'];
            $data[$key]['status'] = 0;
            if(empty($_tovars[$cart_tovar]['percent'])){
                $data[$key]['value'] = $payment_tovars['tovars_count'][$key] * ($_tovars[$cart_tovar]['product_price'] * DEFAULT_PAY_PERCENT/100);
            }else{
                $data[$key]['value'] = $payment_tovars['tovars_count'][$key] * ($_tovars[$cart_tovar]['product_price'] * $_tovars[$cart_tovar]['percent']/100);
            }
        }

        $this->db->insert_batch($this->t_painter_debit,$data);
    }

    private function get_painters_by_tovar($tovars){
        $res = $this->db->select(
                            $this->t_tovars.'.id as tovar_id,'.
                            $this->t_tovars.'.product_id,'.
                            $this->t_pictures.'.master_id,
                            (SELECT price FROM '. $this->t_products .' WHERE id = '. $this->t_tovars.'.product_id) as product_price,
                            IFNULL(
                                (SELECT percent FROM product_pay_limits WHERE product_id = '. $this->t_tovars .'. product_id AND bound < (SELECT SUM(value) FROM '. $this->t_painter_debit .' WHERE painter_id = '. $this->t_pictures.'.master_id AND status = 1) ORDER by bound desc LIMIT 1),
                                (SELECT percent FROM product_pay_limits WHERE product_id = '. $this->t_tovars .'. product_id ORDER by bound asc LIMIT 1)
                                ) as percent'
                          )
                         ->from($this->t_tovars)
                         ->where_in($this->t_tovars.'.id', $tovars)
                         ->join($this->t_pictures,$this->t_pictures.'.id ='. $this->t_tovars .'.picture_id ')
                         ->group_by($this->t_tovars.'.id')->get()->result_array();
        if(count($res) == 0){
            return false;
        }

        foreach($res as $item){
            $data[$item['tovar_id']] = $item;
        }

        return $data;
    }

    public function get_debit_by_period($period,$tovar_categories,$painter_id){

        $res  = $this->db->select($this->t_painter_debit .'.*')
                         ->from($this->t_painter_debit)
                         ->where($this->t_painter_debit .'.painter_id = '. $painter_id)
                         ->where($this->t_painter_debit .'.status = 1')
                         ->where($this->t_painter_debit.".date BETWEEN '". $period['start'] ."' AND '". $period['end'] ."'")
                         ->where($this->t_painter_debit.'.tovar_id IN(SELECT id FROM '. $this->t_tovars .' WHERE category_id IN('. implode(",",$tovar_categories).'))')
                         ->get()->result_array();

        return $res;
    }

    public function get_sold_data($painter_id){
        $res = $this->db->select(
                $this->t_painter_debit.'.*,'.
                $this->t_tovars.'.product_id,'.
                $this->t_pictures.'.title'. SQL_LANG .' as picture_title,'.
                'CONCAT (' .$this->t_pictures.'.preview_name,\'.\',' .$this->t_pictures.'.preview_type) as picture_preview,'.
                $this->t_pictures.'.id as picture_id,'.
                '(SELECT icon FROM '. $this->t_products .' WHERE id = '. $this->t_tovars.'.product_id) as product_icon'
            )
            ->from($this->t_painter_debit)
            ->where(array($this->t_painter_debit.'.painter_id'=> $painter_id,'status'=>1))
            ->join($this->t_tovars,$this->t_tovars.'.id ='. $this->t_painter_debit .'.tovar_id ')
            ->join($this->t_pictures,$this->t_pictures.'.id ='. $this->t_tovars .'.picture_id ')
            ->get()->result_array();

        if(!$res){
            return false;
        }
        $_sum = 0;
        $_count = 0;
        $data = array();
        foreach($res as $item){
            $_sum += $item['value'];
            $_count ++;
            if(!isset($data[$item['picture_id']])){
                $data[$item['picture_id']]['picture_preview'] =  $item['picture_preview'];
                $data[$item['picture_id']]['picture_title'] =  $item['picture_title'];
                $data[$item['picture_id']]['sum'] = $item['value'];
                $data[$item['picture_id']]['products'][$item['product_id']]['count'] = 1;
                $data[$item['picture_id']]['products'][$item['product_id']]['icon'] = $item['product_icon'];
            }else{
                $data[$item['picture_id']]['sum'] += $item['value'];
                if(isset($data[$item['picture_id']]['products'][$item['product_id']])){
                    $data[$item['picture_id']]['products'][$item['product_id']]['count']++;
                    $data[$item['picture_id']]['sum'] += $item['value'];
                }else{
                    $data[$item['picture_id']]['products'][$item['product_id']]['count'] = 1;
                    $data[$item['picture_id']]['products'][$item['product_id']]['icon'] = $item['product_icon'];
                }
            }

        }

        $result = array(
            'sold'=>$data,
            'all_sum'=>$_sum,
            'all_count'=>$_count,
        );

        return $result;
    }

    public function get_credit_sum($painter_id){
        $res = $this->db->select('SUM(value) as sum')->from($this->t_painter_credit)->where(array('painter_id'=>$painter_id,'status'=>1))->get()->row();

        if($res){
            return $res->sum;
        }else{
            return 0;
        }
    }
}