<?php

class Print_image_model extends CI_Model {

    protected $t_prints_images = 'pictures_images';

    public function __construct() {
        parent::__construct();
  
    }

    public function get_image_by_print($print_id = false){
        if(!$print_id){
            return array();
        }
        $result = $this->db->where(array('print_id'=>$print_id))->order_by('template_id')->get($this->t_prints_images)->result_array();
        return $result;
    }
}
