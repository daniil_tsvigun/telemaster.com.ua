<?php

class Menu_item {

    public $parent_id;
    public $menu_id;
    public $item_id;
    public $item_type;
    public $title;
    public $sort;

    public function __construct($source = null) {
        $this->menu_id            = isset($source['menu_id'])?            $source['menu_id']           :0;
        $this->parent_id          = isset($source['parent_id'])?          $source['parent_id']         :0;
        $this->item_id            = isset($source['item_id'])?            $source['item_id']           :0;
        $this->item_type          = isset($source['item_type'])?          $source['item_type']         :0;
        $this->title              = isset($source['title'])?              $source['title']             :'';
        $this->sort               = isset($source['sort'])?               $source['sort']              :0;
    }
}

class Menu_item_model extends MY_Model {

    protected $table_name = 'menu_items';
    protected $t_menu_item_type = 'menu_item_types';
    protected $t_menu = 'menus';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_all_menu_item($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->table_name .'.*, '. $this->t_menu .'.title  as menu')
                            ->from($this->table_name)
                            ->join($this->t_menu,$this->t_menu.'.id = '. $this->table_name .'.menu_id','left')
                            ->get()
                            ->result_array();

        return $result;
    }

    public function get_menu_item_by_menu($menu_id,$only_active = false){

        if($only_active){
            $this->db->where(array('active'=>1));
        }

        $res = $this->db->where(array('menu_id'=>$menu_id))->order_by('sort, parent_id')->get($this->table_name);

        return ($res->num_rows() > 0)? $res->result_array():false;

    }

    public function get_menu_item_type(){
        $_result = $this->db->select($this->t_menu_item_type .'.*, title'. SQL_LANG .' as title')
            ->from($this->t_menu_item_type)
            ->where(array('active'=>1))
            ->get()->result_array();
        if(!$_result){
            return array();
        }


        return $_result;
    }

    public function get_all_menu_type() {

        $_result = $this->get_menu_item_type();
        if(!$_result){
            return array();
        }

        foreach($_result as $item_type){
            if($item_type['item_table'] == 'categories'){
                $data[$item_type['id']] = $item_type;
                $data[$item_type['id']]['content'] = $this->db
                    ->select('c1.*,c1.title'. SQL_LANG .' as title, c_parent.title'. SQL_LANG .' as parent_title')
                    ->from('categories as c1')
                    ->join('categories as c_parent','c_parent.id = c1.parent_id','left')
                    ->where(array('c1.active'=>1))
                    ->get()->result_array();
            }else{
                $data[$item_type['id']] = $item_type;
                $data[$item_type['id']]['content'] = $this->db
                    ->select($item_type['item_table'] .'.*, title'. SQL_LANG .' as title')
                    ->from($item_type['item_table'])
                    ->where(array('active'=>1))
                    ->get()->result_array();
            }

        }

        return $data;
    }


}