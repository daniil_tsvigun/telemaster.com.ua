<?php

class Category {

    public $alias;
    public $parent_id;
    public $title_ru;
    public $description_ru;
    public $seo_title_ru;
    public $seo_keywords_ru	;
    public $seo_description_ru;
    public $title_ua;
    public $description_ua;
    public $seo_title_ua;
    public $seo_keywords_ua	;
    public $seo_description_ua;

    public function __construct($source = null) {
        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->parent_id          = isset($source['parent_id'])?          $source['parent_id']         :0;
        $this->template_id        = isset($source['template_id'])?        $source['template_id']       :0;
        $this->title_ru           = isset($source['title_ru'])?           $source['title_ru']          :'';
        $this->description_ru     = isset($source['description_ru'])?     $source['description_ru']    :'';
        $this->seo_title_ru       = isset($source['seo_title_ru'])?       $source['seo_title_ru']      :'';
        $this->seo_keywords_ru    = isset($source['seo_keywords_ru'])?    $source['seo_keywords_ru']   :'';
        $this->seo_description_ru = isset($source['seo_description_ru'])? $source['seo_description_ru']:'';
        $this->title_ua           = isset($source['title_ua'])?           $source['title_ua']          :'';
        $this->description_ua     = isset($source['description_ua'])?     $source['description_ua']    :'';
        $this->seo_title_ua       = isset($source['seo_title_ua'])?       $source['seo_title_ua']      :'';
        $this->seo_keywords_ua    = isset($source['seo_keywords_ua'])?    $source['seo_keywords_ua']   :'';
        $this->seo_description_ua = isset($source['seo_description_ua'])? $source['seo_description_ua']:'';
    }
}

class Category_model extends MY_Model {

    protected $table_name = 'categories';
    protected $templates_tovar = 'templates_tovar';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_all_categories($pagination = false,$only_on = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }

        if($only_on == true){
            $this->db->where(array('t1.active'=>1));
        }

        $result = $this->db->select(
                                't1.*, t1.title'. SQL_LANG .' as title,'.
                                '(SELECT title'. SQL_LANG .' FROM '. $this->table_name .' as t2 WHERE t2.id = t1.parent_id) as parent_category'
                            )
                           ->from($this->table_name .' as t1')
                           ->get()->result_array();

        return $result;
    }

    public function get_template_by_category($id){
        $result = $this->db->select('templates_tovar.*')
                           ->from($this->table_name)
                           ->where($this->table_name.'.id ='. $id)
                           ->join($this->templates_tovar,$this->templates_tovar.'.id = '. $this->table_name.'.template_id','left')
                           ->get()->row();

        return $result;
    }

    public function get_by_parent_id($parent_id){
        $this->db->where(array('t1.active'=>1,'t1.parent_id'=>$parent_id));
        $result = $this->db->select(
            't1.title'. SQL_LANG .' as title,'.
            't1.alias'
             )
            ->from($this->table_name .' as t1')
            ->get()->result_array();
        return $result;
    }

    public function get_by_alias($alias,$only_active=false){

        if(!alias_validate($alias)){
            return false;
        }

        if($only_active){
            $this->db->where(array('active'=>1));
        }
        $this->db->where(array('alias'=>$alias));
        $res = $this->db->select('*,title'. SQL_LANG .' as title')->from($this->table_name)->get();
        return ($res->num_rows() === 1) ? $res->row_array() : false;
    }

}