<?php
/**
 * New Post library.
 *
 * @version     1.0
 * @license     http://mit-license.org/
 * @author      Tapakan https://github.com/Tapakan
 * @coder       Alexander Oganov <t_tapak@yahoo.com>
 */

/**
 * Class New_Post
 */
class Novaposhta
{
    /**
     * Api key
     *
     * @var string
     */
    private $key;

    /**
     * Default language
     *
     * @var string
     */
    private $language;

    /**
     * @var Novaposhta_model
     */
    private $model;

    CONST API_URI          = 'https://api.novaposhta.ua/v2.0/json/ ';
    const DEFAULT_LANGUAGE = 'UA';
    const CITY_SENDER      = '8d5a980d-391c-11dd-90d9-001a92567626'; //CityId = 4, Киев

    /**
     * New_Post constructor.
     *
     * @param array $options Default options
     *
     * @throws InvalidArgumentException
     */
    public function __construct(array $options)
    {
        if (!array_key_exists('key', $options)) {
            throw new InvalidArgumentException('Required parameter key');
        }

        if (!array_key_exists('language', $options)) {
            $options['language'] = self::DEFAULT_LANGUAGE;
        }

        $ci = get_instance();
        $ci->load->model('Novaposhta_model');

        $this->model    = $ci->Novaposhta_model;
        $this->key      = $options['key'];
        $this->language = $options['language'];
    }

    /**
     * Return array of regions
     *
     * @return array
     */
    public function get_area_list()
    {
        $list = [
            '' => LANG('cart_shipping_novaposhta_region_default_value')
        ];
        $data = $this->call([
            'modelName'    => 'Address',
            'calledMethod' => 'getAreas'
        ]);

        if (isset($data['success'])) {
            array_shift($data['data']); // remove CRIMEA
            foreach ($data['data'] as $area) {
                $list[$area['Ref']] = $area['Description'];
            }
        }

        return $list;
    }

    /**
     * Returns city for area.
     *
     * @param string $area Area ref
     *
     * @return array
     */
    public function get_city_list($area)
    {
        $list   = [
            '' => LANG('cart_shipping_novaposhta_city_default_value')
        ];
        $cities = $this->model->get_cities_by_area($area);

        if ($cities) {
            foreach ($cities as $city) {
                $list[$city['Ref']] = $city['Description'];
            }
        }

        return $list;
    }

    /**
     * Return list of warehouses
     *
     * @param string $city
     *
     * @return array
     */
    public function get_warehouse_list($city)
    {
        $list = [
            '' => LANG('cart_shipping_novaposhta_warehouse_default_value')
        ];
        $data = $this->call([
            'modelName'        => 'Address',
            'calledMethod'     => 'getWarehouses',
            'methodProperties' => [
                'CityRef' => $city
            ]
        ]);

        if (isset($data['success'])) {
            foreach ($data['data'] as $area) {
                $list[$area['Ref']] = $area['Description'];
            }
        }

        return $list;
    }

    /**
     * Returns CityId by City Ref
     *
     * @param string $ref
     *
     * @return null
     */
    public function get_city_id($ref)
    {
        $cityId = null;
        if ($city = $this->model->get_city_by_ref($ref)) {
            $cityId = $city['CityId'];
        }

        return $cityId;
    }

    /**
     * @param string    $recipient
     * @param string    $type
     * @param float     $weight
     * @param float|int $cost
     *
     * @return mixed
     */
    public function get_price($recipient, $type, $weight, $cost)
    {
        $price  = 0;
        $result = $this->call([
            'modelName'        => 'InternetDocument',
            'calledMethod'     => 'getDocumentPrice',
            'methodProperties' => [
                'CitySender'    => self::CITY_SENDER,
                'CityRecipient' => $recipient,
                'ServiceType'   => $type,
                'Weight'        => $weight,
                'Cost'          => $cost
            ]
        ]);
        if(isset($result['success'])) {
            $price = $result['data']['0']['Cost'];
        }

        return $price;
    }

    /**
     * @param  array $data
     *
     * @return mixed
     */
    private function call($data)
    {
        $data['apiKey'] = $this->key;

        $ch = curl_init(self::API_URI);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }
}