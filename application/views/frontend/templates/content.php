<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div data-alias="<?php echo $alias; ?>" class="content single-instruction">
        <div class="wrapper">
            <div class="main-block">

                <h2><?php echo isset($title)?$title:''; ?></h2>
                <?php  ?>
                <div data-first="<?php
                if(isset($first_active)):

                        echo $first_active;

                endif; ?>" class="text">
                    <div>
                        <?php echo isset($content)?$content:''; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $this->load->view('frontend/default/footer'); ?>