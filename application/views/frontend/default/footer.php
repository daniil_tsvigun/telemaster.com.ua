<div class="hidden rate popup fadeIn animated">
    <div class="wrapper">
        <div class="wrap">
            <div class="box">
                <div class="close"></div>
                <div class="rate">
                    <div class="levels" data-choosen="null">
                        <ul>
                            <li class="" data-level_id="0" id="level0"><div></div></li>
                            <li class="" data-level_id="1" id="level1"><div></div></li>
                            <li class="" data-level_id="2" id="level2"><div></div></li>
                            <li class="" data-level_id="3" id="level3"><div></div></li>
                            <li class="" data-level_id="4" id="level4"><div></div></li>
                        </ul>
                    </div>
                    <div class="btn-box">
                        <div id="vote" class="btn btn-type3"><?php echo LANG('rate_master'); ?></div>
                    </div>
                    <div class="auth-box">
                        <div class="fb_auth"></div>
                        <div class="vk_auth"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer> 
    <?php if($this->uri->segment(1)=='registration'): ?>
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <a href="<?php echo site_url('about'); ?>"><?php echo LANG('label_about_us'); ?></a>
                <a href="<?php echo site_url('faq'); ?>"><?php echo LANG('label_faq'); ?></a>
                <a href="<?php echo site_url('rules'); ?>"><?php echo LANG('label_site_rules'); ?></a>
                <a href="<?php echo site_url('reports'); ?>"><?php echo LANG('label_reports_rules'); ?></a>
                <a href="<?php echo site_url('polices'); ?>"><?php echo LANG('label_privacy_policy'); ?></a>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 center">
                <div class="btns">
                    <div id="fb_btn">
                        <a target="_blank" href="https://www.facebook.com/groups/telemaster.com.ua/">fb</a>
                    </div>
                    <div id="vk_btn">
                        <a target="_blank" href="https://vk.com/club133152177">vk</a>
                    </div>
                    <div id="youtube_btn">
                        <a target="_blank" href="https://www.youtube.com/channel/UCUrsf864ut8p5UETF4Ym6Hw">&nbsp;</a>
                    </div>
                </div>
                <?php echo LANG('label_foot_email'); ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p><?php echo LANG('label_masters_coordinate'); ?></p>
                <p class="tel">050 480 82 82</p>
                <p class="tel">093 170 82 82</p>
                <p class="tel">067 011 82 82</p>
            </div>
        </div>
    <?php else: ?>
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <a href="<?php echo site_url('about'); ?>"><?php echo LANG('label_about_us'); ?></a>
                <a href="<?php echo site_url('faq'); ?>"><?php echo LANG('label_faq'); ?></a>
                <a href="<?php echo site_url('rules'); ?>"><?php echo LANG('label_site_rules'); ?></a>
                <a href="<?php echo site_url('reports'); ?>"><?php echo LANG('label_reports_rules'); ?></a>
                <a href="<?php echo site_url('polices'); ?>"><?php echo LANG('label_privacy_policy'); ?></a>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 center">
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p><?php echo LANG('label_site_admin'); ?></p>
                <p>info@telemaster.com.ua</p>
                <div class="btns special">
                    <div id="youtube_btn">
                        <a target="_blank" href="https://www.youtube.com/channel/UCUrsf864ut8p5UETF4Ym6Hw">&nbsp;</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="rights">
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <div class="logo"></div>
            </div>
            <div class="center col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <p>© 2016 All rights reserved</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p>
                    <a href="http://lemon.ua/"><?php echo LANG('developed_by'); ?> Lemon.ua</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="/assets/js/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/lib/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="/assets/lib/chosen/1.4.2/chosen.jquery.min.js"></script> -->
<script type="text/javascript" src="/assets/lib/animate.css/3.3.0/animate.min.css" ></script>
