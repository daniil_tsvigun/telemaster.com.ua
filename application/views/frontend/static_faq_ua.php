<!DOCTYPE html>
<html>
<head>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tele Master</title>
    <link href="/assets/lib/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/lib/animate.css/3.3.0/animate.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css?v=1.3.6"/>
    <script type="text/javascript">
        var site_url = "http://www.telemaster.com.ua/ua";
    </script>

</head>
<body>
<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
            Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a
        ,m)

    })(window,document,'script','https://www.google-analytics.com/analytics.js',
        'ga');

    ga('create', 'UA-88207792-1', 'auto');
    ga('send', 'pageview');

</script>

<div id="vk_api_transport"></div><div data-id="mobile_btn" class="lang-menu">
    <ul>
        <li class="current"><a href="http://www.telemaster.com.ua/ua/faq">Укр</a></li>
        <li class=""><a href="http://www.telemaster.com.ua/ru/faq">Рус</a></li>
    </ul>
</div>
<div data-id="mobile_btn" class="master ">
    <a class="" href="http://www.telemaster.com.ua/ua/registration">Реєстрація майстра</a>
</div>
<section class="nav_bar" >
    <div class="wrapper">
        <div class="menu">
            <a href="http://www.telemaster.com.ua/ua" class="logo-anchor"></a>
            <ul class="main-menu slideInDown animated">

                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua">Головна</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua/calling">Знайти майстра</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua/partners">Знайти провайдера</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ua/instructions">Інструкції</a>
                </li>
                <li class="master ">
                    <a class="" href="http://www.telemaster.com.ua/ua/registration">Реєстрація майстра</a>
                </li>
            </ul>
            <div id="menu-btn"></div>
            <div class="lang-menu">
                <ul>

                    <li class="current"><a href="http://www.telemaster.com.ua/ua/faq">Укр</a></li>
                    <li class=""><a href="http://www.telemaster.com.ua/ru/faq">Рус</a></li>
                </ul>
            </div>
            <div class="master ">
                <a class="" href="http://www.telemaster.com.ua/ua/registration">Реєстрація майстра</a>
            </div>
        </div>

    </div>
</section>    <div data-alias="faq" class="content single-instruction">
    <div class="wrapper">
        <div class="main-block">

            <h2>Питання та відповіді</h2>
            <div data-first="" class="text">
                <div>
                    <div class="faq">
                        <div id="parameters" class="question">
                            <div class="q">Нові параметри супутникового прийому для телеканалів</div>
                            <div class="answer">Параметри супутникового мовлення телеканалів <strong>1+1, ПлюсПлюс, Уніан, Бігуді, Music Box:</strong>
                                <ul>
                                    <li>cупутник - Astra 4A (SES Sirius). У деяких моделях тюнерів може називатися &laquo;Sirius 2/3&raquo; та &laquo;Sirius 4&raquo;;</li>
                                    <li>орбітальна позиція - 5&deg; сх. д.(E)(у деяких тюнерах може позначатись як 4.8&deg;);</li>
                                    <li>частота - 11766 МГц;</li>
                                    <li>символьна швидкість &ndash; 27,5 Мсимв/с (MSymb/s) або 27500 ксимв/с (ksymb/s);</li>
                                    <li>поляризація - горизонтальна (H);</li>
                                    <li>FEC - &frac34;;</li>
                                    <li>модуляція - DVB-S (QPSK).</li>
                                </ul>
                                <br /> Параметри супутникового мовлення телеканалів <strong>СТБ, Новий канал, ICTV, М1, М2, QTV, 24 канал:</strong>
                                <ul>
                                    <li>cупутник &ndash; AMOS-3. У деяких моделях тюнерів може називатися &laquo;AMOS&raquo;, &laquo;AMOS 1&raquo;, та &laquo;AMOS 2&raquo;;</li>
                                    <li>орбітальна позиція - 4&deg;W;</li>
                                    <li>частота &ndash; 11 176 МГц;</li>
                                    <li>символьна швидкість - 30,0 Мсимв/с (MSymb/s), або 30000 ксимв/с (ksymb/s);</li>
                                    <li>поляризація - горизонтальна (H);</li>
                                    <li>FEC - &frac34;;</li>
                                    <li>модуляція - DVB-S (QPSK).</li>
                                </ul>
                            </div>
                        </div>
                        <div class="question">
                            <div class="q">Після сканування з новими параметрами канал "1+1" з'явився у списку, але на ньому немає зображення</div>
                            <div class="answer">Замість частоти 11766 введіть частоту 11765 та проскануйте канали ще раз. У кінці списку каналів з'являться "1+1", "ТЕТ", "2+2", "Уніан", "Бігуді", "ПлюсПлюс", "Music Box" та ін. Після цього видаліть зі списку раніше додані телеканали і залиште щойно проскановані. На щойно доданому каналі "1+1" зображення повинно з'явитись</div>
                        </div>
                        <div class="question">
                            <div class="q">При скануванні транспондера не з'являється ще один канал "1+1" у списку телеканалів</div>
                            <div class="answer">Найімовірніше, канал "1+1" з'явився у списку радіоканалів. Видаліть його, будь-ласка (порядок дій - згідно інструкції з видалення телеканалів, тільки треба обрати радіоканали), та перескануйте транспондер ще раз. Після цього телеканал "1+1" повинен з'явитись у кінці списку телеканалів.</div>
                        </div>
                        <div class="question">
                            <div class="q">З якою метою створено telemaster.com.ua?</div>
                            <div class="answer">Телевізійні канали в Україні поширюються за допомогою різних технологій, зокрема через супутник. У парку супутників, які обслуговують територію України, в 2016 році очікуються зміни. Внаслідок цих змін параметри супутникового сигналу деяких українських каналів можуть змінитися.<br /> Якщо ви користуєтеся супутниковою антеною, при змінах параметрів супутникового сигналу каналів вам буде необхідно переналаштувати параметри їх прийому у вашому домашньому ТВ-тюнері.<br /> На сайті telemaster.com.ua будуть зібрані інструкції по налаштуванню ТВ-тюнерів. Ця інформація буде уточнюватися з телеканалами, які змінюють параметри супутникового сигналу, оновлюватися і поповнюватися на нашому сайті.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як користуватися сайтом?</div>
                            <div class="answer">На сайті telemaster.com.ua розміщуються інструкції телеканалів по налаштуванню нових параметрів мовлення на тюнерах супутникового ТБ. Інформація буде оновлюватися у вигляді текстових і відео інструкцій для телеглядачів. За допомогою інструкцій ви зможете переналаштувати ваш ТВ тюнер самостійно.<br /> Ті телеглядачі, які не зможуть переналаштувати свій тюнер самостійно, можуть потребувати допомоги майстра по супутниковому телебаченню для переналаштування тюнера і / або інших робіт з налагодження, ремонту своєї супутникової системи.<br /> Майстер, зареєстрований на сайті telemaster.com.ua, і його контактна інформація доступні відвідувачам сайту.</div>
                        </div>
                        <div class="question">
                            <div class="q">Які зміни відбуватимуться у найближчий час, як допоможе мені цей ресурс?</div>
                            <div class="answer">В найближчий час більш ніж 40 українських телеканалів змінюють параметри супутникового мовлення. На ресурсі викладені нові параметри мовлення та інструкції з налаштування телеканалів з новими параметрами супутникового мовлення.<br /> Крім того, у 2017р. заплановане повне відключення аналогового телевізійного мовлення (термін повного відключення до 30 червня 2017р.). Абонентам аналогового телевізійного мовлення доведеться вибирати альтернативні варіанти прийому та перегляду телевізійних програм. Наш ресурс допоможе у виборі.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як переналаштувати канали?</div>
                            <div class="answer">Прийом супутникового телебачення ведеться через тюнер. Тюнер &ndash; це невеликий пристрій, що під&rsquo;єднано до телевізора. На передній панелі тюнера знайдіть назву моделі вашого тюнера. Після цього знайдіть ваш тюнер у <a href="../../../../instructions/model/1">списку тюнерів</a>. Якщо вашого тюнера немає у списку, можливо, він належить до моделі, що не дуже поширена на ринку, або до застарілих моделей. У цьому випадку пропонуємо скористатись загальною інструкцією (<a href="../../../../../uploads/1t_%D0%BD%D0%B0%D0%BB%D0%B0%D1%88%D1%82%D1%83%D0%B2%D0%B0%D0%BD%D0%BD%D1%8F_%D1%82%D1%8E%D0%BD%D0%B5%D1%80%D0%B0_orton4100c.pdf">текст</a> / <a href="../../../../instructions/model/1">відео</a>), або звернутись по <a href="../../../../calling">допомогу до майстра</a>.</div>
                        </div>
                        <div class="question">
                            <div class="q">Я не знайшов інструкцію до свого тюнера, що робити?</div>
                            <div class="answer">Ми розмістили на сайті інструкції до найбільш поширених моделей тюнерів, що використовуються зараз в Україні. Можливо, ваш тюнер належить до моделі, що не дуже поширена на ринку, або до застарілих моделей. У цьому випадку пропонуємо скористатись загальною інструкцією (<a href="../../../../../uploads/1t_%D0%BD%D0%B0%D0%BB%D0%B0%D1%88%D1%82%D1%83%D0%B2%D0%B0%D0%BD%D0%BD%D1%8F_%D1%82%D1%8E%D0%BD%D0%B5%D1%80%D0%B0_orton4100c.pdf">текст</a> / <a href="../../../../instructions/model/1">відео</a>), або звернутись по <a href="../../../../calling">допомогу до майстра</a>.</div>
                        </div>
                        <div class="question">
                            <div class="q">У мене телевізор із вбудованим тюнером. Як його налаштувати?</div>
                            <div class="answer">У цьому випадку, меню налаштування тюнера вбудоване у меню телевізора. Скористайтесь, будь-ласка, інструкцією з експлуатації Вашого телевізора та знайдіть у меню відповідний розділ. Якщо ви вперше налаштовуєте нове (неналаштоване обладнання), то, щоб зрозуміти загальні принципи налаштування, використайте інформацію, наведену в універсальній інструкції з налаштування тюнерів (<a href="../../../../../uploads/1t_налаштування_тюнера_orton4100c.pdf" target="_blank">текст</a> / <a href="instructions/model/1">відео</a>). Якщо ви робите це вперше на вже налаштованому обладнанні - радимо звернутись по <a href="calling">допомогу до майстра</a>.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як мені знайти майстра?</div>
                            <div class="answer">Перейдіть до розділу &laquo;<a href="../../../../calling">Знайти майстра</a>&raquo;. У віконці пошуку введіть назву вашого населеного пункту. Натисніть &laquo;Знайти майстра&raquo; під віконцем пошуку. Ви перейшли на сторінку зі списком майстрів, що обслуговують ваш населений пункт. Передзвоніть до майстра, та домовтесь з ним про налаштування вашого тюнера. Послуга виїзду майстра платна. Плату за послугу встановлюють самі майстри, тож вартість послуги у різних майстрів може бути різна.</div>
                        </div>
                        <div class="question">
                            <div class="q">Мого міста немає у списку населених пунктів, як мені викликати майстра?</div>
                            <div class="answer">Ми намагаємось знайти майстрів у всіх районах України. Може статись, що у кількох нечисленних районах на даний момент немає майстрів (майстер у відпустці, хворіє, тощо). У такому випадку пропонуємо шукати майстрів у найближчих районах.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як викликати майстра?</div>
                            <div class="answer">Відповідь: Відвідувачі сайту звертаються безпосередньо до майстра за вказаними ним на сайті телефонами. У разі, якщо вам необхідна допомога майстра, зробіть наступне:
                                <ol>
                                    <li>перейдіть на сторінку &laquo;<a href="../../../../registration">Знайти майстра</a>&raquo; ;</li>
                                    <li>введіть назву вашого населеного пункту у вікні пошуку;</li>
                                    <li>виберіть майстра зі списку;</li>
                                    <li>зв'яжіться з майстром за вказанним номером телефону.</li>
                                </ol>
                            </div>
                        </div>
                        <div class="question">
                            <div class="q">Як скасувати / перенести виїзд майстра?</div>
                            <div class="answer">Зв'яжіться з майстром, з яким ви домовлялися про виїзд.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як оплатити замовлення?</div>
                            <div class="answer">Інформаційний ресурс telemaster.com.ua не регулює фінансові взаємовідносини між майстром і клієнтом. Оплата проводиться за домовленістю клієнта з майстром.</div>
                        </div>
                        <div class="question">
                            <div class="q">Що робити, якщо майстер не приїхав або виконав роботу неякісно?</div>
                            <div class="answer">Відповідь: Ви можете оцінити роботу майстра за допомогою рейтингу або відгуку на сторінці майстра. Сайт залишає за собою право модерації рейтингів і відгуків.</div>
                        </div>
                        <div class="question">
                            <div class="q">Я - майстер. Як користуватися сайтом?</div>
                            <div class="answer">Якщо у Вас є бажання надавати послуги з налаштування супутникового обладнання, або ви вже надаєте такі послуги, пропонуємо зареєструватись на нашому сайті.<br /> Майстер, зареєстрований на сайті telemaster.com.ua, і його контактна інформація будуть доступні відвідувачам сайту. Відвідувачі сайту звертаються до майстра безпосередньо за вказаними ним телефонами. <br />Telemaster.com.ua є інформаційною площадкою, за допомогою якої абонент супутникового прийому отримує інформацію щодо налаштування свого супутникового обладнання.</div>
                        </div>
                        <div class="question">
                            <div class="q">Яку кількість замовлень я отримаю?</div>
                            <div class="answer">Ми працюємо над просуванням telemaster.com.ua в регіонах України. Сайт не фільтрує замовлення, Ви отримаєте натуральний потік звернень користувачів.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як підвищити привабливість мого профілю?</div>
                            <div class="answer">При реєстрації на сайті вкажіть максимально точну інформацію про себе. Наявність фотографії в профілі майстра важлива для ваших клієнтів - так вони будуть впевнені, що майстер, який приїхав за викликом - саме той, до якого вони звернулися на сайті. У картці майстра є його рейтинг та відгуки про нього. Високий рейтинг і позитивні відгуки важливі для збільшення кількості замовлень.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як отримати високий рейтинг і позитивні відгуки?</div>
                            <div class="answer">Майстру необхідно якісно виконати замовлення. Після виконання замовлення попросіть клієнта оцінити вашу роботу на сайті telemaster.com.ua за допомогою виставлення рейтингу або написання відгуку про Вашу роботу.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як відмовитися від замовлення?</div>
                            <div class="answer">Будь ласка, повідомте клієнтові, що на даний момент Ви не можете виконати замовлення. Клієнт може звернутися до іншого майстра в вашому регіоні.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як я можу видалити / змінити свої контакти з сайту?</div>
                            <div class="answer">Зверніться до координаторів майстрів електронною поштою <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> або передзвоніть на Лінію Підтримки Майстрів (050 480 82 82, 093 170 82 82, 067 011 82 82).</div>
                        </div>
                        <div class="question">
                            <div class="q">Кому я можу поскаржитися / написати щодо роботи сайту?</div>
                            <div class="answer">Зверніться до координаторів майстрів електронною поштою <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> або передзвоніть на Лінію Підтримки Майстрів (050 480 82 82, 093 170 82 82, 067 011 82 82).</div>
                        </div>
                        <div class="question">
                            <div class="q">Як я можу розмістити мої контакти на сайті?</div>
                            <div class="answer">Для розміщення інформації про вас, як про майстра, необхідно зробити наступне:
                                <ol>
                                    <li>пройти реєстрацію на сайті в розділі &laquo;<a href="../../../../registration">Реєстрація майстра&raquo;.</a></li>
                                    <li>заповнити анкету, яку ви отримаєте електронною поштою;</li>
                                    <li>пройти сертифікацію і отримати ID номер Майстра;</li>
                                    <li>Реєстрація завершена, ваші контакти опубліковані на сайті.</li>
                                </ol>
                            </div>
                        </div>
                        <div class="question">
                            <div class="q">Я вже зареєстрований на сайті. Як можна додати свою фотографію?</div>
                            <div class="answer">Відправте, будь ласка, фото на email <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> <br /> Для ідентифікації обов'язково потрібно вказати ПІБ, номер телефону, який був вказний при реєстрації.</div>
                        </div>
                        <div class="question">
                            <div class="q">Я пройшов реєстрацію (відправив свою анкету) кілька днів тому. Коли мені зателефонують?</div>
                            <div class="answer">Внесення змін на сайт відбувається протягом 5 робочих днів. Команда Телемастер робитиме все можливе для того, щоб внести зміни в найкоротші терміни.</div>
                        </div>
                        <div class="question">
                            <div class="q">Як відбувається сертифікація?</div>
                            <div class="answer">З вами з зв'яжеться по телефону координатор майстрів. Він задасть вам ряд питань, щоб перевірити ваш досвід в роботі з системами супутникового телебачення.</div>
                        </div>
                        <div class="question">
                            <div class="q">Що таке ID?</div>
                            <div class="answer">Це особистий унікальний номер майстра на сайті telemaster.com.ua.</div>
                        </div>
                        <div class="question">
                            <div class="q">Чи можлива реєстрація юридичної особи в якості майстра?</div>
                            <div class="answer">Так. Зверніться до координаторів майстрів електронною поштою <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> або передзвоніть на Лінію Підтримки Майстрів.</div>
                        </div>
                        <div class="question">
                            <div class="q">Чи є правила для публікації відгуків?</div>
                            <div class="answer">Відгук про роботу майстра може залишити будь-який зареєстрований користувач сайту. Сайт залишає за собою право модерації відгуків. Якщо у майстра виникають питання, що стосуються відгуків, він може звернутися до координаторів майстрів електронною поштою за адресою <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> або передзвонити на Лінію Підтримки Майстрів.</div>
                        </div>
                        <div class="question">
                            <div class="q">Якщо отриманий незаслужений відгук, що робити?</div>
                            <div class="answer">Сайт залишає за собою право модерації відгуків. Якщо у майстра виникають питання, що стосуються відгуків, він може звернутися до координаторів майстрів електронною поштою за адресою <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> або передзвонити на Лінію Підтримки Майстрів.</div>
                        </div>
                        <div class="question">
                            <div class="q">Майстер самостійно встановлює ціну за послугу?</div>
                            <div class="answer">Так, вартість виїзду майстра і робіт на місці встановлюється майстром самостійно, але при цьому клієнт має право вибору і може запросити вартість виїзду і робіт у кількох майстрів в районі.</div>
                        </div>
                        <div class="question">
                            <div class="q">Навіщо додавати фото?</div>
                            <div class="answer">Наявність фотографії в профілі майстра важлива для клієнтів: це дає можливість клієнту побачити майстра перед візитом. Ваша фотографія додасть вам довіри потенційних клієнтів.</div>
                        </div>
                        <div class="question">
                            <div class="q">Які вимоги до фото?</div>
                            <div class="answer">Ми рекомендуємо розміщувати фото, аналогічне фотографіям для офіційних документів (паспорт, віза). Аватари, неякісні фото, малюнки і т.д., а також зображення, що містять рекламний зміст, розміщені не будуть.</div>
                        </div>
                        <div class="question">
                            <div class="q">Якщо клієнт відмовиться платити, хто буде відшкодовувати витрати?</div>
                            <div class="answer">telemaster.com.ua - інформаційний ресурс. Сайт не регулює вартість замовлень і не несе відповідальності по їх оплаті. Вартість замовлення, час виїзду і умови роботи майстру необхідно обговорювати безпосередньо з клієнтом.</div>
                        </div>
                        <div class="question">
                            <div class="q">Скільки я буду заробляти? / Є плата за розміщення моїх контактів на сайті?</div>
                            <div class="answer">Ми не беремо грошей за розміщення контактів майстрів на сайті і не встановлюємо ціни сервісних робіт. У клієнта є право звернутися до іншого майстра в вашому регіоні для порівняння вартості.</div>
                        </div>
                    </div>                    </div>
            </div>

        </div>
    </div>
</div>
<div class="hidden rate popup fadeIn animated">
    <div class="wrapper">
        <div class="wrap">
            <div class="box">
                <div class="close"></div>
                <div class="rate">
                    <div class="levels" data-choosen="null">
                        <ul>
                            <li class="" data-level_id="0" id="level0"><div></div></li>
                            <li class="" data-level_id="1" id="level1"><div></div></li>
                            <li class="" data-level_id="2" id="level2"><div></div></li>
                            <li class="" data-level_id="3" id="level3"><div></div></li>
                            <li class="" data-level_id="4" id="level4"><div></div></li>
                        </ul>
                    </div>
                    <div class="btn-box">
                        <div id="vote" class="btn btn-type3">Проголосувати</div>
                    </div>
                    <div class="auth-box">
                        <div class="fb_auth"></div>
                        <div class="vk_auth"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="wrapper">
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <a href="http://www.telemaster.com.ua/ua/about">Про нас</a>
            <a href="http://www.telemaster.com.ua/ua/faq">Питання та відповіді</a>
            <a href="http://www.telemaster.com.ua/ua/rules">Правила користування сайтом</a>
            <a href="http://www.telemaster.com.ua/ua/reports">Правила модерації відгуків</a>
            <a href="http://www.telemaster.com.ua/ua/polices">Політика конфіденційності</a>

        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 center">
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <p>Адміністратор сайту: </p>
            <p>info@telemaster.com.ua</p>
            <div class="btns special">
                <div id="youtube_btn">
                    <a target="_blank" href="https://www.youtube.com/channel/UCUrsf864ut8p5UETF4Ym6Hw">&nbsp;</a>
                </div>
            </div>
        </div>
    </div>
    <div class="rights">
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <div class="logo"></div>
            </div>
            <div class="center col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <p>© 2016 All rights reserved</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p>
                    <a href="http://lemon.ua/">Створення сайту  Lemon.ua</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="/assets/js/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/lib/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="/assets/lib/chosen/1.4.2/chosen.jquery.min.js"></script> -->
<script type="text/javascript" src="/assets/lib/animate.css/3.3.0/animate.min.css" ></script>
