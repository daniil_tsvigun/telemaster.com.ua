<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>

                <div class="tab-content">
                    <div id="general" class="tab-pane active" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 inside_tab">
                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_main_client_info');?></h5>
                                <p><span><? echo LANG ('label_client_surname');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->client_surname;?>" name="client_surname"> </p>
                                <p><span><? echo LANG ('label_client_name');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->client_name;?>" name="client_name"> </p>
                                <p><span><? echo LANG ('label_client_phone');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->client_phone;?>" name="client_phone"> </p>
                                <p><span><? echo LANG ('label_client_email');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->client_email;?>" name="client_email"> </p>
                                <p><span><? echo LANG ('label_payment_type');?></span>
                                    <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$this->order_model->payment_type,'name'=>'payment_type','selected'=>array($data->payment_type),'params' => array('no_all'=>'yes')));?>
                                </p>
                                <p><span><? echo LANG ('label_client_profile');?>:</span>
                                    <?php if($data->client_id):?>
                                        <a href="<?php echo site_url('panel/users/edit/buyer/'. $data->client_id);?>"><?php echo sprintf(LANG('label_client_reg'),$data->client_id);?></a>
                                    <?php else:?>
                                        <?php echo LANG('label_client_no_reg');?>
                                    <?php endif;?>
                                    <input type="hidden" name="client_id" value="<?php echo $data->client_id;?>">
                                </p>
                            </div>
                            <div class="col-md-6 inside_tab">
                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_delivery_info');?></h5>
                                <p><span><? echo LANG ('label_delivery_type');?></span>
                                    <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$this->order_model->delivery_type,'name'=>'delivery_type','selected'=>array($data->delivery_type),'params' => array('no_all'=>'yes')));?>
                                </p>
                                <p><span><? echo LANG ('label_delivery_area');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->delivery_area;?>" name="delivery_area"> </p>
                                <p><span><? echo LANG ('label_delivery_district');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->delivery_district;?>" name="delivery_district"> </p>
                                <p><span><? echo LANG ('label_delivery_city');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->delivery_city;?>" name="delivery_city"> </p>
                                <p><span><? echo LANG ('label_delivery_warehouse');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->delivery_warehouse;?>" name="delivery_warehouse"> </p>
                                <p><span><? echo LANG ('label_delivery_address');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->delivery_address;?>" name="delivery_address"> </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 inside_tab">
                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_order_progress');?></h5>
                                <input type="hidden" value="<?php echo $data->status;?>" name ="status">
                                <ul data-id="item_id" id="ex_select" class="clearfix">
                                    <li>
                                        <ul class="entity_group">
                                            <?php foreach($this->order_model->order_status as $item):?>
                                                <li data-item_id="<?php echo $item['id']?>" class="col-md-2 <?php if($data->status == $item['id']){ echo 'selected';}?>"><?php echo $item['title']?></li>
                                            <?php endforeach;?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 inside_tab">
                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_order_main_info');?></h5>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-md-3 inside_tab">
                                        <p><?php echo LANG('label_order_price_base');?>:<br/> <?php echo $data->price_base ; ?></p>
                                    </div>
                                    <div class="col-md-3 inside_tab">
                                        <p><?php echo LANG('label_order_price_delivery');?>: <input type="text" placeholder="" class="input" value="<?php echo $data->price_delivery;?>" name="price_delivery"></p>

                                    </div>
                                    <div class="col-md-3 inside_tab">
                                        <p><?php echo LANG('label_order_price_discount');?>:<br/> <?php echo $data->price_discount ; ?></p>
                                    </div>
                                    <div class="col-md-3 inside_tab">
                                        <p><?php echo LANG('label_order_price_final');?>:<br/> <strong><?php echo $data->price_final ; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 inside_tab">
                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_tovars_in_order');?></h5>
                                <?php $this->load->view('panel/orders/tovar_list');?>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    (function ($, undefined) {
        $(document).ready(function () {

            $('.entity_group li').click(function(){
                $('.entity_group li').removeClass('selected');
                $(this).addClass('selected');
                var item_id = $(this).attr('data-item_id');
                $('#cform input[name="status"]').val(item_id);
            });
        });
    })(jQuery);
</script>