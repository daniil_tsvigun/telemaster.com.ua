<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masters extends MY_Controller {

    public $user = '';
    protected $base_url = 'panel/masters';
    protected $home_url = 'panel/masters/';
    protected $config_info;
    private $source_models = array(
        'master'=>'Master',
    );

    public function __construct(){
        parent::__construct();
        $this->load->library('FolderWork');
        $this->load->library('UploadImage');

        $this->user = get_user(true);

        if(!has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel/auth/login');
        }


        $this->config_info = new Config_info('masters');
        $this->lang_exist = $this->config->config['lang_uri_abbr'];
    }


    public function index()
    {
    }

    public function master(){

        $this->_watch('master');
    }

    private function _watch($type = 'master'){

        $_model = $type .'_model';
        $_table = $type.'s';
        $data['data_type'] = $type;
        $data['page_title'] = LANG('page_title_'. $type .'_list');
        $data['base_link'] = $this->base_url;
        $data['base_lang_link'] = site_url() .'/'.$this->base_url;
        $data['current_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['create_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
        $data['remove_link'] = site_url() .'/'. $this->base_url .'/remove/'. $type;
        $data['edit_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type;
        $data['content'] = 'panel/default/default_list';
        $data['display_fields'] = $this->config_info->display_table_fields[$type];
        $pagination_data = get_pagination_data();
        $pagination_data['id']=(isset($_GET['id']))? $_GET['id']:false;
        $pagination_data['master_id']=(isset($_GET['master_id']))? $_GET['master_id']:false;
        $pagination_data['surname']=(isset($_GET['surname']))? $_GET['surname']:false;
        $pagination_data['phone']=(isset($_GET['phone']))? $_GET['phone']:false;
        $pagination_data['active']=(isset($_GET['active']))? $_GET['active']:false;
        $data['sort'] = $pagination_data['sort'];
        $data['table_data'] = $this->$_model->{'get_all_'. $_table}($pagination_data);
        $filter_id=$this->input->get('id');
        $filter_master_id=$this->input->get('master_id');
        $filter_surname=$this->input->get('surname');
        $filter_phone=$this->input->get('phone');

        // foreach($data['table_data'] as $key=>$master ){

        //     if($filter_id!=NULL&&$filter_id!=''&&$filter_id!=$data['table_data'][$key]){
        //         //unset($data['table_data'][$key]);
        //     }    
        // }
        
        if(count($data['table_data']) == 0 && $pagination_data['page'] >1){
            $_link = make_link_by($data['current_link'],array('page'));
            $action_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
            redirect($action_link .'page='.($pagination_data['page'] - 1));
        }
       
        // if($pagination_data['like']!='%%'&&!empty($pagination_data['like'])){
            
        //     // $_link = make_link_by($data['current_link'],array('like'));
        //     // $action_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
        //     // redirect($action_link .'like='.($pagination_data['like'] ));
        // }
        $data['pagination'] = pagination($_table,array(),$pagination_data['page'],$pagination_data['count'],make_link_by($data['current_link'],array('page')));
        $this->load->view('panel/main_layout',$data);
    }

    public function create($type = 'master',$close = false){

        //redirect_has_no_permission($this->user,'create_'. $type .'_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $data = new $_source_model();
        $this->_editor($data,'create',$type,$close);
    }

    public function ajax_rotate_image($filename,$degree){
        $this->master_model->rotate_image($filename,'right');
    }

    public function edit($type = 'master',$id = false,$close = false){

        //redirect_has_no_permission($this->user,'edit_'. $type .'_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $data =  new $_source_model($this->{$type .'_model'}->get_by_id($id));
        $data->image=$this->master_model->get_master_image_by_id($id);
        // var_dump($id);
        $this->db->from('masters_to_regions');
        $this->db->where('master_id', $id);
        $regions=$this->db->get()->result_array();
        foreach($regions as $key=>$item){
            $regions[$key]=$item['region_id'];
        }
        $data->regions=$this->region_model->get_regions_by_ids($regions);
        if(!$data){
            add_system_message("danger", LANG("ntf_npt_'. $type .'_no_exist_title"), LANG("ntf_npt_'. $type .'_no_exist"));
            redirect($this->base_url .'/menu/'. $type);
        }

        $this->_editor($data,$id,$type,$close);
    }
    private function _editor($source,$data_id,$type,$close){

        $_model = $type .'_model';
        $_source_model = $this->source_models[$type];
        $data['page_title'] = LANG('page_title_'. $type .'_edit');
        if($data_id === 'create'){
            $data['save_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/create/'. $type .'/close';
        }else{
            $data['save_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'. $data_id;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'.$data_id .'/close';
        }
        $data['areas']=$this->region_model->get_all_areas();

        /* Removing AR Crimea */
        foreach($data['areas'] as $key=>$area){
            if($area['id']==01){
                //unset($data['areas'][$key]);
            }
        }
        $data['close_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['content'] = $this->base_url .'/'. $type .'_edit';
        $this->form_validation->set_rules($this->config_info->validation_rules[$type]);
        $this->form_validation->data_id = $data_id;
        
        if ($this->form_validation->run() === true) {
            if($data_id ==='create'){

                $data_id = $this->$_model->create_master($this->input->post());
                redirect(site_url().'/'.$this->base_url .'/edit/'. $type.'/'.$data_id);
                add_system_message('success', LANG('ntf_'. $type .'_created_title'), LANG('ntf_'. $type .'_created'));
            }else{

                $this->$_model->master_update($this->input->post(), $data_id,$_source_model);
                add_system_message('success', LANG('ntf_'. $type .'_edited_title'), LANG('ntf_'. $type .'_edited'));
            }
            $data['data'] =  new $_source_model($this->{$type .'_model'}->get_by_id($data_id));
            if($close || $data_id === 'create'){
                redirect($data['close_link']);
            }
        } else {
            if (count($this->input->post()) > 0) {
                $data['data'] = new $_source_model($this->input->post());
            } else {
                $data['data'] = $source;
            }
        }
        if($data_id!='created'){
            $data['data']->image=$this->master_model->get_master_image_by_id($data_id);
        }
        $this->db->from('masters_to_regions');
        $this->db->where('master_id', $data_id);
        $regions=$this->db->get()->result_array();
        foreach($regions as $key=>$item){
            $regions[$key]=$item['region_id'];
        }
        $data['data']->refuses=$this->master_model->get_refuses();
        $data['data']->master_refuses=$this->master_model->get_refuses_by_master($data_id);
        $data['data']->regions=$this->region_model->get_regions_by_ids($regions);
        $data['data']->id = $data_id;
        $data['data']->name_ru = $data['data']->name;
        $data['data']->surname_ru = $data['data']->surname;
        $data['lang_exist'] =$this->lang_exist;
        $this->load->view('panel/main_layout',$data);
    }

    public function ajax_del_photo(){
        $post_data=$this->input->post('data');
        $result=array('status'=>'error');
        if($post_data!=NULL){
            $deleting=$this->master_model->remove_master_image($post_data['id']);
            if($deleting){
                $result['status']='success';
                if($this->input->post('send_message')==true){
                    $_data=$this->master_model->get_by_id($post_data['id']);
                    $_data['to_email']=$_data['email'];
                    $_data['subject'] = 'Модерація фото';
                    $_data['template'] = 'photo_remove';
                    $this->sender_model->message($_data);
                }
            }
        }
        echo json_encode($result);
        return;
    }

}