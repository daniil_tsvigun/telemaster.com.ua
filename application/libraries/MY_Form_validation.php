<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Form_validation extends CI_Form_validation {

    public $data_id = false;

    /*
    public function is_unique($str, $field)
    {
        if (substr_count($field, '.') == 3)
        {
            list($table,$field,$id_field,$id_val) = explode('.', $field);
            $query = $this->CI->db->limit(1)->where($field,$str)->where($id_field.' != ',$id_val)->get($table);
        } else {
            list($table, $field)=explode('.', $field);
            $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
        }
        return $query->num_rows() === 0;
    }
    */

    public function is_unique($str, $field){

        list($table, $field)= explode('.', $field);
        if($this->data_id !== false){
            $query = $this->CI->db->limit(1)->where($field,$str)->where('id != ',$this->data_id)->get($table);
        }else{
            $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
        }

        return $query->num_rows() === 0;
    }
} 