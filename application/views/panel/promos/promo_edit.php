<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                    <?php $this->load->view('panel/templates/action_button');?>
                    <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>

                    <div class="tab-content">
                        <div id="general" class="tab-pane active" role="tabpanel">
                            <div class="row">
                                <div class="col-md-6 inside_tab">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_main_promo_info');?></h5>
                                    <p><span><? echo LANG ('label_title');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->title;?>" name="title"> </p>
                                    <p><span><? echo LANG ('label_word');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->word;?>" name="word"> </p>
                                    <p><span><? echo LANG ('label_value');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->value;?>" name="value"> </p>
                                    <p><span><? echo LANG ('label_start_date');?></span><input class="datetimepicker" type="text" placeholder="" class="input" value="<?php echo $data->start_date;?>" name="start_date"> </p>
                                    <p><span><? echo LANG ('label_end_date');?></span><input class="datetimepicker" type="text" placeholder="" class="input" value="<?php echo $data->end_date;?>" name="end_date"> </p>
                                    <p><span><? echo LANG ('label_promo_type');?></span>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$this->promo_model->promo_type,'name'=>'type','selected'=>array($data->type),'params' => array('no_all'=>'yes')));?>
                                    </p>
                                    <p><span><? echo LANG ('label_promo_one_time');?></span>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$this->promo_model->onetime_list,'name'=>'one_time','selected'=>array($data->one_time),'params' => array('no_all'=>'yes')));?>
                                    </p>
                                </div>
                                <div class="col-md-6 inside_tab">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_promo_categories');?></h5>
                                    <p>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$categories_tree,'name'=>'category[]','selected'=> $data->category,'params' => array('multiple'=>'multiple','no_all'=>'no')));?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
