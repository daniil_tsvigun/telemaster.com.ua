<?php

$lang['error_no_select_values_'] = 'Помилка! Данні відсутні';
$lang['error_no_select_values_permissions'] = 'Помилка! Для данної ролі не вибрано жодного дозволу.';


$lang['error_photo_format_no_confirm'] = 'Не корректний формат фото: ';
$lang['error_file_size_smolly'] = 'Розмір фото меньший, ніж заданий за замовчуванням: ';
$lang['error_no_email'] = 'Введіть корректну адрессу електронної пошти';
$lang['error_no_phone'] = 'Введіть корректний номер телефону';
$lang['error_data_transfer'] = 'Помилка при передачі данних. Спробуйте пізнише, або зверніться до адміністратора системи';
$lang['error_no_tovar_exist'] = 'Помилка. Товар не існує';
$lang['error_generation_error'] = 'Помилка. Генерування зображень не відбулося. Зверніться до адміністратора системи';
$lang['error_more_one_photo'] = 'Помилка. Вибрано більше одного колольора для одного зображення';
$lang['error_no_select_photo'] = 'Помилка. Не вибрано колір для';
$lang['error_n_photo'] = '-го зображення';
$lang['error_no_unik_photo'] = 'Помилка. Кольори повинні бути унікальними';
$lang['error_two_gender_one_product'] = 'Помилка. Ви призначили один продукт більше ніж до одного полу.';
$lang['error_you_mast_select_gender'] = 'Помилка. Ви повинні призначити продукт до одного з гендерних типів';
$lang['error_you_mast_select_this_product'] = 'Помилка. Ви повинні призначити редагуємий продукт до одного з гендерних типів';
$lang['error_product_photo_size'] = 'Помилка. Не корректні розміри фото продукта. Має бути';
$lang['error_product_photo_format_png'] = 'Помилка. Тип файла фото продукта має бути png';
$lang['error_icon_size'] = 'Помилка. Не корректні розміри іконки продукта. Має бути';
$lang['error_icon_format_png'] = 'Помилка. Тип файла іконки продукта має бути png';
$lang['error_no_title'] = 'Заголовок не задано';
$lang['error_no_categories'] = 'Виберіть хочеб одну категорію для принту';
$lang['error_no_tags'] = 'Задайте хочаб один тег';
$lang['error_wrong_file_format'] = 'Помилка. Не допустимий формат файла';
$lang['error_wrong_file_pixels'] = 'Помилка. Розмір зображення не співпадаеє з шаблоном';
$lang['error_'] = '';
