<?php
$lang['label_auth']                                 = 'Вхід';
$lang['label_auth_fb']                              = 'Вхід за допомогою facebook';
$lang['label_registration']                         = 'Реєстрація';
$lang['label_buyer']                                = 'Покупець';
$lang['label_painter']                              = 'Художник';
$lang['label_params']                               = 'Параметри';
$lang['label_forgot']                               = 'Забули?';
$lang['label_name']                                 = 'Ім`я';
$lang['label_surname']                              = 'Прізвище';
$lang['label_title']                                = 'Назва';
$lang['label_description']                          = 'Опис';
$lang['label_content']                              = 'Контент';
$lang['label_seo_title']                            = 'SEO title';
$lang['label_seo_keywords']                         = 'SEO keywords';
$lang['label_seo_description']                      = 'SEO description';
$lang['label_alias']                                = 'Alias';
$lang['label_article']                              = 'Актикул';
$lang['label_prices']                               = 'Ціны';
$lang['label_price']                                = 'Ціна базова';
$lang['label_price_sale']                           = 'Ціна зі скидкою';
$lang['label_flag_new']                             = 'Новий';
$lang['label_flag_action']                          = 'Акція';
$lang['label_flag_sale']                            = 'Розпродаж';
$lang['label_sort']                                 = 'Позиція';
$lang['label_category']                             = 'Категорія';
$lang['label_mockup']                               = 'Макет';
$lang['label_preview']                              = 'Прев`ю';
$lang['label_parent_category']                      = 'Батьківська категорія';
$lang['label_parent_filter_category']               = 'В категоріях';
$lang['label_parent_filter_group']                  = 'Группа фільтрів';
$lang['label_no_parent']                            = '---';
$lang['label_no_select']                            = 'не выбрано';
$lang['label_email']                                = 'Email';
$lang['label_login']                                = 'Логін';
$lang['label_author']                               = 'Автор';
$lang['label_mobile_phone']                         = 'Мобільний телефон';
$lang['label_contacts']                             = 'Контакти';
$lang['label_password']                             = 'Пароль';
$lang['label_password_repeat']                      = 'Повторіть пароль';
$lang['label_enter_password']                       = 'Введіть пароль, якщо необхідно';
$lang['label_parent_menu_item']                     = 'Батьківський елемент';
$lang['label_menu']                                 = 'Меню';
$lang['label_template']                             = 'Шаблон';
$lang['label_tovat_template']                       = 'Шаблон товару';
$lang['label_do']                                   = 'Дії';
$lang['label_photo']                                = 'Фото';
$lang['label_main_photo']                           = 'Головне';
$lang['label_color']                                = 'Колір';
$lang['label_size']                                 = 'Розмір';
$lang['label_photo']                                = 'Фото';
$lang['label_filters']                              = 'Фільтри';
$lang['label_photo_parametrs']                      = 'Розміри запринтовки';
$lang['label_upload_photo']                         = 'Додати фото';
$lang['label_subtitle']                             = 'Підзаголовок';
$lang['label_ua']                                   = 'Укр';
$lang['label_ru']                                   = 'Рус';
$lang['label_en']                                   = 'Eng';
$lang['label_print_size_x']                         = 'Розмір принта(px) на фото вісь X';
$lang['label_print_size_y']                         = 'Розмір принта(px) на фото вісь Y';
$lang['label_print_offset_x']                       = 'Відступ принта(px) на фото вісь X';
$lang['label_print_offset_y']                       = 'Відступ принта(px) на фото вісь Y';
$lang['label_print_and_product']                    = 'Продукт і принт';
$lang['label_product']                              = 'Продукт';
$lang['label_picture']                              = 'Принт';
$lang['label_tovar_preview']                        = 'Пред. перегляд товара';
$lang['label_photo_list']                           = 'Список зображень товара';
$lang['label_photo_generation']                     = 'Генерація зображень товара';
$lang['label_attention_another_product_or_picture'] = 'Увага! Було змінено продукт чи принт для товара! Щоб зміни вступили в силу, вам необхідно перегенерувати зображення.';
$lang['label_tovar_photo_generation_action']        = 'Генерувати зображення товара';
$lang['label_filter_parametr']                      = 'Параметр';
$lang['label_filter_group_template']                = 'Шаблон(если необходимо)';
$lang['label_count']                                = 'Кількість';
$lang['label_price_for_one']                        = 'Ціна за одиницю';
$lang['label_for_tovars']                           = 'За товари';
$lang['label_count_in_cart']                        = 'одиниць в корзині';
$lang['label_tovar_in_cart_edit']                   = 'змінити';
$lang['label_delivery']                             = 'Доставка';
$lang['label_for_present']                          = 'В подарунок';
$lang['label_postal_and_pack']                      = '(Листівка і пакування)';
$lang['label_to_do']                                = 'Активувати';
$lang['label_all_price']                            = 'Загальна сумма';
$lang['label_main_client_info']                     = 'Основна інформація про кліента';
$lang['label_word']                                 = 'Промо код';
$lang['label_value']                                = 'Значення';
$lang['label_promo_type']                           = 'Тип значення';
$lang['label_promo_categories']                     = 'Вибрані категорії';
$lang['label_promo_one_time']                       = 'Тип використання';
$lang['label_percent']                              = 'Відсоток';
$lang['label_cashe']                                = 'Гроші';
$lang['label_profile_data']                         = 'Інформація про мастера';
$lang['label_profile_config']                       = 'Налаштування профіля';
$lang['label_profile_config_main']                  = 'Основні';
$lang['label_profile_config_user_name']             = 'І`мя користувача';
$lang['label_profile_config_user_url']              = 'Унікальний URL користувача';
$lang['label_profile_config_user_city']             = 'Місто';
$lang['label_profile_config_user_country']          = 'Країна';
$lang['label_profile_config_user_description']      = 'Додатково';
$lang['label_profile_config_user_new_pass']         = 'Новий пароль';
$lang['label_profile_config_user_old_pass']         = 'Старий пароль';
$lang['label_profile_config_user_subscription']     = 'Я хочу отримувати розсилку';
$lang['label_profile_config_user_change_password']  = 'Змінити пароль';
$lang['label_profile_config_user_save']             = 'Зберегти';
$lang['label_profile_config_user_vk_link']          = 'vk.com/';
$lang['label_profile_config_user_fb_link']          = 'fb.com/';
$lang['label_profile_config_user_instagram_link']   = 'instagram.com/';
$lang['label_profile_config_user_tw_link']          = 'twitter.com/';
$lang['label_profile_config_user_behance_link']     = 'behance.com/';
$lang['label_hello']                                = 'Вітаємо';
$lang['label_enter_auth_data']                      = 'Введіть ваші данні для входу';
$lang['label_enter_registration_data']              = 'Введіть ваші данні для реєстрації';
$lang['label_client_surname']                       = 'Фамілія';
$lang['label_client_name']                          = 'Им`я';
$lang['label_client_phone']                         = 'Телефон';
$lang['label_client_email']                         = 'Email';
$lang['label_client_profile']                       = 'Профіль користувача';
$lang['label_client_no_reg']                        = 'Анонімний користувач';
$lang['label_client_reg']                           = 'Користувач (ID: %s) ';
$lang['label_delivery_info']                        = 'Реквізити доставки';
$lang['label_tovars_in_order']                      = 'Товари в замовленні';
$lang['label_order_progress']                       = 'Прогрес статуса замовлення';
$lang['label_order_main_info']                      = 'Основна інформація замовлення';
$lang['label_order_price_base']                     = 'Базова ціна замовлених товарів';
$lang['label_order_price_delivery']                 = 'Вартість доставки';
$lang['label_order_price_discount']                 = 'Знижка по промо коду';
$lang['label_order_price_final']                    = 'Остаточна ціна замовлення';
$lang['label_tovar_price_position']                 = 'Вартість';
$lang['label_tovar_filters']                        = 'Опції';
$lang['label_tovar_postal']                         = 'Листівка';
$lang['label_tovar_pack']                           = 'Упаковка';
$lang['label_tovar_price']                          = 'Ціна';
$lang['label_gender_associated']                    = 'Гендерна асоціація';
$lang['label_gender_style']                         = 'Стиль';
$lang['label_male']                                 = 'Чоловік';
$lang['label_female']                               = 'Жінка';
$lang['label_kid']                                  = 'Дитина';
$lang['label_more_for_prints']                      = 'Принт також доступний';
$lang['label_more_for_artist']                      = 'Принти художника';
$lang['label_more_viewed']                          = 'Нещодавно переглянуті';
$lang['label_one_time']                             = 'Єдиноразова';
$lang['label_multi_time']                           = 'Багаторазова';
$lang['label_back_to_shopping']                     = 'Назад до покупок';
$lang['label_enter_promo_code']                     = 'введіть промокод';
$lang['label_cart_steep_cart']                      = 'Ваша корзина';
$lang['label_cart_steep_ship']                      = 'Деталі доставки';
$lang['label_cart_steep_pay']                       = 'Оплата';
$lang['label_block_cart']                           = 'Замовлення';
$lang['label_block_ship']                           = 'Доставка';
$lang['label_block_pay']                            = 'Олата';
$lang['label_block_thank']                          = 'Завершити';
$lang['label_status_begin']                         = 'Нове';
$lang['label_status_in_work']                       = 'В роботі';
$lang['label_status_send']                          = 'Відправлено';
$lang['label_status_done']                          = 'Доставлено';
$lang['label_status_cancel']                        = 'Відмінено';
$lang['label_delivery_type']                        = 'Спосіб доставки';
$lang['label_payment_type']                         = 'Спосіб оплати';
$lang['label_cache']                                = 'Готівка';
$lang['label_card']                                 = 'Карта';
$lang['label_courier']                              = 'Курьер';
$lang['label_np']                                   = 'Нова пошта';
$lang['label_delivery_courier']                     = 'Доставка курьером';
$lang['label_delivery_np']                          = 'Нова пошта';
$lang['label_delivery_me']                          = 'Міст Експрес';
$lang['label_delivery_courier_1']                   = 'Місто отримувача';
$lang['label_delivery_courier_2']                   = 'Назва вулиці';
$lang['label_delivery_courier_3']                   = 'Будинок';
$lang['label_delivery_courier_4']                   = 'Квартира';
$lang['label_delivery_courier_5']                   = 'Коментар';
$lang['label_delivery_back_to_cart']                = 'до замовлення';
$lang['label_delivery_back_to_ship']                = 'до доставки';
$lang['label_main_promo_info']                      = 'Основная інформація';
$lang['label_promo_categories']                     = 'Закріплені категорії';
$lang['label_start_date']                           = 'Початок дії';
$lang['label_end_date']                             = 'Завершення дії';
$lang['label_pay_card']                             = 'Оплата картою';
$lang['label_pay_cache']                            = 'Готівкою';
$lang['label_pay_name']                             = 'Ім`я';
$lang['label_pay_surname']                          = 'Фамілія';
$lang['label_pay_phone']                            = 'Номер телефону';
$lang['label_pay_email']                            = 'Електронна адресса';
$lang['label_pay_limit']                            = 'Поріг нарахувань(до суми)';
$lang['label_pay_limit_base']                       = 'Базовий';
$lang['label_pay_limit_level']                      = 'Рівень';
$lang['label_pay_percent']                          = 'Процент нарахування';
$lang['label_tab_general']                          = 'Основна інформація';
$lang['label_tab_other_info']                       = 'Додаткова інформація';
$lang['label_tab_media']                            = 'Фото';
$lang['label_tab_seo']                              = 'Seo';
$lang['label_tab_painter_pay']                      = 'Винагорода художнику';
$lang['label_tab_print_confirm_status']             = 'Підтвердження публікації принта';
$lang['label_picture_name']                         = 'Назва принта';
$lang['label_upload_file_for_print']                = 'Завантажити файл';
$lang['label_upload_print_to']                      = 'Завантажити принт';
$lang['label_to_portfolio']                         = 'для портфоліо';
$lang['label_artist_shop']                          = 'Магазин';
$lang['label_artist_portfolio']                     = 'Портфоліо';
$lang['label_hide_filters']                         = 'Сховати фильтри';
$lang['LABEL_CART_SHIPPING_NEWPOST_TYPE_TO_DOOR']   = 'До дверей';
$lang['label_confirm_status_progress']              = 'Статус пинту';
$lang['label_confirm_status_new']                   = 'Новий';
$lang['label_confirm_status_confirm']               = 'Підтвердити';
$lang['label_confirm_status_refuse']                = 'Відмовити';
$lang['label_confirm_status_now_new']               = 'Новий';
$lang['label_confirm_status_now_confirm']           = 'Підтверджено';
$lang['label_confirm_status_now_refuse']            = 'Відмовлено';
$lang['label_refuse_message']                       = 'Повідомлення причини відмови';
$lang['label_refuse_message_select']                = 'Виберіть варіант зі списку:';
$lang['label_refuse_message_or_enter']              = 'Або введіть текст(тескт має більший пріорітет)';
$lang['label_tovars_to_generate']                   = 'Товари для створення';
$lang['label_tovars_generate']                      = 'Генерувати товари при підтвержденні принту?';
$lang['label_address']                              = 'Адреса';
$lang['label_icon']                                 = 'Іконка';
$lang['label_user_deleted']                         = 'Користувача видалено';
$lang['label_tovar']                                = 'Товар';
$lang['label_print']                                = 'Принт';
$lang['label_text']                                 = 'Коментар';
$lang['label_user']                                 = 'Користувач';
$lang['label_user_buyer']                           = 'Покупець';
$lang['label_user_painter']                         = 'Художник';
$lang['label_public_comment']                       = 'Опубліковано';
$lang['label_shop']                                 = 'Магазин';
$lang['label_portfolio']                            = 'Портфоліо';
$lang['label_favorites']                            = 'Обрані';
$lang['label_forget']                               = 'Забути';
$lang['label_view_item']                            = 'Переглянути';
$lang['label_to_cart']                              = 'В корзину';
$lang['label_to_favorites']                         = 'В обрані';
$lang['label_del_favorites']                        = 'Обраний';
$lang['label_tovar_description']                    = 'Опис товара';
$lang['label_to_present']                           = 'В подарунок?';
$lang['label_social_links']                         = 'Лінки на соц сіті';
$lang['label_social_vk']                            = 'vk.com/';
$lang['label_social_fb']                            = 'fb.com/';
$lang['label_social_instagram']                     = 'instagram.com/ ';
$lang['label_social_tw']                            = 'twitter.com/';
$lang['label_social_behance']                       = 'behance.com/';
$lang['label_social_description']                   = 'Додатково';
$lang['label_pay_debit_status']                     = 'Нарахування враховується';
$lang['label_pay_credit_status']                    = 'Списання враховується';
$lang['label_order']                                = 'Замовлення';
$lang['label_admin']                                = 'Адміністратор';
$lang['label_search_result']                        = 'Результати пошуку';
$lang['label_sold']                                 = 'Продано';
$lang['label_sold_graph']                           = 'Графік продажів';
$lang['label_search_in_category']                   = 'Пошук в категорії';
$lang['label_search']                               = 'Пошук';
$lang['label_print_edit']                           = 'Редагувати';
$lang['label_profile']                              = 'Профайл';

$lang['label_add_work']                             = 'Додати роботу';
$lang['label_edit_works']                           = 'Редагувати роботу';


$lang['label_delivery_area']                        = 'Область';
$lang['label_delivery_district']                    = 'Район';
$lang['label_delivery_city']                        = 'Місто';
$lang['label_delivery_warehouse']                   = 'Склад';
$lang['label_delivery_address']                      = 'Адресса';

$lang['label_action_cart']                          = 'Корзина';
$lang['label_action_login']                         = 'Вхід/Реєстрація';
$lang['label_action_logout']                        = 'Вихід';
$lang['label_action_search']                        = 'Пошук';

$lang['label_action_registration']                  = 'Реєстрація';

$lang['label_terms']                                = 'Умови оплати і доставки';
$lang['label_share']                                = 'Поділитись';
$lang['label_do_like']                              = 'Віддати серце';
$lang['label_do_comment']                           = 'Коментувати';
$lang['label_comment']                              = 'Коментар';
$lang['label_date_now']                             = 'щойно';
$lang['label_date_mins']                            = 'хвилин тому';
$lang['label_date_hours']                           = 'годин тому';
$lang['label_date_days']                            = 'днів тому';

$lang['label_email_confirmation']                   = 'Для завершення реєстрації перейдіть за посиланням';

$lang['label_agree_terms']                          = 'Я згоден з правилами використання сайту';

$lang['label_sent_successfully']                    = 'На Ваш email було відправлено лист із посилання для активації акаунта';

//viasat

$lang['label_menu_main']                            = 'Головна';
$lang['label_menu_master']                          = 'Знайти майстра';
$lang['label_menu_instructions']                    = 'Інструкції';
$lang['label_menu_forum']                           = 'Форум';
$lang['label_menu_master_reg']                      = 'Реєстрація майстра';
$lang['label_foot_email']                           = '<p>Адміністратор сайту: <a href="mailto:info&#64;telemaster&#46;com&#46;ua" >info@telemaster&#46;com&#46;ua</a></p>';
$lang['label_enter_your_region']                    = 'Введіть назву вашого населеного пункту';
$lang['label_anchor_to_calling']                    = 'Якщо не вдається налаштувати тюнер власноруч за допомогою інструкцій, перевірте <a href="'.site_url('/faq/parameters').'">параметри</a>, або скористайтеся <a href="'.site_url('/calling').'" >допомогою майстра</a>';
$lang['label_setup_tuner_pre']                      = 'Самостійно';
$lang['label_setup_tuner']                          = 'Налаштувати тюнер';
$lang['label_call_master']                          = 'Знайти майстра';
$lang['label_instr_video']                          = 'Інструкції та відео';
$lang['label_main_page_header']                     = 'Маєте супутникове ТБ?<br/>Переналаштуйте тюнер!';
$lang['label_main_page_text']                       = '<p>Українські канали змінюють параметри супутникового мовлення.<br/> Незабаром прийом та перегляд телеканалів буде можливим тільки <a href="'.site_url('/faq/parameters').'"> за новими параметрами</a>. </p><p> Скористайтесь інструкціями для швидкого переналаштування обладнання, або послугами майстра. Потурбуйтесь про своїх рідних та допоможіть їм зробити переналаштування.</p>';

$lang['label_about_us']                             = 'Про нас';
$lang['label_site_rules']                           = 'Правила користування сайтом';
$lang['label_masters_coordinate']                   = 'Лінія підтримки майстрів:';

$lang['label_rate']                                 = 'Рейтинг';
$lang['label_comments']                             = 'Відгуки';
$lang['label_watch_contacts']                       = 'Дивитись контакти';

$lang['label_district']                             = ' район';
$lang['label_obl']                                  = ' область';
$lang['label_regions']                              = 'Області, райони';

$lang['label_master_page']                          = 'Сторінка майстра';
$lang['label_no_masters']                           = 'На жаль майстрів не знайдено';

$lang['label_instructions']                         = 'Інструкції до переналаштування';
$lang['label_choose_tuner']                         = 'Оберіть, будь ласка, модель тюнера.';
$lang['label_ifnotuner']                            = 'Якщо Вашого тюнера немає в списку, скористайтеся ';
$lang['label_uni_instruct']                         = ' універсальними інструкціями';

$lang['label_manufacturer']                         = 'Марка';
$lang['label_model']                                = 'Модель тюнера';
$lang['label_txt_instructions']                     = 'Текстові інструкції';
$lang['label_video_instr']                          = 'Відео інструкції';
$lang['label_watch']                                = 'Дивитись';
$lang['label_pdf']                                  = 'Читати';
$lang['label_universal_guide']                      = 'Універcальна інструкція';

$lang['label_faq']                                  = 'Питання та відповіді';
$lang['label_find_master']                          = 'Знайти майстра';
$lang['label_master_reg']                           = 'Реєстрація майстра';
$lang['label_registration_rule']                    = '<p>Для реєстрації в системі</p><p>заповніть, будь ласка, форму нижче!</p> ';

$lang['label_inn']                                  = 'Ідентифікаційний код';
$lang['label_passport']                             = 'Паспорт (серія, номер)';
$lang['label_passport_when']                        = 'Ким і коли виданий';
$lang['label_phone_number']                         = 'Номер телефону';

$lang['label_success_on_reg']                       = 'Успішно зареєстровано! Вам відправлено email з інструкціями.';

$lang['label_form_error']                           = 'Заповніть всі обов\'язкові поля!';
$lang['label_privacy_policy']                       = 'Політика конфіденційності';

$lang['label_enter_regions']                        = 'Вкажіть області та райони які Ви будете обслуговувати:';

$lang['label_instruct_text']                        = 'текстова';
$lang['label_instruct_video']                       = 'відео';
$lang['rate_master']                                = 'Проголосувати';
$lang['label_add_comment']                          = 'Залишити відгук';
$lang['label_send']                                 = 'Відправити';

$lang['label_comment_add']                          = 'Ваш відгук успішно відправлено!';

$lang['label_already_vote']                         = 'Ви вже проголосували!';
$lang['label_success_voted']                        = 'Ви успішно проголосували!';

$lang['label_please_auth']                          = 'Для того, щоб залишити відгук, авторизуйтеся, будь ласка!  ;)';

$lang['label_change_photo']                         = 'Додати фото';

$lang['label_id']                                   = 'ID';
$lang['page_title_master_edit']                     = 'Сторінка редагуваня майстра';
$lang['ntf_master_edited_title']                    = 'Редагування майстра ';
$lang['ntf_master_edited']                          = 'Майстра відредаговано';

$lang['label_site_admin']                           = 'Адміністратор сайту: ';
$lang['label_registration_phrase']                  = 'Ви – майстер та вмієте працювати із обладнанням для супутникового телебачення? Зареєструйтесь тут та отримуйте замовлення від клієнтів.';

$lang['label_temp_page_text']						= '<p>До уваги інсталяторів обладнання супутникового телебачення.<br/> В найближчий час найбільші українські загальнонаціональні телеканали змінюють параметри супутникового мовлення. </p><p> Нові параметри супутникового мовлення в найближчий час будуть викладені на цьому сайті.
Зараз сайт працює в режимі реєстрації інсталяторів обладнання супутникового телебачення.
Реєструйтесь та приєднуйтесь до команди Telemaster!
</p>';
$lang['label_temp_sub_inst']						= 'В цьому розділі незабаром будуть опубліковані інструкції до переналаштування ТВ тюнерів. ';	
$lang['label_load_image_error']                     = 'Формат зображення може бути jpg або png!';

$lang['label_success_registered_to_master']         = 'Ви успішно зареєструвалися! Найближчим часом з Вами зв\'яжуться для уточнення персональних даних.';
$lang['label_registration_subject']                 = 'Реєстрація на telemaster.com.ua';
$lang['label_success_registered_to_admin']          = 'На сайті зареєструвався майстер!<br/> <br/>Дані майстра:<br/>';
$lang['label_bottom']                               = '<br/>Дякуємо,<br/> telemaster.com.ua.';
$lang['label_reports_rules']						= 'Правила модерації відгуків'; 
$lang['label_registration_helptext']				= 'Увага! Якщо ви вже зареєстровані і хочете змінити / додати інформацію в своєму профілі - зверніться на Лінію підтримки Майстрів (050 480 82 82, 093 170 82 82, 067 011 82 82, info@telemaster.com.ua).';
$lang['developed_by']								= 'Створення сайту ';

$lang['label_unique_constr']                        = 'Користувач з такою email адресою вже існує!';
$lang['label_unique_ph_constr']                     = 'Користувач з таким номером телефону вже існує!';

$lang['table_email']								= 'Email';
$lang['table_id_master']							= 'Master ID';
$lang['table_surname']								= 'Прізвище';
$lang['page_title_master_list']						= 'Список майстрів';

$lang['label_err_on_reg_email']                     = 'Користувач з даною email адресою вже існує!';
$lang['label_err_on_reg_unique']                    = 'Користувач з даною email адресою та номером телефону вже існує!';
$lang['label_err_on_reg_phone']                     = 'Користувач з даним номером телефону вже існує!';

$lang['table_master']								= 'Майстер';

$lang['label_call_partner']                         = 'Знайти провайдера';
$lang['label_find_partner']                         = 'Знайти провайдера';
$lang['label_enter_region']                         = 'Введіть назву населеного пункту';
$lang['label_watch_partner']                        = 'Перейти на сайт';
$lang['label_phone']                                = 'Номер телефону';
$lang['label_menu_partner']                         = 'Знайти провайдера';
$lang['label_provider_comment']                     = '*Інформація має довідковий характер і може містити неточності. Перелік провайдерів наданий національними телеканалами.';
$lang['label_provider_law']                         = '3 1.01.2017 р. в Україні набув чинності закон №1663-VIII «Про внесення змін до Закону України «Про телебачення і радіомовлення» (щодо уточнення умов розповсюдження програм телерадіоорганізацій у складі універсальної програмної послуги)».  У зв’язку з впровадженням цього закону умови ретрансляції національних каналів у кабельних та IPTV мережах змінюються.<br />  <strong>Знайдіть своє місто та перевірте, чи є ваш провайдер у списку* тих, хто продовжує легальну ретрансляцію національних каналів</strong>.';
$lang['label_err_on_reg']							= 'Регіони не вказані!';

$lang['label_send_refuse']                          = 'Відправити лист з відмовою майстру';
$lang['label_instruction']                          = 'Інструкції';
$lang['label_remove_photo']                         = 'Видалити фото';
$lang['label_send_master_photo_refuse']             = 'Відправити майстру повідомлення про видалення фото';