<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

    public $user = '';
    protected $base_url = 'panel/products';
    protected $home_url = 'panel/products/';
    protected $config_info;
    private $source_models = array(
        'product'=>'Product',
    );

    public function __construct(){
        parent::__construct();
        $this->user = get_user(true);

        if(!has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel/auth/login');
        }

        if(!has_permission('list_products_permissions',$this->user)){
            redirect(site_url() .'/panel/');
        }

        $this->config_info = new Config_info('products');
        $this->lang_exist = $this->config->config['lang_uri_abbr'];
    }


    public function index()
    {
    }

    public function product(){

        $this->_watch('product');
    }

    private function _watch($type = 'product'){

        $_model = $type .'_model';
        $_table = $type.'s';
        $data['data_type'] = $type;
        $data['page_title'] = LANG('page_title_'. $type .'_list');
        $data['base_link'] = $this->base_url;
        $data['base_lang_link'] = site_url() .'/'.$this->base_url;
        $data['current_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['create_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
        $data['remove_link'] = site_url() .'/'. $this->base_url .'/remove/'. $type;
        $data['edit_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type;
        $data['content'] = 'panel/default/default_list';
        $data['display_fields'] = $this->config_info->display_table_fields[$type];
        $pagination_data = get_pagination_data();
        $data['sort'] = $pagination_data['sort'];
        $data['table_data'] = $this->$_model->{'get_all_'. $type}($pagination_data);
        if(count($data['table_data']) == 0 && $pagination_data['page'] >1){
            $_link = make_link_by($data['current_link'],array('page'));
            $action_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
            redirect($action_link .'page='.($pagination_data['page'] - 1));
        }
        $data['pagination'] = pagination($_table,array(),$pagination_data['page'],$pagination_data['count'],make_link_by($data['current_link'],array('page')));
        $this->load->view('panel/main_layout',$data);
    }

    public function create($type = 'product',$close = false){

        redirect_has_no_permission($this->user,'create_'. $type .'_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $data = new $_source_model();
        $this->_editor($data,'create',$type,$close);
    }

    public function edit($type = 'product',$id = false,$close = false){

        redirect_has_no_permission($this->user,'edit_'. $type .'_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $data =  new $_source_model($this->{$type .'_model'}->get_by_id($id));
        if(!$data){
            add_system_message("danger", LANG("ntf_npt_'. $type .'_no_exist_title"), LANG("ntf_npt_'. $type .'_no_exist"));
            redirect($this->base_url .'/menu/'. $type);
        }

        $this->_editor($data,$id,$type,$close);
    }
    private function _editor($source,$data_id,$type,$close){

        $_model = $type .'_model';
        $_source_model = $this->source_models[$type];
        $data['page_title'] = LANG('page_title_'. $type .'_edit');
        if($data_id === 'create'){
            $data['save_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/create/'. $type .'/close';
        }else{
            $data['save_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'. $data_id;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'.$data_id .'/close';
        }
        $data['close_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['content'] = $this->base_url .'/'. $type .'_edit';
        $this->form_validation->set_rules($this->config_info->validation_rules[$type]);
        $this->form_validation->data_id = $data_id;

        if ($this->form_validation->run() === true) {
            if($data_id ==='create'){
                $data_id = $this->$_model->create($this->input->post(),$_source_model);
                add_system_message('success', LANG('ntf_'. $type .'_created_title'), LANG('ntf_'. $type .'_created'));
            }else{
                $this->$_model->update($this->input->post(), $data_id,$_source_model);
                add_system_message('success', LANG('ntf_'. $type .'_edited_title'), LANG('ntf_'. $type .'_edited'));
            }
            $data['data'] =  new $_source_model($this->{$type .'_model'}->get_by_id($data_id));;

            if($close || $data_id === 'create'){
                redirect($data['close_link']);
            }
        } else {
            if (count($this->input->post()) > 0) {
                $data['data'] = new $_source_model($this->input->post());
            } else {
                $data['data'] = $source;
            }
        }
        $data['data']->id = $data_id;
        $data['all_filters']=$this->filter_model->get_all_filter();
        $data['filters'] =  $this->filter_model->get_filters_and_groups(($data_id == 'create')?false:$data['data']->category_id,true);
        $data['product_filters'] = $this->filter_model->get_product_filter_ids(($data_id == 'create')?false:$data['data']->id);
        $data['image_template'] = ($data_id == 'create')? 'other':$this->category_model->get_template_by_category($data['data']->category_id)->template;
        if(isset($this->category_model->get_template_by_category($data['data']->category_id)->id)){
            $data['image_template_id'] = $this->category_model->get_template_by_category($data['data']->category_id)->id;
        }
        $data['product_images'] = $this->product_image_model->get_image_by_product(($data_id == 'create')?false:$data['data']->id);
        foreach ($data['product_images'] as $key=>$product_img){
            $data['product_images'][$key]['data']=json_decode($product_img['data'],true);
        }
        //Removing filter for print categories
        foreach($data['filters'] as $f_group_title=>$filter_group){
            foreach($filter_group as $filter){
                if($filter['parent_id']==21){       //21-id of print categories
                    unset($data['filters'][$f_group_title]);
                }
            }
        }
        $data['lang_exist'] =$this->lang_exist;
        $all_categories = $this->category_model->get_all_categories(false,true);
        //var_dump($all_categories);
        $data['categories_tree'] = level_tree_from_arr($all_categories);
        $data['templates'] = $this->panel_model->get_all_templates();
        $data['gender_associated'] = $this->product_model->get_gender_association($data['data']->id);
        $data['product_list_to_gender']= $this->product_model->get_product_list_to_gender($data['data']->category_id);
        $data['pay_limits']= $this->product_model->get_pay_limits($data['data']->id);
        $this->load->view('panel/main_layout',$data);
    }

    public function ajax_products(){
        $type='product';
        $_source_model = $this->source_models[$type];
        $form_input=$this->input->post();
        $choosen_cat=$this->input->post('category_id');
        $form_object=(object) $form_input;
        $data_id=$form_object->orig_data_id;
        $data['all_filters']=$this->filter_model->get_all_filter();
        $data['data'] = new $_source_model($this->{$type .'_model'}->get_by_id($data_id));
        $data['data']->id=$data_id;
        $data['data']->category_id=$choosen_cat;
        $data['data']->images=$form_object->images;
        $data['product_filters'] = $this->filter_model->get_product_filter_ids(($data_id == 'create')?false:$data['data']->id);
        $data['filters'] =  $this->filter_model->get_filters_and_groups(($data_id == 'create')?false:$data['data']->category_id,true);
        $data['image_template'] = $this->category_model->get_template_by_category($data['data']->category_id)->template;
        $data['product_images'] = $this->product_image_model->get_image_by_product(($data_id == 'create')?false:$data['data']->id);
        $data['lang_exist'] =$this->lang_exist;
        $all_categories = $this->category_model->get_all_categories(false,true);
        $data['categories_tree'] = level_tree_from_arr($all_categories);
        $data['templates'] = $this->panel_model->get_all_templates();
        $data['data']=$form_object;
        $data['data']->id=$data_id;
        $data['gender_associated'] = $this->product_model->get_gender_association($data['data']->id);
        $data['product_list_to_gender']= $this->product_model->get_product_list_to_gender($data['data']->category_id);
        $this->load->view('panel/products/image_templates/'. $data['image_template'],$data);
    }

    public function ajax_filters(){
        $filter=$this->input->post('filter');
        $filters =  $this->filter_model->get_filters_and_groups($this->input->post('category_id'),true);
        foreach($filters as $group_title => $filters_data){
            foreach($filters_data as $fd){
                if($fd['id']==$filter){
                    $group=$fd['filter_group'];
                    break;
                }
            }

        }
        $data=array(
            'filter_group'=>$group
        );
        echo json_encode($data);
    }

    public function _length_valid($value,$param){

        $_param = ($param == 'x')? DEFAULT_MAX_PHOTO_SIZE_X: DEFAULT_MAX_PHOTO_SIZE_Y;

        if($value <= $_param){
            return true;
        }else{
            $this->form_validation->set_message('_length_valid', LANG('error_photo_size') . $_param );
            return false;
        }
    }


}