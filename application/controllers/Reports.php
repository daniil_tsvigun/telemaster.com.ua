<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();

    }

    public function index(){
    	$this->load->view('frontend/static_rules'.SQL_LANG);
    }

}