<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {

    public $user = '';
    protected $base_url = 'panel/categories';
    protected $home_url = 'panel/categories/';
    protected $config_info;

    public function __construct(){
        parent::__construct();
        $this->user = get_user(true);

        if(!has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel/auth/login');
        }

        if(!has_permission('list_categories_permissions',$this->user)){
            redirect(site_url() .'/panel/');
        }

        $this->config_info = new Config_info('categories');
        $this->lang_exist = $this->config->config['lang_uri_abbr'];
    }


    public function index()
    {


    }

    public function category(){

        $data['page_title'] = LANG('page_title_categories_list');
        $data['base_link'] = $this->base_url;
        $data['base_lang_link'] = site_url() .'/'.$this->base_url;
        $data['current_link'] = site_url() .'/'. $this->base_url .'/category';
        $data['create_link'] = site_url() .'/'. $this->base_url .'/create';
        $data['remove_link'] = site_url() .'/'. $this->base_url .'/remove/category';
        $data['edit_link'] = site_url() .'/'. $this->base_url .'/edit';
        $data['data_type'] = 'category';
        $data['content'] = '/panel/default/default_list';

        $data['display_fields'] = $this->config_info->display_table_fields;
        $pagination_data = get_pagination_data();
        $data['sort'] = $pagination_data['sort'];

        $data['table_data'] = $this->category_model->get_all_categories($pagination_data);
        if(count($data['table_data']) == 0 && $pagination_data['page'] >1){
            $_link = make_link_by($data['current_link'],array('page'));
            $action_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
            redirect($action_link .'page='.($pagination_data['page'] - 1));
        }

        $data['pagination'] = pagination('categories',array(),$pagination_data['page'],$pagination_data['count'],make_link_by($data['current_link'],array('page')));

        $this->load->view('panel/main_layout',$data);
    }

    public function create($close = false){

        redirect_has_no_permission($this->user,'create_category_permissions',$this->base_url .'/categories');

        $category = new Category();
        $this->_editor($category,'create',$close);

    }

    public function edit($id = false,$close = false){
        redirect_has_no_permission($this->user,'edit_category_permissions',$this->base_url .'/categories');

        $category =  new Category($this->category_model->get_by_id($id));

        if(!$category){
            add_system_message("danger", LANG("ntf_npt_categories_no_exist_title"), LANG("ntf_npt_categories_no_exist"));
            redirect($this->base_url .'/categories');
        }

        $this->_editor($category,$id,$close);
    }

    private function _editor($source,$category_id,$close){

        $data['page_title'] = LANG('page_title_category_edit');
        if($category_id === 'create'){
            $data['save_link'] = site_url() .'/'. $this->base_url .'/create';
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/create/close';
        }else{
            $data['save_link'] = site_url() .'/'. $this->base_url .'/edit/'. $category_id;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/edit/'.$category_id .'/close';
        }

        $data['close_link'] = site_url() .'/'. $this->base_url .'/category';
        $data['content'] = $this->base_url .'/category_edit';

        $this->form_validation->set_rules($this->config_info->validation_rules);
        $this->form_validation->data_id = $category_id;

        if ($this->form_validation->run() === true) {

            if($category_id ==='create'){
                $this->category_model->create($this->input->post(),'Category');
                add_system_message('success', LANG('ntf_category_created_title'), LANG('ntf_category_created'));
            }else{
                $this->category_model->update($this->input->post(), $category_id,'Category');
                add_system_message('success', LANG('ntf_category_edited_title'), LANG('ntf_category_edited'));
            }
            $data['category'] =  new Category($this->input->post());

            if($close || $category_id === 'create'){
                redirect($data['close_link']);
            }
        } else {
            if (count($this->input->post()) > 0) {
                $data['category'] = new Category($this->input->post());
            } else {
                $data['category'] = $source;
            }
        }
        $data['category']->id = $category_id;
        $all_categories = $this->category_model->get_all_categories();
        $data['categories_tree'] = level_tree_from_arr($all_categories);
        $data['tovar_templates'] = $this->tovar_model->get_tovar_templates();
        $data['lang_exist'] = $this->lang_exist;
        $this->load->view('panel/main_layout',$data);
    }


}