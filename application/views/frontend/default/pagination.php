<?php
$options ='';
$action_link = (substr_count($page_link,'?') > 0)? $page_link .'&':$page_link .'/?';
?>
<div id="pagination">
    <div class="pstrnav program-pagination">
        <?for($i=1;$i <= $pages;$i++):?>

            <?if($i==1&&false):?>
                <?if($page != 1):?>
                    <span><a href="<?php echo $page_link?>" class="navig pagenav"><< <i class="fa fa-angle-double-left"></i></a></span>
                <?endif;?>
            <?endif;?>

            <?if($i==$page):?>
<!--                <span class="pagenav current-page">--><?php //echo $page .' '. LANG('pagination_from') .' '. $pages;?><!--</span>-->
                    <span><a href="<?php echo $action_link?>page=<?php echo $page;?>" class="navig current-page"><?php echo $page; ?></a></span>
            <?endif;?>

            <?if($i==$page-1):?>
                <span><a href="<?php echo $action_link?>page=<?php echo $page-1;?>" class="navig pagenav">Prev <i class="fa fa-angle-left"></i></a></span>
            <?endif;?>
            <?if($i==$page-1&&$pages>1&&$i>1):?>

                <span><a href="<?php echo $action_link?>page=1" class="navig first">1</a></span>
                <span>...</span>
            <?endif;?>
            <?if($i==$page-1):?>
                <span><a href="<?php echo $action_link?>page=<?php echo $page-1;?>" class="navig"><?php echo $page-1;?> </a></span>
            <?endif;?>
            <?if($i==$page+1):?>
                <span><a href="<?php echo $action_link?>page=<?php echo $page+1;?>" class="navig"><?php echo $page+1;?> </a></span>
            <?endif;?>
            <?if($i==$page+1&&$i<$pages):?>
                <?if($page != $pages):?>
                    <span>...</span>
                    <span><a href="<?php echo $action_link?>page=<?php echo $pages;?>" class="navig last"> <?php echo $pages; ?> <i class="fa"></i></a></span>
                <?endif;?>
            <?endif;?>
            <?if($i==$page+1):?>
                <span><a href="<?php echo $action_link?>page=<?php echo $page+1;?>" class="navig pagenav">Next <i class="fa fa-angle-right"></i></a></span>

            <?endif;?>


            <?if($i==$pages&&false):?>
                <?if($page != $pages):?>
                    <span><a href="<?php echo $action_link?>page=<?php echo $pages;?>" class="navig pagenav">>> <i class="fa fa-angle-double-right"></i></a></span>

                <?endif;?>
            <?endif;?>


            <?php
            $sel = ($i==$page)? 'selected="selected"':'';
            $options .='<option '. $sel .' value="'. $action_link .'page='. $i .'">'. $i .'</option>'?>
        <?endfor;?>
    </div>

    <!--<select onchange="location.href=this.value">
        <?php echo $options;?>
    </select>-->

</div>

