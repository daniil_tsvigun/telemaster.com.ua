<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                    <?php $this->load->view('panel/templates/action_button');?>
                    <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="general" href="#general" aria-expanded="true"><?php echo LANG('label_tab_general')?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active" role="tabpanel">
                            <div class="row">
                                <div class="col-md-8 inside_tab">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                            <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="data_<?php echo $lang_key;?>" href="#data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                            <?php $_first = false;
                                        endforeach;?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                            <div id="data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                                <?php if($lang_key=='ru'):
                                                    $sufix='';
                                                else :
                                                    $sufix='_'.$lang_key;
                                                endif; ?>
                                                <label for=""><?php echo LANG('label_name'); ?></label>
                                                <input type="text" placeholder="" class="input" value="<?php echo $data->{'name_'. $lang_key};?>" name="name<?php echo $sufix;?>">
                                                <label for=""><?php echo LANG('label_surname'); ?></label>
                                                <input type="text" placeholder="" class="input" value="<?php echo $data->{'surname_'. $lang_key};?>" name="surname<?php echo $sufix;?>">
                                            </div>

                                            <?php $_first = false;
                                        endforeach;?>
                                        <input type="hidden" value="<?php echo $data->active; ?>" name="active">
                                        <label for="">Email</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->email;?>" name="email">
                                        <label for=""><?php echo LANG('label_passport'); ?></label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->passport;?>" name="passport">
                                        <label for=""><?php echo LANG('label_passport_when'); ?></label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->passport_data;?>" name="passport_data">
                                        <label for="">ИНН</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->inn;?>" name="inn">
                                        <label for="">Тел. 1</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->phone1;?>" name="phone1">
                                        <label for="">Тел. 2</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->phone2;?>" name="phone2">
                                        <label for="">Тел. 3</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->phone3;?>" name="phone3">
                                        <label for="">Тел. 4</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->phone4;?>" name="phone4">
                                        <label for="">Сумма голосов</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->rate;?>" name="rate">
                                        <label for="">Количество проголосовавших</label>
                                        <input type="text" placeholder="" class="input" value="<?php echo $data->rate_amount;?>" name="rate_amount">
                                        <br>
                                        <select name="master_refuses">
                                            <option value="0"></option>
                                            <?php $user_refuse=(isset($data->master_refuses['id_refuses']))? $data->master_refuses['id_refuses']: 0; ?>
                                            <?php foreach ($data->refuses as $item):?>
                                                <?php $selected=($user_refuse==$item['id'])?'selected=="selected"':''; ?>
                                                <option <?php echo $selected; ?> value="<?php echo $item['id'] ?>"><?php echo $item['name_ru'];  ?></option>
                                            <?php endforeach;
                                            ?>
                                        </select>

                                        <input type="checkbox" value="yes" name="refuses_send_email">
                                        <label><?php echo LANG('label_send_refuse'); ?></label>
                                        <br>
                                        <?php //var_dump($data->refuses); ?>
                                        <label for="">Комментарий к причине</label>
                                        <textarea placeholder="" rows="1" cols="1" name="refuses_comment">
                                            <?php echo (isset($data->master_refuses['comment']))? $data->master_refuses['comment']: ""; ?>

                                        </textarea>
                                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_regions');?></h5>
                                        <input id="current_regions" value="" type="hidden">
                                        <div class="region-block">
                                            <div class="areas">
                                                <input name="areas[]" type="hidden">
                                                <p><?php echo LANG('label_enter_regions'); ?></p>
                                                <p><?php echo LANG('label_delivery_area'); ?></p>
                                                <ul>
                                                    <?php
                                                    if(isset($areas)):

                                                        foreach($areas as $area):
                                                            $active='';

                                                            foreach($data->regions as $item){
                                                               if($item->{'area_name'.SQL_LANG}==$area['area_name'.SQL_LANG]){
                                                                   $active='checked';
                                                                   if($data->id=='create'){
                                                                        $active='';
                                                                   }
                                                               }
                                                            }
                                                        ?>

                                                            <li>
                                                                <input <?php echo $active; ?> class="<?php echo $active; ?>" data-area_id="<?php echo $area['id']; ?>" name="areas[<?php echo $area['KOATUU']; ?>]" type="checkbox">
                                                                <label ><?php echo $area['area_name'.SQL_LANG]; ?></label>
                                                            </li>
                                                        <?php endforeach;
                                                    endif;
                                                    ?>
                                                </ul>
                                            </div>

                                            <div class="districts">
                                                <p><?php echo LANG('label_delivery_district'); ?></p>
                                                <div>
                                                    <span style="font-size: 1.16em; padding-right: 10px; font-weight: 600;" >Все</span><input type="checkbox" name="all">
                                                </div>
                                                <ul>

                                                </ul>
                                            </div>
                                        </div>
                                        </div>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                                    <div class="widget-body">
                                        <p><span style="min-width: auto; padding-right: 10px;"><? echo LANG ('label_id');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->id_master;?>" name="id_master"> </p>
                                        <?php
                                        if(isset($data->image)):
                                            if($data->image==NULL): ?>
                                                <div class="master_avatar">
                                                    <img class="img img-responsive" src="http://placehold.it/320x320" alt="">
                                                    <input name="image" type="file">
                                                </div>
                                        <?php
                                            else: ?>
                                                <div class="master_avatar">
                                                    <img style="margin: auto;" class="img img-responsive" src="<?php echo base_url("/images/avatars").'/'.$data->image['master_id'].'/'.$data->image['img_name'].'/d_'.$data->image['img_name'].'.'.$data->image['img_type']; ?>" alt="">
                                                    <input name="image" type="file">
                                                </div>
                                                <div data-master_id="<?php echo $data->image['master_id']; ?>" data-image_name="<?php echo $data->image['img_name']; ?>" id="master_avatar_remove" style="width: 100%; margin: 25px 0;" class="btn red">
                                                    <i class="icon-trash"></i>
                                                    <?php echo LANG('label_remove_photo'); ?>
                                                </div>
                                                <input style="float: left; margin-right: 15px;" value="yes" type="checkbox" name="send_refuse_photo" >
                                                <label style="display: block;" for=""><?php echo LANG('label_send_master_photo_refuse'); ?></label>
                                                <!-- <div class="buttons">
                                                    <div id="rotate_left"></div>
                                                    <div id="rotate_right"></div>
                                                </div> -->
                                            <?php endif;
                                        else: ?>
                                            <div class="master_avatar">
                                                <img class="img img-responsive" src="http://placehold.it/320x320" alt="">
                                                <input name="image" type="file">
                                            </div>
                                        <?php endif;
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
            </form>
        </div>
    </div>
</div>
<style>
    .master_avatar{
        position: relative;
    }
    .master_avatar input{
        opacity: 0;
        position: absolute;
        top: 0;
        width: 100%;
        height: 100%;
        cursor: pointer;
    }
    .region-block>div>p{
        display: block;
        font-size: 1.5em;
        font-weight: 200;
        color: #898a8a;
        margin: 0;
    }
    .region-block ul{
        padding: 0;
        list-style-position: inside;
        -moz-column-count: 3;
        -webkit-column-count: 3;
        column-count: 3;
        min-height: 150px;
    }
    .region-block ul>li{
        line-height: 30px;
    }
    .region-block input[type="checkbox"]{
        margin: 4px 0 0;
        line-height: normal;
        float: left;
        margin-right: 13px;
        width: 19px;
        height: 19px;
    }
    .form-wrapper label{
        display: block;
        font-size: 1.5em;
        font-weight: 200;
        color: initial;
        margin: 0;
    }
</style>
<script>
    var site_url = "<?php echo site_url();?>";
    $('input[name="all"]').change(function(){
        if($(this).prop('checked')==true){
            $('.districts input[type="checkbox"]').prop('checked',true);
        } else {
            $('.districts input[type="checkbox"]').prop('checked',false);
        }
    });
    $('.region-block .areas input[type="checkbox"]').click(function(){
        var areas=[];
        var current=[];

        $('.districts li').each(function(){
            if($(this).find('input').prop('checked')==true){
                current.push($(this).find('input').attr('data-koatuu'));
            }
        });
        $('.region-block .areas input[type="checkbox"]').each(function(){
            if($(this).prop('checked')==true){
                areas.push($(this).attr('data-area_id'));
            } else {
                $('.region-block .districts ul>li').remove();
            }
        });


        get_districts(areas,current);
    });

    $(document).ready(function(){

        $("#master_avatar_remove").click(function(){
            var ph_data={'id':$(this).attr('data-master_id'),'photo_name':$(this).attr('data-image_name')};
            var send_trigger=false;
            if($('input[name="send_refuse_photo"]').prop('checked')){
                send_trigger=true;
            }
            $.ajax({
                url: site_url+'/panel/masters/ajax_del_photo',
                type:"POST",
                data:{"data":ph_data,"send_message":send_trigger},
                success:function(result){
                    var obj = $.parseJSON(result);
                    if(obj.status=='success'){
                        $('.master_avatar img').attr('src','http://placehold.it/320x320');
                        $('#master_avatar_remove').hide();
                    }
                }
            });
        });
        var active=[];
        var current=[];
        var regions=<?php echo json_encode($data->regions);  ?>;
        for(var k=0;k<regions.length;k++){
            current.push(regions[k].district_id);
        }
        $('.region-block ul>li input').each(function(){
            if($(this).prop('checked')==true){
                active.push($(this).attr('data-area_id'));
            }

        });
        get_districts(active,current);
    });

    if (window.File && window.FileList && window.FileReader) {
        $('body').on('change', "input[name='image']", function (e) {
            Make_preview(e, $('.master_avatar img'));
        });
    }

//    $('.buttons div#rotate_left').click(function(){
//        $.ajax({
//            url: site_url +'/panel/masters/ajax_rotate_image',
//            type: 'POST',
//            data: {"data":data},
//            success: function (result){
//
//                var obj = $.parseJSON(result);
//
//                if(obj.status == 'success'){
//
//                }else{
//
//                }
//            }
//        });
//    });

    function Make_preview(input, obj) {
        var reader = new FileReader();
        var files = input.target.files || input.dataTransfer.files;
        reader.readAsDataURL(files[0]);
        reader.onload = function (e) {
            obj.attr('src', e.target.result);
        }
    }

    function get_districts(data,current){

        $.ajax({
            url: site_url +'/registration/ajax_get_districts',
            type: 'POST',
            data: {"data":data},
            success: function (result){

                var obj = $.parseJSON(result);
                var list= $('.region-block>div.districts ul');
                list.find('li').remove();
                if(obj.status == 'success'){
                    for(var i in obj.data){
                        if(obj.data[i].district_id!=45){
                            var prop='';
                            for(var j=0; j<current.length; j++){
                               if(current[j]==obj.data[i].KOATUU){
                                   prop='checked';
                               }
                            }
                            list.append('' +
                                '<li>' +
                                '   <input data-koatuu="'+obj.data[i].KOATUU+'" name="regions['+obj.data[i].KOATUU+']" type="checkbox" '+ prop +'>' +
                                '   <label for="">'+obj.data[i]["district_place"+obj.lang]+'</label>' +
                                '</li>');
                        }
                    }
                }else{

                }
            }
        });
    }
</script>