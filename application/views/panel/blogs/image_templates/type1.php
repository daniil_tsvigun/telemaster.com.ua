<h5 class="widget-name"><i class="icon-picture"></i><?php echo LANG('label_photo');?></h5>
<div class="row" style="margin-top:15px;">
    <div class="col-md-2"><?php echo LANG('label_photo');?></div>
    
<div class="col-md-2"><?php echo LANG('label_do');?></div>
</div>
<input name="images" value="<?php foreach($blog_images as $image){ echo $image['id'];} ?>" type="hidden">
<div class="media sortable ui-sortable" id="product-medias">
    <?php if(count($blog_images) > 0):?>
        <?php foreach($blog_images as $image):?>
            <div class="row stripped ui-sortable-handle">
                <div class="col-md-2" data-id="images[<?php echo $image['id'];?>]" data-product_id="images[<?php echo $image['blog_id'];?>]">
                    <img src="/images/blogs/<?php echo $image['blog_id'];?>/<?php echo $image['name'];?>/orig_<?php echo $image['name'];?>.<?php echo $image['type'];?>" style="display: block;margin: 0 auto;max-height:120px;">
                </div>
                
                <div class="col-md-2">
                    <a href="#" class="del" data-id="images[<?php echo $image['id'];?>]" data-product_id="images[<?php echo $image['blog_id'];?>]"><i class="icon-trash"></i></a>
                </div>
            </div>
        <? endforeach;?>
    <? endif;?>
</div>
<div class="media">
    <div class="row stripped" id="product-medias-new">
        <div class="col-md-2 upload_img_conteiner">
            <label class="upload_image_label btn green label-input-file" style="padding: 38px 57px;">
                <input style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" class="upload_image_button" name="photo_0" title="Upload images" type="file">
                <span><?php echo LANG('label_upload_photo');?></span>
            </label>
        </div>
    </div>
</div>
<div id="cropbox_wrap"></div>

<style>
    .upload_img_conteiner{
        height: 100px;
        width: 235px;
    }
    .upload_img_conteiner img{
        display: block;
        margin: 0 auto;
        max-height: 100%;
        max-width: 100%;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){

        var iter = 1;
        if (window.File && window.FileList && window.FileReader) {
            $('body').on('change',".upload_image_button",function(e){
                Make_preview(e,$(this).closest('.upload_image_label'));
                iter++;
            });
        }

        function Make_preview(input,obj) {
            var reader = new FileReader();
            var files = input.target.files || input.dataTransfer.files;

            if(obj.prev('.upload_image').length > 0){
                obj.prev('.upload_image').remove();
            }else{

                var str ='<div class="col-md-2 upload_img_conteiner" >';
                str +='<label class="upload_image_label btn green label-input-file" style="padding: 38px 57px;">';
                str +='<input style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" class="upload_image_button" multiple="" name="photo_'+ iter +'" title="Upload images" type="file">';
                str +='<span><?php echo LANG('label_upload_photo');?></span>';
                str +='</label>';
                str +='</div>';
                obj.closest('.upload_img_conteiner').after(str);

            }
            reader.readAsDataURL(files[0]);

            reader.onload = function (e) {
                obj.before('<image class="upload_image">');
                obj.prev('.upload_image').attr('src', e.target.result);
                obj.hide();
            }
        }

        $('body').on('click','.upload_image',function(){
            $(this).siblings('.upload_image_label').trigger('click');
        });

    });
</script>