<?php

class Menu {

    public $alias;
    public $title;



    public function __construct($source = null) {
        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->title              = isset($source['title'])?              $source['title']             :'';
    }
}

class Menu_model extends MY_Model {

    protected $table_name = 'menus';
    protected $t_menu_items = 'menu_items';
    protected $t_menu_item_type = 'menu_item_types';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_all_menu($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->table_name .'.*')->from($this->table_name)->get()->result_array();

        return $result;
    }

    public function get_menu_tree($alias){

        $menu_data = $this->get_by_alias($alias,true);
        if(!$menu_data){
            return false;
        }
        $menu_data_types = $this->_get_menu_item_types();
        if(!$menu_data_types){
            return false;
        }

        $menu_items = $this->menu_item_model->get_menu_item_by_menu($menu_data['id'],true);
        if(!$menu_items){
            return false;
        }

        foreach($menu_items as $item){
            $data[$item['id']] = array(
                        'id'=>$item['id'],
                        'parent_id'=>$item['parent_id'],
                        'item_type'=>$item['item_type'],
                        'sort'=>$item['sort'],
                         );
            $_res = $this->db->get_where($menu_data_types[$item['item_type']]['item_table'],array('id'=>$item['item_id']))->row_array();
            if(!$_res){
                unset($data[$item['id']]);
            }else{
                $data[$item['id']]['title'] = $_res['title_'. LANG];
                $data[$item['id']]['description'] = isset($_res['description_'. LANG])?$_res['description_'. LANG]:'';
                $data[$item['id']]['item_parent_id'] = isset($_res['parent_id'])?$_res['parent_id']:0;
                $data[$item['id']]['item_id'] = $_res['id'];
                $data[$item['id']]['alias'] = $_res['alias'];
                $data[$item['id']]['seo_title'] = isset($_res['seo_title_'. LANG])?$_res['seo_title_'. LANG]:'';
                $data[$item['id']]['seo_description'] = isset($_res['seo_description_'. LANG])?$_res['seo_description_'. LANG]:'';
                $data[$item['id']]['seo_keywords'] = isset($_res['seo_keywords_'. LANG])?$_res['seo_keywords_'. LANG]:'';
            }
        }

        return $data;

    }

    private function _get_menu_item_types(){

        $_result = $this->menu_item_model->get_menu_item_type();
        if(!$_result){
            return false;
        }

        foreach($_result as $item){
            $data[$item['id']] = $item;
        }

        return $data;
    }


}