<?

$lang['ntf_npt_auth_no_combo_title']  = 'Данна комбінація логіна і пароля не знайдена';

$lang['ntf_npt_open_admin_panel_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_open_admin_panel']        = 'Ви не можете увійти в панель керування.';

$lang['ntf_npt_roles_no_exist_title']  = 'Вказана роль не існує.';
$lang['ntf_npt_roles_no_exist']        = ' ';


$lang['ntf_npt_create_user_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_create_user_permissions']        = 'Ви не маєте прав на створення користувачів';

$lang['ntf_npt_edit_user_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_edit_user_permissions']        = 'Ви не маєте прав на редагування користувачів';

$lang['ntf_npt_changes_administrative_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_changes_administrative_permissions']        = 'Ви не маєте прав на сміну статуса користувачів ролі Адміністратор';
$lang['ntf_npt_remove_administrative_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_remove_administrative_permissions']        = 'Ви не маєте прав на видалення користувачів ролі Адміністратор';

$lang['ntf_npt_changes_buyer_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_changes_buyer_permissions']        = 'Ви не маєте прав на сміну статуса користувачів ролі Покупець';
$lang['ntf_npt_remove_buyer_permissions_title']   = 'В доступі відмовлено!';
$lang['ntf_npt_remove_buyer_permissions']         = 'Ви не маєте прав на видалення користувачів ролі Покупець';

$lang['ntf_npt_changes_painter_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_changes_painter_permissions']        = 'Ви не маєте прав на сміну статуса користувачів ролі Художник';
$lang['ntf_npt_remove_painter_permissions_title']   = 'В доступі відмовлено!';
$lang['ntf_npt_remove_painter_permissions']         = 'Ви не маєте прав на видалення користувачів ролі Художник';

$lang['ntf_npt_remove_partner_permissions_title']   = 'В доступі відмовлено!';
$lang['ntf_npt_remove_partner_permissions']         = 'Ви не маєте прав на видалення провайдерів';

$lang['ntf_category_created_title']        = 'Категорію успішно створено';
$lang['ntf_category_created']              = ' ';

$lang['ntf_master_created_title']        = 'Майстра успішно створено';
$lang['ntf_master_created']              = ' ';

$lang['ntf_category_edited_title']        = 'Категорію успішно відредаговано';
$lang['ntf_category_edited']              = ' ';

$lang['ntf_category_remove_title']        = 'Категорію успішно видалено';
$lang['ntf_category_remove']              = ' ';

$lang['ntf_buyer_title']                = 'Покупця створено';
$lang['ntf_buyer_created']              = ' ';

$lang['ntf_buyer_edited_title']        = 'Покупця успішно відредаговано';
$lang['ntf_buyer_edited']              = ' ';

$lang['ntf_buyer_remove_title']        = 'Покупця успішно видалено';
$lang['ntf_buyer_remove']              = ' ';

$lang['ntf_menu_created_title']        = 'Меню створено';
$lang['ntf_menu_created']              = ' ';

$lang['ntf_menu_edited_title']        = 'Меню успішно відредаговано';
$lang['ntf_menu_edited']              = ' ';

$lang['ntf_menu_remove_title']        = 'Меню успішно видалено';
$lang['ntf_menu_remove']              = ' ';

$lang['ntf_menu_item_created_title']        = 'Пункт меню створено';
$lang['ntf_menu_item_created']              = ' ';

$lang['ntf_menu_item_edited_title']        = 'Пункт меню успішно відредаговано';
$lang['ntf_menu_item_edited']              = ' ';

$lang['ntf_menu_item_remove_title']        = 'Пункт меню успішно видалено';
$lang['ntf_menu_item_remove']              = ' ';

$lang['ntf_blog_created_title']        = 'Запис успішно створено';
$lang['ntf_blog_created']              = ' ';

$lang['ntf_blog_edited_title']        = 'Запис успішно відредаговано';
$lang['ntf_blog_edited']              = ' ';

$lang['ntf_blog_remove_title']        = 'Запис успішно видалено';
$lang['ntf_blog_remove']              = ' ';

$lang['ntf_comment_created_title']        = 'Коментар успішно створено';
$lang['ntf_comment_created']              = ' ';

$lang['ntf_comment_edited_title']        = 'Коментар успішно відредаговано';
$lang['ntf_comment_edited']              = ' ';

$lang['ntf_comment_remove_title']        = 'Коментар успішно видалено';
$lang['ntf_comment_remove']              = ' ';

$lang['ntf_promo_created_title']        = 'Промо успішно створено';
$lang['ntf_promo_created']              = ' ';

$lang['ntf_promo_edited_title']        = 'Промо успішно відредаговано';
$lang['ntf_promo_edited']              = ' ';

$lang['ntf_promo_remove_title']        = 'Промо успішно видалено';
$lang['ntf_promo_remove']              =' ';

$lang['ntf_page_created_title']        = 'Сторінку успішно створено';
$lang['ntf_page_created']              = ' ';

$lang['ntf_page_edited_title']        = 'Сторінку успішно відредаговано';
$lang['ntf_page_edited']              = ' ';

$lang['ntf_page_remove_title']        = 'Сторінку успішно видалено';
$lang['ntf_page_remove']              = ' ';

$lang['ntf_credit_created_title']        = 'Списання успішно створено';
$lang['ntf_credit_created']              = ' ';

$lang['ntf_credit_edited_title']        = 'Списання успішно відредаговано';
$lang['ntf_credit_edited']              = ' ';

$lang['ntf_credit_remove_title']        = 'Списання успішно видалено';
$lang['ntf_credit_remove']              = ' ';

$lang['ntf_debit_created_title']        = 'Нарахування успішно створено';
$lang['ntf_debit_created']              = ' ';

$lang['ntf_debit_edited_title']        = 'Нарахування успішно відредаговано';
$lang['ntf_debit_edited']              = ' ';

$lang['ntf_debit_remove_title']        = 'Нарахування успішно видалено';
$lang['ntf_debit_remove']              = ' ';

$lang['ntf_order_created_title']        = 'Замовлення успішно створено';
$lang['ntf_order_created']              = ' ';

$lang['ntf_order_edited_title']        = 'Замовлення успішно відредаговано';
$lang['ntf_order_edited']              = ' ';

$lang['ntf_order_remove_title']        = 'Замовлення успішно видалено';
$lang['ntf_order_remove']              = ' ';

$lang['ntf_npt_create_category_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_create_category_permissions']        = 'Ви не маєте прав на створення категорії';
$lang['ntf_npt_edit_category_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_edit_category_permissions']        = 'Ви не маєте прав на редагування категорії';
$lang['ntf_npt_changes_category_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_changes_category_permissions']        = 'Ви не маєте прав на сміну статуса категорії';
$lang['ntf_npt_remove_category_permissions_title']   = 'В доступі відмовлено!';
$lang['ntf_npt_remove_category_permissions']         = 'Ви не маєте прав на видалення категорії';

$lang['ntf_filter_group_created_title']        = 'Групу фільтрів успішно створено';
$lang['ntf_filter_group_created']              = ' ';

$lang['ntf_filter_group_edited_title']        = 'Групу фільтрів успішно відредаговано';
$lang['ntf_filter_group_edited']              = ' ';

$lang['ntf_filter_group_remove_title']        = 'Групу фільтрів успішно видалено';
$lang['ntf_filter_group_remove']              = ' ';

$lang['ntf_npt_create_filter_group_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_create_filter_group_permissions']        = 'Ви не маєте прав на створення групи фільтрів';
$lang['ntf_npt_edit_filter_group_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_edit_filter_group_permissions']        = 'Ви не маєте прав на редагування групи фільтрів';
$lang['ntf_npt_changes_filter_group_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_changes_filter_group_permissions']        = 'Ви не маєте прав на сміну статуса групи фільтрів';
$lang['ntf_npt_remove_filter_group_permissions_title']   = 'В доступі відмовлено!';
$lang['ntf_npt_remove_filter_group_permissions']         = 'Ви не маєте прав на видалення групи фільтрів';

$lang['ntf_filter_created_title']        = 'Фільтр успішно створено';
$lang['ntf_filter_created']              = ' ';

$lang['ntf_filter_edited_title']        = 'Фільтр успішно відредаговано';
$lang['ntf_filter_edited']              = ' ';

$lang['ntf_filter_remove_title']        = 'Фільтр успішно видалено';
$lang['ntf_filter_remove']              = ' ';

$lang['ntf_product_edited_title']        = 'Продукт успішно відредаговано';
$lang['ntf_product_edited']              = ' ';

$lang['ntf_npt_create_filter_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_create_filter_permissions']        = 'Ви не маєте прав на створення фільтра';
$lang['ntf_npt_edit_filter_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_edit_filter_permissions']        = 'Ви не маєте прав на редагування фільтра';
$lang['ntf_npt_changes_filter_permissions_title']  = 'В доступі відмовлено!';
$lang['ntf_npt_changes_filter_permissions']        = 'Ви не маєте прав на сміну статуса фільтра';
$lang['ntf_npt_remove_filter_permissions_title']   = 'В доступі відмовлено!';
$lang['ntf_npt_remove_filter_permissions']         = 'Ви не маєте прав на видалення фільтра';

$lang['ntf_print_edited_title']='Принт успішно відредаговано';
$lang['ntf_print_edited']=' ';

$lang['ntf_print_created_title'] ='Принт створено';
$lang['ntf_print_created'] = ' ';

$lang['ntf_tovar_edited_title']='Товар успішно відредаговано';
$lang['ntf_tovar_edited']=' ';

$lang['ntf_tovar_created_title'] ='Товар створено';
$lang['ntf_tovar_created'] = ' ';

$lang['ntf_tovars_created_title'] ='Товари з принтом успішно строворено';
$lang['ntf_tovars_created'] = ' ';

$lang['ntf_tovars_created_title'] ='Товари з принтом успішно строворено';
$lang['ntf_tovars_created'] = ' ';

$lang['ntf_picture_confirm_status_confirm_title'] ='Коричтувачу надіслано повідомлення про підтвердження публікації';
$lang['ntf_picture_confirm_status_confirm'] = ' ';

$lang['ntf_picture_confirm_status_refuse_title'] ='Коричтувачу надіслано повідомлення про відмову в публікації';
$lang['ntf_picture_confirm_status_refuse'] = ' ';

$lang['ntf_picture_deleted_title'] ='Принт успішно видалено';
$lang['ntf_picture_deleted'] = ' ';

$lang['ntf_picture_edited_title'] ='Принт успішно відредаговано';
$lang['ntf_picture_edited'] = ' ';












$lang['ntf_role_edited_title']  = 'Роль успішно відредаговано';
$lang['ntf_role_edited']        = ' ';

$lang['ntf_role_edit_error_title']  = 'Во время редактирования произошла ошибка, обратитесь к администратору';
$lang['ntf_role_edit_error']        = ' ';

$lang['ntf_user_created_title']  = 'Користувача успішно строворено';
$lang['ntf_user_created']        = ' ';

$lang['ntf_user_edited_title']  = 'Користувача успішно відредаговано';
$lang['ntf_user_edited']        = ' ';

$lang['ntf_user_no_edit_title']  = 'Помилка. Данні не відредаговано';
$lang['ntf_user_no_edit']        = ' ';

$lang['ntf_error_user_remove_title']  = 'Під час видалення користувачів виникла помилка. Видалення не можливе';
$lang['ntf_error_user_remove']        = ' ';

$lang['ntf_error_user_remove_no_users_title']  = 'Помилка, ви не вибрали користувачів для видалення';
$lang['ntf_error_user_removeno_users_']        = ' ';

$lang['ntf_user_remove_title']  = 'Користувачі успішно видалені';
$lang['ntf_user_remove']        = ' ';

$lang['ntf_master_remove_title']	= 'Майстра успішно видалено';
$lang['ntf_master_remove']			= ' ';

$lang['ntf_partner_remove_title']	= 'Провайдера успішно видалено';
$lang['ntf_partner_remove']			= ' ';

$lang['ntf_partner_edited_title']  = 'Провайдера успішно відредаговано';
$lang['ntf_partner_edited']        = ' ';