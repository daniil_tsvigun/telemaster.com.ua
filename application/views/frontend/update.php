<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content registration">
        <div class="wrapper">
            <div class="main-block">
                <h2><?php echo LANG('label_master_reg'); ?></h2>

                <div class="form-wrapper">
                    <form id="register-form" action ="<?php echo site_url('/registration/update');?>" method="post" accept-charset="utf-8">
                        <div class="main">
                            <?php
                            $masters=$this->master_model->get_all_masters();
                            ?>
                            <select name="masters" id="masters">
                                <option value="0">NULL</option>
                                <?php foreach($masters as $master): ?>
                                    <option value="<?php echo $master['id']; ?>"><?php echo $master['name'].' '.$master['surname'].' '.$master['id_master']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="masteeer">

                            <?php foreach($masters as $master): ?>
                            <div data_id="<?php echo $master['id']; ?>">
                                <?php $r=$this->master_model->get_master_by_id($master['id']); ?>
                                <?php echo '<strong>'.$r->id.' '.$r->name.' '.$r->surname.'</strong> '; ?>
                                <?php
                                if($r->regions!=null) {
                                    echo '<ul>';
                                    foreach ($r->regions as $r) {
                                        if (isset($r->district_place_ru)) {
                                            echo '<li>'.$r->district_place_ru.'</li>';
                                        }
                                    }
                                    echo '</ul>';
                                }
                                ?>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="row region-block">
                            <div class="areas">
                                <input name="areas[]" type="hidden">
                                <p><?php echo LANG('label_enter_regions'); ?></p>
                                <p><?php echo LANG('label_delivery_area'); ?></p>

                                <ul>

                                    <?php
                                    if(isset($areas)):
                                        foreach($areas as $area): ?>
                                            <li>
                                                <input data-area_id="<?php echo $area['id']; ?>" name="areas[<?php echo $area['KOATUU']; ?>]" type="checkbox">
                                                <label ><?php echo $area['area_name'.SQL_LANG]; ?></label>
                                            </li>
                                        <?php endforeach;
                                    endif;
                                    ?>

                                </ul>
                            </div>
                            <div class="districts">
                                <p><?php echo LANG('label_delivery_district'); ?></p>
                                <div>
                            <span style="
                            font-size: 1.5em;
                            padding-right: 10px;
                            " >Все</span><input type="checkbox" name="all">

                                </div>
                                <ul>

                                </ul>

                            </div>
                        </div>
                        <div class="row">
                            <p class="message hidden"><?php echo LANG('label_form_error'); ?></p>
                            <button id="upd" form="register-form" type="submit" class="btn btn-type3"><?php echo LANG('label_action_registration'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!--<div class="content">-->
<!---->
<!---->
<!--    <form enctype="multipart/form-data" method="post" accept-charset="utf-8" action="--><?php //echo site_url('/registration/up'); ?><!--">-->
<!--        <input name="file" type="file">-->
<!--        <input name="master_id"  type="number">-->
<!--        <button type="submit">YEs</button>-->
<!--    </form>-->
<!--</div>-->

<?php $this->load->view('frontend/default/footer'); ?>