<?php

class Blog_image_model extends CI_Model {

    protected $t_blogs_images = 'blogs_images';

    public function __construct() {
        parent::__construct();

    }

    public function get_image_by_blog($blog_id = false){
        if(!$blog_id){
            return array();
        }

        $result = $this->db->where(array('blog_id'=>$blog_id))->get($this->t_blogs_images)->result_array();
        return $result;
    }
    
    public function get_all_blog_images(){
        $result = $this->db->get($this->t_blogs_images)->result_array();
        return $result;
    }

}
