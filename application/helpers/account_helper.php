<?php
if ( ! function_exists('get_user')) {
    function get_user($admin=false){

        $_ses = ($admin)? 'user_admin':'user';
        $user = isset($_SESSION[$_ses])? $_SESSION[$_ses]:false;

        if(!$user){
            $user = array(
                'user_name'=>'',
                'user_password'=>'',
                'role_id'=>1,
                'role'=>'anonymous',
            );
        }

        $ci =& get_instance();

        switch ($user['role']){
            case 'administrative':
                $user = $ci->administrative_model->get_user($user,'administrative');
                break;

            // case 'buyer':
            //     $user = $ci->buyer_model->get_user($user,'buyer');
            //     break;

            // case 'painter':
            //     $user = $ci->painter_model->get_user($user,'painter');
            //     break;

            case 'anonymous':
            default:
                $user = $ci->anonymous_model->get_user($user,'anonymous');
                break;
        }

        return $user;

    }
}

if ( ! function_exists('add_system_message')) {
    function add_system_message($type, $title, $text){
        $ci =& get_instance();
        if (!isset($_SESSION['messages'])){
            $_SESSION['messages'] = [];
        }
        $_SESSION['messages'][] = ['type' => $type, 'title' => $title, 'text' => $text];
    }
}

if ( ! function_exists('get_system_messages')) {
    function get_system_messages(){
        $ci =& get_instance();
        if (!isset($_SESSION['messages'])){
            $_SESSION['messages'] = [];
        }
        $messages = $_SESSION['messages'];
        $_SESSION['messages'] = [];
        return $messages;
    }
}

if ( ! function_exists('has_permission')) {
    function has_permission($permission, $user = null){

        $user = ($user) ? $user : get_user();
        return in_array($permission,$user->permissions)? true:false;
    }
}


if ( ! function_exists('redirect_has_no_permission')) {
    function redirect_has_no_permission($user,$permission, $url, $type = null, $title = null, $message = null){
        if (!has_permission($permission,$user)) {
            if ($type === null or $title === null or $message === null) {
                add_system_message('danger', LANG('ntf_npt_'.$permission.'_title'), LANG('ntf_npt_'.$permission));
            } else {
                add_system_message($type, $title, $message);
            }
            if (!headers_sent()) {
                redirect($url, 'location');
            } else {
                redirect($url, 'refresh');
            }
        }
    }
}

if ( ! function_exists('auth_user')) {
    function auth_user($email,$password){
        $ci =& get_instance();

        $user = null;
        if (($user = $ci->administrative_model->get_user_by_email($email)) !== null) {
            $user['role']  = 'administrative';
        } 
        // elseif (($user = $ci->painter_model->get_user_by_email($email)) !== null) {
        //     $user['role']  = 'painter';
        // } elseif (($user = $ci->buyer_model->get_user_by_email($email)) !== null) {
        //     $user['role']  = 'buyer';
        // }

        if($user === null){
            add_system_message("danger", LANG("ntf_npt_auth_no_combo_title"), LANG("ntf_npt_auth_no_combo_exist"));
            return false;
        }

        $pass_hesh = md5($password . DEFAULT_SALT);
        if($pass_hesh != $user['password']){
            add_system_message("danger", LANG("ntf_npt_auth_no_combo_title"), LANG("ntf_npt_auth_no_combo_exist"));
            return false;
        }

        return $user;

    }
}

if (!function_exists('data_is_email')){
    function data_is_email($data=false) {
        if(preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $data)){
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('data_is_phone')){
    function data_is_phone($data=false) {
        if(preg_match("/^([0-9\(\)\/\+ \-]*)$/", $data)){
            return true;
        } else {
            return false;
        }
    }
}