<?php

if ( ! function_exists('panel_left_menu')) {
    function panel_left_menu(){
        $ci =& get_instance();
        $data['left_menu'] = $ci->panel_model->left_menu();
        $data['active'] = $ci->uri->segment(2);
        $ci->load->view('panel/default/left_menu',$data);
    }
}


if ( ! function_exists('pagination')) {
    function pagination($table,$parametrs, $page, $items_on_page, $href){
        $CI =& get_instance();

        if(count($parametrs) > 0){
            foreach($parametrs as $place => $param){
                $CI->db->where($place . $param);
            }
        }

        $num_rows = $CI->db->get($table)->num_rows();
        if($num_rows <= $items_on_page)
            return '';

        return $CI->load->view('panel/default/pagination',array('pages'=>ceil($num_rows/$items_on_page),'page'=>$page,'page_link'=>$href),true);
    }
}
if ( ! function_exists('get_pagination_data')) {
    function get_pagination_data($default_count = DEFAULT_PAGINATION_COUNT,$default_sort = DEFAULT_SORT){

        $data['sort'] = (isset($_GET['sort']))? $_GET['sort']:$default_sort;
        $data['count'] = (isset($_GET['count']))? (int)$_GET['count']:$default_count;
        $data['page'] = (isset($_GET['page']))? (int)$_GET['page']:1;
        
        return $data;
    }
}

if ( ! function_exists('make_link_by')) {
    function make_link_by($base_link,$ignore_param){

        if(count($_GET) == 0){
            return $base_link;
        }
        $_data = array();
        foreach($_GET as $key => $val){
            if(in_array($key,$ignore_param)){
                continue;
            }
            $_data[] = $key .'='. $val;
        }
        if(count($_data) == 0){
            return $base_link;
        }

        $data = implode("&",$_data);
        $link = $base_link .'/?'.$data;

        return $link;
    }
}

if ( ! function_exists('get_img_params')) {
    function get_img_params($file){
        switch (strtolower(preg_replace('/^(.+)\.(jpg|jpeg|png|gif)$/si', '$2', $file['name']))) {
            case 'jpg':
            case 'jpeg':
                $img = imagecreatefromjpeg($file['tmp_name']); // Создаем временный файл

                break;
            case 'png':
                $img = imagecreatefrompng($file['tmp_name']); // Создаем временный файл

                break;
            case 'gif':
                $img = imagecreatefromgif($file['tmp_name']); // Создаем временный файл
                break;

            default:
                $img = false;
                break;
        }

        if($img){
            $data = array(
                'error' => false,
                'size_x'=>imagesx($img),
                'size_y'=>imagesy($img),
            );
        }else{
            $data = array(
                'error' => true,
                'message' => LANG('error_photo_format_no_confirm') .' '. $file['name']
            );
        }
        unset($img);
        return $data;
    }
}
