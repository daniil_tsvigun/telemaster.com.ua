<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                    <?php $this->load->view('panel/templates/action_button');?>
                    <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="general" href="#general" aria-expanded="true"><?php echo LANG('label_tab_general')?></a></li>
                        <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="seo" href="#seo" aria-expanded="false"><?php echo LANG('label_tab_seo')?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active" role="tabpanel">
                            <div class="row">
                                <div class="col-md-8 inside_tab">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_print_and_product');?></h5>
                                    <div class="row">
                                        <div class="col-md-6 inside_tab">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_product');?></h5>
                                            <?php $this->load->view('panel/templates/select_photo',array('select_data'=>$product_list,'name'=>'product_id','selected'=> $data->product_id,'params' => array('no_all'=>'yes','type'=>'products')));?>
                                            <div class="row">
                                                <div class="col-md-12 inside_tab">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 inside_tab">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_picture');?></h5>
                                            <?php $this->load->view('panel/templates/select_photo',array('select_data'=>$picture_list,'name'=>'picture_id','selected'=> $data->picture_id,'params' => array('no_all'=>'yes','type'=>'pictures')));?>
                                            <div class="row">
                                                <div class="col-md-12 inside_tab">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_tovar_preview');?></h5>
                                    <div class="row">
                                        <div id="tovar_preview" class="col-md-12 inside_tab">
                                            <div id="tovar_preview_print" style="width: 800px;height: 800px;">
                                                <img id="tovar_preview_product" src="" style="position:relative;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 inside_tab">
                                    <?php if($data->id != 'create'):?>
                                        <ul role="tablist" class="nav nav-tabs">
                                            <?php $_first = true;?>
                                            <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                                <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="data_<?php echo $lang_key;?>" href="#data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                                <?php $_first = false;
                                            endforeach;?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php $_first = true;?>
                                            <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                                <div id="data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                                    <input type="text" placeholder="" class="input" value="<?php echo $data->{'title_'. $lang_key};?>" name="title_<?php echo $lang_key;?>"><br>
                                                 </div>
                                                <?php $_first = false;
                                            endforeach;?>
                                        </div>
                                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                                        <p><span><? echo LANG ('label_alias');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->alias;?>" name="alias"> </p>

                                        <div id="base_tovar_parametrs">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_photo_generation');?></h5>
                                            <p class="red-danger"><?php echo LANG('label_attention_another_product_or_picture');?></p>
                                            <p id="tovar_photo_generation" class="cform btn green" style="float:none;display: inline-block;">
                                                <i class="icon-ok"></i>
                                                <span><?php echo LANG('label_tovar_photo_generation_action');?></span>
                                            </p>
                                        </div>
                                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_photo_list');?></h5>
                                        <div id="view_tovar_image_list"class="row ">
                                            <span>loading...</span>
                                        </div>
                                    <?php else:?>
                                        <input type="hidden" name="alias" value="create">
                                        <input type="hidden" name="title_ru" value="create">
                                        <input type="hidden" name="title_ua" value="create">
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <div id="seo" class="tab-pane" role="tabpanel">
                            <div class="row inside_tab">
                                <ul role="tablist" class="nav nav-tabs">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="seo_data_<?php echo $lang_key;?>" href="#seo_data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                        <?php $_first = false;
                                    endforeach;?>
                                </ul>
                                <div class="col-md-8 tab-content">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <div id="seo_data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input name="seo_title_<?php echo $lang_key;?>" type="text" placeholder="" class="input" value="<?php echo $data->{'seo_title_'. $lang_key};?>" ><br>
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_keywords');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input name="seo_keywords_<?php echo $lang_key;?>" type="text" placeholder="" class="input" value="<?php echo $data->{'seo_keywords_'. $lang_key};?>" ><br>
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_description');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <textarea name="seo_description_<?php echo $lang_key;?>" id="description" aria-hidden="true"><?php echo $data->{'seo_description_'. $lang_key};?></textarea>
                                        </div>
                                        <?php $_first = false;
                                    endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input id="tovar_category_id" type="hidden" name="category_id" value="<?php echo $data->category_id;?>">
            </form>
        </div>
    </div>
</div>
<div>
    <div id="ajax_preloader_background" class="ajax_hidden"></div>
    <img id="ajax_preloader_image" src="/assets/img/ajax_loader.gif" class="ajax_hidden">
</div>

<script type="text/javascript">
    $(document).ready(function(){

        var base_tovar_product = <?php echo $data->product_id;?>;
        var base_tovar_picture = <?php echo $data->picture_id;?>;
        var base_tovar_id = "<?php echo $data->id;?>";

        view_prototipe();
        view_tovar_image_list();

        $('body').on('change','.select_photo',function(){
            var src = $(this).find('option[value="'+ $(this).val()+'"]').data('src');
            $(this).closest('.inside_tab').find('.preview_photo').attr('src',src);
            view_prototipe();
        });

        $('body').on('change','#select_photo_products',function(){

            $.ajax({
                url: '<?php echo site_url(); ?>'+'/panel/tovars/ajax_get_prints',
                type: 'POST',
                dataType:'html',
                data:{template:$(this).find('option[value="'+ $(this).val()+'"]').data('template-id')},
                beforeSend:function(){
                    startLoadingAnimation();
                },
                success: function (result) {
                    $('#select_photo_pictures').replaceWith(result);
                    view_prototipe();
                    stopLoadingAnimation();
                },
                error:function(){
                    stopLoadingAnimation();
                }
            });

        });

        $('body').on('click','#tovar_photo_generation',function(){

            var product_id = $('#select_photo_products').val();
            var picture_id = $('#select_photo_pictures').val();
            var tovar_id = base_tovar_id;

            $.ajax({
                url: '<?php echo site_url(); ?>'+'/panel/tovars/ajax_make_tovar_image',
                type: 'POST',
                data:{"product_id":product_id,"picture_id":picture_id,"tovar_id":tovar_id},
                beforeSend:function(){
                    startLoadingAnimation();
                },
                success: function (result) {
                    var obj = $.parseJSON(result);
                    if(obj.error !=''){
                        error_message_clear();
                        error_message_push(obj.error);
                        stopLoadingAnimation();
                        return;
                    }
                    if(obj.callback == 'redirect'){
                        page_redirect(obj.redirect_link);
                    }
                    $('#base_tovar_parametrs').hide();
                    view_tovar_image_list();
                    stopLoadingAnimation();
                },
                error:function(){
                    stopLoadingAnimation();
                }
            });

        });

        function view_tovar_image_list(){

            $.ajax({
                url: '<?php echo site_url(); ?>'+'/panel/tovars/ajax_get_tovar_images_list',
                type: 'POST',
                data:{"tovar_id":base_tovar_id},

                success: function (result) {
                   $('#view_tovar_image_list').html(result);
                }

            });

        }

        function view_prototipe(){
            var product_obj = $('#select_photo_products').find('option[value='+ $('#select_photo_products').val() +']');
            var picture_obj = $('#select_photo_pictures').find('option[value='+ $('#select_photo_pictures').val() +']');

            if($('#base_tovar_parametrs').length){
                if(base_tovar_product != product_obj.attr('value') || base_tovar_picture != picture_obj.attr('value')){
                    $('#base_tovar_parametrs').show();
                }else{
                    $('#base_tovar_parametrs').hide();
                }
            }


            $('#tovar_preview_product').attr('src',product_obj.data('src'));
            $('#tovar_category_id').val(product_obj.data('category-id'));
            var print = $('#tovar_preview_print');
            print.css('background','url("'+ picture_obj.data('src') +'")');
            print.css('background','rgba(0, 0, 0, 0) url("'+ picture_obj.data('src') +'") no-repeat scroll '+ product_obj.data('print-offset-x') +'px '+ product_obj.data('print-offset-y') +'px / '+ product_obj.data('print-size-x') +'px '+ product_obj.data('print-size-y') +'px');

        }

    });


</script>