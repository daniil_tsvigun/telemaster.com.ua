<?php
class Role {

	public $permissions;

    public function __construct($source = null) {
        $this->permissions = (isset($source['permissions'])) ? $source['permissions'] : array();

    }
}

class Role_model extends CI_Model {

	protected $table_name = 'roles';
	protected $role_to_permission_table = "role_to_permission";
	protected $permission_table = "permissions";

	public function __construct() {
		parent::__construct();
	}

    public function get_role_by_id($id) {
        return $this->get_by_id($id);
    }

	public function get_role_by_name($name) {
		$q = $this->db->get_where($this->table_name, ['name' => $name]);
		return ($q->num_rows() === 1) ? $q->row(): null;
	}

	public function get_by_id($id) {
		$q = $this->db->get_where($this->table_name, ['id' => $id]);

        if(($q->num_rows() !== 1)){
            return null;
        }
        $res = $q->row();
        $res->permissions = $this->get_role_permissions_by_id($res->id);

		return $res;
	}

    public function get_role_permissions_by_name($role_name){
        $role_id = $this->get_role_by_name($role_name)->id;
        return $this->get_role_permissions_by_id($role_id);

    }

	public function get_role_permissions_by_id($role_id) {
        $res = $this->db->select(
                    $this->role_to_permission_table.'.*,'.
                    $this->permission_table.'.name'
                        )
                        ->from($this->role_to_permission_table)
                        ->join($this->permission_table, $this->permission_table.'.id ='. $this->role_to_permission_table .'.permission_id','left')
                        ->where($this->role_to_permission_table.'.role_id = '. $role_id)
                        ->get()
                        ->result_array();

		$result = [];

		foreach ($res as $permission) {
			$result[$permission['permission_id']] = $permission['name'];
		}
		return $result;
	}

    public function get_all_roles($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }

		$result = [];
		$pre = $this->db->get($this->table_name)->result_array();
		foreach ($pre as $role) {
			$result[$role['id']] = $role;
			$result[$role['id']]['title'] = $role['title'. SQL_LANG];
		}
		return $result;
	}

    public function update_role($source, $role_id) {
        $_role = $this->get_role_by_id($role_id);
        if ($_role !== null) {
            $role = new Role($source);
            $permissions = $role->permissions;
            unset($role->permissions);
            //$this->db->update($this->table_name, $role, ['id' => $role_id]);
            $role->id = $role_id;
            $this->db->where(array('role_id'=>$role_id))->delete($this->role_to_permission_table);
            if(count($permissions) > 0){
                foreach($permissions as $permission_id){
                    $role_to_permission[$permission_id]['role_id'] = $role_id;
                    $role_to_permission[$permission_id]['permission_id'] = $permission_id;
                }
                $this->db->insert_batch($this->role_to_permission_table,$role_to_permission);
            }
            return true;
        }
        return false;
    }
}
