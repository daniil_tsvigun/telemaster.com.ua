<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content single-instruction">
        <div class="wrapper">
            <div class="main-block">
                <h2><?php echo LANG('label_instructions'); ?></h2>
                <div class="text">
                    <iframe width="1265" height="712" src="https://www.youtube.com/embed/<?php echo isset($model)?$model:''; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/default/footer'); ?>