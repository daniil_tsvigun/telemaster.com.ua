<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();

    }

    protected $sort_data=array(
        0=>'Megogo',
        1=>'Домашнє ТБ',
        2=>'Oll.tv'
    );

    public function index(){

        $this->load->view('frontend/calling_partner');
    }

    public function region($region=false){
        if($region==false){
            redirect(site_url('calling'));
        } else {
            $data=array();
            $regions['id']=$region;
            $data['regions']=$this->region_model->get_region_by_id($region);
            $providers=$this->partner_model->get_partners_by_region($regions);
            if(isset($this->sort_data)){
                $result_array=array();
                foreach($this->sort_data as $index=>$sort){
                    foreach($providers as $key=>$p) {
                        if ($p->name == $this->sort_data[$index]) {
                            $result_array[] = $providers[$key];
                            unset($providers[$key]);
                        }
                    }
                }
                $res=array_merge($result_array,$providers);
                $data['masters']=$res;
            } else{
                $data['masters']=$providers;
            }

            $this->load->view('frontend/regions_partner',$data);
        }

    }
    //ajax functions

    public function ajax_search_regions(){
        $keyword=$this->input->post('keyword');
        $result = array('status'=>'error');
        if( !text_validate($keyword)){
            echo json_encode($result);
            return;
        }
        $status = $this->region_model->search_region_by_name($keyword);
        if($status){

            $result = array(
                'status'=>'success',
                'data'=>$status,
                'lang'=>SQL_LANG,
                'label_obl'=>LANG('label_obl'),
                'label_district'=>LANG('label_district'),
            );
        }
        echo json_encode($result);
        return;
    }

}