<?php
$lang['label_password'] = 'Пароль';

$lang['label_auth']                                 = 'Вход';
$lang['label_auth_fb']                              = 'Вход с помощью facebook';
$lang['label_registration']                         = 'Регистрация';
$lang['label_buyer']                                = 'Покупатель';
$lang['label_painter']                              = 'Художник';
$lang['label_params']                               = 'Параметры';
$lang['label_forgot']                               = 'Забыли?';
$lang['label_name']                                 = 'Имя';
$lang['label_surname']                              = 'Фамилия';
$lang['label_title']                                = 'Название';
$lang['label_description']                          = 'Описание';
$lang['label_content']                              = 'Контент';
$lang['label_seo_title']                            = 'SEO title';
$lang['label_seo_keywords']                         = 'SEO keywords';
$lang['label_seo_description']                      = 'SEO description';
$lang['label_alias']                                = 'Alias';
$lang['label_article']                              = 'Актикул';
$lang['label_prices']                               = 'Цены';
$lang['label_price']                                = 'Цена базовая';
$lang['label_price_sale']                           = 'Цена со скидкой';
$lang['label_flag_new']                             = 'Новый';
$lang['label_flag_action']                          = 'Акция';
$lang['label_flag_sale']                            = 'Распродажа';
$lang['label_sort']                                 = 'Позиция';
$lang['label_category']                             = 'Категория';
$lang['label_mockup']                               = 'Макет';
$lang['label_preview']                              = 'Превью';
$lang['label_parent_category']                      = 'Родительская категория';
$lang['label_parent_filter_category']               = 'В категориях';
$lang['label_parent_filter_group']                  = 'Группа фильтров';
$lang['label_no_parent']                            = '---';
$lang['label_no_select']                            = 'не выбрано';
$lang['label_email']                                = 'Email';
$lang['label_login']                                = 'Логін';
$lang['label_author']                               = 'Автор';
$lang['label_mobile_phone']                         = 'Мобильный телефон';
$lang['label_contacts']                             = 'Контакты';
$lang['label_password']                             = 'Пароль';
$lang['label_password_repeat']                      = 'Повторите пароль';
$lang['label_enter_password']                       = 'Введите пароль, если необходимо';
$lang['label_parent_menu_item']                     = 'Родительский элемент';
$lang['label_menu']                                 = 'Меню';
$lang['label_template']                             = 'Шаблон';
$lang['label_tovat_template']                       = 'Шаблон товара';
$lang['label_do']                                   = 'Действия';
$lang['label_photo']                                = 'Фото';
$lang['label_main_photo']                           = 'Головне';
$lang['label_color']                                = 'Колір';
$lang['label_size']                                 = 'Розмір';
$lang['label_photo']                                = 'Фото';
$lang['label_filters']                              = 'Фільтри';
$lang['label_photo_parametrs']                      = 'Розміри запринтовки';
$lang['label_upload_photo']                         = 'Додати фото';
$lang['label_subtitle']                             = 'Підзаголовок';
$lang['label_ua']                                   = 'Укр';
$lang['label_ru']                                   = 'Рус';
$lang['label_en']                                   = 'Eng';
$lang['label_print_size_x']                         = 'Розмір принта(px) на фото вісь X';
$lang['label_print_size_y']                         = 'Розмір принта(px) на фото вісь Y';
$lang['label_print_offset_x']                       = 'Відступ принта(px) на фото вісь X';
$lang['label_print_offset_y']                       = 'Відступ принта(px) на фото вісь Y';
$lang['label_print_and_product']                    = 'Продукт і принт';
$lang['label_product']                              = 'Продукт';
$lang['label_picture']                              = 'Принт';
$lang['label_tovar_preview']                        = 'Пред. перегляд товара';
$lang['label_photo_list']                           = 'Список зображень товара';
$lang['label_photo_generation']                     = 'Генерація зображень товара';
$lang['label_attention_another_product_or_picture'] = 'Увага! Було змінено продукт чи принт для товара! Щоб зміни вступили в силу, вам необхідно перегенерувати зображення.';
$lang['label_tovar_photo_generation_action']        = 'Генерувати зображення товара';
$lang['label_filter_parametr']                      = 'Параметр';
$lang['label_filter_group_template']                = 'Шаблон(если необходимо)';
$lang['label_count']                                = 'Кількість';
$lang['label_price_for_one']                        = 'Ціна за одиницю';
$lang['label_for_tovars']                           = 'За товари';
$lang['label_count_in_cart']                        = 'одиниць в корзині';
$lang['label_tovar_in_cart_edit']                   = 'змінити';
$lang['label_delivery']                             = 'Доставка';
$lang['label_for_present']                          = 'В подарунок';
$lang['label_postal_and_pack']                      = '(Листівка і пакування)';
$lang['label_to_do']                                = 'Активувати';
$lang['label_all_price']                            = 'Загальна сумма';
$lang['label_main_client_info']                     = 'Основна інформація про кліента';
$lang['label_word']                                 = 'Промо код';
$lang['label_value']                                = 'Значення';
$lang['label_promo_type']                           = 'Тип значення';
$lang['label_promo_categories']                     = 'Вибрані категорії';
$lang['label_promo_one_time']                       = 'Тип використання';
$lang['label_percent']                              = 'Відсоток';
$lang['label_cashe']                                = 'Гроші';
$lang['label_profile_data']                         = 'Інформація про мастера';
$lang['label_profile_config']                       = 'Налаштування профіля';
$lang['label_profile_config_main']                  = 'Основні';
$lang['label_profile_config_user_name']             = 'І`мя користувача';
$lang['label_profile_config_user_url']              = 'Унікальний URL користувача';
$lang['label_profile_config_user_city']             = 'Місто';
$lang['label_profile_config_user_country']          = 'Країна';
$lang['label_profile_config_user_description']      = 'Додатково';
$lang['label_profile_config_user_new_pass']         = 'Новий пароль';
$lang['label_profile_config_user_old_pass']         = 'Старий пароль';
$lang['label_profile_config_user_subscription']     = 'Я хочу отримувати розсилку';
$lang['label_profile_config_user_change_password']  = 'Змінити пароль';
$lang['label_profile_config_user_save']             = 'Зберегти';
$lang['label_profile_config_user_vk_link']          = 'vk.com/';
$lang['label_profile_config_user_fb_link']          = 'fb.com/';
$lang['label_profile_config_user_instagram_link']   = 'instagram.com/';
$lang['label_profile_config_user_tw_link']          = 'twitter.com/';
$lang['label_profile_config_user_behance_link']     = 'behance.com/';
$lang['label_hello']                                = 'Вітаємо';
$lang['label_enter_auth_data']                      = 'Введіть ваші данні для входу';
$lang['label_enter_registration_data']              = 'Введіть ваші данні для реєстрації';
$lang['label_client_surname']                       = 'Фамилия';
$lang['label_client_name']                          = 'Имя';
$lang['label_client_phone']                         = 'Телефон';
$lang['label_client_email']                         = 'Email';
$lang['label_client_profile']                       = 'Профиль пользователя';
$lang['label_client_no_reg']                        = 'Анонимный пользователь';
$lang['label_client_reg']                           = 'Пользователь (ID: %s) ';
$lang['label_delivery_info']                        = 'Реквизиты доставки';
$lang['label_tovars_in_order']                      = 'Товары в заказе';
$lang['label_order_progress']                       = 'Прогрес статуса замовлення';
$lang['label_order_main_info']                      = 'Основна інформація замовлення';
$lang['label_order_price_base']                     = 'Базова ціна замовлених товарів';
$lang['label_order_price_delivery']                 = 'Вартість доставки';
$lang['label_order_price_discount']                 = 'Знижка по промо коду';
$lang['label_order_price_final']                    = 'Остаточна ціна замовлення';
$lang['label_tovar_price_position']                 = 'Вартість';
$lang['label_tovar_filters']                        = 'Опції';
$lang['label_tovar_postal']                         = 'Листівка';
$lang['label_tovar_pack']                           = 'Упаковка';
$lang['label_tovar_price']                          = 'Ціна';
$lang['label_gender_associated']                    = 'Гендерна асоціація';
$lang['label_gender_style']                         = 'Стиль';
$lang['label_male']                                 = 'Мужчина';
$lang['label_female']                               = 'Женщина';
$lang['label_kid']                                  = 'Ребенок';
$lang['label_more_for_prints']                      = 'Принт також доступний';
$lang['label_more_for_artist']                      = 'Принти художника';
$lang['label_more_viewed']                          = 'Нещодавно переглянуті';
$lang['label_one_time']                             = 'Єдиноразова';
$lang['label_multi_time']                           = 'Багаторазова';
$lang['label_back_to_shopping']                     = 'Назад до покупок';
$lang['label_enter_promo_code']                     = 'введіть промокод';
$lang['label_cart_steep_cart']                      = 'Ваша корзина';
$lang['label_cart_steep_ship']                      = 'Деталі доставки';
$lang['label_cart_steep_pay']                       = 'Оплата';
$lang['label_block_cart']                           = 'Замовлення';
$lang['label_block_ship']                           = 'Доставка';
$lang['label_block_pay']                            = 'Олата';
$lang['label_block_thank']                          = 'Завершити';
$lang['label_status_begin']                         = 'Нове';
$lang['label_status_in_work']                       = 'В роботі';
$lang['label_status_send']                          = 'Відправлено';
$lang['label_status_done']                          = 'Доставлено';
$lang['label_status_cancel']                        = 'Відмінено';
$lang['label_delivery_type']                        = 'Спосіб доставки';
$lang['label_payment_type']                         = 'Спосіб оплати';
$lang['label_cache']                                = 'Готівка';
$lang['label_card']                                 = 'Карта';
$lang['label_courier']                              = 'Курьер';
$lang['label_np']                                   = 'Нова пошта';
$lang['label_delivery_courier']                     = 'Доставка курьером';
$lang['label_delivery_np']                          = 'Нова пошта';
$lang['label_delivery_me']                          = 'Міст Експрес';
$lang['label_delivery_courier_1']                   = 'Місто отримувача';
$lang['label_delivery_courier_2']                   = 'Назва вулиці';
$lang['label_delivery_courier_3']                   = 'Будинок';
$lang['label_delivery_courier_4']                   = 'Квартира';
$lang['label_delivery_courier_5']                   = 'Коментар';
$lang['label_delivery_back_to_cart']                = 'до замовлення';
$lang['label_delivery_back_to_ship']                = 'до доставки';
$lang['label_main_promo_info']                      = 'Основная інформація';
$lang['label_promo_categories']                     = 'Закріплені категорії';
$lang['label_start_date']                           = 'Початок дії';
$lang['label_end_date']                             = 'Завершення дії';
$lang['label_pay_card']                             = 'Оплата картою';
$lang['label_pay_cache']                            = 'Готівкою';
$lang['label_pay_name']                             = 'Ім`я';
$lang['label_pay_surname']                          = 'Фамілія';
$lang['label_pay_phone']                            = 'Номер телефону';
$lang['label_pay_email']                            = 'Електронна адресса';
$lang['label_pay_limit']                            = 'Поріг нарахувань(до суми)';
$lang['label_pay_limit_base']                       = 'Базовий';
$lang['label_pay_limit_level']                      = 'Рівень';
$lang['label_pay_percent']                          = 'Процент нарахування';
$lang['label_tab_general']                          = 'Основна інформація';
$lang['label_tab_other_info']                       = 'Додаткова інформація';
$lang['label_tab_media']                            = 'Фото';
$lang['label_tab_seo']                              = 'Seo';
$lang['label_tab_painter_pay']                      = 'Винагорода художнику';
$lang['label_tab_print_confirm_status']             = 'Підтвердження публікації принта';
$lang['label_picture_name']                         = 'Назва принта';
$lang['label_upload_file_for_print']                = 'Завантажити файл';
$lang['label_upload_print_to']                      = 'Завантажити принт';
$lang['label_to_portfolio']                         = 'для портфоліо';
$lang['label_artist_shop']                          = 'Магазин';
$lang['label_artist_portfolio']                     = 'Портфоліо';
$lang['label_hide_filters']                         = 'Сховати фильтри';
$lang['LABEL_CART_SHIPPING_NEWPOST_TYPE_TO_DOOR']   = 'До дверей';
$lang['label_confirm_status_progress']              = 'Статус пинту';
$lang['label_confirm_status_new']                   = 'Новий';
$lang['label_confirm_status_confirm']               = 'Підтвердити';
$lang['label_confirm_status_refuse']                = 'Відмовити';
$lang['label_confirm_status_now_new']               = 'Новий';
$lang['label_confirm_status_now_confirm']           = 'Підтверджено';
$lang['label_confirm_status_now_refuse']            = 'Відмовлено';
$lang['label_refuse_message']                       = 'Повідомлення причини відмови';
$lang['label_refuse_message_select']                = 'Виберіть варіант зі списку:';
$lang['label_refuse_message_or_enter']              = 'Або введіть текст(тескт має більший пріорітет)';
$lang['label_tovars_to_generate']                   = 'Товари для створення';
$lang['label_tovars_generate']                      = 'Генерувати товари при підтвержденні принту?';
$lang['label_address']                              = 'Адреса';
$lang['label_icon']                                 = 'Іконка';
$lang['label_user_deleted']                         = 'Користувача видалено';
$lang['label_tovar']                                = 'Товар';
$lang['label_print']                                = 'Принт';
$lang['label_text']                                 = 'Коментар';
$lang['label_user']                                 = 'Користувач';
$lang['label_user_buyer']                           = 'Покупець';
$lang['label_user_painter']                         = 'Художник';
$lang['label_public_comment']                       = 'Опубліковано';
$lang['label_shop']                                 = 'Магазин';
$lang['label_portfolio']                            = 'Портфоліо';
$lang['label_favorites']                            = 'Обрані';
$lang['label_forget']                               = 'Забути';
$lang['label_view_item']                            = 'Переглянути';
$lang['label_to_cart']                              = 'В корзину';
$lang['label_to_favorites']                         = 'В обрані';
$lang['label_del_favorites']                        = 'Обраний';
$lang['label_tovar_description']                    = 'Опис товара';
$lang['label_to_present']                           = 'В подарунок?';
$lang['label_social_links']                         = 'Лінки на соц сіті';
$lang['label_social_vk']                            = 'vk.com/';
$lang['label_social_fb']                            = 'fb.com/';
$lang['label_social_instagram']                     = 'instagram.com/ ';
$lang['label_social_tw']                            = 'twitter.com/';
$lang['label_social_behance']                       = 'behance.com/';
$lang['label_social_description']                   = 'Додатково';
$lang['label_pay_debit_status']                     = 'Нарахування враховується';
$lang['label_pay_credit_status']                    = 'Списання враховується';
$lang['label_order']                                = 'Замовлення';
$lang['label_admin']                                = 'Адміністратор';
$lang['label_search_result']                        = 'Результати пошуку';
$lang['label_sold']                                 = 'Продано';
$lang['label_sold_graph']                           = 'Графік продажів';
$lang['label_search_in_category']                   = 'Поиск в категории';
$lang['label_search']                               = 'Поиск';
$lang['label_print_edit']                           = 'Редактировать';
$lang['label_profile']                              = 'Профайл';

$lang['label_add_work']                             = 'Добавить работу';
$lang['label_edit_works']                           = 'Редактировать работу';


$lang['label_delivery_area']                        = 'Область';
$lang['label_delivery_district']                    = 'Район';
$lang['label_delivery_city']                        = 'Город';
$lang['label_delivery_warehouse']                   = 'Склад';
$lang['label_delivery_address']                      = 'Адрес';

$lang['label_action_cart']                          = 'Корзина';
$lang['label_action_login']                         = 'Вход/Регистрация';
$lang['label_action_logout']                        = 'Выход';
$lang['label_action_search']                        = 'Поиск';

$lang['label_action_registration']                  = 'Регистрация';

$lang['label_terms']                                = 'Умови оплати і доставки';
$lang['label_share']                                = 'Поділитись';
$lang['label_do_like']                              = 'Віддати серце';
$lang['label_do_comment']                           = 'Коментувати';
$lang['label_comment']                              = 'Коментар';
$lang['label_date_now']                             = 'щойно';
$lang['label_date_mins']                            = 'хвилин тому';
$lang['label_date_hours']                           = 'годин тому';
$lang['label_date_days']                            = 'днів тому';

$lang['label_email_confirmation']                   = 'Для завершення реєстрації перейдіть за посиланням';

$lang['label_agree_terms']                          = 'Я згоден з правилами використання сайту';

$lang['label_sent_successfully']                    = 'На Ваш email было отправлено письмо со ссылкой для активации аккаунта';

//viasat
$lang['label_menu_main']                            = 'Главная';
$lang['label_menu_master']                          = 'Найти мастера';
$lang['label_menu_instructions']                    = 'Инструкции';
$lang['label_menu_forum']                           = 'Форум';
$lang['label_menu_master_reg']                      = 'Регистрация мастера';

$lang['label_setup_tuner_pre']                      = 'Самостоятельно';
$lang['label_setup_tuner']                          = 'Настроить тюнер';
$lang['label_call_master']                          = 'Найти мастера';
$lang['label_instr_video']                          = 'Инструкции и видео';
$lang['label_main_page_header']                     = 'Смотрите спутниковое ТВ?<br/>Перенастройте тюнер!';
$lang['label_main_page_text']                       = '<p>Украинские каналы меняют параметры спутникового вещания.<br/> Вскоре прием и просмотр телеканалов будет возможным только <a href="'.site_url('/faq/parameters').'"> по новым параметрам.</a> </p><p>Воспользуйтесь инструкциями для быстрой перенастройки оборудования или услугами мастера. Позаботьтесь о своих родных и помогите им сделать перенастройки.</p>';

$lang['label_about_us']                             = 'О нас';
$lang['label_site_rules']                           = 'Правила пользования сайтом';
$lang['label_masters_coordinate']                   = 'Линия поддержки мастеров:';

$lang['label_rate']                                 = 'Рейтинг';
$lang['label_comments']                             = 'Отзывы';
$lang['label_watch_contacts']                       = 'Смотреть контакты';

$lang['label_district']                             = ' район';
$lang['label_obl']                                  = ' область';
$lang['label_regions']                              = 'Области, районы';

$lang['label_master_page']                          = 'Страница мастера';
$lang['label_no_masters']                           = 'К сожалению, мастера не найдены';

$lang['label_instructions']                         = 'Инструкции для перенастройки';
$lang['label_choose_tuner']                         = 'Выберите, пожалуйста, модель тюнера.';
$lang['label_ifnotuner']                            = 'Если Вашего тюнера нет в списке, воспользуйтесь';
$lang['label_uni_instruct']                         = ' универсальными инструкциями';

$lang['label_manufacturer']                         = 'Марка';
$lang['label_model']                                = 'Модель тюнера';
$lang['label_txt_instructions']                     = 'Текстовые инструкции';
$lang['label_video_instr']                          = 'Видео инструкции';
$lang['label_watch']                                = 'Смотреть';  
$lang['label_pdf']                                  = 'Читать';
$lang['label_universal_guide']                      = 'Универсальная инструкция';

$lang['label_faq']                                  = 'Вопросы и ответы';
$lang['label_find_master']                          = 'Найти мастера';
$lang['label_master_reg']                           = 'Регистрация мастера';
$lang['label_registration_rule']                    = '<p>Для регистрации в системе</p><p>заполните, пожалуйста, форму ниже!</p> ';

$lang['label_inn']                                  = 'Идентификационный код';
$lang['label_passport']                             = 'Паспорт (серия, номер)';
$lang['label_passport_when']                        = 'Кем и когда выдан';
$lang['label_phone_number']                         = 'Номер телефона';

$lang['label_success_on_reg']                       = 'Регистрация прошла успешно! Вам отправлен email с инструкциями.';

$lang['label_form_error']                           = 'Заполните все обязательные поля! ';
$lang['label_privacy_policy']                       = 'Политика конфиденциальности';

$lang['label_enter_regions']                        = 'Укажите области и районы, которые Вы будете обслуживать:';


$lang['label_instruct_text']                        = 'текстовая';
$lang['label_instruct_video']                       = 'видео';

$lang['rate_master']                                = 'Проголосовать';
$lang['label_add_comment']                          = 'Добавить отзыв';
$lang['label_send']                                 = 'Отправить';

$lang['label_already_vote']                         = 'Вы уже проголосовали!';
$lang['label_success_voted']                        = 'Ви успешно проголосовали!';

$lang['label_please_auth']                          = 'Для того, чтобы оставить отзыв, авторизируйтесь! ;)';

$lang['label_comment_add']                          = 'Отзыв успешно отправлен!';
$lang['label_change_photo']                         = 'Изменить фото';
$lang['label_anchor_to_calling']                    = 'Если не удалось настроить тюнер самостоятельно с помощью инструкций, проверьте <a href="'.site_url('/faq/parameters').'">параметры</a> или воспользуйтесь <a href="'.site_url('/calling').'" >помощью мастера</a>';

$lang['label_site_admin']                           = 'Администратор сайта: ';
$lang['label_enter_your_region']                    = 'Введите название вашего населенного пункта';
$lang['label_foot_email']                           = '<p>Администратор сайта: <a href="mailto:info&#64;telemaster&#46;com&#46;ua" >info@telemaster&#46;com&#46;ua</a></p>';
$lang['label_registration_phrase']                  = 'Вы – мастер и умеете работать с оборудованием для спутникового телевидения?  Регистрируйтесь тут и получайте заказы от клиентов.';
$lang['label_temp_page_text']						= '<p>Вниманию инсталляторов оборудования спутникового телевидения.<br/>В ближайшее время крупнейшие украинские общенациональные телеканалы меняют параметры спутникового вещания.</p><p> Новые параметры спутникового вещания в ближайшее время будут выложены на этом сайте.
Сейчас сайт работает в режиме регистрации инсталляторов оборудования спутникового телевидения.
Регистрируйтесь и присоединяйтесь к команде Telemaster!</p>';
$lang['label_temp_sub_inst']						= 'В этом разделе вскоре будут опубликованы инструкции к перестройке ТВ тюнеров.';	
$lang['label_load_image_error']                     = 'Формат изображения может быть jpg или png!';

$lang['label_success_registered_to_master']         = 'Вы успешно зарегистрировались! В ближайшее время с Вами свяжутся для уточнения персональных данных.';
$lang['label_registration_subject']                 = 'Регистрация на telemaster.com.ua';
$lang['label_success_registered_to_admin']          = 'На сайте зарегистрировался мастер!<br/> <br/>Данные мастера:<br/>';
$lang['label_bottom']                               = '<br/>Спасибо,<br/> telemaster.com.ua.';
$lang['label_reports_rules']						= 'Правила модерации отзывов';

$lang['label_registration_helptext']				= 'Внимание! Если вы уже зарегистрированы и хотите изменить/добавить информацию в своем профиле – обратитесь на Линию поддержки Мастеров (050 480 82 82, 093 170 82 82, 067 011 82 82, info@telemaster.com.ua).';
$lang['developed_by']								= 'Создание сайта ';

$lang['label_unique_constr']                        = 'Пользователь с таким email адресом уже существует!';
$lang['label_unique_ph_constr']                     = 'Пользователь с таким номером телефона уже существует!';

$lang['label_err_on_reg_email']                     = 'Пользовател с данным email адресом уже существует!';
$lang['label_err_on_reg_unique']                    = 'Пользовател с данным email адресом и номером телефона уже существет!';
$lang['label_err_on_reg_phone']                     = 'Пользовател с веденным номером телефона уже существет!';

$lang['label_call_partner']                         = 'Найти провайдера';
$lang['label_find_partner']                         = 'Найти провайдера';
$lang['label_enter_region']                         = 'Введите название населенного пункта';
$lang['label_watch_partner']                        = 'Перейти на сайт';
$lang['label_phone']                                = 'Номер телефона';
$lang['label_menu_partner']                         = 'Найти провайдера';
$lang['label_provider_comment']                     = '* Информация носит справочный характер и может содержать неточности.  Перечень провайдеров предоставлен национальными телеканалами.';
$lang['label_provider_law']                         = '01.01.2017г. в Украине вступил в силу закон №1663-VIII "О внесении изменений в Закон Украины «О телевидении и радиовещании» (относительно уточнения условий распространения программ телерадиоорганизаций в составе универсальной программной услуги)». В связи с внедрением этого закона условия ретрансляции национальных каналов в кабельных и IPTV сетях меняются.<br />  <strong>Найдите свой город и проверьте, есть ли ваш провайдер в списке* тех, кто продолжит легальную ретрансляцию национальных каналов</strong>.';

$lang['label_err_on_reg']							= 'Регионы не указаны!';
$lang['label_send_refuse']                          = 'Отправить письмо с отказом мастеру';
$lang['label_instruction']                          = 'Инструкции';
$lang['label_remove_photo']                         = 'Удалить фото';
$lang['label_send_master_photo_refuse']             = 'Отправить мастеру сообщение об удалении фото';