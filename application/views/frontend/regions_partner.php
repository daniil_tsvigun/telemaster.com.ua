<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content region partner">
        <div class="wrapper">
            <div class="main-block">
                <h2><?php if(isset($regions->{'district_place'.SQL_LANG})){
                        if($regions->type==3){
                            echo $regions->{'district_place'.SQL_LANG}.LANG('label_district');
                        } else {
                            echo $regions->{'district_place'.SQL_LANG};
                        }

                    }else{
                        LANG('label_no_masters');
                    }  ?></h2>
                <table class="table table-striped masters">
                    <tbody>
                    <?php foreach($masters as $master): ?>
                        <?php if($master->active==1): ?>
                            <tr>
                                <td class="master-info">
                                    <div class="wrapper master">
                                        <div class="name">
                                            <?php echo $master->name; ?>
                                        </div>
                                    </div>
                                    <div class="text id">
                                        <?php //echo $master->company; ?>
                                    </div>
                                </td>

                                <td class="master-info">
                                    <div class="wrapper master">
                                        <div class="text">
                                            <?php echo LANG('label_phone').': '.$master->phone; ?>
                                        </div>
                                    </div>
                                </td>
                                <td class="btn-box">
                                    <a href="<?php echo $master->link; ?>"  target="_blank" class="btn btn-type3"><?php echo LANG('label_watch_partner'); ?></a>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $this->load->view('frontend/default/footer'); ?>