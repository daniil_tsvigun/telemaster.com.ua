<?php

class Sender {

    public $id;

    public function __construct($source = null) {
        $this->id           = isset($source['id'])?           $source['id']          :'';


    }
}

class Sender_model extends MY_Model {

    protected $table_name = 'message';

    public function __construct() {
        parent::__construct();
    }

    public function message($data){
        
        $send_info['email']=(isset($data['to_email']))?$data['to_email']:'dtsvigun@lemon.ua';
        if(isset($data['role'])){
            if($data['role']==1){
                $send_info['email']='reports@telemaster.com.ua';
            }
        }
        $send_info['subject']=(isset($data['subject']))?$data['subject']:'Telemaster';
        $send_info['message_data']=isset($data['message'])?$data['message']:'';
        if(isset($data['template'])){

            $send_info['content']=$this->load->view('mail/'.$data['template'],$send_info,true);
        } else {
            $send_info['content']=$this->load->view('mail/default',$send_info,true);
        }
        $headers = "From: noreply@telemaster.com.ua \r\n";
        $headers .= "MIME-Version: 1.0\r\n"."Content-type: text/html; charset=utf-8\r\n";
        $send_info['headers']=$headers;
        return $this->send($send_info);
    }

    private function save_message($data){
        $this->db->insert($this->table_name,array(
            'email'=>$data['email'],
            'subject'=>$data['subject'],
            'data'=>$data['content'],
        ));
    }

    public function send($data){
        //$this->save_message($data);
        $_status = mail($data['email'], $data['subject'], $data['content'], $data['headers']);
        return $_status;
    }
}