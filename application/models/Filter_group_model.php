<?php

class Filter_group {

    public $alias;
    public $parent_id;
    public $title_ru;
    public $title_ua;
    public $sort;
    public $category;


    public function __construct($source = null) {
        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->parent_id          = isset($source['parent_id'])?          $source['parent_id']         :0;
        $this->title_ru           = isset($source['title_ru'])?           $source['title_ru']          :'';
        $this->title_ua           = isset($source['title_ua'])?           $source['title_ua']          :'';
        $this->sort               = isset($source['sort'])?               $source['sort']              :0;
        $this->data               = isset($source['data'])?               $source['data']              :'';
        $this->category           = isset($source['category'])?           $source['category']          :array();
    }
}

class Filter_group_model extends MY_Model {

    protected $table_name = 'filters';
    protected $t_filters_to_categories = 'filters_to_categories';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_by_id($source_id){

        $res = $this->db->get_where($this->table_name,array('id'=>$source_id));

        if($res->num_rows() !== 1)
            return null;

        $data = $res->row_array();
        $res_category = $this->get_category_to_filter($data['id']);
        if(count($res_category) > 0){
            foreach($res_category as $item){
                $data['category'][$item['category_id']] = $item['category_id'];
            }
        }else{
            $data['category'] = array();
        }

        return $data;

    }

    public function create($source,$source_class){
        $data = new $source_class($source);
        $_category = $data->category;
        unset($data->category);
        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        if(count($_category) > 0){
            foreach($_category as $category_id){
                $_arr[$category_id]['category_id'] = $category_id;
                $_arr[$category_id]['filter_id'] = $_id;
            }
            $this->db->insert_batch($this->t_filters_to_categories,$_arr);
        }
    }

    public function update($source, $source_id,$source_class) {
        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            $_category = $data->category;
            unset($data->category);
            $this->db->update($this->table_name, $data, ['id' => $source_id]);

            $this->db->where(array('filter_id'=>$source_id))->delete($this->t_filters_to_categories);
            if(count($_category) > 0){
                foreach($_category as $category_id){
                    $_arr[$category_id]['category_id'] = $category_id;
                    $_arr[$category_id]['filter_id'] = $source_id;
                }

                $this->db->insert_batch($this->t_filters_to_categories,$_arr);
            }

            return true;
        }
        return false;
    }

    public function get_all_filter_group($pagination = false,$only_on = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }
        if($only_on){
            $this->db->where(array('active' => 1));
        }

        $this->db->where('parent_id = 0');

        $result = $this->db->select($this->table_name .'.*, title'. SQL_LANG .' as title')->from($this->table_name)->get()->result_array();

        return $result;
    }

    public function get_category_to_filter($filter_id){

        $res = $this->db->get_where($this->t_filters_to_categories,array('filter_id'=>$filter_id));
        return ($res->num_rows()> 0) ? $res->result_array() : null;
    }
}