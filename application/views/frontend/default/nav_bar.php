<div data-id="mobile_btn" class="lang-menu">
    <ul>
        <li class="<?php if(LANG=='ua'): echo 'current';  endif; ?>"><a href="<?php echo base_url('/ua').$this->uri->uri_string; ?>"><?php echo LANG('label_ua'); ?></a></li>
        <li class="<?php if(LANG=='ru'): echo 'current';  endif; ?>"><a href="<?php echo base_url('/ru').$this->uri->uri_string; ?>"><?php echo LANG('label_ru'); ?></a></li>
    </ul>
</div>
<div data-id="mobile_btn" class="master <?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='registration'): echo 'current'; endif; endif; ?>">
    <a class="<?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='registration'): echo 'current'; endif; endif; ?>" href="<?php echo site_url('registration'); ?>"><?php echo LANG('label_menu_master_reg'); ?></a>
</div>
<section class="nav_bar" >
    <div class="wrapper">
        <div class="menu">
            <a href="<?php echo site_url(); ?>" class="logo-anchor"></a>
            <ul class="main-menu slideInDown animated">

                <li>
                    <a class="<?php if(!isset($this->uri->segments[1])): echo 'current'; endif; ?>" href="<?php echo site_url(); ?>"><?php echo LANG('label_menu_main'); ?></a>
                </li>
                <li>
                    <a class="<?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='calling'): echo 'current'; endif; endif; ?>" href="<?php echo site_url('calling'); ?>"><?php echo LANG('label_menu_master'); ?></a>
                </li>
                <li>
                    <a class="<?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='partners'): echo 'current'; endif; endif; ?>" href="<?php echo site_url('partners'); ?>"><?php echo LANG('label_menu_partner'); ?></a>
                </li>
                <li>
                    <a class="<?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='instructions'): echo 'current'; endif; endif; ?>" href="<?php echo site_url('instructions'); ?>"><?php echo LANG('label_menu_instructions'); ?></a>
                </li>
<!--                <li>-->
<!--                    <a class="--><?php //if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='forum'): echo 'current'; endif; endif; ?><!--" href="--><?php //echo site_url('forum'); ?><!--">--><?php //echo LANG('label_menu_forum'); ?><!--</a>-->
<!--                </li>-->
                <li class="master <?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='registration'): echo 'current'; endif; endif; ?>">
                    <a class="<?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='registration'): echo 'current'; endif; endif; ?>" href="<?php echo site_url('registration'); ?>"><?php echo LANG('label_menu_master_reg'); ?></a>
                </li>
            </ul>
            <div id="menu-btn"></div>
            <div class="lang-menu">
                <ul>
                   
                    <li class="<?php if(LANG=='ua'): echo 'current';  endif; ?>"><a href="<?php echo base_url('/ua').$this->uri->uri_string; ?>"><?php echo LANG('label_ua'); ?></a></li>
                    <li class="<?php if(LANG=='ru'): echo 'current';  endif; ?>"><a href="<?php echo base_url('/ru').$this->uri->uri_string; ?>"><?php echo LANG('label_ru'); ?></a></li>
                </ul>
            </div>
            <div class="master <?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='registration'): echo 'current'; endif; endif; ?>">
                <a class="<?php if(isset($this->uri->segments[1])): if($this->uri->segments[1]=='registration'): echo 'current'; endif; endif; ?>" href="<?php echo site_url('registration'); ?>"><?php echo LANG('label_menu_master_reg'); ?></a>
            </div>
        </div>

    </div>
</section>