<?php
/**
 * Payment.
 *
 * @package     hairdressers
 * @version     1.0
 * @license     http://mit-license.org/
 * @author      Tapakan https://github.com/Tapakan
 * @coder       Alexander Oganov <t_tapak@yahoo.com>
 */

/**
 * Class Payment
 */
class Payment
{
    /**
     * @type int
     */
    public $id;

    /**
     * @type int
     */
    public $user_id;

    /**
     * @type int
     */
    public $order_id;

    /**
     * @type float
     */
    public $amount;

    /**
     * Currency code.
     *
     * @type string
     */
    public $currency;

    /**
     * @type int
     */
    public $status;

    /**
     * @type datetime
     */
    public $date;

    const STATUS_NOT_PAID = 0;
    const STATUS_PAID     = 1;

    /**
     * City_directory constructor.
     *
     * @param array $source
     */
    public function __construct($source = array())
    {
        $this->id       = isset($source['id']) ? $source['id'] : null;
        $this->user_id  = isset($source['user_id']) ? $source['user_id'] : null;
        $this->order_id = isset($source['order_id']) ? $source['order_id'] : null;
        $this->amount   = isset($source['amount']) ? $source['amount'] : null;
        $this->currency = isset($source['currency']) ? $source['currency'] : 'UAH';
        $this->status   = isset($source['status']) ? $source['status'] : 0;
        $this->date     = isset($source['date']) ? $source['date'] : date('Y-m-d H:i:s');
        $this->ip       = isset($source['ip']) ? $source['ip'] : '';

        $this->liqpay_ip              = isset($source['liqpay_ip']) ? $source['liqpay_ip'] : null;
        $this->liqpay_payment_id      = isset($source['payment_id']) ? $source['payment_id'] : null;
        $this->liqpay_order_id        = isset($source['liqpay_order_id']) ? $source['liqpay_order_id'] : null;
        $this->liqpay_err_code        = isset($source['err_code']) ? $source['err_code'] : null;
        $this->liqpay_err_description = isset($source['err_description']) ? $source['err_description'] : null;
    }

    /**
     * Return id
     *
     * @return int
     */
    public function get_id()
    {
        return $this->id;
    }

    /**
     * Returns users id
     *
     * @return string
     */
    public function get_user_id()
    {
        return $this->user_id;
    }

    /**
     * Return payment amount.
     *
     * @return float|mixed|null
     */
    public function get_amount()
    {
        return $this->amount;
    }

    /**
     * Return payment status.
     *
     * @return int
     */
    public function get_status()
    {
        return $this->status;
    }

    /**
     * Return payment datetime.
     *
     * @return int
     */
    public function get_date()
    {
        return $this->date;
    }
}

/**
 * Class City_model
 */
class Payment_model extends MY_Model
{
    /**
     * @type string
     */
    public $table_name = 'payments';

    /**
     * City_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $source
     * @param $source_class
     */
    public function create($source, $source_class = 'Payment')
    {
        return parent::create($source, $source_class);
    }

    /**
     * @param array  $source
     * @param string $source_key
     * @param string $source_class
     *
     * @return bool
     */
    public function update($source, $source_key = 'order_id', $source_class = 'Payment')
    {
        $id    = $source[$source_key];
        $_data = $this->get_by_id($id);
        if ($_data) {
            $source = array_merge($_data, $source);
            $data   = new $source_class($source);
            $this->db->update($this->table_name, $data, ['id' => $id]);

            return true;
        }

        return false;
    }

    /**
     * @param        $source
     * @param string $source_key
     * @param string $source_class
     *
     * @return bool
     */
    public function update_by_order_id($source, $source_key = 'order_id', $source_class = 'Payment')
    {
        $id    = $source[$source_key];
        $_data = $this->get_where([
            'order_id' => $id
        ]);

        if ($_data) {
            $source = array_merge($_data, $source);
            $data   = new $source_class($source);
            $this->db->update($this->table_name, $data, ['order_id' => $id]);

            return true;
        }

        return false;
    }

    /**
     * @param $where
     */
    public function get_where($where)
    {
        return $this->db->get_where($this->table_name, $where)->row_array();
    }

    /**
     * Returns country by id.
     *
     * @param int $id
     *
     * @return null
     */
    public function get_by_id($id)
    {
        return parent::get_by_id((int)$id);
    }
}
