<?php
$lang['auth_no_such_account_found'] = ' Некорректный адрес электронной почты либо пароль';
$lang['auth_login_denied'] = ' Вы не можете зарегистрироваться на портале. Пожалуйста, обратитесь к администратору.';

// $lang['ntf_question_time_limit_reached_title'] = 'Предыдущий вопрос не засчитан';

$lang['message_no_results_to_display'] = 'Нет результатов для отображения';
$lang['message_no_data_to_display'] = 'Нет данных для отображения';

$lang['message_404_title'] = 'К сожалению, запрошенная страница не найдена';
$lang['message_404_text'] = 'К сожалению, запрошенная страница не найдена.
Вы можете вернуться в предыдущее меню, нажав кнопку «Назад» на панели Вашего браузера либо обратиться в службу поддержки по ссылке <a href="%s" class="btn-light">Помощь</a>';

$land['message_recovery_sent'] = 'На введенный адрес эелектронной почты отправленна ссылка для смены пароля.';


$lang['pagination_from'] = 'из';
$lang['pagination_now'] = 'текуцая страница';
$lang['pagination_begin'] = 'начальная страница';
$lang['pagination_last'] = 'последняя страница';
$lang['pagination_prev'] = 'предидущая страница';
$lang['pagination_next'] = 'следующая страница';
$lang[''] = ' ';








$lang[''] = ' ';
