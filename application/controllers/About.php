<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();

    }

    public function test(){
      // $region_data['id']='0710100000';
      // var_dump($this->master_model->get_masters_by_region_test($region_data));
    	 phpinfo();
    }


    public function images($active='all'){
      if($active=='1'){
        $this->db->join('masters','masters.id=master_id');
        $this->db->where(array('active'=>1));
      } elseif($active=='0') {
        $this->db->join('masters','masters.id=master_id');
        $this->db->where(array('active'=>0));
      } 
       $images=$this->db->from('master_images')->get()->result_array();

        foreach($images as $im){ ?>
            id: <?php echo $im['master_id']; ?>;
            <a href="<?php echo base_url().'panel/masters/edit/master/'.$im['master_id']; ?>"><img src="<?php echo base_url(); ?>images/avatars/<?php echo $im['master_id']; ?>/<?php echo $im['img_name']; ?>/<?php echo 'b_'.$im['img_name']; ?>.<?php echo $im['img_type']; ?>" alt="">
            <a download href="<?php echo base_url(); ?>images/avatars/<?php echo $im['master_id']; ?>/<?php echo $im['img_name']; ?>/<?php echo 'd_'.$im['img_name']; ?>.<?php echo $im['img_type']; ?>">Скачать</a>

            <br/>
        <?php }
    }

    public function index(){

        $data = $this->page_model->get_by_alias('about',true);
        $this->load->view('frontend/templates/content',$data);
    }
   //  public function aaa(){
   //  	$id=0;
   //      $ids=array(
   //        666870,
			// 666871,
			// 666872,
			// 666873,
			// 666874,
			// 666875,
			// 666876

   //      );
   //      foreach($ids as $id){

        
   //          $data=$this->master_model->get_master_by_id($id);
        
   //          $state='';
   //          foreach($data->regions as $region){
   //              $state.=$region->area_name_ua.', '.$region->district_place_ua.'; ';
   //          }
   //          echo $id.' '.$state.'<br/>';
        
   //      }
   //  }

    public function xls(){
        $today=getdate();
        $start_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'00'.':'.'00'.':'.'00';
        $end_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'23'.':'.'59'.':'.'59';
        $masters = $this->master_model->get_master_between_dates($start_date,$end_date);
        foreach($masters as $key=>$master){
            $data=$this->master_model->get_master_by_id($master['id']);
            $state='';
            foreach($data->regions as $region){
                $state.=$region->area_name_ua.', '.$region->district_place_ua.'; ';
            }
            $masters[$key]['region']= $state;      
        }

        $xls = new Excel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('masters');
        for($i = 1; $i < count($masters); $i++){
            $j=0;
            foreach($masters[$i] as $key=>$value){
                if($key=='name'||$key=='surname'||$key=='id'||$key=='phone1'||$key=='phone2'||$key=='phone3'||$key=='phone4'||$key=='email'||$key=='region'){
                    $sheet->setCellValueByColumnAndRow( $j, $i, $key );
                   $j++; 
                }
            }
            break;
        }
        for ($i = 2; $i < count($masters)+2; $i++) {
            $j=2;
            foreach($masters[$i-2] as $key=>$value){
                if($key=='name'||$key=='surname'||$key=='id'||$key=='phone1'||$key=='phone2'||$key=='phone3'||$key=='phone4'||$key=='email'||$key=='region'){
                    $sheet->setCellValueByColumnAndRow( $j - 2, $i, $value );
                   $j++; 
                }
            }
        }
        // Выводим HTTP-заголовки
         // header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
         // header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
         // header ( "Cache-Control: no-cache, must-revalidate" );
         // header ( "Pragma: no-cache" );
         // header ( "Content-type: application/vnd.ms-excel" );
         // header ( "Content-Disposition: attachment; filename=masters_".$today['year'].'-'.$today['mon'].'-'.($today['mday']-1).'.xls' );
        // Выводим содержимое файла
         $objWriter = new PHPExcel_Writer_Excel5($xls);
         $objWriter->save('uploads/reports/masters_'.$today['year'].'-'.$today['mon'].'-'.($today['mday']-1).'.xls');
    }

//     public function hello(){
// //        header("Pragma: public");
// //        header("Expires: 0");
// //        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
// //        header("Cache-Control: private", false);
// //        header("Content-Type: application/x-msexcel");
// //        header("Content-Disposition: attachment; filename=\"" . iconv('UTF-8', 'CP1251', 'file.xls') . "\";");
// //        header("Content-Transfer-Encoding:­ binary");
//         //header("Content-Length: " . '200');
//         $result=$this->master_model->get_all_masters();
//         $data['data']=array();
//         foreach($result as $res){
//             if($res['active']==0){
//                 array_push($data['data'],$res);
//             }
//         }

//         $this->load->view('sheets',$data);

// //        $data=array(
// //            0=>1212600000,
// //            1=>1212400000,
// //            2=>1213500000
// //        );
// //        $a=$this->region_model->get_regions_by_ids($data);
// //        var_dump($a);
//         //$region['region_id']=510100000;
//         //var_dump($this->region_model->get_region_by_id($region['region_id']));
// //        $filename='http://viasat_masters.lemon.ua/images/avatars/156/ab8b2/a_ab8b2.jpg';
// //        $degrees = -90;
// //        $source = imagecreatefromjpeg($filename);
// //        $rotate = imagerotate($source, $degrees, 0);
// //        imagejpeg($rotate,'images/avatars/156/ab8b2/a_ab8b2.jpg');

//     //$row = 1;
// //    if (($handle = fopen(base_url()."/uploads/file_15_11.csv", "r")) !== FALSE) {
// //        while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
// //            $num = count($data);
// //            //echo "<p> $num полей в строке $row: <br /></p>\n";
// //            //$row++;
// //
// ////            for ($c=0; $c < $num; $c++) {
// ////                var_dump($data[$c]);
// ////                //echo $data[$c] . "<br />\n";
// ////            }
// //            //var_dump($data);
// //        }
// //
// //        fclose($handle);
// //    }
//         //$data['areas']=$this->region_model->get_all_areas();

//         //$this->load->view('frontend/update',$data);
//         //$region_data['id']=7110100000;

//         //var_dump($this->master_model->get_masters_by_region_test($region_data));
//         if (isset($_FILES) && !empty($_FILES)){
//             //$source['id']=$this->input->post('master_id');
//             //$this->master_model->create_master($source);
//         }
//     }

}
