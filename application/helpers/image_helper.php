<?php


if ( ! function_exists('save_base64_img')) {
    function save_base64_img($base64_img,$save_url = '/images/tmp'){

        $img = $base64_img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $new_image = imagecreatefromstring(base64_decode($img));
        $w=imagesx($new_image);
        $h=imagesy($new_image);
        imageAlphaBlending($new_image, false);
        imageSaveAlpha($new_image, true);
        $data = array('image'=>$new_image,'width'=>$w,'height'=>$h);

        $dest = imagecreate($w, $h);   // Создаём пустую картинку размером $x на $y
        $dst1_x=$dst1_y=0;

        imagecopy ($dest, $data['image'], $dst1_x, $dst1_y, 0, 0, imagesx($data['image']), imagesy($data['image']));

        $file = $save_url .'/'. uniqid() . '.png';
        imagePng($dest, $file);

        return $file;
    }
}
if ( ! function_exists('save_base64_img')) {
    function save_base64_jpeg($base64_img,$save_url = '/images/tmp'){

        $img = $base64_img;
        $img = str_replace('data:image/jpg;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $new_image = imagecreatefromstring(base64_decode($img));
        $w=imagesx($new_image);
        $h=imagesy($new_image);
        imageAlphaBlending($new_image, false);
        imageSaveAlpha($new_image, true);
        $data = array('image'=>$new_image,'width'=>$w,'height'=>$h);

        $dest = imagecreate($w, $h);   // Создаём пустую картинку размером $x на $y
        $dst1_x=$dst1_y=0;

        imagecopy ($dest, $data['image'], $dst1_x, $dst1_y, 0, 0, imagesx($data['image']), imagesy($data['image']));

        $file = $save_url .'/'. uniqid() . '.jpg';
        imagejpeg($dest, $file);

        return $file;
    }
}

if ( ! function_exists('print_to_product')) {
    /*
     * $print_h,$print_w - размеры принта
     * $print_position_x,$print_position_y - позиционирование принта на палитре
     * $print_img - уменьшеное изображение принта в которое потом перезапишем палитру с размещенным по координатам принтом
     * */
    function print_to_palitra($print_img,$print_h,$print_w,$print_position_x,$print_position_y){

        $img = imagecreatetruecolor(DEFAULT_MAX_PHOTO_SIZE_X,DEFAULT_MAX_PHOTO_SIZE_Y); //- создаем палитру
        $water_img = imagecreatefromjpeg($print_img);

        imagecopy ($img, $water_img, $print_position_x,$print_position_y, 0, 0,  $print_w, $print_h); //накладываем изображение на палитру.
        imagejpeg($img,$print_img); //сохраняем изображение на палитре
    }
}
if ( ! function_exists('print_to_product')) {
    function print_to_product($tmp_print_img,$product_img){

        $img = imagecreatefromjpeg($tmp_print_img); //читаем изображение палитры с принтом
        $water_img = imagecreatefrompng($product_img); //читаем изображение продукта
        imagecopy ($img, $water_img, 0, 0, 0, 0, DEFAULT_MAX_PHOTO_SIZE_X, DEFAULT_MAX_PHOTO_SIZE_Y); //соединяем палитру и продукт.

        $save_url = 'images/tmp';
        $_file = $save_url .'/'. uniqid() . '.jpeg';
        imagejpeg($img,$_file); //выводим изображение
        return $_file;
    }
}
if ( ! function_exists('imageresize')) {
    function imageresize($outfile,$infile,$neww,$newh,$quality) {
        $im=imagecreatefromjpeg($infile);
        $k1=$neww/imagesx($im);
        $k2=$newh/imagesy($im);
        $k=$k1>$k2?$k2:$k1;

        $w=intval(imagesx($im)*$k);
        $h=intval(imagesy($im)*$k);

        $im1=imagecreatetruecolor($w,$h);
        imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));

        imagejpeg($im1,$outfile,$quality);
        imagedestroy($im);
        imagedestroy($im1);
    }
}

if ( ! function_exists('make_template_print')) {
    function make_template_print($print_height,$print_width,$print_img) {

        $tmp_img = imagecreatetruecolor($print_width,$print_height);
        $file = 'images/tmp/' . uniqid() . '.jpeg';
        imagejpeg($tmp_img,$file);
        imageresize($file,$print_img,$print_width,$print_height,75);

        return $file;
    }
}
if ( ! function_exists('tiff2jpg')) {
    function tiff2jpg($file) {
        $mgck_wnd = NewMagickWand();
        MagickReadImage($mgck_wnd, $file);

        $img_colspc = MagickGetImageColorspace($mgck_wnd);
        if ($img_colspc == MW_CMYKColorspace) {
            MagickSetImageColorspace($mgck_wnd, MW_RGBColorspace);
        }
        MagickSetImageFormat($mgck_wnd, 'JPG' );
        MagickWriteImage($mgck_wnd, str_replace('.tif', '.jpg', $file));
    }
}