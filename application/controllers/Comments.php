<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();
    }

    public function ajax_set_comment(){
        $result = array('status'=>'error');
        $data=$this->input->post('data');
        $master_id = (int)$data['master_id'];
        $user_id = (int)$data['user_id'];
        $name=$data['name'];
        $text = $data['text'];

        if(!text_validate($text)){
            echo json_encode($result);
            return;
        }
        //$checking=$this->comment_model->get_last_user_comment($user_id);

        $status = $this->comment_model->set_comment(array('master_id'=>$master_id,'user_id'=>$user_id,'text'=>$text,'user_name'=>$name));
        if($status){
            $result = array(
                'status'=>'success',
                'data'=>LANG('label_comment_add'),

            );
        }
        echo json_encode($result);
        return;
    }

    public function ajax_del_comment(){
        $result = array('status'=>'error');
        $tovar_id = (int)$this->input->post('tovar_id');
        if($tovar_id == 0 || $this->user->role_id == 1){
            echo json_encode($result);
            return;
        }

        $this->comment_model->del_comment(array('tovar_id'=>$tovar_id,'user_id'=>$this->user->id,'role_id'=>$this->user->role_id));
        $result = array('status'=>'success');

        echo json_encode($result);
        return;
    }

}