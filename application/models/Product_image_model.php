<?php

class Product_image_model extends CI_Model {

    protected $t_products_images = 'products_images';

    public function __construct() {
        parent::__construct();

    }

    public function get_image_by_product($product_id = false){
        if(!$product_id){
            return array();
        }

        $result = $this->db->where(array('product_id'=>$product_id))->get($this->t_products_images)->result_array();
        return $result;
    }
}
