<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                    <?php $this->load->view('panel/templates/action_button');?>
                    <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="general" href="#general" aria-expanded="true"><?php echo LANG('label_tab_general')?></a></li>
                        <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="seo" href="#seo" aria-expanded="false"><?php echo LANG('label_tab_seo')?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active" role="tabpanel">
                            <div class="row">
                                <div class="col-md-8 inside_tab">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                            <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="data_<?php echo $lang_key;?>" href="#data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                        <?php $_first = false;
                                        endforeach;?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <div id="data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input type="text" placeholder="" class="input" value="<?php echo $category->{'title_'. $lang_key};?>" name="title_<?php echo $lang_key;?>"><br>
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_description');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <textarea name="description_<?php echo $lang_key;?>" id="description" aria-hidden="true"><?php echo $category->{'description_'. $lang_key};?></textarea>
                                        </div>
                                        <?php $_first = false;
                                        endforeach;?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                                    <div class="widget-body">
                                        <p><span><? echo LANG ('label_alias');?></span><input type="text" placeholder="" class="input" value="<?php echo $category->alias;?>" name="alias"> </p>
                                        <p><span><? echo LANG ('label_parent_category');?></span>
                                            <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$categories_tree,'name'=>'parent_id','selected'=>array($category->parent_id),'params' => array(),'current'=>$category->id));?>
                                        </p>
                                        <p><span><? echo LANG ('label_tovat_template');?></span>
                                            <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$tovar_templates,'name'=>'template_id','selected'=>array($category->template_id),'params' => array('no_all'=>'yes'),'current'=>$category->id));?>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="seo" class="tab-pane" role="tabpanel">
                            <div class="row inside_tab">
                                <ul role="tablist" class="nav nav-tabs">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="seo_data_<?php echo $lang_key;?>" href="#seo_data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                        <?php $_first = false;
                                    endforeach;?>
                                </ul>
                                <div class="col-md-8 tab-content">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <div id="seo_data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input name="seo_title_<?php echo $lang_key;?>" type="text" placeholder="" class="input" value="<?php echo $category->{'seo_title_'. $lang_key};?>" ><br>
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_keywords');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input name="seo_keywords_<?php echo $lang_key;?>" type="text" placeholder="" class="input" value="<?php echo $category->{'seo_keywords_'. $lang_key};?>" ><br>
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_description');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <textarea name="seo_description_<?php echo $lang_key;?>" id="description" aria-hidden="true"><?php echo $category->{'seo_description_'. $lang_key};?></textarea>
                                        </div>
                                        <?php $_first = false;
                                    endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>