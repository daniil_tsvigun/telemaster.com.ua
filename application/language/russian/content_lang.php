<?php


$lang['month_01']                               = "Январь";
$lang['month_02']                               = "Февраль";
$lang['month_03']                               = "Март";
$lang['month_04']                               = "Апрель";
$lang['month_05']                               = "Май";
$lang['month_06']                               = "Июнь";
$lang['month_07']                               = "Июль";
$lang['month_08']                               = "Август";
$lang['month_09']                               = "Сентябрь";
$lang['month_10']                               = "Октябрь";
$lang['month_11']                               = "Декабрь";
$lang['month_12']                               = "Январь";

$lang['day_of_week_1']                          ="Понедельник";
$lang['day_of_week_2']                          ="Вторник";
$lang['day_of_week_3']                          ="Среда";
$lang['day_of_week_4']                          ="Четверг";
$lang['day_of_week_5']                          ="Пятниця";
$lang['day_of_week_6']                          ="Суббота";
$lang['day_of_week_0']                          ="Воскресенье";

$lang['message_title']                          = "Заголовок/тема";
$lang['message_type']                           = "Тип";
$lang['message_type_all']                       = "Всем";
$lang['message_type_no_all']                    = "Частное";

//login
$lang['login_hello']	                        = "Вход к личному кабинету";
$lang['login_login']	                        = "Логин";
$lang['login_password']	                        = "Пароль";
$lang['login_foget_password']	                = "Забыли пароль";
$lang['login_reg']	                            = "Регистрация";
$lang['login_enter']	                        = "Вход";





/* End of file content_lang.php */
