<div class="media sortable ui-sortable" id="order_tovar_list">
    <?php if(count($order_tovars) > 0): ?>
        <?php foreach($order_tovars as $tovar): ?>
            <div class="row stripped ui-sortable-handle">
                <div class="col-md-2">
                    <img src="/images/tovars/<?php echo $tovar['tovar_id']; ?>/<?php echo $tovar['preview']; ?>/c_<?php echo $tovar['preview']; ?>.jpg" style="display: block;margin: 0 auto;max-height:120px;">
                </div>
                <div class="col-md-2">
                    <?php echo $tovar['tovar_title']; ?>
                    <a href="<?php echo site_url($tovar['alias']); ?>" target="_blank"><?php echo $tovar['alias']; ?></a>
                </div>
                <div class="col-md-2">
                    <a href="/images/prints/<?php echo $tovar['picture_id']; ?>/<?php echo $tovar['picture_name']; ?>.<?php echo $tovar['picture_type']; ?>" download><img src="/images/prints/<?php echo $tovar['picture_id']; ?>/preview/<?php echo $tovar['picture_preview_name']; ?>.<?php echo $tovar['picture_preview_type']; ?>" style="display: block;margin: 0 auto;max-height:120px;"></a>
                </div>
                <div class="col-md-1">
                    <p><?php echo LANG('label_tovar_price_for_one');?></p>
                    <p><?php echo LANG('label_tovar_price_base');?>: <?php echo $tovar['price_base']; ?></p>
                    <p><?php echo LANG('label_tovar_price_discount');?>: <?php echo $tovar['price_discount']; ?></p>
                    <p><?php echo LANG('label_tovar_price_final');?>: <strong><?php echo $tovar['price_final']; ?></strong></p>
                </div>
                <div class="col-md-1">
                   <?php echo LANG('label_category');?>: <?php echo $tovar['category_title']; ?>
                </div>
                <div class="col-md-1">
                   <?php echo LANG('label_tovar_filters');?>:<br/>
                    <?php $_filters_arr = explode(",",$tovar['filters']);
                        $_f = array();
                        foreach($_filters_arr as $item){
                            $_f[] =  $filters[$item]['filter_group'] .': '. $filters[$item]['title'].'<br/>';
                        }
                       echo implode("",$_f);
                    ?>
                </div>
                <div class="col-md-1">
                   <?php echo LANG('label_tovar_postal');?>
                    <input 	type="hidden"   name="tovars[<?php echo $tovar['id']; ?>][postal]" value="0" checked="checked">
                    <input 	type="checkbox" name="tovars[<?php echo $tovar['id']; ?>][postal]" class="switcher checked" value="1"
                          data-name="tovars[<?php echo $tovar['id']; ?>][postal]"
                          data-style="default"
                          data-selected="<?php echo($tovar['postal'] == 1)? 'true':'false';?>"
                          data-disabled="false">
                </div>
                <div class="col-md-1">
                   <?php echo LANG('label_tovar_pack');?>
                    <input 	type="hidden"   name="tovars[<?php echo $tovar['id']; ?>][pack]" value="0" checked="checked">
                    <input 	type="checkbox" name="tovars[<?php echo $tovar['id']; ?>][pack]" class="switcher checked" value="1"
                              data-name="tovars[<?php echo $tovar['id']; ?>][pack]"
                              data-style="default"
                              data-selected="<?php echo($tovar['pack'] == 1)? 'true':'false';?>"
                              data-disabled="false">
                </div>
                <div class="col-md-1">
                   <?php echo LANG('label_count');?>: <input style="width: 45px;float: left;min-width: 45px;" type="text" name="tovars[<?php echo $tovar['id']; ?>][count]" maxlength="2" value ="<?php echo $tovar['count']; ?>">
                    <a href="#" class="del" data-id="<?php echo $tovar['id']; ?>"  ><i class="icon-trash"></i></a>
                    <p><?php echo LANG('label_tovar_price_position');?>: <strong><?php echo $tovar['price_final']*$tovar['count']; ?></strong></p>
                </div>
            </div>
        <? endforeach;?>
    <? endif;?>
</div>