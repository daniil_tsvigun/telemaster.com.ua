<\?xml version="1.0"?\>
<\?mso-application progid="Excel.Sheet"?\>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">
    <Styles>
        <Style ss:ID="bold">
            <Font ss:Bold="1"/>
        </Style>
    </Styles>
    <Worksheet ss:Name="WorksheetName">
        <Table>
            <Row>
                <?php

                foreach($data as $item): ?>
                    <?php foreach($item as $key=>$val): ?>
                         <Cell ss:StyleID="bold"><Data ss:Type="String"><?php echo $key; ?></Data></Cell>
                    <?php endforeach; ?>
                    <?php break; ?>
                <?php endforeach; ?>
            </Row>
            <?php foreach($data as $item):?>
            <Row>
                <?php foreach($item as $cell): ?>
                <Cell><Data ss:Type="String"><?php echo $cell; ?></Data></Cell>
                <?php endforeach; ?>
            </Row>
            <?php endforeach; ?>
        </Table>
    </Worksheet>
</Workbook>