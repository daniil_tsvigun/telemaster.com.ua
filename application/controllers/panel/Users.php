<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

    public $user = '';
    protected $base_url = 'panel/users';
    protected $home_url = 'panel/users/';
    protected $config_info;
    private $source_models = array(
        'administrative'=>'User_administrative',
        'buyer'=>'User_buyer',
        'painter'=>'User_painter',
    );

    public function __construct(){
        parent::__construct();
        $this->user = get_user(true);

        if(!has_permission('open_admin_panel',$this->user)){
            redirect(site_url() .'/panel/auth/login');
        }

        if(!has_permission('list_users_permissions',$this->user)){
            redirect(site_url() .'/panel/');
        }

        $this->config_info = new Config_info('users');
    }


    public function index()
    {


    }
    public function administrative(){

        $this->_watch('administrative');
    }

    public function buyer(){

        $this->_watch('buyer');

    }

    public function painter(){

        $this->_watch('painter');

    }

    private function _watch($type = 'buyer'){

        $_model = $type .'_model';
        $_table = $type .'_users';
        $data['data_type'] = $type;
        $data['page_title'] = LANG('page_title_users_'. $type .'_list');
        $data['base_link'] = $this->base_url;
        $data['base_lang_link'] = site_url() .'/'.$this->base_url;
        $data['current_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['create_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
        $data['remove_link'] = site_url() .'/'. $this->base_url .'/remove/'. $type;
        $data['edit_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type;
        $data['content'] = 'panel/default/default_list';

        $data['display_fields'] = $this->config_info->display_table_fields;
        $pagination_data = get_pagination_data();
        $data['sort'] = $pagination_data['sort'];

        $data['table_data'] = $this->$_model->get_all_users($pagination_data);
        if(count($data['table_data']) == 0 && $pagination_data['page'] >1){
            $_link = make_link_by($data['current_link'],array('page'));
            $action_link = (substr_count($_link,'?') > 0)? $_link .'&':$_link .'/?';
            redirect($action_link .'page='.($pagination_data['page'] - 1));
        }

        $data['pagination'] = pagination($_table,array(),$pagination_data['page'],$pagination_data['count'],make_link_by($data['current_link'],array('page')));

        $this->load->view('panel/main_layout',$data);
    }

    public function create($type = 'buyer',$close = false){

        redirect_has_no_permission($this->user,'create_user_permissions',$this->base_url .'/'.$type);
        $_source_model = $this->source_models[$type];
        $user = new $_source_model();
        $this->_editor($user,'create',$type,$close);

    }

    public function edit($type = 'buyer',$id = false,$close = false){
        redirect_has_no_permission($this->user,'edit_user_permissions',$this->base_url .'/'.$type);

        $_source_model = $this->source_models[$type];
        $user =  new $_source_model($this->{$type .'_model'}->get_by_id($id));

        if(!$user){
            add_system_message("danger", LANG("ntf_npt_user_no_exist_title"), LANG("ntf_npt_user_no_exist"));
            redirect($this->base_url .'/users/'. $type);
        }

        $this->_editor($user,$id,$type,$close);
    }

    private function _editor($source,$user_id,$type,$close){

        $_model = $type .'_model';
        $_source_model = $this->source_models[$type];
        $data['page_title'] = LANG('page_title_users_'. $type .'_edit');
        if($user_id === 'create'){
            $data['save_link'] = site_url() .'/'. $this->base_url .'/create/'. $type;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/create/'. $type .'/close';
        }else{
            $data['save_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'. $user_id;
            $data['save_close_link'] = site_url() .'/'. $this->base_url .'/edit/'. $type .'/'.$user_id .'/close';
        }

        $data['close_link'] = site_url() .'/'. $this->base_url .'/'. $type;
        $data['content'] = $this->base_url .'/'. $type .'_edit';

        $this->form_validation->set_rules($this->config_info->validation_rules[$type]);

        if ($user_id === 'create'){
            $this->form_validation->set_rules('password', 'lang:label_password', 'required|min_length[6]|max_length[40]');
            $this->form_validation->set_rules('password_repeat', 'lang:label_password_repeat', 'required|matches[password]');
        } else {
            $this->form_validation->set_rules('password', 'lang:label_password', 'min_length[6]|max_length[40]');
            $this->form_validation->set_rules('password_repeat', 'lang:label_password_repeat', 'matches[password]');
        }
        $this->form_validation->data_id = $user_id;

        if ($this->form_validation->run() === true) {

            if($user_id ==='create'){
                $this->$_model->create($this->input->post(),$_source_model);
                add_system_message('success', LANG('ntf_user_created_title'), LANG('ntf_user_created'));
            }else{
                $this->$_model->update($this->input->post(), $user_id,$_source_model);
                add_system_message('success', LANG('ntf_user_edited_title'), LANG('ntf_user_edited'));
            }
            $data['user'] =  new $_source_model($this->input->post());

            if($close || $user_id === 'create'){
                redirect($data['close_link']);
            }
        } else {
            if (count($this->input->post()) > 0) {
                $data['user'] = new $_source_model($this->input->post());
            } else {
                $data['user'] = $source;
            }
        }

        $this->load->view('panel/main_layout',$data);
    }


    public function _is_email($email) {
        if (data_is_email($email)) {
            return true;
        } else {
            $this->form_validation->set_message('_is_email', LANG('error_no_email'));
            return false;
        }
    }

    public function _is_phone($phone) {
        if (data_is_phone($phone)) {
            return true;
        } else {
            $this->form_validation->set_message('_is_phone', LANG('error_no_phone'));
            return false;
        }
    }

}