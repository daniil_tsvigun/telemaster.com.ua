<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                <ul role="tablist" class="nav nav-tabs">
                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="general" href="#general" aria-expanded="true"><?php echo LANG('label_tab_general')?></a></li>
                </ul>
                <div class="tab-content">
                    <div id="general" class="tab-pane active" role="tabpanel">
                        <div class="row">
                            <div class="col-md-8 inside_tab">

                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_text');?> </h5>
                                <textarea name="text" id="content" aria-hidden="true"><?php echo $data->text;?></textarea>

                            </div>
                            <div class="col-md-4">
                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                                <div class="widget-body">
                                    <p><span><? echo LANG ('label_user');?></span>
                                        <?php $this->load->view('panel/templates/select_user',array('select_data'=>$users,'name'=>'user_id','selected'=>$data->user_id));?>
                                    </p>
                                    <p><span><? echo LANG ('label_tovar');?></span>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$tovars,'name'=>'tovar_id','selected'=>array($data->tovar_id),'params' => array('no_all'=>'yes')));?>
                                    </p>
                                    <p><span><? echo LANG ('label_print');?></span>
                                        <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$prints,'name'=>'print_id','selected'=>array($data->print_id),'params' => array('no_all'=>'yes')));?>
                                    </p>
                                    <p>
                                        <span class="head_status"><? echo LANG ('label_public_comment');?></span>
                                        <input 	type="hidden"   name="active" value="0" checked="checked">
                                        <input 	type="checkbox" name="active" class="switcher checked" value="1"
                                                  data-name="active"
                                                  data-entity_id="1"
                                                  data-style="default"
                                                  data-selected="<?php echo($data->active == 1)? 'true':'false';?>"
                                                  data-disabled="false">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>