<?php
$lang['cart_shipping_novaposhta_default_value']             = 'Виберіть зі списку';
$lang['cart_shipping_meestexpress_default_value']           = 'Виберіть зі списку';
$lang['cart_shipping_novaposhta_region_default_value']      = 'Виберіть регіон';
$lang['cart_shipping_novaposhta_city_default_value']        = 'Виберіть місто';
$lang['cart_shipping_novaposhta_warehouse_default_value']   = 'Виберіть склад';
$lang['cart_shipping_meestexpress_region_default_value']    = 'Виберіть регіон';
$lang['cart_shipping_meestexpress_district_default_value']  = 'Виберіть район';
$lang['cart_shipping_meestexpress_city_default_value']      = 'Виберіть місто';
$lang['cart_shipping_meestexpress_warehouse_default_value'] = 'Виберіть склад';