<?php

class Instruction_model extends MY_Model {

    protected $table_instructions='instructions';


    public function __construct() {
        parent::__construct();
    }

    public function get_all_instructions($pagination=false){
    	$this->db->order_by('manufacturer','asc');
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }
        $this->db->from($this->table_instructions);
        $result = $this->db->get()->result_array();
        return $result;
    }



}