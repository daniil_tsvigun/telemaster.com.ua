<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                <div class="row">
                    <div class="col-md-8 inside_tab">
                        <ul role="tablist" class="nav nav-tabs">
                            <?php $_first = true;?>
                            <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="data_<?php echo $lang_key;?>" href="#data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                <?php $_first = false;
                            endforeach;?>
                        </ul>
                        <div class="tab-content">
                            <?php $_first = true;?>
                            <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                <div id="data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                    <input type="text" placeholder="" class="input" value="<?php echo $data->{'title_'. $lang_key};?>" name="title_<?php echo $lang_key;?>"><br>
                                </div>
                                <?php $_first = false;
                            endforeach;?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                        <div class="widget-body">
                            <p><span><? echo LANG ('label_alias');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->alias;?>" name="alias"> </p>
                            <p><span><? echo LANG ('label_filter_group_template');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->data;?>" name="data"> </p>
                            <p><span><? echo LANG ('label_sort');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->sort;?>" name="sort"> </p>
                            <p><span><? echo LANG ('label_parent_filter_category');?></span>
                                <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$categories_tree,'name'=>'category[]','selected'=> $data->category,'params' => array('multiple'=>'multiple','no_all'=>'no')));?>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>