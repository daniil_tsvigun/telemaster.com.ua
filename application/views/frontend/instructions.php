<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('body').css('height','100%');
            //$('.content').css({height:$(window).height()-152+'px'});
        });
    </script>
    <script>
    /**
    * Функция, которая отслеживает клики по исходящим ссылк в Analytics.
    * Эта функция применяет в качестве аргумента строку с действительным URL, после чего использует ее
    * как ярлык события. Если указать beacon в качестве метода передачи, данные обращений
    * будут отправляться с использованием метода navigator.sendBeacon в поддерживающих его браузерах.
    */

    var trackOutboundLink = function(url) {
       ga('send', 'event', 'outbound', 'click', url, {
         'transport': 'beacon',
         'hitCallback': function(){
            window.open(url, '_blank');
            
        }
       });
    }
    </script>
     <div class="content instructions">
        <div class="wrapper">
            <div class="main-block">
                <h2><?php echo LANG('label_instructions'); ?></h2>
                <?php if(false): ?><p class="subtitle" ><?php echo LANG('label_temp_sub_inst'); ?></p> <?php endif; ?>
                <?php if(true): ?>
                    <p class="subtitle" ><?php echo LANG('label_choose_tuner'); ?></p>
                    <p class="subtitle" ><?php echo LANG('label_ifnotuner'); ?> <?php echo LANG('label_uni_instruct'); ?>.</p>
                    <p class="subtitle" ><?php echo LANG('label_anchor_to_calling'); ?></p>

                    <div class="table-wrapper">
                        <div class="tabs-container">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div data-id="normal" class="active wrapper">
                                    <ul class="chanel-ico">
                                        <li><img src="/assets/img/icons/tv-ico/1plus1-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/unian-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/plus-plus-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/bigudi-ico.png" alt=""></li>
                                        <?php if(false): ?><li><img src="/assets/img/icons/tv-ico/kvartal-tv-ico.png" alt=""></li><?php endif; ?>
                                        <li><img src="/assets/img/icons/tv-ico/MB_logo_redes_2015_color_mini.png" alt=""></li>
                                    </ul>
                                    <div class="filter-white"></div>
                                </div>
                            </div>
                            <div  class="col-md-6 col-sm-6 col-xs-6">
                                <div data-id="slm" class="wrapper">
                                    <ul class="chanel-ico">
                                        <li><img src="/assets/img/icons/tv-ico/stb-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/n-canal-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/ictv-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/m1-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/m2-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/qtv-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/Logo_24_kanal_round_mini.png"></li>  
                                    </ul>
                                    <div class="filter-white"></div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="t_manuf table_border-right"><?php echo LANG('label_manufacturer'); ?></th>
                                <th class="t_model table_border-right"><?php echo LANG('label_model'); ?></th>
                                <th class="t_txt table_border-right" >
                                    <div class="sub-cat-top col-md-12 col-sm-12 col-xs-12">
                                        <ul class="chanel-ico">
                                            <li><img src="/assets/img/icons/tv-ico/1plus1-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/unian-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/plus-plus-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/bigudi-ico.png" alt=""></li>
                                            <?php if(false): ?><li><img src="/assets/img/icons/tv-ico/kvartal-tv-ico.png" alt=""></li><?php endif; ?>
                                            <li><img src="/assets/img/icons/tv-ico/MB_logo_redes_2015_color_mini.png" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6"><?php echo LANG('label_txt_instructions'); ?></div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6"><?php echo LANG('label_video_instr'); ?></div>
                                </th>
                                <th class="t_video" >
                                    <div class="sub-cat-top col-md-12 col-sm-12 col-xs-12">
                                        <ul class="chanel-ico">
                                            <li><img src="/assets/img/icons/tv-ico/stb-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/n-canal-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/ictv-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/m1-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/m2-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/qtv-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/Logo_24_kanal_round_mini.png"></li>
                                        </ul>
                                    </div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6"><?php echo LANG('label_txt_instructions'); ?></div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6"><?php echo LANG('label_video_instr'); ?></div>
                                </th>
                                <th class="text_video_instr mobile"><?php echo LANG('label_instruction'); ?></th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr class="universal-guide_row">
                                <td class="t_manuf table_border-right"></td>
                                <td class="table_border-right">
                                    <div><?php echo LANG('label_universal_guide'); ?></div>
                                </td>
                                <td data-tab-id="normal" class="tab-object guide_cell table_border-right t_txt">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <?php
                                        if(LANG=='ru'){
                                            $uni_instr='1_Настройка_1+1_Orton_4100c_рус-min.pdf';
                                            $slm_uni_instr='1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf';
                                        } else{
                                            $uni_instr='1T_Налаштування_тюнера_Orton4100c-min.pdf';
                                            $slm_uni_instr='Налаштування_тюнера_Orton4100c_SLM-min.pdf';
                                        }
                                        ?>
                                        <a target="_blank" href="<?php echo base_url('uploads/'.$uni_instr); ?>"><?php echo LANG('label_pdf'); ?></a>
                                        <a class="mobile text" target="_blank" href="<?php echo base_url('uploads/'.$uni_instr); ?>"></a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="<?php echo site_url('instructions/model/0'); ?>"><?php echo LANG('label_watch'); ?></a>
                                        <a class="mobile video" href="<?php echo site_url('instructions/model/0'); ?>"></a>
                                    </div>
                                </td>
                                <td data-tab-id="slm" class="twisted tab-object guide_cell t_video">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a target="_blank" href="<?php echo base_url('uploads/'.$slm_uni_instr); ?>"><?php echo LANG('label_pdf'); ?></a>
                                        <a class="mobile text" target="_blank" href="<?php echo base_url('uploads/'.$slm_uni_instr); ?>"></a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="<?php echo site_url('instructions/model/slm/0'); ?>"><?php echo LANG('label_watch'); ?></a>
                                        <a class="mobile video" href="<?php echo site_url('instructions/model/slm/0'); ?>"></a>
                                    </div>
                                </td>
                            </tr>
                            <?php if(isset($instructions)&&!is_null($instructions)): ?>
                                <?php foreach($instructions as $instruction): ?>
                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div><?php echo isset($instruction['manufacturer'])?$instruction['manufacturer']:''; ?></div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div><?php echo isset($instruction['model'])?$instruction['model']:''; ?></div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <?php
                                                if(LANG=='ru'&&isset($instruction['txt_instruction_ru'])&&$instruction['txt_instruction_ru']!=NULL&&$instruction['txt_instruction_ru']!=''){
                                                    $txt_instr=$instruction['txt_instruction_ru'];
                                                } elseif(isset($instruction['txt_instruction'])&&$instruction['txt_instruction']!=''&&$instruction['txt_instruction']!=NULL){
                                                    $txt_instr=$instruction['txt_instruction'];
                                                } else{
                                                    $txt_instr='Orton4100c_021116.pdf';
                                                }
                                                ?>
                                                <a target="_blank" onclick="trackOutboundLink('<?php echo isset($txt_instr)? base_url('/uploads/'.$txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>'); return false;"  href="<?php echo isset($txt_instr)? base_url('/uploads/'.$txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>"><?php echo LANG('label_pdf'); ?></a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('<?php echo isset($txt_instr)? base_url('/uploads/'.$txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>'); return false;"  href="<?php echo isset($txt_instr)? base_url('/uploads/'.$txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="<?php echo site_url('instructions/model/'.$instruction['id']); ?>"><?php echo LANG('label_watch'); ?></a>
                                                <a class="mobile video" href="<?php echo site_url('instructions/model/'.$instruction['id']); ?>"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <?php
                                                if(LANG=='ru'&&isset($instruction['slm_txt_instruction_ru'])&&$instruction['slm_txt_instruction_ru']!=NULL&&$instruction['slm_txt_instruction_ru']!=''){
                                                    $slm_txt_instr=$instruction['slm_txt_instruction_ru'];
                                                } elseif(isset($instruction['slm_txt_instruction'])&&$instruction['slm_txt_instruction']!=''&&$instruction['txt_instruction']!=NULL){
                                                    $slm_txt_instr=$instruction['slm_txt_instruction'];
                                                } else{
                                                    $slm_txt_instr='1_Orton4100c_SLM.pdf';
                                                }
                                                ?>
                                                <a target="_blank" onclick="trackOutboundLink('<?php echo isset($slm_txt_instr)? base_url('/uploads/'.$slm_txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>'); return false;" href="<?php echo isset($slm_txt_instr)? base_url('/uploads/'.$slm_txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>"><?php echo LANG('label_pdf'); ?></a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('<?php echo isset($slm_txt_instr)? base_url('/uploads/'.$slm_txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>'); return false;" href="<?php echo isset($slm_txt_instr)? base_url('/uploads/'.$slm_txt_instr) : base_url('uploads/Orton4100c_021116.pdf') ?>"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <?php if($instruction['slm_video'] !=NULL): ?>
                                                    <a href="<?php echo site_url('instructions/model/slm/'.$instruction['id']); ?>"><?php echo LANG('label_watch'); ?></a>
                                                    <a class="mobile video" href="<?php echo site_url('instructions/model/slm/'.$instruction['id']); ?>"></a>
                                                <?php endif; ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <script>
        var ua=navigator.userAgent;
        if(ua.search(/Safari/)>-1){
            $('a').removeAttr('onclick');     
        }
    </script>
<?php $this->load->view('frontend/default/footer'); ?>