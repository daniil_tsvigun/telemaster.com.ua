<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                    <ul role="tablist" class="nav nav-tabs">
                    <?php $firs_tab = true;?>
                    <?php foreach($permission_by_group as $group_title => $group_list):?>
                        <li class="<?php echo ($firs_tab)?'active':'';?>"role="presentation">
                            <a  data-toggle="tab" role="tab" aria-controls="<?php echo $group_title;?>" href="#<?php echo $group_title;?>" aria-expanded="false">
                                <?php echo LANG('permission_title_'.$group_title);?>
                            </a>
                        </li>
                    <?php
                    $firs_tab = false;
                    endforeach;?>
                    </ul>
                    <div class="tab-content">
                        <?php $firs_tab = true;?>
                        <?php foreach($permission_by_group as $group_title => $group_list):?>
                            <div id="<?php echo $group_title;?>" class="tab-pane <?php echo ($firs_tab)?'active':'';?>" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="data-table-articles" class="table table-striped table-bordered table-checks">
                                            <tbody>
                                                <?php foreach($group_list as $permission):?>
                                                    <tr>
                                                        <td width="25px">
                                                            <input type="checkbox" name="permissions[<?php echo $permission['id']?>]" value = "<?php echo $permission['id']?>" class="styled" <?php echo (isset($role->permissions[$permission['id']]))? 'checked="checked"':'';?> />
                                                        </td>
                                                        <td width="300px">
                                                            <?php echo $permission['name']?>
                                                        </td>
                                                        <td>
                                                            <?php echo $permission['description']?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                 </div>
                            </div>
                        <?php
                        $firs_tab = false;
                        endforeach;?>
                    </div>
                    <input type="hidden" name ="work" value="role">
            </form>
        </div>
    </div>
</div>