<!DOCTYPE html>
<html>
<head>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tele Master</title>
    <link href="/assets/lib/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="/assets/lib/chosen/1.4.2/chosen.min.css" rel="stylesheet"> -->
    <!-- <link href="/assets/lib/font-awesome/4.3.0/font-awesome.min.css" rel="stylesheet"> -->
    <link href="/assets/lib/animate.css/3.3.0/animate.min.css" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css?v=1.3.6"/>
    <script type="text/javascript">
        var site_url = "http://www.telemaster.com.ua/ru";
    </script>

</head>
<body>
<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
            Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a
        ,m)

    })(window,document,'script','https://www.google-analytics.com/analytics.js',
        'ga');

    ga('create', 'UA-88207792-1', 'auto');
    ga('send', 'pageview');

</script>

<div id="vk_api_transport"></div><div data-id="mobile_btn" class="lang-menu">
    <ul>
        <li class=""><a href="http://www.telemaster.com.ua/ua/faq">Укр</a></li>
        <li class="current"><a href="http://www.telemaster.com.ua/ru/faq">Рус</a></li>
    </ul>
</div>
<div data-id="mobile_btn" class="master ">
    <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
</div>
<section class="nav_bar" >
    <div class="wrapper">
        <div class="menu">
            <a href="http://www.telemaster.com.ua/ru" class="logo-anchor"></a>
            <ul class="main-menu slideInDown animated">

                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru">Главная</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/calling">Найти мастера</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/partners">Найти провайдера</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/instructions">Инструкции</a>
                </li>
                <!--                <li>-->
                <!--                    <a class="--><!--" href="--><!--">--><!--</a>-->
                <!--                </li>-->
                <li class="master ">
                    <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
                </li>
            </ul>
            <div id="menu-btn"></div>
            <div class="lang-menu">
                <ul>

                    <li class=""><a href="http://www.telemaster.com.ua/ua/faq">Укр</a></li>
                    <li class="current"><a href="http://www.telemaster.com.ua/ru/faq">Рус</a></li>
                </ul>
            </div>
            <div class="master ">
                <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
            </div>
        </div>

    </div>
</section>    <div data-alias="faq" class="content single-instruction">
    <div class="wrapper">
        <div class="main-block">

            <h2>Вопросы и ответы</h2>
            <div data-first="" class="text">
                <div>
                    <div class="faq">
                        <div id="parameters" class="question">
                            <div class="q">Новые параметры спутникового приема для телеканалов</div>
                            <div class="answer">Параметры спутникового вещания для каналов <strong>1+1, ПлюсПлюс, Униан, Бигуди, Music Box</strong>:
                                <ul>
                                    <li>cпутник - Astra 4A (SES Sirius) В некоторых моделях тюнеров спутник может называться &laquo;Sirius 2/3&raquo; или &laquo;Sirius 4&raquo;;</li>
                                    <li>орбитальная позиция - 5&deg; вост. д. (E)(в некоторых тюнерах может обозначаться как 4.8&deg;);</li>
                                    <li>частота - 11766 MГц;</li>
                                    <li>символьная скорость - 27,5 Мсимв/с (MSymb/s) или 27500 ксимв/с (ksymb/s);</li>
                                    <li>поляризация - горизонтальная (H);</li>
                                    <li>FEC - &frac34;;</li>
                                    <li>модуляция - DVB-S (QPSK).</li>
                                </ul>
                                Параметры спутникового вещания для каналов <strong>СТБ, Новый канал, ICTV, М1, М2, QTV, 24 канал:</strong>
                                <ul>
                                    <li>cпутник &ndash; AMOS-3. В некоторых моделях тюнеров может называться &laquo;AMOS&raquo;, &laquo;AMOS 1&raquo;, или &laquo;AMOS 2&raquo;;</li>
                                    <li>орбитальная позиция - 4&deg;W;</li>
                                    <li>частота &ndash; 11 176 МГц;</li>
                                    <li>символьная скорость - 30,0 Мсимв/с (MSymb/s), или 30000 ксимв/с (ksymb/s);</li>
                                    <li>поляризация - горизонтальная (H);</li>
                                    <li>FEC - &frac34;;</li>
                                    <li>модуляция - DVB-S (QPSK).</li>
                                </ul>
                            </div>
                        </div>
                        <div class="question">
                            <div class="q">После сканирования с новыми параметрами канал "1+1" появился в списке, однако изображение отсутствует</div>
                            <div class="answer">Вместо частоты 11766 введите частоту 11765 и просканируйте каналы еще раз. В конце списка каналов появятся "1+1", "ТЕТ", "2+2", "Уніан", "Бігуді", "ПлюсПлюс", "Music Box" и др. После этого удалите из списка ранее добавленные каналы и оставьте просканированные только что. На вновь просканированном канале "1+1" должно появиться изображение.</div>
                        </div>
                        <div class="question">
                            <div class="q">При сканировании транспондера не обнаруживается еще один канал "1+1" в списке телеканалов</div>
                            <div class="answer">Скорее всего, канал "1+1" появился в списке радиоканалов. Удалите его, пожалуйста (порядок действий - согласно инструкции по удалению телеканалов, только надо выбрать радиоканалы), и пересканируйте транспондер еще раз. После этого телеканал "1+1" должен появиться в конце списка телеканалов.</div>
                        </div>
                        <div class="question">
                            <div class="q">Для чего создан сайт telemaster.com.ua?</div>
                            <div class="answer">Телевизионные каналы в Украине распространяются при помощи разных технологий, в частности через спутник. В парке спутников, которые обслуживают территорию Украины, в 2016 году ожидаются изменения. Вследствие этих изменений параметры спутникового сигнала некоторых украинских каналов могут измениться. <br /> Если вы пользуетесь спутниковой антенной, при изменениях параметров спутникового сигнала каналов вам будет необходимо перенастроить параметры их приема в вашем домашнем ТВ тюнере.<br /> На сайте telemaster.com.ua будут собраны инструкции по перенастройке тюнеров. Эта информация будет уточнятся с телеканалами, которые меняют параметры спутникового сигнала, обновляться и пополняться на нашем сайте</div>
                        </div>
                        <div class="question">
                            <div class="q">Как пользоваться сайтом?</div>
                            <div class="answer">На сайте telemaster.com.ua размещаются инструкции телеканалов по настройке новых параметров вещания на тюнерах спутникового ТВ. Информация будет обновляться в виде текстовых и видео инструкций для телезрителей. С помощью инструкций вы сможете перенастроить ваш ТВ тюнер самостоятельно.<br /> Те телезрители, которые не смогут перенастроить свой тюнер самостоятельно, могут нуждаться в помощи мастера по спутниковому телевидению для перенастройки тюнера и / или других работ по наладке, ремонту своей спутниковой системы.<br /> Мастер, зарегистрированный на сайте telemaster.com.ua, и его контактная информация доступны посетителям сайта.</div>
                        </div>
                        <div class="question">
                            <div class="q">Какие изменения будут происходить в ближайшее время, как поможет мне этот ресурс?</div>
                            <div class="answer">В ближайшее время более 40 украинских телеканалов меняют параметры спутникового вещания. На ресурсе выложены новые параметры вещания и инструкции по настройке телеканалов с новыми параметрами спутникового вещания.<br /> Кроме того, в 2017г. запланировано полное отключение аналогового телевизионного вещания (срок полного отключения до 30 июня 2017.). Абонентам аналогового телевизионного вещания придется выбирать альтернативные варианты приема и просмотра телевизионных программ. Наш ресурс поможет в выборе.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как перенастроить каналы?</div>
                            <div class="answer">Прием спутникового телевидения ведется через тюнер. Тюнер &ndash; это небольшое устройство, подключенное к вашему телевизору. На передней панели тюнера найдите название модели вашего тюнера. После этого найдите ваш тюнер в <a href="../../../../../ru/instructions">списке тюнеров</a>. Если вашего тюнера нет в списке, возможно, такая модель тюнера не очень распространена на рынке или относится к устаревшим. В этом случае предлагаем вам воспользоваться общей инструкцией (<a href="../../../../../uploads/1t_%D0%BD%D0%B0%D0%BB%D0%B0%D1%88%D1%82%D1%83%D0%B2%D0%B0%D0%BD%D0%BD%D1%8F_%D1%82%D1%8E%D0%BD%D0%B5%D1%80%D0%B0_orton4100c.pdf">текст</a> / <a href="../../../../instructions/model/1">видео</a>), или обратиться за <a href="../../../../../ru/calling">помощью к мастеру</a>.</div>
                        </div>
                        <div class="question">
                            <div class="q">Я не нашел инструкцию к своему тюнеру, что делать?</div>
                            <div class="answer">Мы разместили на сайте инструкции для наиболее распространенных моделей тюнеров, используемых сейчас в Украине. Возможно, ваша модель тюнера отсутсвует в этом списке, если такая модель тюнера не очень распространена на рынке или относится к устаревшим. В этом случае предлагаем вам воспользоваться общей инструкцией (<a href="../../../../../uploads/1t_%D0%BD%D0%B0%D0%BB%D0%B0%D1%88%D1%82%D1%83%D0%B2%D0%B0%D0%BD%D0%BD%D1%8F_%D1%82%D1%8E%D0%BD%D0%B5%D1%80%D0%B0_orton4100c.pdf">текст</a> / <a href="../../../../instructions/model/1">видео</a>), или обратиться за <a href="../../../../../ru/calling">помощью к мастеру</a>.</div>
                        </div>
                        <div class="question">
                            <div class="q">У меня телевизор со встроенным тюнером. Как его настроить?</div>
                            <div class="answer">В этом случае меню настроек тюнера встроено в меню телевизора. Воспользуйтесь, пожалуйста, инструкцией по эксплуатации Вашего телевизора и найдите в меню соответствующий раздел. Если вы впервые настраиваете новое (ненастроенное оборудование), то, чтобы понять общие принципы настройки, используйте информацию, приведенную в универсальной инструкции по настройке тюнеров (<a href="../../../../../uploads/1t_налаштування_тюнера_orton4100c.pdf" target="_blank">текст</a> / <a href="instructions/model/1">видео</a>). Если вы делаете это впервые, на уже настроенном оборудовании - советуем обратиться за <a href="calling">помощью к мастеру</a>.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как мне найти мастера?</div>
                            <div class="answer">Перейдите в раздел &laquo;<a href="../../../../../ru/calling">Найти мастера</a>&raquo;. В окошке поиска введите название вашего населенного пункта. Кликните на кнопку &laquo;Найти мастера&raquo; под окошком поиска. Вы перешли на страницу со списком мастеров, обслуживающих ваш населенный пункт. Перезвоните мастеру, договоритесь с ним о настройке вашего тюнера. Услуга выезда мастера платная. Плату за услугу устанавливают сами мастера, поэтому стоимость услуги у разных мастеров может быть разной.</div>
                        </div>
                        <div class="question">
                            <div class="q">Моего города нет в списке населенных пунктов. Как мне вызвать мастера?</div>
                            <div class="answer">Мы прилагаем все усилия для того, чтобы найти для вас мастеров во всех районах Украины. Однако, может так случиться, что в некоторых немногочисленных районах в данный момент активных мастеров нет (мастер в отпуске, на больничном и т.д.). В этом случае предлагаем поискать мастеров в близлежащих районах.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как вызвать мастера?</div>
                            <div class="answer">Посетители сайта обращаются к мастеру напрямую по указанным им телефонам на сайте. В случае, если вам необходима помощь мастера, сделайте следующее:
                                <ol>
                                    <li>перейдите на страницу &laquo;<a href="www.telemaster.com.ua/ru/calling">Найти мастера</a>&raquo; ;</li>
                                    <li>введите название вашего населенного пункта в окне поиска;</li>
                                    <li>выберите мастера из списка;</li>
                                    <li>свяжитесь с мастером по указанному им номеру телефона.</li>
                                </ol>
                            </div>
                        </div>
                        <div class="question">
                            <div class="q">Как отменить/перенести выезд мастера?</div>
                            <div class="answer">Свяжитесь с мастером, с которым вы договаривались о выезде.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как оплатить заказ?</div>
                            <div class="answer">Информационный ресурс telemaster.com.ua не регулирует финансовые взаимоотношения между мастером и клиентом. Оплата производится по договоренности клиента с мастером.</div>
                        </div>
                        <div class="question">
                            <div class="q">Что делать, если мастер не приехал или выполнил работу некачественно?</div>
                            <div class="answer">Вы можете оценить работу мастера с помощью рейтинга или отзыва на странице мастера. Сайт оставляет за собой право модерации рейтингов и отзывов.</div>
                        </div>
                        <div class="question">
                            <div class="q">Я &ndash; мастер. Как пользоваться сайтом?</div>
                            <div class="answer">Если у Вас есть желание оказывать услуги по настройке спутникового оборудования, или вы уже предоставляете такие услуги, предлагаем зарегистрироваться на нашем сайте. <br /> Мастер, зарегистрированный на сайте telemaster.com.ua, и его контактная информация будут доступны посетителям сайта. Посетители сайта обращаются непосредственно к мастеру по указанным им телефонам. <br /> Telemaster.com.ua является информационной площадкой, с помощью которой абонент спутникового приема получает информацию по настройке своего спутникового оборудования.</div>
                        </div>
                        <div class="question">
                            <div class="q">Сколько я получу заказов?</div>
                            <div class="answer">Мы работаем над продвижением telemaster.com.ua в регионах Украины. Сайт не фильтрует заказы, Вы получите натуральный поток обращений пользователей.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как повысить привлекательность моего профиля?</div>
                            <div class="answer">При регистрации на сайте укажите максимально точную информацию о себе. Наличие фотографии в профиле мастера важно для ваших клиентов &ndash; так они будут уверены, что мастер, приехавший по вызову &ndash; именно тот, к которому они обратились на сайте. В карточке мастера есть его рейтинг и отзывы. Высокий рейтинг и положительные отзывы важны для увеличения количества заказов.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как получить высокий рейтинг и положительные отзывы?</div>
                            <div class="answer">Мастеру нужно качественно выполнить заказ. После выполнения заказа попросите клиента оценить вашу работу на сайте telemaster.com.ua с помощью выставления рейтинга или написания отзыва о Вашей работе.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как отказаться от заказа?</div>
                            <div class="answer">Сообщите клиенту, что на данный момент Вы не можете выполнить заказ. Клиент может обратиться к другому мастеру в данном регионе.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как я могу удалить/изменить свои контакты с сайта?</div>
                            <div class="answer">Обратитесь к координаторам мастеров по электронной почте <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> или перезвоните на Линию Поддержки Мастеров (050 480 82 82, 093 170 82 82, 067 011 82 82).</div>
                        </div>
                        <div class="question">
                            <div class="q">Кому я могу пожаловаться/задать вопрос?</div>
                            <div class="answer">Обратитесь к координаторам мастеров по электронной почте <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> или перезвоните на Линию Поддержки Мастеров (050 480 82 82, 093 170 82 82, 067 011 82 82).</div>
                        </div>
                        <div class="question">
                            <div class="q">Как я могу разместить мои контакты на сайте?</div>
                            <div class="answer">Для размещения информации о вас, как о мастере, необходимо сделать следующее:
                                <ol>
                                    <li>пройти регистрацию на сайте в разделе &laquo;<a href="../../../../../ru/registration">Регистрация мастера</a>&raquo;.</li>
                                    <li>заполнить анкету, которую вы получите по электронной почте;</li>
                                    <li>пройти сертификацию и получить ID номер Мастера;</li>
                                    <li>регистрация завершена, ваши контакты опубликованы на сайте.</li>
                                </ol>
                            </div>
                        </div>
                        <div class="question">
                            <div class="q">Я уже зарегистрирован на сайте. Как можно добавить на сайт мою фотографию?</div>
                            <div class="answer">Отправьте, пожалуйста, фото на <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a>.<br /> Для вашей идентификации необходимо указать ваши фамилию, имя, отчество и номер телефона, который был указан при регистрации.</div>
                        </div>
                        <div class="question">
                            <div class="q">Я прошел регистрацию (отправил свою анкету) несколько дней назад. Когда мне перезвонят?</div>
                            <div class="answer">Внесение изменений на сайт для целей регистрации мастера может занимать до 5 рабочих дней. Команда Telemaster делает все возможное, чтобы зарегистрировать мастеров в максимально короткие сроки.</div>
                        </div>
                        <div class="question">
                            <div class="q">Как происходит сертификация?</div>
                            <div class="answer">С вами свяжется по телефону координатор мастеров. Он задаст вам ряд вопросов, чтобы проверить ваш опыт в работе с системами спутникового телевидения.</div>
                        </div>
                        <div class="question">
                            <div class="q">Что такое ID?</div>
                            <div class="answer">Это личный уникальный номер мастера на сайте telemaster.com.ua.</div>
                        </div>
                        <div class="question">
                            <div class="q">Возможна ли регистрация юридического лица в качестве мастера?</div>
                            <div class="answer">Да. Обратитесь к координаторам мастеров по эклектронной почте <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> или перезвоните на Линию Поддержки Мастеров.</div>
                        </div>
                        <div class="question">
                            <div class="q">Есть ли правила для публикации отзывов?</div>
                            <div class="answer">Ответ: Отзыв о работе мастера может оставить любой зарегистрированняй пользователь сайта. Сайт оставляет за собой право модерации отзывов. Если у мастера возникают вопросы, касающиеся отзывов, он может обратиться к координаторам мастеров по эклектронной почте <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> или перезвонить на Линию Поддержки Мастеров (050 480 82 82, 093 170 82 82, 067 011 82 82).</div>
                        </div>
                        <div class="question">
                            <div class="q">Если получен незаслуженный отзыв, что делать?</div>
                            <div class="answer">Ответ: Сайт оставляет за собой право модерации отзывов. Если у мастера возникают вопросы, касающиеся отзывов, он может обратиться к координаторам мастеров по эклектронной почте <a href="mailto:info@telemaster.com.ua">info@telemaster.com.ua</a> или перезвонить на Линию Поддержки Мастеров (050 480 82 82, 093 170 82 82, 067 011 82 82).</div>
                        </div>
                        <div class="question">
                            <div class="q">Мастер самостоятельно устанавливает цену за услугу?</div>
                            <div class="answer">Да, стоимость выезда мастера и работ на месте устанавливается мастером самостоятельно, но при этом клиент имеет право выбора и может запросить стоимость выезда и работ у нескольких мастеров в районе.</div>
                        </div>
                        <div class="question">
                            <div class="q">Зачем добавлять фото?</div>
                            <div class="answer">Наличие фотографии в профиле мастера важно для клиентов: это дает возможность клиенту увидеть мастера перед визитом. Ваша фотография добавит вам доверия потенциальных клиентов.</div>
                        </div>
                        <div class="question">
                            <div class="q">Какие требования к фото?</div>
                            <div class="answer">Мы рекомендуем размещать фото, аналогичное фотографиям для официальных документов (паспорт, виза). Аватары, некачественные фото, рисунки и т.д., а также изображения, носящие рекламный характер, размещены не будут.</div>
                        </div>
                        <div class="question">
                            <div class="q">Если клиент откажется платить, кто будет возмещать расходы?</div>
                            <div class="answer">telemaster.com.ua &ndash; информационный ресурс. Сайт не регулирует стоимость заказов и не несет ответственности по их оплате. Стоимость заказа, время выезда и условия работы мастеру необходимо обсуждать напрямую с клиентом.</div>
                        </div>
                        <div class="question">
                            <div class="q">Сколько я буду зарабатывать? / Есть плата за размещение моих контактов на сайте?</div>
                            <div class="answer">Мы не берем денег за размещение контактов мастеров на сайте и не устанавливаем цены сервисных работ. У клиента есть право обратиться к другому мастеру в вашем регионе для сравнения стоимости.</div>
                        </div>
                    </div>                    </div>
            </div>

        </div>
    </div>
</div>
<div class="hidden rate popup fadeIn animated">
    <div class="wrapper">
        <div class="wrap">
            <div class="box">
                <div class="close"></div>
                <div class="rate">
                    <div class="levels" data-choosen="null">
                        <ul>
                            <li class="" data-level_id="0" id="level0"><div></div></li>
                            <li class="" data-level_id="1" id="level1"><div></div></li>
                            <li class="" data-level_id="2" id="level2"><div></div></li>
                            <li class="" data-level_id="3" id="level3"><div></div></li>
                            <li class="" data-level_id="4" id="level4"><div></div></li>
                        </ul>
                    </div>
                    <div class="btn-box">
                        <div id="vote" class="btn btn-type3">Проголосовать</div>
                    </div>
                    <div class="auth-box">
                        <div class="fb_auth"></div>
                        <div class="vk_auth"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="wrapper">
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <a href="http://www.telemaster.com.ua/ru/about">О нас</a>
            <a href="http://www.telemaster.com.ua/ru/faq">Вопросы и ответы</a>
            <a href="http://www.telemaster.com.ua/ru/rules">Правила пользования сайтом</a>
            <a href="http://www.telemaster.com.ua/ru/reports">Правила модерации отзывов</a>
            <a href="http://www.telemaster.com.ua/ru/polices">Политика конфиденциальности</a>

        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 center">
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <p>Администратор сайта: </p>
            <p>info@telemaster.com.ua</p>
            <div class="btns special">
                <div id="youtube_btn">
                    <a target="_blank" href="https://www.youtube.com/channel/UCUrsf864ut8p5UETF4Ym6Hw">&nbsp;</a>
                </div>
            </div>
        </div>
    </div>
    <div class="rights">
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <div class="logo"></div>
            </div>
            <div class="center col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <p>© 2016 All rights reserved</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p>
                    <a href="http://lemon.ua/">Создание сайта  Lemon.ua</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="/assets/js/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/lib/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="/assets/lib/chosen/1.4.2/chosen.jquery.min.js"></script> -->
<script type="text/javascript" src="/assets/lib/animate.css/3.3.0/animate.min.css" ></script>