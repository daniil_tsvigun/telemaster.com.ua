<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content region">
        <div class="wrapper">
            <div class="main-block">
                <h2><?php if(isset($regions->{'district_place'.SQL_LANG})){
                        if($regions->type==3){
                            echo $regions->{'district_place'.SQL_LANG}.LANG('label_district');
                        } else {
                            echo $regions->{'district_place'.SQL_LANG};
                        }

                    }else{
                        LANG('label_no_masters');
                    }  ?></h2>
                <table class="table table-striped masters">
                    <tbody>
                    <?php foreach($masters as $master): ?>
                        <?php if($master->active==1): ?>
                        <tr>
                            <td class="avatar">
                                <div class="avatar-wrapper">
                                    <div class="frame"></div>
                                    <?php $str='images/avatars/'.$master->id.'/'. $master->image['img_name'] .'/b_' . $master->image['img_name'] . '.' . $master->image['img_type']; ?>
                                    <img style="<?php if($master->id==26&&false): echo 'transform: rotate(90deg);'; endif; ?>" class="img img-responsive" src="<?php
                                        if($master->image['img_name']!=NULL&&$master->image['img_type']!=NULL&&file_exists($str)) {
                                            echo base_url('images/avatars') . '/'.$master->id.'/'. $master->image['img_name'] .'/b_' . $master->image['img_name'] . '.' . $master->image['img_type'];
                                        } else {
                                            echo base_url('assets/img/master_av.png');//'http://placehold.it/136x120/ffd32d';
                                        }
                                    ?>" alt="no-image">
                                </div>
                            </td>
                            <td class="master-info">
                                <div class="wrapper master">
                                    <div class="name">
                                    <?php $prefix=SQL_LANG; 
                                        if(LANG=='ru'){
                                            $prefix='';
                                        }
                                        $name=$master->{'name'.$prefix};
                                        $surname=$master->{'surname'.$prefix};
                                        if($name!=''||$surname!=''){
                                            echo $name.' '.$surname;
                                        } elseif(LANG=='ru'){
                                            echo $master->name_ua.' '.$master->surname_ua;
                                        } else{
                                            echo $master->name.' '.$master->surname;
                                        }
                                    ?>
                                    
                                        <?php //echo $master->name.' '.$master->surname; ?>
                                    
                                    </div>
                                    <div class="text id">
                                        id: <?php echo $master->id_master; ?>
                                    </div>
                                </div>
                            </td>
                            <td class="label_r">
                                <div class="text wrapper rate_label">
                                    <?php echo LANG('label_rate'); ?>
                                </div>
                            </td>
                            <td class="chart">
                                <div class="rate">
                                    <div class="levels">
                                        <ul>
                                            <li class="<?php if( $master->rate>=0.2): echo 'active'; endif; ?>" data-level_id="0" id="level0"><div></div></li>
                                            <li class="<?php if( $master->rate>=0.4): echo 'active'; endif; ?>" data-level_id="1" id="level1"><div></div></li>
                                            <li class="<?php if( $master->rate>=0.6): echo 'active'; endif; ?>" data-level_id="2" id="level2"><div></div></li>
                                            <li class="<?php if( $master->rate>=0.8): echo 'active'; endif; ?>" data-level_id="3" id="level3"><div></div></li>
                                            <li class="<?php if( $master->rate>=1): echo 'active'; endif; ?>" data-level_id="4" id="level4"><div></div></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                            <?php $count=0; ?>
                                    <?php foreach($master->comments as $comment):
                                        if($comment['active']==1):
                                            $count++;
                                        endif;
                                    endforeach; ?>
                            <td class="comments_label">
                                <div class="text comments" ><?php echo LANG('label_comments'); ?>(<?php echo $count; ?>)</div>
                            </td>
                            <td class="btn-box">
                                <a href="<?php echo site_url('calling/master/'.$master->id); ?>" class="btn btn-type3"><?php echo LANG('label_watch_contacts'); ?></a>
                            </td>
                        </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $this->load->view('frontend/default/footer'); ?>