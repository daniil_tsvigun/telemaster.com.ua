<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();

    }

    public function index()
    {
        $this->login();
    }

    public function login($redirect = false){

        $data['email'] = ($this->input->post('email'))? $this->input->post('email'):'';
        $data['password'] = ($this->input->post('password'))?$this->input->post('password'):'';


        if($data['email'] && $data['password']){

            if(!data_is_email($data['email'])){
                add_system_message("danger", LANG("ntf_npt_auth_no_combo_title"), LANG("ntf_npt_auth_no_combo_exist"));
            }else{
                $user = auth_user($data['email'],$data['password']);
                if($user){
                    $_SESSION['user'] = $user;
                    if($redirect){
                        redirect(site_url($redirect));
                    }else{
                        redirect(site_url());
                    }
                }
            }
        }

        $data['auth_message'] = get_system_messages();

        $this->load->view('frontend/auth/login',$data);

    }
    public function registration($key=false){

        $data['user'] = new User($this->input->post());
        $this->form_validation->set_rules('name', 'lang:label_name', 'required|min_length[6]|max_length[40]');
        $this->form_validation->set_rules('role_id', 'lang:label_role', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('email', 'lang:label_email', 'required|callback__is_email|is_unique[buyer_users.email]|is_unique[painter_users.email]');
        $this->form_validation->set_rules('password', 'lang:label_password', 'required|min_length[6]|max_length[40]');
        $this->form_validation->set_rules('password_repeat', 'lang:label_password_repeat', 'required|matches[password]');

        if ($this->form_validation->run() === true) {
            if($this->input->post('role_id') == 4){
                $_id=$this->painter_model->create($this->input->post(),'User_painter');
            }else{
                $_id=$this->buyer_model->create($this->input->post(),'User_buyer');
            }
            $key=substr(base64_encode(substr(crypt($this->input->post('email'),RECOVERY_SALT_PARAMS.RECOVERY_SALT),strlen(RECOVERY_SALT_PARAMS))),0,-2);

            $_message_data = array(
                'user_id'=>$_id,
                'user_role_id'=>$this->input->post('role_id'),
                'template'=>'email_confirmation',
                'message'=>'',
            );
            $_message_data['message']['confirm_link']=site_url().'/auth/registration/'.$key.'/?email='.$this->input->post('email');
            $this->sender_model->message($_message_data);
            $data['send_message']=LANG('label_sent_successfully');
            $this->load->view('frontend/auth/registration',$data);

        }

        $data['user'] = ($this->input->post('role_id') == 4)? new User_painter($this->input->post()): new User_buyer($this->input->post());
        $data['auth_message'] = get_system_messages();
        if($key!=false){

            $email= $this->input->get('email');
            $user_painter_data=$this->painter_model->get_user_by_email_registration($email);
            $user_buyer_data=$this->buyer_model->get_user_by_email_registration($email);

            if(!is_null($user_painter_data)){
                $user_data=$user_painter_data;
            } elseif(!is_null($user_buyer_data)){
                $user_data=$user_buyer_data;
            } else {
                $user_data=false;
            }

            if(isset($email)&&!empty($email)&&$user_data!==false){
                $generate=substr(crypt($email,RECOVERY_SALT_PARAMS.RECOVERY_SALT),strlen(RECOVERY_SALT_PARAMS));
                if($generate==base64_decode($key)){
                    $this->buyer_model->email_confirmation($email);
                    $_message_data = array(
                        'user_id'=>$user_data['id'],
                        'user_role_id'=>$user_data['role_id'],
                        'template'=>'registration_greeting',
                        'message'=>'',
                    );
                    $this->sender_model->message($_message_data);
                    redirect(site_url('/auth/login'));
                } else {
                    redirect(site_url());
                }
            } else {

            }
        } else {
            $this->load->view('frontend/auth/registration',$data);
        }

    }

    public function recovery($key='send'){

        if($key=='send'){
            $this->form_validation->set_rules('email', 'lang:label_email', 'required|callback__is_email');
            $this->load->view('frontend/auth/sendmail');

            if ($this->form_validation->run() === true) {

                $email = $this->input->post('email');

                $user_painter_data=$this->painter_model->get_user_by_email($email);
                $user_buyer_data=$this->buyer_model->get_user_by_email($email);
                if(!is_null($user_painter_data)){
                    $user_data=$user_painter_data;
                } elseif(!is_null($user_buyer_data)){
                    $user_data=$user_buyer_data;
                } else {
                    $user_data=false;
                }
                if($user_data!==false){
                    $key=substr(base64_encode(substr(crypt($email,RECOVERY_SALT_PARAMS.RECOVERY_SALT),strlen(RECOVERY_SALT_PARAMS))),0,-2);
                    $_message_data = array(
                        'user_id'=>$user_data['id'],
                        'user_role_id'=>$user_data['role_id'],
                        'template'=>'recovery',
                        'message'=>'',
                    );
                    $_message_data['message']['recover_link']=site_url().'/auth/recovery'.'/'.$key.'/?email='.$email;
                    $this->sender_model->message($_message_data);

                    //redirect(site_url('/auth/login'));
                }
            }

        } else {
            $email= $this->input->get('email');
            $user_painter_data=$this->painter_model->get_user_by_email($email);
            $user_buyer_data=$this->buyer_model->get_user_by_email($email);
            if(!is_null($user_painter_data)){
                $user_data=$user_painter_data;
            } elseif(!is_null($user_buyer_data)){
                $user_data=$user_buyer_data;
            } else {
                $user_data=false;
            }
            if(isset($email)&&!empty($email)&&$user_data!==false){
                $generate=substr(crypt($email,RECOVERY_SALT_PARAMS.RECOVERY_SALT),strlen(RECOVERY_SALT_PARAMS));
                if($generate==base64_decode($key)){
                    $data['form_url']=site_url().'/auth/recovery'.'/'.substr(base64_encode($generate),0,-2).'/?email='.$email;
                    $this->form_validation->set_rules('password', 'lang:label_password', 'required|min_length[6]|max_length[40]');
                    $this->form_validation->set_rules('password_repeat', 'lang:label_password_repeat', 'required|matches[password]');
                    $this->load->view('frontend/auth/recovery',$data);
                    if ($this->form_validation->run() === true) {
                        $_model = ($user_data['role_id']== 4)?'painter_model':'buyer_model';
                        $_source_class = ($user_data['role_id'] == 4)?'User_painter':'User_buyer';
                        $user_data['password']=$this->input->post('password');
                        $this->$_model->update($user_data,$user_data['id'],$_source_class);
                        redirect(site_url('/auth/login'));
                    }
                } else {
                    redirect(site_url());
                }
            } else {
                redirect(site_url());
            }
        }
    }

    public function _is_email($email) {
        if (data_is_email($email)) {
            return true;
        } else {
            $this->form_validation->set_message('_is_email', LANG('error_no_email'));
            return false;
        }
    }

    public function logout(){
        unset($_SESSION['user']);
        redirect(site_url() .'/auth/login');
    }

}
