<?php if(count($menu_types) >0):?>
    <?php foreach($menu_types as $item_type_data):?>
        <h5 class="widget-name"><i class="icon-reorder"></i> <?php echo $item_type_data['title'];?></h5>
        <?php if($item_type_data['content']):?>
        <ul data-id="item_id" id="ex_select" class="clearfix">
            <li>
                <ul class="entity_group">
                    <?php foreach($item_type_data['content'] as $item):?>
                    <li data-item_type="<?php echo $item_type_data['id'];?>" data-item_id="<?php echo $item['id']?>" class="col-md-4 <?php if($data->item_type == $item_type_data['id'] && $data->item_id == $item['id']){ echo 'selected';}?>"><?php echo $item['title']?><? echo (isset($item['parent_title']) && $item['parent_title'])? '('. $item['parent_title'] .')':'';?></li>
                   <?php endforeach;?>
                </ul>
            </li>
        </ul>
        <?php endif;?>
    <?php endforeach;?>
<?php endif;?>