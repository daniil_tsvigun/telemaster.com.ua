<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

    public $user = '';

    public function __construct(){
        parent::__construct();
        $this->user = get_user();
        $this->load->library('FolderWork');
        $this->load->library('UploadImage');
    }

    public function index(){

        $this->form_validation->set_rules('name', 'lang:label_name', 'required|max_length[50]');
        $this->form_validation->set_rules('surname', 'lang:label_surname', 'required|max_length[50]');
        $this->form_validation->set_rules('inn', 'lang:label_inn', '');
        $this->form_validation->set_rules('email', 'lang:label_email', 'required|max_length[100]|valid_email');
        $this->form_validation->set_rules('phone1', 'lang:label_phone', 'required|max_length[50]|numeric');
        $this->form_validation->set_rules('passport', 'lang:label_passport', 'max_length[100]');
        $this->form_validation->set_rules('passport_data', 'lang:label_passport_data', 'max_length[150]');
        if ($this->form_validation->run() === true) {
            $check_email=count($this->master_model->get_master_by_email($this->input->post('email')));
            $check_phone=count($this->master_model->get_master_by_phone('+38'.$this->input->post('index1').$this->input->post('phone1')));
            $check_regions=false;
            if($this->input->post('regions')!=NULL&&count($this->input->post('regions'))>0){
                $check_regions=true;
            }
            if($check_email>0&&$check_phone>0){
                $data['success_message']=LANG('label_err_on_reg_unique');
            }elseif($check_phone>0){
                $data['success_message']=LANG('label_err_on_reg_phone');
            } elseif($check_email>0){
                $data['success_message']=LANG('label_err_on_reg_email');
            } elseif(!$check_regions){
                $data['success_message']=LANG('label_err_on_reg');
            } else {
                $_id=$this->master_model->create_master($this->input->post());
                //$data['id']=$_id;
                if($_id!=false||$_id!=NULL){
                    $message=$this->master_model->get_master_by_id($_id);
                    $data['to_email']=$message->email;
                    $data['role']=1;
                    $data['template']='to_admin_registered';
                    $data['subject'] = LANG('label_registration_subject');
                    $data['message'] = $message;
                    $this->sender_model->message($data);
                    $data['role']=0;
                    $data['template']='to_master_registered';
                    $this->sender_model->message($data);
                    $data['success_message']=LANG('label_success_on_reg');
                } 
                
            }
        }
        $data['areas']=$this->region_model->get_all_areas();

        /* Removeing AR Crimea */
        foreach($data['areas'] as $key=>$area){
            if($area['id']==01){
                //unset($data['areas'][$key]);
            }
        }

        $this->load->view('frontend/registration',$data);
    }

    public function update(){
        $post=$this->input->post();
        if(!empty($post)){
            $this->master_model->update_master($this->input->post());
            $data['success_message']=LANG('label_success_on_reg');
        }
        $data['areas']=$this->region_model->get_all_areas();
        $this->load->view('frontend/update',$data);
    }

    public function make_report(){
        $today=getdate();
        $d=new DateTime($today['year'].'-'.$today['mon'].'-'.$today['mday']);
        $d->modify("-1 day");
        $start_date = $d->format('Y-m-d').' '.'00'.':'.'00'.':'.'00';
        $end_date = $d->format('Y-m-d').' '.'23'.':'.'59'.':'.'59';
        // $start_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'00'.':'.'00'.':'.'00';
        // $end_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'23'.':'.'59'.':'.'59';
        $masters = $this->master_model->get_master_between_dates($start_date,$end_date);
        foreach($masters as $key=>$master){
            $data=$this->master_model->get_master_by_id($master['id']);
            $state='';
            foreach($data->regions as $region){
                $state.=$region->area_name_ua.', '.$region->district_place_ua.'; ';
            }
            $masters[$key]['region']= $state;      
        }

        $xls = new Excel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('masters');
        for($i = 1; $i < count($masters); $i++){
            $j=0;
            foreach($masters[$i] as $key=>$value){
                if($key=='name'||$key=='surname'||$key=='id'||$key=='id_master'||$key=='phone1'||$key=='phone2'||$key=='phone3'||$key=='phone4'||$key=='email'||$key=='region'||$key=='date'){
                    $sheet->setCellValueByColumnAndRow( $j, $i, $key );
                   $j++; 
                }
            }
            break;
        }
        for ($i = 2; $i < count($masters)+2; $i++) {
            $j=2;
            foreach($masters[$i-2] as $key=>$value){
                if($key=='name'||$key=='surname'||$key=='id'||$key=='id_master'||$key=='phone1'||$key=='phone2'||$key=='phone3'||$key=='phone4'||$key=='email'||$key=='region'||$key=='date'){
                    $sheet->setCellValueByColumnAndRow( $j - 2, $i, $value );
                   $j++; 
                }
            }
        }
        $this->report_all();
        $this->xls_comments();
        $this->xls_perregion_report();
        // Выводим HTTP-заголовки
         // header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
         // header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
         // header ( "Cache-Control: no-cache, must-revalidate" );
         // header ( "Pragma: no-cache" );
         // header ( "Content-type: application/vnd.ms-excel" );
         // header ( "Content-Disposition: attachment; filename=masters_".$today['year'].'-'.$today['mon'].'-'.($today['mday']-1).'.xls' );
        // Выводим содержимое файла
         $objWriter = new PHPExcel_Writer_Excel5($xls);
         $objWriter->save('uploads/reports/masters_'.$d->format('Y-m-d').'.xls');
        $headers = "From: noreply@telemaster.com.ua \r\n";
        $headers .= "MIME-Version: 1.0\r\n"."Content-type: text/html; charset=utf-8\r\n";
        $content='Файл с мастерами зарегистрировавшимися за '.$d->format('Y-m-d').' доступен по ссылке - <a href="'. base_url().'/uploads/reports/masters_'.$d->format('Y-m-d').'.xls'.'">Файл</a><br/> ';
        // Сводный отчет по всем мастерам - <a href="'. base_url().'/uploads/reports/reports_all_'.$today['year'].'-'.$today['mon'].'-'.($today['mday']-1).'.xls'.'">Файл</a><br/>
        //Файл выгруженный для карт - <a href="'. base_url().'/uploads/reports/map_all_'.$today['year'].'-'.$today['mon'].'-'.($today['mday']-1).'.xls'.'" >Файл</a>
          mail('info@telemaster.com.ua,Tatyana.Klivenok@viasat.ua,Oksana.Chub@viasat.ua,lkutova@lemon.ua,reports@telemaster.com.ua', 'Registration report', $content, $headers);
          //$a=mail('dtsvigun@lemon.ua,markre@ukr.net', 'Registration report', $content, $headers);
          // var_dump($a);
        // $objWriter->save('php://output');
    }

    public function xls_perregion_report(){
        $today=getdate();
        $d=new DateTime($today['year'].'-'.$today['mon'].'-'.$today['mday']);
        $d->modify("-1 day");
        $start_date = $d->format('Y-m-d').' '.'00'.':'.'00'.':'.'00';
        $end_date = $d->format('Y-m-d').' '.'23'.':'.'59'.':'.'59';
        // $start_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'00'.':'.'00'.':'.'00';
        // $end_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'23'.':'.'59'.':'.'59';
        $masters = $this->master_model->get_master_between_dates(false,$end_date);
        $report=array();
        foreach($masters as $key=>$master){
            $data=$this->master_model->get_master_by_id($master['id']);
            foreach($data->regions as $region){
                $masters[$key]['region']='';
                $masters[$key]['district']='';
                $masters[$key]['region']=$region->area_name_ua;
                $masters[$key]['district']=$region->district_place_ua;
                $report[]=$masters[$key];
            }
                  
        }        
        $xls = new Excel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('masters');
        for($i = 1; $i < count($report); $i++){
            $j=0;
            foreach($report[$i] as $key=>$value){
                if($key=='name'||$key=='surname'||$key=='id_master'||$key=='id'||$key=='region'||$key=='district'||$key=='date'||$key=='active'){
                	if($key=='id'){
                		$key='id_';
                	}
                    $sheet->setCellValueByColumnAndRow( $j, $i, $key );
                   $j++; 
                }
            }
            break;
        }
        for ($i = 2; $i < count($report)+2; $i++) {
            $j=2;
            //if($masters[$i-2]['active']==1):
            foreach($report[$i-2] as $key=>$value){
                 if($key=='name'||$key=='surname'||$key=='id_master'||$key=='id'||$key=='region'||$key=='district'||$key=='date'||$key=='active'){
                    $sheet->setCellValueByColumnAndRow( $j - 2, $i, $value );
                   $j++; 
                }
            }
            //endif;
        }
        // Выводим HTTP-заголовки
         // header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
         // header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
         // header ( "Cache-Control: no-cache, must-revalidate" );
         // header ( "Pragma: no-cache" );
         // header ( "Content-type: application/vnd.ms-excel" );
         // header ( "Content-Disposition: attachment; filename=file.xls");
        // Выводим содержимое файла
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('uploads/reports_maps/map_all_'.$d->format('Y-m-d').'.xls');
        //$objWriter->save('php://output');
        
    }

    public function xls_comments(){
        $today=getdate();
        $d=new DateTime($today['year'].'-'.$today['mon'].'-'.$today['mday']);
        $d->modify("-1 day");
        $start_date = $d->format('Y-m-d').' '.'00'.':'.'00'.':'.'00';
        $end_date = $d->format('Y-m-d').' '.'23'.':'.'59'.':'.'59';
        // $start_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'00'.':'.'00'.':'.'00';
        // $end_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'23'.':'.'59'.':'.'59';
        $masters = $this->master_model->get_comments_between_dates($start_date,$end_date);

        $xls = new Excel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('comments');
        
        for($i = 0; $i < count($masters); $i++){

            $j=0;
            foreach($masters[$i] as $key=>$value){
                    if($key=='id'){
                		$key='id_';
                	}
                    $sheet->setCellValueByColumnAndRow( $j, $i+1, $key );
                   $j++; 
                
            }
            break;
        }
        for ($i = 2; $i < count($masters)+2; $i++) {
            $j=2;
            //if($masters[$i-2]['active']==1):
            foreach($masters[$i-2] as $key=>$value){
                $sheet->setCellValueByColumnAndRow( $j - 2, $i, $value );
                $j++; 
            }
            //endif;
        }
        // // Выводим HTTP-заголовки
        //  header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        //  header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        //  header ( "Cache-Control: no-cache, must-revalidate" );
        //  header ( "Pragma: no-cache" );
        //  header ( "Content-type: application/vnd.ms-excel" );
        //  header ( "Content-Disposition: attachment; filename=file.xls");
        // Выводим содержимое файла
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('uploads/reports/comment_report_'.$d->format('Y-m-d').'.xls');

        $headers = "From: noreply@telemaster.com.ua \r\n";
        $headers .= "MIME-Version: 1.0\r\n"."Content-type: text/html; charset=utf-8\r\n";
        $content='Файл с отзывами за '.$today['year'].'-'.$d->format('Y-m-d').' доступен по ссылке - <a href="'. base_url().'/uploads/reports/comment_report_'.$d->format('Y-m-d').'.xls'.'">Файл</a>';
        if(count($masters)==0){
            $content='За '.$d->format('Y-m-d').' отзывов нет.';
        }
         mail('info@telemaster.com.ua,Tatyana.Klivenok@viasat.ua,lkutova@lemon.ua,Oksana.Chub@viasat.ua,reports@telemaster.com.ua', 'Comments report', $content, $headers);
         //mail('dtsvigun@lemon.ua,markre@ukr.net', 'Comments report', $content, $headers);
        //$objWriter->save('php://output');
    }

    public function report_all(){

        $today=getdate();
        $d=new DateTime($today['year'].'-'.$today['mon'].'-'.$today['mday']);
        $d->modify("-1 day");
        $start_date = $d->format('Y-m-d').' '.'00'.':'.'00'.':'.'00';
        $end_date = $d->format('Y-m-d').' '.'23'.':'.'59'.':'.'59';
        // $start_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'00'.':'.'00'.':'.'00';
        // $end_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'23'.':'.'59'.':'.'59';
        //$masters = $this->comment_model->get_all_comment();
        $refuse_list=$this->master_model->get_refuses();
        
        $masters = $this->master_model->get_master_between_dates(false,$end_date);
        foreach($masters as $key=>$master){
            $data=$this->master_model->get_master_by_id($master['id']);

            $refuse=$this->master_model->get_refuses_by_master($master['id']);
            $state='';
            foreach($data->regions as $region){
                $state.=$region->area_name_ua.', '.$region->district_place_ua.'; ';
            }
            if($refuse==NULL){
                $masters[$key]['refuse']='';
                $masters[$key]['refuse_comment']='';
            } else{
                foreach($refuse_list as $r){
                    if($r['id']==$refuse['id_refuses']){
                        $masters[$key]['refuse']=$r['name_ua'];
                    }
                }
                $masters[$key]['refuse_comment']=$refuse['comment'];
            }
            
            $masters[$key]['region']= $state;      
        }

        $xls = new Excel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('masters');
        for($i = 1; $i < count($masters); $i++){
            $j=0;
            foreach($masters[$i] as $key=>$value){
                if($key=='name'||$key=='surname'||$key=='id_master'||$key=='id'||$key=='phone1'||$key=='phone2'||$key=='phone3'||$key=='phone4'||$key=='email'||$key=='region'||$key=='date'||$key=='active'||$key=='refuse'||$key=='refuse_comment'){
                	if($key=='id'){
                		$key='id_';
                	}
                    $sheet->setCellValueByColumnAndRow( $j, $i, $key );
                   $j++; 
                }
            }
            break;
        }
        for ($i = 2; $i < count($masters)+2; $i++) {
            $j=2;
            //if($masters[$i-2]['active']==1):
            foreach($masters[$i-2] as $key=>$value){
                 if($key=='name'||$key=='surname'||$key=='id_master'||$key=='id'||$key=='phone1'||$key=='phone2'||$key=='phone3'||$key=='phone4'||$key=='email'||$key=='region'||$key=='date'||$key=='active'||$key=='refuse'||$key=='refuse_comment'){
                    $sheet->setCellValueByColumnAndRow( $j - 2, $i, $value );
                   $j++; 
                }
            }
            //endif;
        }
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        // Выводим HTTP-заголовки
        // header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        // header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        // header ( "Cache-Control: no-cache, must-revalidate" );
        // header ( "Pragma: no-cache" );
        // header ( "Content-type: application/vnd.ms-excel" );
        // header ( "Content-Disposition: attachment; filename=file.xls");
        // Выводим содержимое файла
        
        $objWriter->save('uploads/reports/reports_all_'.$d->format('Y-m-d').'.xls');
        //$objWriter->save('php://output');
        
    }

    public function stat_report(){
        $today=getdate();
        $start_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'00'.':'.'00'.':'.'00';
        $end_date = $today['year'].'-'.$today['mon'].'-'.($today['mday']-1).' '.'23'.':'.'59'.':'.'59';
        $count=0;
        $masters = $this->master_model->get_master_between_dates(false,$end_date);
        $area_total=array();

        $xls = new Excel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('masters');

        foreach($masters as $key=>$master){

            $data=$this->master_model->get_master_by_id($master['id']);

            $state='';
            $raw=array();
            foreach($data->regions as $key=>$region){
                $state.=$region->area_name_ua.', '.$region->district_place_ua.'; ';
                if(!in_array($region->area_name_ua,$raw)){
                        $raw[$key]=$region->area_name_ua;
                }
                    
            }
            $masters[$key]['region']= $state; 
            $masters[$key]['area'] =$raw;
            //var_dump($masters[$key]);
            foreach($raw as $area){
                if($area!='Автономна Республіка Крим'&&$area!='Севастополь'){
                    if(!array_key_exists($area,$area_total)){
                        $area_total[$area]=0;
                    } 
                    $area_total[$area]+=1;
                }
            }
            if($count>5){
                //break;
            }
            $count++;
        }
        foreach($area_total as $key=>$amount){
        	//echo $key.' - '.$amount.';<br/>';

        }
		$active_count=0;
        $non_active_count=0;
        $rate_amount=0;
		foreach($masters as $master){
			$rate_amount+=$master['rate_amount'];
        	if($master['active']==0){
        		$non_active_count++;
        	} else {
        		$active_count++;
        	}
        }
        $i=0;
        foreach($area_total as $key=>$amount){
            $sheet->setCellValueByColumnAndRow( 0, $i, $key );
            $sheet->setCellValueByColumnAndRow( 1, $i, $amount );
            
            $i++;
        }
        


        $sheet->setCellValueByColumnAndRow( 0, $i+1, 'Active masters' );
        $sheet->setCellValueByColumnAndRow( 1, $i+1, $active_count );
        $sheet->setCellValueByColumnAndRow( 0, $i+2, 'Non-active' );
        $sheet->setCellValueByColumnAndRow( 1, $i+2, $non_active_count);
        $sheet->setCellValueByColumnAndRow( 0, $i+3, 'Masters total');
        $sheet->setCellValueByColumnAndRow( 1, $i+3, count($masters));
        $sheet->setCellValueByColumnAndRow( 0, $i+4, 'Общее количество проголосовавших');
        $sheet->setCellValueByColumnAndRow( 1, $i+4, $rate_amount);
        
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=file.xls");
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
       
        //$objWriter->save('uploads/reports/statistics_'.$today['year'].'-'.$today['mon'].'-'.($today['mday']-1).'.xls');
        // echo '<br/>Active masters: '.$active_count.'<br/> Non-active: '.$non_active_count;
        // echo '<br/>Masters total: '.count($masters);
        // echo '<br/>Общее количество проголосовавших: '.$rate_amount;
        
    }

    public function up(){
        if (isset($_FILES) && !empty($_FILES)) {

            //$_url = 'http://viasatmasters.lemon.ua/images/avatars/158/47974/b_47974.jpg';
//            $image = imagecreatefromjpeg($_url);
//
//
//
//            $exif = exif_read_data ($_FILES['file']['tmp_name'],0,true);
//
//            switch ($exif['IFD0']['Orientation']) {
//                // Поворот на 180 градусов
//                case 3: {
//                    $image = imagerotate($image,180,0);
//                    break;
//                }
//                // Поворот вправо на 90 градусов
//                case 6: {
//
//                    $image = imagerotate($image,-90,0);
//                    break;
//                }
//                // Поворот влево на 90 градусов
//                case 8: {
//                    $image = imagerotate($image,90,0);
//                    break;
//                }
//            }
            //$source['id']=$this->input->post('master_id');
            //$this->master_model->create_master($source);
        }

        //$this->load->view('frontend/update');

    }

    public function ajax_check_unique_fields(){
        $email=$this->input->post('email');
        $phone=$this->input->post('phone');
        $result = array('status'=>'error','data'=>'');
        $status = $this->master_model->get_master_by_email($email);

        $count_email = count($status);
        $status1 =  $this->master_model->get_master_by_phone($phone);
        $count_phone=count($status1);
        $result = array('status'=>'success','email'=>$count_email,'phone'=>$count_phone);
        echo json_encode($result);
        return;
    }

    public function ajax_get_districts(){
        $data=$this->input->post('data');

        $result = array('status'=>'error');

        $status = $this->region_model->get_districts_by_area_id($data);
        if($status){
            $result = array(
                'status'=>'success',
                'data'=>$status,
                'lang'=>SQL_LANG,
                'label'=>LANG('label_obl')
            );
        }
        echo json_encode($result);
        return;
    }
    public function ajax_get_by_district_koatuu(){
        $data=$this->input->post('data');

        $result = array('status'=>'error');

        $status = $this->region_model->get_by_district_koatuu($data);
        if($status){
            $result = array(
                'status'=>'success',
                'data'=>$status,
                'lang'=>SQL_LANG,
            );
        }
        echo json_encode($result);
        return;
    }
}
