<div class="container-fluid clearfix">
    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                <?php $this->load->view('panel/templates/action_button');?>
                <?php $this->load->view('panel/default/system_message');?>
                <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                <ul role="tablist" class="nav nav-tabs">
                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="general" href="#general" aria-expanded="true"><?php echo LANG('label_tab_general')?></a></li>
                    <?php if(isset($data->confirm_status) && ($data->confirm_status == 'new' || ($this->input->post('confirm_status') && !$data->updated))):?>
                        <li class="" role="presentation"><a data-toggle="tab" role="tab" aria-controls="print_confirm_status" href="#print_confirm_status" aria-expanded="true"><?php echo LANG('label_tab_print_confirm_status')?></a></li>
                    <?php endif;?>
                </ul>
                <div class="tab-content">
                    <div id="general" class="tab-pane active" role="tabpanel">
                        <div class="row">
                            <div class="col-md-8 inside_tab">
                                <ul role="tablist" class="nav nav-tabs">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="data_<?php echo $lang_key;?>" href="#data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                        <?php $_first = false;
                                    endforeach;?>
                                </ul>
                                <div class="tab-content">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <div id="data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input type="text" placeholder="" class="input" value="<?php echo $data->{'title_'. $lang_key};?>" name="title_<?php echo $lang_key;?>"><br>
                                        </div>
                                        <?php $_first = false;
                                    endforeach;?>
                                    <h5 class="widget-name"><i class="icon-picture"></i><?php echo LANG('label_photo'); ?></h5>
                                    <div class="row" style="margin-top:15px;">
                                        <div class="col-md-4"><?php echo LANG('label_photo'); ?></div>

                                        <div class="col-md-4"><span><?php echo LANG('label_mockup'); ?></span></div>
                                        <div class="col-md-2"><?php echo LANG('label_template'); ?></div>

                                        <div class="col-md-2"><?php echo LANG('label_do'); ?></div>
                                    </div>
                                    <input name="images" value="" type="hidden">
                                    <div class="media sortable ui-sortable" id="product-medias">
                                        <?php if(count($print_images) > 0): ?>
                                            <?php foreach($print_images as $image): ?>
                                            <div class="row stripped ui-sortable-handle">

                                                <div class="col-md-4" data-id="images[<?php echo $image['id']; ?>]" data-print_id="images[<?php echo $image['print_id']; ?>]">
                                                    <img src="/images/prints/<?php echo $image['print_id']; ?>/<?php echo $image['name']; ?>.<?php echo $image['type']; ?>" style="display: block;margin: 0 auto;max-height:120px;">
                                                </div>

                                                <div class="col-md-4 mockup-upload-container">
                                                    <input name="mockup[<?php echo $image['id']; ?>]" id="mockup" type="file" accept=".psd,.tiff,.tif"  />
                                                    <?php

                                                    $mockup=$this->$model->get_mockup_by_id(($data->id == 'create')?false:$image['id']);
                                                    if(!empty($mockup)):
                                                        foreach($mockup as $mock):
                                                            if($mock['picture_id']==$image['id']):
                                                                ?>
                                                                <a href="/images/prints/<?php echo $image['print_id']; ?>/mockup/<?php echo $mock['name'].'.'.$mock['type']; ?>">Скачать макет</a>
                                                                <?php
                                                            endif;
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php echo $tovar_templates[$image['template_id']]['title'.SQL_LANG];?>
                                                    <input class="template_id" type="hidden" name="images[<?php echo $image['id']; ?>][template_id]" value="<?php echo  $image['template_id'];?>">
                                                </div>

                                                <div class="col-md-1">
                                                    <a href="#" class="del" data-id="images[<?php echo $image['id']; ?>]" data-print_id="images[<?php echo $image['print_id']; ?>]"><i class="icon-trash"></i></a>
                                                </div>
                                            </div>
                                            <? endforeach;?>
                                        <? endif;?>
                                    </div>

                                    <div class="media">
                                        <div class="row stripped" id="product-medias-new">
                                            <div class="col-md-2 upload_img_conteiner">
                                                <label class="upload_image_label btn green label-input-file" style="padding: 38px 57px;">
                                                    <input style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" class="upload_image_button" multiple="" name="photo_0" title="Upload images" type="file">
                                                    <span><?php echo LANG('label_upload_photo'); ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="cropbox_wrap"></div>
                                </div>
                            </div>

                            <div class="col-md-4 parametrs_in_line">
                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                            <div class="widget-body">
                                <p><span><? echo LANG ('label_alias');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->alias;?>" name="alias"> </p>
                                <p><span><? echo LANG ('label_author');?></span>
                                    <?php $this->load->view('panel/templates/select_authors',array('select_data'=>$author_list,'name'=>'master_id','selected'=> $data->master_id,'params' => array()));?>
                                </p>
                            </div>

                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo key($print_categories);?></h5>
                            <div id="print-categories" class="widget-body">
                                <ul>
                                    <?php foreach($print_categories as $categories): ?>
                                        <?php foreach($categories as $category): ?>
                                            <li>
                                                <input <?php echo isset($p_category[$category['id']])?'checked':''; ?> name="categories[<?php echo $data->id; ?>][<?php echo $category['id']; ?>]" id="categories[<?php echo $category['id']; ?>]" type="checkbox" value="<?php echo $category['id']; ?>" />
                                                <label for="categories[<?php echo $category['id']; ?>]"><?php echo $category['title']; ?></label>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_preview');?></h5>
                            <div class="media" id="preview">
                                <div class="row stripped" id="product-medias-new">
                                    <div class="col-md-2 upload_img_conteiner">
                                        <?php
                                        if(isset($data->preview_name)&&isset($data->preview_type)){
                                            echo "<div class='wrapper' ><label for='preview_image' class='help'>Нажмите для замены</label></div>";
                                            echo "<image class='upload_image' src='/images/prints/".$data->id."/preview/".$data->preview_name.".".$data->preview_type."' >";

                                        }
                                        ?>
                                        <label  class="upload_image_label btn green label-input-file <?php echo isset($data->preview_name)?'hidden':'' ?>" style="padding: 38px 57px;">
                                            <input id="preview_image" style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" class="upload_image_button preview" multiple="" name="preview" title="Upload images" type="file">
                                            <span><?php echo LANG('label_upload_photo'); ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="cropbox_wrap"></div>
                        </div>
                    </div>
                </div>
                    <?php if(isset($data->confirm_status) && ($data->confirm_status == 'new' || ($this->input->post('confirm_status') && !$data->updated))):?>
                        <div id="print_confirm_status" class="tab-pane" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12 inside_tab">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_confirm_status_progress');?></h5>
                                    <input type="hidden" value="<?php echo $data->confirm_status;?>" name ="confirm_status">
                                    <ul data-id="item_id" id="ex_select" class="clearfix">
                                        <li>
                                            <ul class="entity_group">
                                                <li data-item_id="new" class="col-md-2 <?php if($data->confirm_status == 'new'){ echo 'selected';}?>"><?php echo LANG('label_confirm_status_new');?></li>
                                                <li data-item_id="confirm" class="col-md-2 <?php if($data->confirm_status == 'confirm'){ echo 'selected';}?>"><?php echo LANG('label_confirm_status_confirm');?></li>
                                                <li data-item_id="refuse" class="col-md-2 <?php if($data->confirm_status == 'refuse'){ echo 'selected';}?>"><?php echo LANG('label_confirm_status_refuse');?></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div id="action_refuse_message" class="<?php if($data->confirm_status != 'refuse'){ echo 'hidden';}?> col-md-12">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_refuse_message');?></h5>
                                    <p>
                                        <span><? echo LANG ('label_refuse_message_select');?></span>
                                        <select style=" max-height: 100px;" name="refuse_message_id">
                                            <?php foreach ($refuse_messages as $ref_id => $ref_message):?>
                                                <option value="<?php echo $ref_id;?>" <?php if(isset($data->refuse_message_id)): echo ($data->refuse_message_id == $ref_id)? 'selected="selected"':''; endif;?>><?php echo $ref_message;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </p>
                                    <p>
                                        <span><? echo LANG ('label_refuse_message_or_enter');?></span>
                                        <textarea name ="refuse_message"><?php echo isset($data->refuse_message)? $data->refuse_message:'';?></textarea>
                                    </p>

                                </div>
                                <div id="action_tovars_to_generate" class="col-md-12 <?php if($data->confirm_status == 'refuse'){ echo 'hidden';}?>">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_tovars_to_generate');?></h5>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="product_changer_status">
                                                <span class="head_status"><? echo LANG ('label_tovars_generate');?></span>
                                                <input 	type="hidden"   name="tovar_generate" value="0" checked="checked">
                                                <input 	type="checkbox" name="tovar_generate" class="switcher checked" value="1"
                                                          data-name="tovar_generate"
                                                          data-entity_id="1"
                                                          data-style="default"
                                                          data-selected="<?php echo($data->tovar_generate == 1)? 'true':'false';?>"
                                                          data-disabled="false">
                                            </p>
                                        </div>
                                    </div>
                                    <?php foreach($print_images as $image): ?>
                                        <?php if(!isset($products_list[$image['template_id']])){continue;}?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="product_changer_head">
                                                     <span><?php echo $tovar_templates[$image['template_id']]['title'.SQL_LANG];?></span>
                                                </p>
                                                <p class="product_changer">
                                                     <?php foreach($products_list[$image['template_id']] as $product):?>
                                                         <span class="product_changer <?php echo (isset($products_to_generate[$image['template_id']][$product['id']]))? 'active"':'';?>">
                                                             <label title="<?echo $product['title'];?>" class="product_changer" style="background: url('/images/products/<?echo $product['id'];?>/icon/<?echo $product['icon'];?>');" for="product_<?php echo $image['template_id'].'_'. $product['id'];?>"></label>
                                                         </span>
                                                         <input class="product_changer" id="product_<?php echo $image['template_id'].'_'. $product['id'];?>" value = "<?php echo $product['id'];?>" type="checkbox" name="products[<?php echo $image['template_id'];?>][<?php echo $product['id'];?>]" <?php echo (isset($products_to_generate[$image['template_id']][$product['id']]))? 'checked="checked"':'';?>>
                                                     <?php endforeach;?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
            </form>
        </div>
    </div>
</div>
<style>
    .upload_img_conteiner{
        height: 100px;
        width: 235px;
    }
    .upload_img_conteiner img{
        display: block;
        margin: 0 auto;
        max-height: 100%;
        max-width: 100%;
    }
    #product-medias label.small-input-block input[type='radio']{
        float: left;
        display: inline;
        width: 15px;
    }
    .media#preview img{
        cursor:pointer;

    }
    .media#preview .upload_img_conteiner{
        width:100%;
        padding: 0px 40px;
    }
    .media#preview .help{
        display: none;
        font-weight: 200;
        cursor:pointer;
        text-align:center;
        border:1px solid #fff;
        background-color:#000;
        opacity:0.7;
        color:#fff;
        width:200px;
        height: 35px;
        position:absolute;
        line-height:35px;
        margin-top:30px;
    }
    .media#preview .help:hover{
        opacity: 0.4;
    }
    .media#preview .wrapper{
        width:200px;
        margin:auto;
    }
    #print-categories label{
        cursor:pointer;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('.media#preview').mouseover(function(){
            $(this).find('.help').show();
        }).mouseleave(function(){
            $(this).find('.help').hide();
        });

        $('.template_id').each(function(){
            if($(this).val()=='1'){

                $(this).parent().parent().find('#mockup,.mockup-upload-container a').show();
            } else {
                $(this).parent().parent().find('#mockup,.mockup-upload-container a').hide();
                $(this).parent().parent().find('#mockup,.mockup-upload-container a').val('');
            }
        });

        $('.template_id').change(function(){

            if($(this).val()=='1' || $(this).val()=='4'){
                $(this).parent().parent().find('.mockup-upload-container input,.mockup-upload-container a').show();
            } else{
                $(this).parent().parent().find('.mockup-upload-container input,.mockup-upload-container a').hide();
                $(this).parent().parent().find('.mockup-upload-container input,.mockup-upload-container a').val('');
            }
        });

        var iter = 1
        if (window.File && window.FileList && window.FileReader) {
            $('body').on('change', ".upload_image_button", function (e) {
                Make_preview(e, $(this).closest('.upload_image_label'));
                iter++;
            });
        }

        function Make_preview(input, obj) {
            var reader = new FileReader();
            var files = input.target.files || input.dataTransfer.files;

            if (obj.prev('.upload_image').length > 0) {
                obj.prev('.upload_image').remove();
            } else {

                var str = '<div class="col-md-2 upload_img_conteiner" >';
                str += '<label class="upload_image_label btn green label-input-file" style="padding: 38px 57px;">';
                str += '<input style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" class="upload_image_button" multiple="" name="photo_' + iter + '" title="Upload images" type="file">';
                str += '<span><?php echo LANG('label_upload_photo'); ?></span>';
                str += '</label>';
                str += '</div>';
                obj.closest('.upload_img_conteiner').after(str);

            }
            reader.readAsDataURL(files[0]);

            reader.onload = function (e) {
                obj.before('<image class="upload_image">');
                obj.prev('.upload_image').attr('src', e.target.result);
                obj.hide();
            }
        }

        $('body').on('click', '.upload_image', function () {
            $(this).siblings('.upload_image_label').trigger('click');
        });

        $('.entity_group li').click(function(){
            $('.entity_group li').removeClass('selected');
            $(this).addClass('selected');
            var item_id = $(this).attr('data-item_id');
            $('#cform input[name="confirm_status"]').val(item_id);
            if(item_id == 'refuse'){
                $('#action_refuse_message').removeClass('hidden');
                $('#action_tovars_to_generate').addClass('hidden');
            }else{
                $('#action_refuse_message').addClass('hidden');
                $('#action_tovars_to_generate').removeClass('hidden');
            }
        });

        $('span.product_changer').click(function(){
            $(this).toggleClass('active');
        });
    });
</script>
