<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content master">
        <div class="wrapper">
            <div class="main-block">
                <h2><?php echo LANG('label_master_page'); ?></h2>
                <div data-master_id="<?php echo $master->id; ?>" class="profile">
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <div class="img-wrapper">

                        <?php
                        $str='';
                            if(isset($master->image['img_name'])&&isset($master->image['img_type'])){
                                $str='images/avatars/'.$master->id.'/'. $master->image['img_name'] .'/d_' . $master->image['img_name'] . '.' . $master->image['img_type'];
                            }

                          ?>
                            <?php if(isset($master->image['img_name'])&&isset($master->image['img_type'])&&file_exists($str)){ ?>
                                <img style="<?php if($master->id==26&&false): echo 'transform: rotate(90deg);'; endif; ?>" class="img img-responsive" src="<?php echo base_url('/images/avatars'). '/'.$master->id.'/'. $master->image['img_name'] .'/d_' . $master->image['img_name'] . '.' . $master->image['img_type']; ?>" alt="no-image">
                            <?php } else{ ?>
                                <img class="img img-responsive" src="<?php echo base_url('assets/img/master_av_mini.png'); ?>" alt="no-image">
                            <?php } ?>
                            <div class="frame"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <div class="info">
                            <div class="name">
                            <?php //echo $master->name.' '.$master->surname; ?>
                                <?php $prefix=SQL_LANG; 
                                        if(LANG=='ru'){
                                            $prefix='';
                                        }
                                        $name=$master->{'name'.$prefix};
                                        $surname=$master->{'surname'.$prefix};
                                        
                                        if($name!=''||$surname!=''){
                                            echo $name.' '.$surname;
                                        } elseif(LANG=='ru'){
                                            echo $master->name_ua.' '.$master->surname_ua;
                                        } else{
                                            echo $master->name.' '.$master->surname;
                                        }
                                    ?>
                            </div>
                            <div class="id"><?php echo 'id: '.$master->id_master; ?></div>
                            <div class="phone-numbers">
                                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                    <p><?php echo $master->phone1; ?></p>
                                    <p><?php echo $master->phone2; ?></p>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                    <p><?php echo $master->phone3; ?></p>
                                    <p><?php echo $master->phone4; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <div class="rate">
                            <div class="label_r"><?php echo LANG('label_rate'); ?></div>
                            <div class="levels">
                                <ul>
                                    <li class="<?php if( $master->rate>=0.2): echo 'active'; endif; ?>" data-level_id="0" id="level0"><div></div></li>
                                    <li class="<?php if( $master->rate>=0.4): echo 'active'; endif; ?>" data-level_id="1" id="level1"><div></div></li>
                                    <li class="<?php if( $master->rate>=0.6): echo 'active'; endif; ?>" data-level_id="2" id="level2"><div></div></li>
                                    <li class="<?php if( $master->rate>=0.8): echo 'active'; endif; ?>" data-level_id="3" id="level3"><div></div></li>
                                    <li class="<?php if( $master->rate>=1): echo 'active'; endif; ?>" data-level_id="4" id="level4"><div></div></li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-box">
                            <div id="add_rate" class="btn btn-type3"><?php echo LANG('rate_master'); ?></div>
                        </div>
                    </div>
                </div>
                <div class="tabs master_tabs">
                    <div class="nav">
                        <div class="btn active" id="regions"><?php echo LANG('label_regions'); ?></div>
                        <?php
                        $count=0;
                        foreach($master->comments as $comment):
                            if($comment['active']!=0){
                                $count++;
                            }
                        endforeach; ?>
                        <div class="btn" id="comments"><?php echo LANG('label_comments').' ('. $count.')'; ?></div>
                        <div class="btn" id="add_comment"><?php echo LANG('label_add_comment'); ?></div>
                    </div>
                    <div class="tabs-content">
                        <div class="regions active">

                            <?php if(isset($master->regions)&&$master->regions!=NULL): foreach($master->regions as $region): ?>

                                <p><?php
                                    if($region!=NULL){

                                        if($region->{'district_place'.SQL_LANG}!=NULL){
                                            echo $region->{'district_place'.SQL_LANG};
                                        }
                                        if($region->type=='3'){
                                            echo LANG('label_district').', ';
                                        } elseif($region->type=='0'){

                                        } else {
                                            echo ', ';
                                        }
                                        if($region->{'area_name'.SQL_LANG}!=NULL){
                                            if($region->{'area_name'.SQL_LANG}=='Київ'||$region->{'area_name'.SQL_LANG}=='Киев'||$region->area_name_ru=='Крым'){
                                                 echo $region->{'area_name'.SQL_LANG};
                                            } else{
                                                echo $region->{'area_name'.SQL_LANG}.LANG('label_obl').';';
                                            }
                                            
                                        }
                                    }
                                    ?></p>

                            <?php endforeach; endif;?>
                        </div>
                        <div class="comments">
                            <?php foreach($master->comments as $comment):
                                if($comment['active']!=0):
                                    echo '<div class="single_comment">';
                                        echo $comment['text'];
                                    echo '</div>';
                                endif;
                            endforeach; ?>
                        </div>
                        <div class="add_comment">
                            <div class="col-md-3 col-sm-3 col-xs-12 auth-wrapp">
                                <p>
                                    <?php echo LANG('label_please_auth'); ?>
                                </p>
                                <div class="auth-box">
                                    <div class="fb_auth"></div>
                                    <div class="vk_auth"></div>
                                </div>
                            </div>
                            <div class="edit-box col-md-9 col-sm-9 col-xs-12">
                                <textarea name="new_comment" id="new_comment" disabled cols="60" rows="10"></textarea>
                                <div class="btn-box">
                                    <div id="action_comment" class="btn btn-type3"><?php echo LANG('label_send'); ?></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/default/footer'); ?>