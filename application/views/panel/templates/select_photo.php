
    <select id="select_photo_<?php echo $params['type'];?>" class="select_photo" <? echo (isset($params['multiple']))? 'multiple="multiple"':'';?> style=" <? echo (isset($params['multiple']))? 'height:300px;':'max-height: 100px;';?>" name="<?php echo $name;?>">
        <?php if($select_data):?>
            <?php if(!isset($params['no_all'])):?><option value ="0"><?php echo LANG('label_no_parent');?></option><?php endif;?>
            <?php $_select_obj = false; ?>
            <?php foreach($select_data as $key => $item):?>
                <?php $_src =''; ?>
                <?php $_print_size_x ='';?>
                <?php $_print_size_y ='';?>
                <?php $_print_offset_x ='';?>
                <?php $_print_offset_y ='';?>
                <?php $_template_id ='';?>
                <?php $_category_id ='';?>
                <?php if($item['photo_name'] && $item['photo_type']):?>
                    <?php if($params['type'] == 'products'):
                        $_src = '/images/products/'. $item['id'] .'/'. $item['photo_name'] .'.'. $item['photo_type'];
                        $_print_size_x = $item['print_size_x'];
                        $_print_size_y = $item['print_size_y'];
                        $_print_offset_x = $item['print_offset_x'];
                        $_print_offset_y = $item['print_offset_y'];
                        $_template_id = $item['template_id'];
                        $_category_id = $item['category_id'];
                    elseif($params['type'] == 'pictures'):
                        $_src = '/images/prints/'. $item['id'] .'/'. $item['photo_name'] .'.'. $item['photo_type'];

                    endif;
                 endif;?>
                <option  data-category-id="<?php echo $_category_id; ?>" data-template-id="<?php echo $_template_id; ?>" data-print-offset-x ="<?php echo $_print_offset_x;?>"data-print-offset-y ="<?php echo $_print_offset_y;?>"data-print-size-x ="<?php echo $_print_size_x;?>" data-print-size-y ="<?php echo $_print_size_y;?>"  data-src="<?php echo $_src;?>" value ="<?php echo $item['id']?>" <?php echo ($item['id'] == $selected)? 'selected="selected"':'';?>><?php echo $item['title']?></option>
                <?php if($item['id'] == $selected){$_select_obj = $item;}?>
            <?php endforeach;?>
        <?php else:?>
            <option  >---</option>
        <?php endif;?>
    </select>



