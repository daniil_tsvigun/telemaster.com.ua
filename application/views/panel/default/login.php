<!DOCTYPE html>
<html lang="ru" class="no-js">
<head>
    <meta charset="utf-8">
    <title><?php echo LANG('page_title_control_panel');?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/css/login.css">

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <script type="text/javascript">
        (function ($, undefined) {

            $(document).ready(function () {
                $(".input").focusin(function() {
                    $(this).find("span").animate({
                        "opacity": "0"
                    }, 200);
                });

                $(".input").focusout(function() {
                    $(this).find("span").animate({
                        "opacity": "1"
                    }, 300);
                });

                $(".login").submit(function() {
                    $(this).find(".submit i").removeAttr('class').addClass("fa fa-check").css({
                        "color": "#fff"
                    });
                    $(".submit").css({
                        "background": "#2ecc71",
                        "border-color": "#2ecc71"
                    });
                    $(".feedback").show().animate({
                        "opacity": "1",
                        "bottom": "-80px"
                    }, 400);
                    $("input").css({
                        "border-color": "#2ecc71"
                    });
                    setTimeout(function(){
                        $(this).submit();
                    },800);
                    //return false;
                });
            });
        })(jQuery);
    </script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form action ="<?php echo site_url();?>/panel/auth/login" method="post" accept-charset="utf-8" class="login">
        <fieldset>

            <legend class="legend"><?php echo LANG('label_auth');?>
                <?php if($auth_message):?>
                    <?php foreach($auth_message as $message):?>
                    <span id="auth_error" ><?php echo $message['title'];?></span>
                    <?php endforeach;?>
                <?php endif;?>
            </legend>

            <div class="input">
                <input type="text" name="email" placeholder="email" required value="<?php echo $email;?>"/>
                <span><i class="fa fa-envelope-o"></i></span>
            </div>

            <div class="input">
                <input name="password" type="password" placeholder="password" required value="<?php echo $password;?>"/>
                <span><i class="fa fa-lock"></i></span>
            </div>

            <button type="submit" class="submit"><i class="fa fa-long-arrow-right"></i></button>

        </fieldset>

        <div class="feedback">
            login successful <br />
            redirecting...
        </div>

    </form>
</body>
</html>

