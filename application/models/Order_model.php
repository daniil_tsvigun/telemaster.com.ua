<?php

class Order {

    public $client_id;
    public $client_surname;
    public $client_name;
    public $client_email;
    public $client_phone;
    public $promo_id;
    public $delivery_type;
    public $payment_type;
    public $status;

    public function __construct($source = null) {
        $this->client_id          = isset($source['client_id']) ?          $source['client_id']          : 0;
        $this->client_role_id     = isset($source['client_role_id']) ?     $source['client_role_id']     : 1;
        $this->client_surname     = isset($source['client_surname']) ?     $source['client_surname']     : '';
        $this->client_name        = isset($source['client_name']) ?        $source['client_name']        : '';
        $this->client_email       = isset($source['client_email']) ?       $source['client_email']       : '';
        $this->client_phone       = isset($source['client_phone']) ?       $source['client_phone']       : '';
        $this->price_final        = isset($source['price_final']) ?        $source['price_final']        : '';
        $this->price_base         = isset($source['price_base']) ?         $source['price_base']         : '';
        $this->price_delivery     = isset($source['price_delivery']) ?     $source['price_delivery']     : 0;
        $this->price_discount     = isset($source['price_discount']) ?     $source['price_discount']     : '';
        $this->promo_id           = isset($source['promo_id']) ?           $source['promo_id']           : '';
        $this->delivery_type      = isset($source['delivery_type']) ?      $source['delivery_type']      : '';
        $this->delivery_area      = isset($source['delivery_area']) ?      $source['delivery_area']      : '';
        $this->delivery_district  = isset($source['delivery_district']) ?  $source['delivery_district']  : '';
        $this->delivery_city      = isset($source['delivery_city']) ?      $source['delivery_city']      : '';
        $this->delivery_warehouse = isset($source['delivery_warehouse']) ? $source['delivery_warehouse'] : '';
        $this->delivery_address   = isset($source['delivery_address']) ?   $source['delivery_address']   : '';
        $this->payment_type       = isset($source['payment_type']) ?       $source['payment_type']       : '';
        $this->status             = isset($source['status']) ?             $source['status']             : '';

    }
}

class Order_model extends MY_Model {

    protected $table_name = 'orders';
    protected $t_tovars = 'tovars';
    protected $t_products = 'products';
    protected $t_pictures = 'pictures';
    protected $t_pictures_images = 'pictures_images';
    protected $t_categories = 'categories';
    protected $t_tovars_to_orders = 'tovars_to_orders';
    protected $t_painter_users = 'painter_users';
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;
    public $payment_type;
    public $delivery_type;
    public $order_status;

    public function __construct() {
        parent::__construct();
        $this->payment_type = array(
            array('id'=>'cache','title'=> LANG('label_cache')),
            array('id'=>'card','title'=> LANG('label_card'))
        );
        $this->delivery_type = array(
            array('id'=>'np','title'=> LANG('label_np')),
            array('id'=>'courier','title'=> LANG('label_courier'))
        );
        $this->order_status = array(
            array('id'=>'begin','title'=> LANG('label_status_begin')),
            array('id'=>'in_work','title'=> LANG('label_status_in_work')),
            array('id'=>'send','title'=> LANG('label_status_send')),
            array('id'=>'done','title'=> LANG('label_status_done')),
            array('id'=>'cancel','title'=> LANG('label_status_cancel'))
        );
    }

    public function make_order($data,$user=false){
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        die();
        $_res_promo = (isset($data['promo']))? $this->promo_model->get_promo_by_word($data['promo']):'';
        $_delivery_data = $data['delivery'];

        $type   = $_delivery_data['type'];
        $source = array(
            'client_id'          => $user->id,
            'client_role_id'     => $user->role_id,
            'client_surname'     => $data['buyer']['surname'],
            'client_name'        => $data['buyer']['name'],
            'client_email'       => $data['buyer']['email'],
            'client_phone'       => $data['buyer']['phone'],
            'price_final'        => $data['status']['promo_price'],
            'price_base'         => $data['status']['cart_price'],
            'price_delivery'     => $data['status']['delivery_price'],
            'price_discount'     => $data['status']['promo_discount'],
            'promo_id'           => ($_res_promo) ? $_res_promo['id'] : 0,
            'delivery_type'      => $_delivery_data['type'],
            'delivery_area'      => isset($_delivery_data[$type . '_area_text']) ? $_delivery_data[$type . '_area_text'] : null,
            'delivery_district'  => isset($_delivery_data[$type . '_district_text']) ? $_delivery_data[$type . '_district_text'] : null,
            'delivery_city'      => isset($_delivery_data[$type . '_city_text']) ? $_delivery_data[$type . '_city_text'] : null,
            'delivery_warehouse' => isset($_delivery_data[$type . '_warehouse_text']) ? $_delivery_data[$type . '_warehouse_text'] : null,
            'delivery_address'   => isset($_delivery_data[$type . '_address']) ? $_delivery_data[$type . '_address'] : null,
            'delivery_comment'   => isset($_delivery_data[$type . '_comment']) ? $_delivery_data[$type . '_comment'] : null,
            'payment_type'       => $data['buyer']['type'],
            'status'             => 'begin',
        );

        $this->create($source,'Order');

        $_order_id = $this->db->insert_id();
        foreach($data['data'] as $key => $tovar){
            $insert_arr[$key] = array(
                'order_id'=>$_order_id,
                'tovar_id'=>$tovar['tovar_id'],
                'price_base'=>$tovar['price'],
                'price_final'=>(isset($data['status']['promo_tovars'][$key]))?($tovar['price'] - $data['status']['promo_tovars'][$key]):$tovar['price'],
                'price_discount'=>(isset($data['status']['promo_tovars'][$key]))? $data['status']['promo_tovars'][$key]:0,
                'filters'=>(is_array($tovar['filters']))?implode(",",$tovar['filters']):$tovar['filters'],
                'pack'=>$tovar['pack'],
                'postal'=>$tovar['postal'],
                'count'=>$tovar['count'],
                'preview'=>$tovar['tovar_photo'],
            );

            $payment_arr['tovars'][$key] = $tovar['tovar_id'];
            $payment_arr['tovars_count'][$key] = $tovar['count'];

        }
        $this->db->insert_batch($this->t_tovars_to_orders,$insert_arr);
        $this->payment_statistic_model->make_payment_debit($_order_id,$payment_arr);
        return $_order_id;
    }

    public function update($source, $source_id,$source_class) {
        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            $data->id = $source_id;
            $this->db->update($this->table_name, $data, ['id' => $source_id]);

            $this->update_order_tovars($source,$source_id);

            return $source_id;
        }
        return false;
    }

    public function update_order_tovars($source,$source_id){

        $_tovars_data = $this->db->get_where($this->t_tovars_to_orders,array('order_id'=>$source_id))->result_array();
        if(!$_tovars_data){
            return;
        }
        $_delete_tovar = array();
        $_update_tovar = array();
        $_update_order = array(
            'price_final'=>0,
            'price_base'=>0,
            'price_delivery'=>0,
            'price_discount'=>0,
        );
        foreach($_tovars_data as $tovar){
            if(isset($source['tovars'][$tovar['id']])){
                $_update_tovar[$tovar['id']]['id'] = $tovar['id'];
                $_update_tovar[$tovar['id']]['count'] = $source['tovars'][$tovar['id']]['count'];
                $_update_tovar[$tovar['id']]['pack'] = $source['tovars'][$tovar['id']]['pack'];
                $_update_tovar[$tovar['id']]['postal'] = $source['tovars'][$tovar['id']]['postal'];
                $_update_order['price_final'] += $tovar['price_final']*$source['tovars'][$tovar['id']]['count'];
                $_update_order['price_base'] += $tovar['price_base']*$source['tovars'][$tovar['id']]['count'];
                $_update_order['price_discount'] += $tovar['price_discount']*$source['tovars'][$tovar['id']]['count'];
            }else{
                $_delete_tovar[$tovar['id']] = $tovar['id'];
            }
        }
        $_update_order['price_delivery'] = $source['price_delivery'];
        $_update_order['price_final'] += $source['price_delivery'];

        $this->db->update($this->table_name, $_update_order, ['id' => $source_id]);
        if(count($_delete_tovar) > 0){
            $this->db->where_in('id',$_delete_tovar)->delete($this->t_tovars_to_orders);
        }
        if(count($_update_tovar) > 0){
            $this->db->update_batch($this->t_tovars_to_orders,$_update_tovar,'id');
        }
    }

    public function get_all_order($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select($this->table_name .'.*')
                           ->from($this->table_name)
                           ->get()->result_array();

        return $result;
    }
    public function get_order_tovars($order_id){

        $res = $this->db->select($this->t_tovars_to_orders.'.*,'.
                                 $this->t_tovars.'.title'. SQL_LANG .' as tovar_title,'.
                                 $this->t_tovars.'.alias,'.
                                 $this->t_tovars.'.picture_id,'.
                                 $this->t_categories.'.title'. SQL_LANG .' as category_title,'.
                                 $this->t_pictures.'.preview_name as picture_preview_name,'.
                                 $this->t_pictures.'.preview_type as picture_preview_type,'.
                                 $this->t_pictures_images.'.name as picture_name,'.
                                 $this->t_pictures_images.'.type as picture_type'
                        )
                        ->from($this->t_tovars_to_orders)
                        ->where($this->t_tovars_to_orders.'.order_id ='. $order_id)
                        ->join($this->t_tovars,$this->t_tovars.'.id = '. $this->t_tovars_to_orders .'.tovar_id' ,'left')
                        ->join($this->t_categories,$this->t_categories.'.id = '. $this->t_tovars .'.category_id' ,'left')
                        ->join($this->t_pictures,$this->t_tovars.'.picture_id = '. $this->t_pictures .'.id' ,'left')
                        ->join($this->t_pictures_images,$this->t_pictures_images.'.print_id = '. $this->t_pictures .'.id AND '. $this->t_categories .'.template_id = '. $this->t_pictures_images.'.template_id' ,'left')
                        ->group_by($this->t_tovars_to_orders. '.tovar_id')
                        ->get()->result_array();
        return $res;
    }

    public function change($name,$id,$changes){

        $this->db->where(array('id'=>$id))->update($this->table_name,array($name => $changes));
        if($name == 'payment_status'){
            $this->db->where(array('order_id'=>$id))->update('painter_debit',array('status' => $changes));
        }
    }

    public function get_orders_by_user($user){

        $res = $this->db->select(
                            $this->table_name.'.*,'.
                            $this->t_tovars_to_orders.'.filters,'.
                            $this->t_tovars_to_orders.'.count,'.
                            $this->t_tovars_to_orders.'.pack,'.
                            $this->t_tovars_to_orders.'.postal,'.
                            $this->t_tovars_to_orders.'.preview,'.
                            $this->t_tovars_to_orders.'.price_final,'.
                            $this->t_tovars.'.title'. SQL_LANG .' as tovar_title,'.
                            $this->t_tovars.'.id as tovar_id,
                            (SELECT name FROM '. $this->t_painter_users.' WHERE id = (SELECT master_id FROM '. $this->t_pictures .' WHERE id = '. $this->t_tovars .'.picture_id LIMIT 1 ) LIMIT 1) as author'
                        )
                        ->from($this->table_name)
                        ->where($this->table_name .'.client_id = '. $user->id)
                        ->where($this->table_name .'.client_role_id = '. $user->role_id)
                        ->where($this->table_name .'.active = 1')
                        ->join($this->t_tovars_to_orders,$this->t_tovars_to_orders .'.order_id = '.$this->table_name.'.id')
                        ->join($this->t_tovars,$this->t_tovars .'.id = '.$this->t_tovars_to_orders.'.tovar_id')
                        ->get()->result_array();

        if(!$res){
            return false;
        }

        foreach($res as $item){
            if(!isset($data[$item['id']])){
                $data[$item['id']] = $item;
            }
            $_key = $item['tovar_id'] . $item['filters'] . $item['count'] . $item['pack'];
            $data[$item['id']]['tovars'][$_key]['filters'] = $item['filters'];
            $data[$item['id']]['tovars'][$_key]['price_final'] = $item['price_final'];
            $data[$item['id']]['tovars'][$_key]['count'] = $item['count'];
            $data[$item['id']]['tovars'][$_key]['pack'] = $item['pack'];
            $data[$item['id']]['tovars'][$_key]['postal'] = $item['postal'];
            $data[$item['id']]['tovars'][$_key]['tovar_title'] = $item['tovar_title'];
            $data[$item['id']]['tovars'][$_key]['tovar_id'] = $item['tovar_id'];
            $data[$item['id']]['tovars'][$_key]['author'] = $item['author'];
            $data[$item['id']]['tovars'][$_key]['preview'] = $item['preview'];
        }

        return $data;
    }

}