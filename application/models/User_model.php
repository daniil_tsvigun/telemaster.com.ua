<?php

class User {
    
    public $id;
    public $name;
    public $role_id;
    public $email;
    public $mobile_phone;
    public $permissions;
    //public $approved;


    public function __construct($source = null) {
        $this->id           = isset($source['id'])?           $source['id']          :'';
        $this->name         = isset($source['name'])?         $source['name']        :'';
        $this->email        = isset($source['email'])?        $source['email']       :'';
        $this->mobile_phone = isset($source['mobile_phone'])? $source['mobile_phone']:'';
        $this->permissions  = isset($source['permissions'])?  $source['permissions'] :array();
        $this->role_id      = isset($source['role_id'])?      (int)$source['role_id']:1;
        if(isset($source['password']) && !empty($source['password'])){
            $this->password = md5($source['password'] . DEFAULT_SALT);
        }

    }
}

class User_model extends MY_Model {

    protected $table_name = '';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;
    protected $user_role = '';
    protected $save_path = '';
    protected $photo_config = array(
        'bg_img'=>array(
            'path'=>'bg',
            'height'=>'256',
            'width'=>'1360',
            'method'=>'4',
            'options' => array('bg_color' => '255,255,255'),
        ),
        'avatar_img'=>array(
            'path'=>'avatar',
            'height'=>'150',
            'width'=>'150',
            'method'=>'3',
            'options' => array('bg_color' => '255,255,255'),
        )
    );

    public function __construct() {
        parent::__construct();
    }

    public function get_user_by_email($user_email){

        $res = $this->db->get_where($this->table_name,array('email'=>$user_email,'active'=>1));
        return ($res->num_rows() === 1) ? $res->row_array() : null;
    }

    public function get_user_by_email_registration($user_email){

        $res = $this->db->get_where($this->table_name,array('email'=>$user_email));
        return ($res->num_rows() === 1) ? $res->row_array() : null;
    }

    public function email_confirmation($user_email){

        $table_data=$this->db->get_where($this->table_name,array('email'=>$user_email));
        $_data=$table_data->row_array();

        if($table_data->num_rows() === 1){
            $data = array(
                'active' => 1
            );
            $this->db->update($this->table_name, $data, ['id' => $_data['id']]);
            return true;
        } else {
            return null;
        }
        return true;
    }

    public function get_user($user_data,$model = false){

        $user_info = $this->db->get_where($this->table_name,array('id'=>$user_data['id']))->row_array();
        $user_info['permissions'] = $this->role_model->get_role_permissions_by_id($user_data['role_id']);
        $user_info['approved'] = true;
        $_user_model = ($model)? 'User_'.$model:'User';
        return new $_user_model($user_info);
    }

    public function get_all_users($pagination = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $this->db->order_by(str_replace("-"," ",$pagination['sort']));
        }

        $result = $this->db->get($this->table_name)->result_array();

        return $result;
    }

    public function create($source,$source_class){
        $data = new $source_class($source);
        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();

        if(isset($_FILES) && !empty($_FILES)){
            $this->save_photos($_FILES,$_id);
        }
        return $_id;
    }


    public function update($source, $source_id,$source_class) {
        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            $data->id = $source_id;

            $this->db->update($this->table_name, $data, ['id' => $source_id]);

            if(isset($_FILES) && !empty($_FILES)){
                $this->save_photos($_FILES,$source_id);
            }

            return true;
        }
        return false;
    }

    public function save_photos($photos,$user_id){

        foreach($photos as $up_key => $up_file){

            if(empty($up_file['name'])){
                continue;
            }

            $_config = (isset($this->photo_config[$up_key]))?$this->photo_config[$up_key]:false;
            if(!$_config){
                continue;
            }
            $_url = 'images/'. $this->save_path .'/'. $user_id .'/'. $_config['path'] ;

            $FolderWork = new FolderWork($_url);
            $FolderWork->chek_and_make_path();
            $FolderWork->clear();

            $UploadImage = new UploadImage;
            $UploadImage->do_action($up_file ,'/'. $FolderWork->path .'/', $_config['width'], $_config['height'], 10000, $user_id, 90, $_config['method']);
            $UploadImage->addOptions($_config['options']);
            $UploadImage->save_format = 'png';
            $UploadImage->save();

        }

    }


}