<?php $this->load->view('frontend/default/header'); ?>
<?php $this->load->view('frontend/default/nav_bar'); ?>
    <div class="content registration">
        <div class="wrapper">
            <div class="main-block">
                <h2><?php echo LANG('label_master_reg'); ?></h2>
                <?php if(false): ?>
                <div class="subtitle">
                    <div class="girl"></div>
                    <div class="info">
                        <?php echo LANG('label_registration_rule'); ?>
                    </div>
                </div>
                <?php endif; ?>
                <p class="sub"><?php echo LANG('label_registration_phrase'); ?></p>
                <p class="<?php echo isset($success_message)?'':'hidden'; ?>" id="message-box"><?php echo isset($success_message)?$success_message:''; ?></p>
                <div class="form-wrapper">
                    <form enctype="multipart/form-data" id="register-form" action ="<?php echo site_url('/registration');?>" method="post" accept-charset="utf-8">
                        <div class="row main">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="field">
                                    <label for=""><?php echo LANG('label_name'); ?><span class="require"> *</span></label>
                                    <input required name="name" type="text">
                                </div>
                                <div class="field">
                                    <label for=""><?php echo LANG('label_surname'); ?><span class="require"> *</span></label>
                                    <input required name="surname" type="text"></div>
                                <?php if(false): ?>
                                <div class="field">
                                    <label for=""><?php echo LANG('label_inn'); ?></label>
                                    <input name="inn" type="number">
                                </div>
                                <div class="field">
                                    <label for=""><?php echo LANG('label_passport'); ?></label>
                                    <input name="passport" type="text"></div>
                                <div class="field">
                                    <label for=""><?php echo LANG('label_passport_when'); ?></label>
                                    <input name="passport_data" type="text">
                                </div>
                                <?php endif; ?>
                                <div class="field">
                                    <label for=""><?php echo LANG('label_email'); ?><span class="require"> *</span></label>
                                    <input required name="email" type="email"/>
                                    <div class="" id="email_unique">
                                        <?php echo LANG('label_unique_constr'); ?>
                                    </div>
                                </div>

                            </div>
                            <div class="second col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="field tel">
                                    <label for=""><?php echo LANG('label_phone_number'); ?> 1<span class="require"> *</span></label>
                                    <div class="row">

                                        <select name="prefix1" class="prefix">
                                            <option value="+38">+38</option>
                                            <option value="+79">+79</option>
                                        </select>
                                        <input required name="index1" class="index" type="number"/>
                                        <input required name="phone1" type="number"/>
                                    </div>
                                    <div class="" id="phone_unique">
                                        <?php echo LANG('label_unique_ph_constr'); ?>
                                    </div>
                                </div>
                                <div class="field tel">
                                    <label for=""><?php echo LANG('label_phone_number'); ?> 2</label>
                                    <div class="row">

                                        <select name="prefix2" class="prefix">
                                            <option value="+38">+38</option>
                                            <option value="+79">+79</option>
                                        </select>
                                        <input name="index2" class="index" type="number"/>
                                        <input name="phone2" type="number"/>
                                    </div>
                                </div>
                                <div class="field tel">
                                    <label for=""><?php echo LANG('label_phone_number'); ?> 3</label>
                                    <div class="row">

                                        <select name="prefix3" class="prefix" >
                                            <option value="+38">+38</option>
                                            <option value="+79">+79</option>
                                        </select>
                                        <input name="index3" class="index" type="number"/>
                                        <input name="phone3" type="number"/>
                                    </div>
                                </div>
                                <?php if(false): ?>
                                    <div class="field tel">
                                        <label for=""><?php echo LANG('label_phone_number'); ?> 4</label>
                                        <div class="row">

                                            <select name="prefix4" class="prefix">
                                                <option value="+38">+38</option>
                                                <option value="+79">+79</option>
                                            </select>
                                            <input name="index4" class="index" type="number"/>
                                            <input name="phone4" type="number"/>
                                        </div>

                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="row region-block">
                            <div class="areas">
                                <input name="areas[]" type="hidden">
                                <p><?php echo LANG('label_enter_regions'); ?></p>
                                <p><?php echo LANG('label_delivery_area'); ?></p>

                                <ul>
                                    <?php
                                    if(isset($areas)):
                                        foreach($areas as $area): ?>
                                            <li>
                                                <input data-area_id="<?php echo $area['id']; ?>" name="areas[<?php echo $area['id']; ?>]" type="checkbox">
                                                <label ><?php echo $area['area_name'.SQL_LANG]; ?></label>
                                            </li>
                                        <?php endforeach;
                                    endif;
                                    ?>

                                </ul>
                            </div>
                            <div class="districts">
                                <p><?php echo LANG('label_delivery_district'); ?></p>
                                <ul>

                                </ul>

                            </div>
                        </div>
                        <div class="row image-up-block">
                            <p><?php echo LANG('label_photo'); ?></p>
                            <div class="label">
                                <input type="file" name="avatar" id="avatar">
                                <img src="<?php echo base_url(); ?>assets/img/master_default_mini.png" alt="no-image">
                            </div>
                            <label for="avatar"><?php echo LANG('label_change_photo'); ?></label>
                        </div>
                        <div class="row">
                            <p class="message hidden"><?php echo LANG('label_form_error'); ?></p>
                            <p class="load_image hidden"><?php echo LANG('label_load_image_error'); ?></p>
                            <button id="reg" form="register-form" type="submit" class="btn btn-type3"><?php echo LANG('label_action_registration'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/default/footer'); ?>