<div class="container-fluid clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="cform" class="form-added-item" accept-charset="utf-8" method="post" action="http://skysite.lemon.ua/panel/save"><div class="crumbs row">
                    <?php $this->load->view('panel/templates/action_button');?>
                    <?php $this->load->view('panel/default/system_message');?>
                    <?php echo validation_errors('<div class="alert alert-danger" style="margin-top: 16px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');?>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="general" href="#general" aria-expanded="true"><?php echo LANG('label_tab_general')?></a></li>
                        <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="media" href="#media" aria-expanded="true"><?php echo LANG('label_tab_other_info')?></a></li>
                        <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="painter_pay" href="#painter_pay" aria-expanded="true"><?php echo LANG('label_tab_painter_pay')?></a></li>
                        <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="seo" href="#seo" aria-expanded="false"><?php echo LANG('label_tab_seo')?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active" role="tabpanel">
                            <div class="row">
                                <div class="col-md-8 inside_tab">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                            <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="data_<?php echo $lang_key;?>" href="#data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                            <?php $_first = false;
                                        endforeach;?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php $_first = true;?>
                                        <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                            <div id="data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                                <input type="text" placeholder="" class="input" value="<?php echo $data->{'title_'. $lang_key};?>" name="title_<?php echo $lang_key;?>"><br>
                                                <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_description');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                                <textarea name="description_<?php echo $lang_key;?>" id="description" aria-hidden="true"><?php echo $data->{'description_'. $lang_key};?></textarea>
                                            </div>
                                            <?php $_first = false;
                                        endforeach;?>
                                    </div>
                                </div>
                                <div class="col-md-4 parametrs_in_line">
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_params');?></h5>
                                    <div class="widget-body">
                                        <p><span><? echo LANG ('label_category');?></span>
                                            <?php $this->load->view('panel/templates/select_tree',array('select_data'=>$categories_tree,'name'=>'category_id','selected'=>array($data->category_id),'params' => array()));?>
                                        </p>
                                        <p><span><? echo LANG ('label_alias');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->alias;?>" name="alias"> </p>
                                        <p><span><? echo LANG ('label_article');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->article;?>" name="article"> </p>
                                        <p><span><? echo LANG ('label_flag_new');?></span><input type="checkbox" placeholder="" class="input" <?php echo($data->flag_new)? 'checked="checked"':'';?> value="1" name="flag_new"> </p>
                                        <p><span><? echo LANG ('label_flag_action');?></span><input type="checkbox" placeholder="" class="input" <?php echo($data->flag_action)? 'checked="checked"':'';?> value="1" name="flag_action"> </p>
                                        <p><span><? echo LANG ('label_flag_sale');?></span><input type="checkbox" placeholder="" class="input" <?php echo($data->flag_sale)? 'checked="checked"':'';?> value="1" name="flag_sale"> </p>
                                    </div>
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_prices');?></h5>
                                    <div class="widget-body">
                                        <p><span><? echo LANG ('label_price');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->price;?>" name="price"> </p>
                                        <!--<p><span><? echo LANG ('label_price_sale');?></span><input type="text" placeholder="" class="input" value="<?php echo $data->price_sale;?>" name="price_sale"> </p>-->
                                     </div>
                                    <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_icon');?></h5>
                                    <div id="preview" class="media">
                                        <div id="product-medias-new" class="row stripped">
                                            <div class="col-md-2 upload_img_conteiner">
                                                <div class="wrapper"><label class="help" for="preview_image" style="display: none;">Нажмите для замены</label></div><img src="<?php echo ($data->icon)? '/images/products/'. $data->id .'/icon/'. $data->icon : 'http://placehold.it/41x44'?>" class="upload_image">                                            <label style="padding: 38px 57px;" class="upload_image_label btn green label-input-file hidden">
                                                    <input type="file" title="Upload icon" name="photo_icon" multiple="" class="upload_image_button preview" style="position:static; opacity:0; visibility:hidden;width: 0;display:none;" id="preview_image">
                                                    <input type="hidden" name ="icon" value="<?php echo $data->icon;?>">
                                                    <span><?php echo LANG('label_upload_icon'); ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="media" class="tab-pane" role="tabpanel">
                             <?php $this->load->view('panel/products/image_templates/'. $image_template);?>
                        </div>
                        <div id="painter_pay" class="tab-pane" role="tabpanel">
                            <div class="row inside_tab">
                                    <?php for($i=0;$i < 8;$i++):?>
                                        <div class="col-md-3 tab-content">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo ($i == 0)? LANG('label_pay_limit_base'):LANG('label_pay_limit_level').' '.$i;?></h5>
                                            <p><span><? echo LANG ('label_pay_limit');?></span><input type="text" placeholder="" class="input" value="<?php echo isset($pay_limits[$i]['bound'])?$pay_limits[$i]['bound']:'';?>" name="pay[<?php echo $i;?>][bound]"> </p>
                                            <p><span><? echo LANG ('label_pay_percent');?></span><input type="text" placeholder="" class="input" value="<?php echo isset($pay_limits[$i]['percent'])?$pay_limits[$i]['percent']:'';?>" name="pay[<?php echo $i;?>][percent]"> </p>
                                        </div>
                                    <?php endfor;?>
                            </div>
                        </div>
                        <div id="seo" class="tab-pane" role="tabpanel">
                            <div class="row inside_tab">
                                <ul role="tablist" class="nav nav-tabs">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <li class="<?php echo($_first)? 'active':'';?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="seo_data_<?php echo $lang_key;?>" href="#seo_data_<?php echo $lang_key;?>" aria-expanded="true"><? echo LANG('label_'. $lang_key);?></a></li>
                                        <?php $_first = false;
                                    endforeach;?>
                                </ul>
                                <div class="col-md-8 tab-content">
                                    <?php $_first = true;?>
                                    <?php foreach($lang_exist as $lang_key => $lang_name):?>
                                        <div id="seo_data_<?php echo $lang_key;?>"class="tab-pane <?php echo($_first)? 'active':'';?>" role="tabpanel">
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_title');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input name="seo_title_<?php echo $lang_key;?>" type="text" placeholder="" class="input" value="<?php echo $data->{'seo_title_'. $lang_key};?>" ><br>
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_keywords');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <input name="seo_keywords_<?php echo $lang_key;?>" type="text" placeholder="" class="input" value="<?php echo $data->{'seo_keywords_'. $lang_key};?>" ><br>
                                            <h5 class="widget-name"><i class="icon-reorder"></i><?php echo LANG('label_seo_description');?> (<? echo LANG('label_'. $lang_key);?>)</h5>
                                            <textarea name="seo_description_<?php echo $lang_key;?>" id="description" aria-hidden="true"><?php echo $data->{'seo_description_'. $lang_key};?></textarea>
                                        </div>
                                        <?php $_first = false;
                                    endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('.media#preview').mouseover(function(){
            $(this).find('.help').show();
        }).mouseleave(function(){
                $(this).find('.help').hide();
        });

        var orig_data_id=<?php echo ($data->id=='create')?-1:$data->id; ?>;
        var cat_select=$('select[name="category_id"]');
//        var orig_template_id=<?php //echo isset($image_template_id)?$image_template_id:''; ?>//;
        cat_select.change(function(){
            var form= $('form').serialize();
            var cur_cat= $('select[name="category_id"] option[selected="selected"]');
            var choosen_cat=$('select[name="category_id"] option:selected').val();
            var data=form+'&orig_cat='+cur_cat.val()+'&orig_data_id='+orig_data_id;
            if(choosen_cat!=cur_cat){
                $('select[name="category_id"] option[selected="selected"]').removeAttr('selected');
                $('select[name="category_id"] option:selected').attr('selected','selected');
            }
            var data_id=<?php echo ($data->id=='create')?-1:$data->id; ?>;
                $.ajax({
                    url: '<?php echo site_url(); ?>'+'/panel/products/ajax_products',
                    type: 'POST',
                    dataType:'html',
                    data:data,
                    success: function (data) {
                        $('.tab-content>#media').children().remove();
                        $('.tab-content>#media').append(data);
                    }
                });

        });


        /*
        $('#product-medias input').click(function(){
            var value=$(this).val();
            var state=$(this).is(':checked');

            if($(this).parent().parent().find('label').length>1){

            }
        });*/
    });
    
    
</script>
<style>
    .upload_img_conteiner{
        height: 100px;
        width: 235px;
    }
    .upload_img_conteiner img{
        display: block;
        margin: 27px auto 0;
        max-height: 100%;
        max-width: 100%;
    }
    #product-medias label.small-input-block input[type='radio']{
        float: left;
        display: inline;
        width: 15px;
    }
    .media#preview img{
        cursor:pointer;

    }
    .media#preview .upload_img_conteiner{
        width:100%;
        padding: 0px 40px;
    }
    .media#preview .help{
        display: none;
        font-weight: 200;
        cursor:pointer;
        text-align:center;
        border:1px solid #fff;
        background-color:#000;
        opacity:0.7;
        color:#fff;
        width:200px;
        height: 35px;
        position:absolute;
        line-height:35px;
        margin-top:30px;
    }
    .media#preview .help:hover{
        opacity: 0.4;
    }
    .media#preview .wrapper{
        width:200px;
        margin:auto;
    }
    #print-categories label{
        cursor:pointer;
    }
</style>