<?php

class Filter {

    public $alias;
    public $parent_id;
    public $title_ru;
    public $title_ua;
    public $sort;


    public function __construct($source = null) {
        $this->alias              = isset($source['alias'])?              $source['alias']             :'';
        $this->parent_id          = isset($source['parent_id'])?          $source['parent_id']         :0;
        $this->title_ru           = isset($source['title_ru'])?           $source['title_ru']          :'';
        $this->title_ua           = isset($source['title_ua'])?           $source['title_ua']          :'';
        $this->data               = isset($source['data'])?               $source['data']              :'';
        $this->sort               = isset($source['sort'])?               $source['sort']              :0;
    }
}

class Filter_model extends MY_Model {

    protected $table_name = 'filters';
    protected $t_filters_to_categories = 'filters_to_categories';
    protected $t_filters_to_products = 'filters_to_products';
    protected $t_filters_to_prints = 'filters_to_print';
    protected $t_filters_to_tovars = 'filters_to_tovars';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_all_filter($pagination = false,$only_on = false) {

        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $_sort = str_replace("title","title". SQL_LANG,$_sort);
            $this->db->order_by($_sort);
        }
        if($only_on == true){
            $this->db->where(array('t1.active'=>1));
        }

        $result = $this->db->select(
            't1.*, t1.title'. SQL_LANG .' as title,'.
            '(SELECT title'. SQL_LANG .' FROM '. $this->table_name .' as t2 WHERE t2.id = t1.parent_id) as filter_group'
        )
            ->from($this->table_name .' as t1')
            ->where('t1.parent_id != 0')
            ->get()->result_array();

        return $result;
    }

    public function get_filters_and_groups($category_id = false,$only_on = false){

        if($only_on == true){
            $this->db->where(array('t1.active'=>1));
        }

        if($category_id == true){
            $this->db->where('t1.parent_id IN(SELECT filter_id FROM '. $this->t_filters_to_categories .' WHERE '. $this->t_filters_to_categories.'. category_id = '. $category_id .')');
        }

        $result = $this->db->select(
            't1.*, t1.title'. SQL_LANG .' as title,'.
            '(SELECT title'. SQL_LANG .' FROM '. $this->table_name .' as t2 WHERE t2.id = t1.parent_id) as filter_group'
            )
            ->from($this->table_name .' as t1')
            ->where('t1.parent_id != 0')
            ->get()->result_array();

        if(!$result){
            return array();
        }
        foreach($result as $row){
            $data[$row['filter_group']][] = $row;
        }

        return $data;
    }

    public function get_filters_by_tovar($tovar_id){
        $res = $this->db->select($this->table_name.'.*')
                        ->from($this->table_name)
                        ->where($this->table_name.'.id IN(SELECT filter_id FROM '. $this->t_filters_to_tovars .' WHERE tovar_id = '. $tovar_id .')')
                        ->get()->result_array();

        if(!$res){
            return array();
        }

        foreach($res as $filter){
            $data[$filter['parent_id']][$filter['id']] = $filter;
        }
        return $data;
    }

    public function get_product_filter_ids($product_id = false){

        if(!$product_id){
            return array();
        }

        $result = $this->db->select($this->t_filters_to_products .'.*')
                           ->from($this->t_filters_to_products)
                           ->where(array('product_id'=>$product_id))
                           ->get()->result_array();

        if(!$result){
            return array();
        }

        foreach($result as $val){
            $data[$val['filter_id']] = $val['filter_id'];
        }

        return $data;
    }

    public function get_print_filter_ids($print_id = false){
        if(!$print_id){
            return array();
        }

        $result = $this->db->select($this->t_filters_to_prints .'.*')
            ->from($this->t_filters_to_prints)
            ->where(array('print_id'=>$print_id))
            ->get()->result_array();

        if(!$result){
            return array();
        }

        foreach($result as $val){
            $data[$val['filter_id']] = $val['filter_id'];
        }

        return $data;

    }

    public function get_filters_for_author_prints($filters_alias,$filter_groups){

        if(count($filter_groups) > 0){
           $this->db->where_in('t2.parent_id',$filter_groups);
        }

        $_res = $this->db->select('t2.id,'.
        't2.parent_id,'.
        't2.alias,'.
        't2.data,'.
        't2.title'.SQL_LANG .' as title, '.
        $this->table_name. '.data as template, '.
        $this->table_name. '.title'.SQL_LANG .' as group_title ')
            ->from($this->table_name)
            ->join($this->table_name .' as t2','t2.parent_id = '.$this->table_name .'.id','left')
            ->order_by($this->table_name .'.sort')
            ->get();

        if($_res->num_rows() == 0){
            return false;
        }
        $res = $_res->result_array();
        $data['filters_on'] = array();
        $data['filters_on_alias'] = array();
        foreach($res as $item){

            $data['filters'][$item['parent_id']]['title'] = $item['group_title'];
            $data['filters'][$item['parent_id']]['template'] = $item['template'];
            $data['filters'][$item['parent_id']]['sub'][$item['id']] = $item;
            $data['filters'][$item['parent_id']]['sub'][$item['id']]['on'] = (in_array($item['alias'],$filters_alias))? 1:0;
            if(in_array($item['alias'],$filters_alias)){
                $data['filters_on'][$item['parent_id']][$item['id']] = $item['id'];
                $data['filters_on_alias'][$item['alias']] = $item['id'];
            }
        }

        return $data;
    }

    public function get_filters_for_catalog($filters_alias,$category_id){

        $_res = $this->db->select('t2.id,'.
                                  't2.parent_id,'.
                                  't2.alias,'.
                                  't2.data,'.
                                  't2.title'.SQL_LANG .' as title, '.
                                  $this->table_name. '.data as template, '.
                                  $this->table_name. '.title'.SQL_LANG .' as group_title ')
                         ->from($this->table_name)
                         ->where($this->table_name.'.id IN(SELECT filter_id FROM '. $this->t_filters_to_categories.' WHERE '. $this->t_filters_to_categories.'.category_id = '. $category_id .') AND '. $this->table_name.'.active = 1 AND '. $this->table_name.'.catalog_view = 1')
                         ->join($this->table_name .' as t2','t2.parent_id = '.$this->table_name .'.id','left')
                         ->order_by($this->table_name .'.sort')
                         ->get();

        if($_res->num_rows() == 0){
            return false;
        }
        $res = $_res->result_array();
        $data['filters_on'] = array();
        $data['filters_on_alias'] = array();
        foreach($res as $item){

            $data['filters'][$item['parent_id']]['title'] = $item['group_title'];
            $data['filters'][$item['parent_id']]['template'] = $item['template'];
            $data['filters'][$item['parent_id']]['sub'][$item['id']] = $item;
            $data['filters'][$item['parent_id']]['sub'][$item['id']]['on'] = (in_array($item['alias'],$filters_alias))? 1:0;
            if(in_array($item['alias'],$filters_alias)){
                $data['filters_on'][$item['parent_id']][$item['id']] = $item['id'];
                $data['filters_on_alias'][$item['alias']] = $item['id'];
            }
        }

        return $data;
    }

    public function real_tovars_filtes($tovars_ids,$filters_data){


        if($filters_data && !$tovars_ids){
           return $this->filters_no_tovars($filters_data);
        }

        $_res = $this->db->select('DISTINCT filter_id',false)
                         ->from($this->t_filters_to_tovars)
                         ->where_in('tovar_id',$tovars_ids)
                         ->get()->result_array();


        foreach($_res as $filter){
            $data[$filter['filter_id']] = $filter['filter_id'];
        }


        foreach($filters_data['filters'] as $filter_group_id => $filter_group){
            foreach($filter_group['sub'] as $filter_id => $filter_data){
                if(!isset($data[$filter_id])){
                    unset($filters_data['filters'][$filter_group_id]['sub'][$filter_id]);
                }
            }
        }
        return $filters_data;
    }
    public function real_pictures_filtes($print_ids,$filters_data){


        if(($filters_data && !$print_ids) || count($print_ids) == 0){
           return $this->filters_no_tovars($filters_data);
        }

        $_res = $this->db->select('DISTINCT filter_id',false)
                         ->from($this->t_filters_to_prints)
                         ->where_in('print_id',$print_ids)
                         ->get()->result_array();


        foreach($_res as $filter){
            $data[$filter['filter_id']] = $filter['filter_id'];
        }


        foreach($filters_data['filters'] as $filter_group_id => $filter_group){
            foreach($filter_group['sub'] as $filter_id => $filter_data){
                if(!isset($data[$filter_id])){
                    unset($filters_data['filters'][$filter_group_id]['sub'][$filter_id]);
                }
            }
        }
        return $filters_data;
    }

    private function filters_no_tovars($filters_data){

        foreach($filters_data['filters'] as $group_id => $group_data){
            if(!isset($filters_data['filters_on'][$group_id])){
                $filters_data['filters'][$group_id]['sub'] = array();
            }
        }

        return $filters_data;
    }

    public function get_filters(){
        $_res = $this->get_all_filter();
        foreach($_res as $item){
            $data[$item['id']] = $item;
        }

        return $data;
    }
}