<?php

class Partner
{
    public function __construct($source = null)
    {
        $this->id = isset($source['id']) ? $source['id'] : '';
        $this->name = isset($source['name']) ? $source['name'] : '';
        $this->company = isset($source['company']) ? $source['company'] : '';
        $this->link = isset($source['link']) ? $source['link'] : '';
        $this->regions = isset($source['regions']) ? $source['regions'] : '';
        $this->phone = isset($source['phone']) ? $source['phone'] : '';
        $this->active = isset($source['active']) ? $source['active'] : 0;
    }
}

class Partner_model extends MY_Model
{

    protected $table_participated_users = 'participated_users';
    protected $table_name = 'partners';
    protected $table_masters_to_regions = 'partners_to_regions';
    protected $table_master_images = 'partner_images';

    public function __construct()
    {

        parent::__construct();
    }

    protected $kyiv_districts=array(
        '8036100000',
        '8036300000',
        '8036400000',
        '8036600000',
        '8038000000',
        '8038200000',
        '8038500000',
        '8038600000',
        '8038900000',
        '8039100000'
    );

    public function get_partner_by_id($id, $pagination = false)
    {

        if ($pagination) {
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $this->db->order_by(str_replace("-", " ", $pagination['sort']));
        }
        $master_info = $this->db->get_where($this->table_name, array('id' => (int)$id))->row_array();
        $this->db->from($this->table_masters_to_regions);
        $this->db->where('master_id', $master_info['id']);
        $regions=$this->db->get()->result_array();
        foreach($regions as $key=>$item){
            $regions[$key]=$item['region_id'];
        }
        $master_info['regions']=$this->region_model->get_regions_by_ids($regions);

//        foreach ($regions as $key => $region) {
//            $master_info['regions'][$key] = $this->region_model->get_region_by_id($region['region_id']);
//        }
        //var_dump($master_info);
        $master_info['image'] = $this->get_master_image_by_id($master_info['id']);
        return new Master($master_info);
    }

    public function get_all_partners($pagination = false)
    {
        if ($pagination) {
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $this->db->order_by(str_replace("-", " ", $pagination['sort']));
        }

        $result = $this->db->get($this->table_name)->result_array();
        return $result;
    }


    public function get_partners_by_region($region_data, $pagination = false)
    {
        if ($pagination) {
            $this->db->limit($pagination['count'], $pagination['count'] * ($pagination['page'] - 1));
            $this->db->order_by(str_replace("-", " ", $pagination['sort']));
        }
        $this->db->distinct();
        $this->db->select(
            $this->table_name . '.id,' .
            $this->table_name . '.name,' .
            $this->table_name . '.company,' .
            $this->table_name . '.phone,' .
            $this->table_name . '.link,' .
            $this->table_name . '.active'
        );
        $this->db->from($this->table_name);
        $this->db->join($this->table_masters_to_regions, $this->table_name . ".id=" . $this->table_masters_to_regions . '.partner_id', 'inner');
        $this->db->where($this->table_masters_to_regions . '.region_id', $region_data['id']);
        $master_info = $this->db->get()->result_array();
        $result = array();
        foreach ($master_info as $master) {
            unset($master['master_id']);
            $result[] = new Partner($master);
        }

        return $result;
    }

    public function partner_update($source, $source_id,$source_class){
        $_data = $this->get_by_id($source_id);
        if ($_data) {
            $data = new $source_class($source);
            $regions=$data->regions;
            unset($data->regions);
            unset($data->image);
            $data->id = $source_id;
            $this->db->update($this->table_name, $data, ['id' => $source_id]);

            if(isset($regions)&&$regions!=''&&!empty($regions)){
                $flag=false;
                $source['id']=$data->id;
                $source['regions']=$regions;
                foreach ($source['regions'] as $key => $active) {
                    if(in_array(intval($key),$this->kyiv_districts)){
                        $flag=true;
                    }
                }
                if($flag){
                    $source['regions']['8000000000']='on';
                }

                $this->update_locations($source);
            }

            return true;
        }
        return false;
    }

    public function create_partner($source)
    {

        $data = new Partner($source);

        unset($data->id);
        $regions=$data->regions;
        unset($data->regions);

        $this->db->insert($this->table_name, $data);
        $_id = $this->db->insert_id();
        //$_id=$source['id'];
        if (isset($_id) && $_id != NULL && $_id != '' && isset($source['regions'])) {

            $flag=false;
            foreach ($source['regions'] as $key => $active) {

                if(in_array(intval($key),$this->kyiv_districts)){
                    $flag=true;
                }
                $d_exists=false;
                if (!is_array($active)) {
                    foreach ($source['regions'] as $k1 => $k2) {
                        if ($key == $k1) {
                            if (is_array($k2)) {
                                $d_exists = true;
                            }
                        }
                    }
                }

                if(!$d_exists){

                    if($active!='on'&&is_array($active)){

                        foreach(array_keys($active) as $arr_key){
                            $this->db->insert($this->table_masters_to_regions, array('partner_id' => $_id, 'region_id' =>  $arr_key));
                        }

                    } else {

                        $this->db->insert($this->table_masters_to_regions, array('partner_id' => $_id, 'region_id' => $key));
                    }

                    if($flag){
                        $this->db->insert($this->table_masters_to_regions, array('partner_id' => $_id, 'region_id' => '8000000000'));
                    }
                }
            }

        }

        return $_id;
    }

    public function update_locations($source){
        $_id=$source['id'];
        if (isset($_id) && $_id != NULL && $_id != '') {
            $this->db->delete($this->table_masters_to_regions, array('partner_id' => $_id));

            foreach ($source['regions'] as $key => $active) {

                if(in_array(intval($key),$this->kyiv_districts)){
                    $flag=true;
                }
                $d_exists=false;
                if (!is_array($active)) {
                    foreach ($source['regions'] as $k1 => $k2) {
                        if ($key == $k1) {
                            if (is_array($k2)) {
                                $d_exists = true;
                            }
                        }
                    }
                }

                if(!$d_exists){

                    if($active!='on'&&is_array($active)){

                        foreach(array_keys($active) as $arr_key){
                            $this->db->insert($this->table_masters_to_regions, array('partner_id' => $_id, 'region_id' =>  $arr_key));
                        }

                    } else {

                        $this->db->insert($this->table_masters_to_regions, array('partner_id' => $_id, 'region_id' => $key));
                    }

                }
            }
        }

    }

}