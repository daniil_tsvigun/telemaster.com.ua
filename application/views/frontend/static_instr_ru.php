
<!DOCTYPE html>
<html>
<head>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tele Master</title>
    <link href="/assets/lib/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="/assets/lib/chosen/1.4.2/chosen.min.css" rel="stylesheet"> -->
    <!-- <link href="/assets/lib/font-awesome/4.3.0/font-awesome.min.css" rel="stylesheet"> -->
    <link href="/assets/lib/animate.css/3.3.0/animate.min.css" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="http://www.telemaster.com.ua/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css?v=1.3.6"/>
    <script type="text/javascript">
        var site_url = "http://www.telemaster.com.ua/ru";
    </script>

</head>
<body>
<script>
 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
 
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a
,m)
 
})(window,document,'script','https://www.google-analytics.com/analytics.js',
'ga');

  ga('create', 'UA-88207792-1', 'auto');
  ga('send', 'pageview');

</script>

<div id="vk_api_transport"></div><div data-id="mobile_btn" class="lang-menu">
    <ul>
        <li class=""><a href="http://www.telemaster.com.ua/ua/instructions">Укр</a></li>
        <li class="current"><a href="http://www.telemaster.com.ua/ru/instructions">Рус</a></li>
    </ul>
</div>
<div data-id="mobile_btn" class="master ">
    <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
</div>
<section class="nav_bar" >
    <div class="wrapper">
        <div class="menu">
            <a href="http://www.telemaster.com.ua/ru" class="logo-anchor"></a>
            <ul class="main-menu slideInDown animated">

                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru">Главная</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/calling">Найти мастера</a>
                </li>
                <li>
                    <a class="" href="http://www.telemaster.com.ua/ru/partners">Найти провайдера</a>
                </li>
                <li>
                    <a class="current" href="http://www.telemaster.com.ua/ru/instructions">Инструкции</a>
                </li>
<!--                <li>-->
<!--                    <a class="--><!--" href="--><!--">--><!--</a>-->
<!--                </li>-->
                <li class="master ">
                    <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
                </li>
            </ul>
            <div id="menu-btn"></div>
            <div class="lang-menu">
                <ul>
                   
                    <li class=""><a href="http://www.telemaster.com.ua/ua/instructions">Укр</a></li>
                    <li class="current"><a href="http://www.telemaster.com.ua/ru/instructions">Рус</a></li>
                </ul>
            </div>
            <div class="master ">
                <a class="" href="http://www.telemaster.com.ua/ru/registration">Регистрация мастера</a>
            </div>
        </div>

    </div>
</section>    <script type="text/javascript">
        $(document).ready(function(){
            $('body').css('height','100%');
            //$('.content').css({height:$(window).height()-152+'px'});
        });
    </script>
    <script>
    /**
    * Функция, которая отслеживает клики по исходящим ссылк в Analytics.
    * Эта функция применяет в качестве аргумента строку с действительным URL, после чего использует ее
    * как ярлык события. Если указать beacon в качестве метода передачи, данные обращений
    * будут отправляться с использованием метода navigator.sendBeacon в поддерживающих его браузерах.
    */

    var trackOutboundLink = function(url) {
       ga('send', 'event', 'outbound', 'click', url, {
         'transport': 'beacon',
         'hitCallback': function(){
            window.open(url, '_blank');
            
        }
       });
    }
    </script>
     <div class="content instructions">
        <div class="wrapper">
            <div class="main-block">
                <h2>Инструкции для перенастройки</h2>
                                                    <p class="subtitle" >Выберите, пожалуйста, модель тюнера.</p>
                    <p class="subtitle" >Если Вашего тюнера нет в списке, воспользуйтесь  универсальными инструкциями.</p>
                    <p class="subtitle" >Если не удалось настроить тюнер самостоятельно с помощью инструкций, проверьте <a href="http://www.telemaster.com.ua/ru/faq/parameters">параметры</a> или воспользуйтесь <a href="http://www.telemaster.com.ua/ru/calling" >помощью мастера</a></p>

                    <div class="table-wrapper">
                        <div class="tabs-container">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div data-id="normal" class="active wrapper">
                                    <ul class="chanel-ico">
                                        <li><img src="/assets/img/icons/tv-ico/1plus1-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/unian-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/plus-plus-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/bigudi-ico.png" alt=""></li>
                                                                                <li><img src="/assets/img/icons/tv-ico/MB_logo_redes_2015_color_mini.png" alt=""></li>
                                    </ul>
                                    <div class="filter-white"></div>
                                </div>
                            </div>
                            <div  class="col-md-6 col-sm-6 col-xs-6">
                                <div data-id="slm" class="wrapper">
                                    <ul class="chanel-ico">
                                        <li><img src="/assets/img/icons/tv-ico/stb-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/n-canal-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/ictv-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/m1-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/m2-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/tv-ico/qtv-ico.png" alt=""></li>
                                        <li><img src="/assets/img/icons/Logo_24_kanal_round_mini.png"></li>  
                                    </ul>
                                    <div class="filter-white"></div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="t_manuf table_border-right">Марка</th>
                                <th class="t_model table_border-right">Модель тюнера</th>
                                <th class="t_txt table_border-right" >
                                    <div class="sub-cat-top col-md-12 col-sm-12 col-xs-12">
                                        <ul class="chanel-ico">
                                            <li><img src="/assets/img/icons/tv-ico/1plus1-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/unian-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/plus-plus-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/bigudi-ico.png" alt=""></li>
                                                                                        <li><img src="/assets/img/icons/tv-ico/MB_logo_redes_2015_color_mini.png" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6">Текстовые инструкции</div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6">Видео инструкции</div>
                                </th>
                                <th class="t_video" >
                                    <div class="sub-cat-top col-md-12 col-sm-12 col-xs-12">
                                        <ul class="chanel-ico">
                                            <li><img src="/assets/img/icons/tv-ico/stb-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/n-canal-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/ictv-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/m1-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/m2-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/tv-ico/qtv-ico.png" alt=""></li>
                                            <li><img src="/assets/img/icons/Logo_24_kanal_round_mini.png"></li>
                                        </ul>
                                    </div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6">Текстовые инструкции</div>
                                    <div class="sub-cat col-md-6 col-lg-6 col-sm-6 col-xs-6">Видео инструкции</div>
                                </th>
                                <th class="text_video_instr mobile">Инструкции</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr class="universal-guide_row">
                                <td class="t_manuf table_border-right"></td>
                                <td class="table_border-right">
                                    <div>Универсальная инструкция</div>
                                </td>
                                <td data-tab-id="normal" class="tab-object guide_cell table_border-right t_txt">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                <a target="_blank" href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                        <a class="mobile text" target="_blank" href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="http://www.telemaster.com.ua/ru/instructions/model/0">Смотреть</a>
                                        <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/0"></a>
                                    </div>
                                </td>
                                <td data-tab-id="slm" class="twisted tab-object guide_cell t_video">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a target="_blank" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                        <a class="mobile text" target="_blank" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/0">Смотреть</a>
                                        <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/0"></a>
                                    </div>
                                </td>
                            </tr>
                                                                                                <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>AG</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>R-5500d</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/1">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/1"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/1">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/1"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Allsky</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>X-610</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/28">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/28"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/28">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/28"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Amiko</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>Mini HD SE, HD 8260+, impulse sat/sat wi-fi</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/34">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/34"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/34">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/34"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Arrox</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>Free tiger</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/2">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/2"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/2">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/2"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Axess</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>HP-8000</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/3">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/3"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/3">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/3"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>BigSat</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>6500, DSR 6500 Prima – DSR 7500 Prima</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/4">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/4"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/4">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/4"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Chess</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>Digital 4000-4100</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/5">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/5"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/5">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/5"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>COSMOSAT</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>7100</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/27">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/27"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/27">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/27"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Digital</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>3000-6100, Telekom mini X-1001, FTA</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/6">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/6"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/6">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/6"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>DSN</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>GR 6100 FTA</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/7">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/7"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/7">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/7"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Euromax</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>777</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/8">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/8"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/8">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/8"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Eurosat</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>3023, 3023 Super, 3023 DVR, 8004, 8004 Super</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/31">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/31"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/31">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/31"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Eurosky</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>3023, 8004 SUPER</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/3_Настройка_1+1_eurosky_dvb_3023_super_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/30">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/30"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/3_Настройка_тюнера_Eurosky_DVB_3023_SUPER_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/30">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/30"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>EuroStar</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>100 FTA</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/9">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/9"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/9">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/9"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Euston</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>STV-2005</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/10">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/10"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/10">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/10"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Frontier</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>618, 638, 828, DV-65</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/11">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/11"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/11">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/11"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>GI</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>HD Micro Plus</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/5_Настройка_1+1_GI_hd_micro_plus_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/33">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/33"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/5_Настройка_тюнера_GI_HDMicroPlus_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/33">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/33"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>GI</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>Matrix Lite, HD Mini, HD Micro</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/6_Настройка_1+1_GI_matrix_lite_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/6_Настройка_1+1_GI_matrix_lite_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/6_Настройка_1+1_GI_matrix_lite_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/6_Настройка_1+1_GI_matrix_lite_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/35">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/35"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/6_Настройка_тюнера_GI_Matrix_Lite_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/6_Настройка_тюнера_GI_Matrix_Lite_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/6_Настройка_тюнера_GI_Matrix_Lite_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/6_Настройка_тюнера_GI_Matrix_Lite_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/35">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/35"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Globo</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>3000-9100a</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/12">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/12"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/12">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/12"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Joker</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>11000, Media 1045 Plus</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/13">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/13"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/13">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/13"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Lumax</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>698</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/14">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/14"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/14">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/14"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Neosat</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>1600, 1600 Plus</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/15">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/15"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/15">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/15"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Openbox</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>X-600, X-620</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/2_Настройка_1+1_Openbox_x600_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/29">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/29"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/2_Настройка_тюнера_OB_X600_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/29">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/29"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Openbox</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>S1, S2 Mini HD/ HD+, S3 mini HD, S3 CI HD, X-590 CI, X-540, X-560, F-500 FTA</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/7_Настройка_1+1_Openbox_S3_mini_hd_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/7_Настройка_1+1_Openbox_S3_mini_hd_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/7_Настройка_1+1_Openbox_S3_mini_hd_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/7_Настройка_1+1_Openbox_S3_mini_hd_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/36">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/36"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/7_Настройка тюнера_ОВ_S3_mini_HD_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/7_Настройка тюнера_ОВ_S3_mini_HD_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/7_Настройка тюнера_ОВ_S3_mini_HD_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/7_Настройка тюнера_ОВ_S3_mini_HD_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/36">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/36"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Openbox</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>SX4, SX4c Base, SX6, SX9, SX9 Combo HD</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/8_Настройка_1+1_Openbox_sx4c base_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/8_Настройка_1+1_Openbox_sx4c base_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/8_Настройка_1+1_Openbox_sx4c base_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/8_Настройка_1+1_Openbox_sx4c base_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/37">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/37"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/8_Настройка тюнера_OB_SX4_Base_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/8_Настройка тюнера_OB_SX4_Base_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/8_Настройка тюнера_OB_SX4_Base_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/8_Настройка тюнера_OB_SX4_Base_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/37">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/37"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Openbox</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>S4-S9 HD PVR</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/9_Настройка_1+1_Openbox_S4_hd_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/9_Настройка_1+1_Openbox_S4_hd_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/9_Настройка_1+1_Openbox_S4_hd_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/9_Настройка_1+1_Openbox_S4_hd_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/38">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/38"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/9_Настройка_тюнера_OB_S4HD_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/9_Настройка_тюнера_OB_S4HD_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/9_Настройка_тюнера_OB_S4HD_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/9_Настройка_тюнера_OB_S4HD_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/38">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/38"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>OpenFox</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>4100</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/16">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/16"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/16">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/16"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Opentel</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>ODS 3500F</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/17">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/17"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/17">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/17"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Opticum</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>2003 FTA, 3000-5000, 7000c</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/18">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/18"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/18">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/18"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Orton</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>4050c, 4100c</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/19">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/19"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/19">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/19"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Orton</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>9500 HD, 9600-HD Prima, 9600-HD-TS</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/4_Настройка_1+1_Orton_9500_hd_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/4_Настройка_1+1_Orton_9500_hd_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/4_Настройка_1+1_Orton_9500_hd_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/4_Настройка_1+1_Orton_9500_hd_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/32">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/32"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/4_Настройка_тюнера_ORTON_9500_HD_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/4_Настройка_тюнера_ORTON_9500_HD_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/4_Настройка_тюнера_ORTON_9500_HD_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/4_Настройка_тюнера_ORTON_9500_HD_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/32">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/32"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>SAB</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>S5000</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/20">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/20"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/20">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/20"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Sad</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>5500</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/21">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/21"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/21">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/21"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Star track</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>510, 550D, 650D CI, 750D CI, 750D CU, SR-55x</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/22">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/22"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/22">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/22"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Tiger</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>Star 8100</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/23">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/23"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/23">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/23"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>Truman</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>618, 638, DV-65, TM-150</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/24">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/24"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/24">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/24"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>WinQuest</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>4050c, 4100c</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/25">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/25"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/25">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/25"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                    <tr>
                                        <td class="t_manuf table_border-right">
                                            <div>YU-MA-TU</div>
                                        </td>
                                        <td class="t_model table_border-right">
                                            <div>MX-300 – MX-1000</div>
                                        </td>
                                        <td data-tab-id="normal" class="tab-object t_txt table_border-right" >
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf'); return false;"  href="http://www.telemaster.com.ua/uploads/1_Настройка_1+1_Orton_4100c_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="http://www.telemaster.com.ua/ru/instructions/model/26">Смотреть</a>
                                                <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/26"></a>
                                            </div>
                                        </td>
                                        <td data-tab-id="slm" class="twisted tab-object t_video">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <a target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf">Читать</a>
                                                <a class="mobile text" target="_blank" onclick="trackOutboundLink('http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf'); return false;" href="http://www.telemaster.com.ua/uploads/1_Настройка_тюнера_Orton4100c_SLM_рус-min.pdf"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                    <a href="http://www.telemaster.com.ua/ru/instructions/model/slm/26">Смотреть</a>
                                                    <a class="mobile video" href="http://www.telemaster.com.ua/ru/instructions/model/slm/26"></a>
                                                                                            </div>
                                        </td>
                                    </tr>
                                                                                        </tbody>
                        </table>
                    </div>
                            </div>
        </div>
    </div>
    <script>
        var ua=navigator.userAgent;
        if(ua.search(/Safari/)>-1){
            $('a').removeAttr('onclick');     
        }
    </script>
<div class="hidden rate popup fadeIn animated">
    <div class="wrapper">
        <div class="wrap">
            <div class="box">
                <div class="close"></div>
                <div class="rate">
                    <div class="levels" data-choosen="null">
                        <ul>
                            <li class="" data-level_id="0" id="level0"><div></div></li>
                            <li class="" data-level_id="1" id="level1"><div></div></li>
                            <li class="" data-level_id="2" id="level2"><div></div></li>
                            <li class="" data-level_id="3" id="level3"><div></div></li>
                            <li class="" data-level_id="4" id="level4"><div></div></li>
                        </ul>
                    </div>
                    <div class="btn-box">
                        <div id="vote" class="btn btn-type3">Проголосовать</div>
                    </div>
                    <div class="auth-box">
                        <div class="fb_auth"></div>
                        <div class="vk_auth"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer> 
            <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <a href="http://www.telemaster.com.ua/ru/about">О нас</a>
                <a href="http://www.telemaster.com.ua/ru/faq">Вопросы и ответы</a>
                <a href="http://www.telemaster.com.ua/ru/rules">Правила пользования сайтом</a>
                <a href="http://www.telemaster.com.ua/ru/reports">Правила модерации отзывов</a>
                <a href="http://www.telemaster.com.ua/ru/polices">Политика конфиденциальности</a>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 center">
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p>Администратор сайта: </p>
                <p>info@telemaster.com.ua</p>
                <div class="btns special">
                    <div id="youtube_btn">
                        <a target="_blank" href="https://www.youtube.com/channel/UCUrsf864ut8p5UETF4Ym6Hw">&nbsp;</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="rights">
        <div class="wrapper">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <div class="logo"></div>
            </div>
            <div class="center col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <p>© 2016 All rights reserved</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <p>
                    <a href="http://lemon.ua/">Создание сайта  Lemon.ua</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="/assets/js/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/lib/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="/assets/lib/chosen/1.4.2/chosen.jquery.min.js"></script> -->
<script type="text/javascript" src="/assets/lib/animate.css/3.3.0/animate.min.css" ></script>
