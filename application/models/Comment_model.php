<?php

class Comment {

    public $id;

    public function __construct($source = null) {
        $this->user_id	       = isset($source['user_id'])?        $source['user_id']       :0;
        $this->master_id	   = isset($source['master_id'])?      $source['master_id']     :0;
        $this->user_name	   = isset($source['user_name'])?      $source['user_name']     :'';
        $this->text	           = isset($source['text'])?           $source['text']          :'';
        $this->active	       = isset($source['active'])?         $source['active']        :0;
    }
}

class Comment_model extends MY_Model {

    protected $table_name = 'comments';
    protected $t_buyer_users = 'buyer_users';
    protected $t_painter_users = 'painter_users';
    protected $t_pictures = 'pictures';
    protected $t_tovars = 'tovars';

    public function __construct() {
        parent::__construct();
    }

    public function get_master_comments($master_id,$pagination = false){
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $this->db->order_by($_sort);
        }
        $result = $this->db->select(
            $this->table_name .'.*,
                           '.$this->table_name.'.user_name as author,
                           '.$this->table_name.'.master_id as master'
            ,false)
            ->from($this->table_name)
            ->where(array('master_id'=>$master_id))
            ->order_by('date',"desc")
            ->get()->result_array();
        return $result;
    }

    public function get_last_user_comment($user_id){
        $date=$this->db->select_max('date')
            ->from($this->table_name)
            ->where(array('user_id'=>$user_id))
            ->get()
            ->row();
        $d=new DateTime($date->date);
        var_dump($d->getTimestamp());
        $this->db->reset_query();
        $result=$this->db->from($this->table_name)
            ->where(array('user_id'=>$user_id))
            ->where('date >=', $d->getTimestamp()-86400)
            ->where('date <=', $d->getTimestamp()+86400)
            ->get()
            ->result_array();

        return $result;
    }

    public function get_all_comment($pagination = false){
        if($pagination){
            $this->db->limit($pagination['count'],$pagination['count']*($pagination['page']-1));
            $_sort = str_replace("-"," ",$pagination['sort']);
            $this->db->order_by($_sort);
        }

        $result = $this->db->select(
                            $this->table_name .'.*,
                           '.$this->table_name.'.user_name as author,
                           '.$this->table_name.'.master_id as master'
                            ,false)
                            ->from($this->table_name)
                            ->get()->result_array();
        return $result;
    }

    public function set_comment($source){

        $_res = $this->master_model->get_master_by_id($source['master_id']);
        if(!$_res){
            return false;
        }

        $data = new Comment($source);
        $this->db->insert($this->table_name,$data);
        return true;
    }

    public function del_comment($source){
        $data = new Comment($source);
        $this->db->where(array('user_id'=>$data->user_id,'role_id'=>$data->role_id,'tovar_id'=>$data->tovar_id,'print_id'=>$data->print_id))->delete($this->table_name);
    }


}