<?php

class Cart_model extends CI_Model {


    public function __construct() {
        parent::__construct();
    }

    public function add_tovar($tovar_to_cart){

        $cart = $this->get_cart();
        $cart_key = implode("_",$tovar_to_cart);
        if(isset($cart['data'][$cart_key])){
            $cart['data'][$cart_key]['count'] += 1;
        }else{
            $_data = $this->tovar_model->get_by_alias($tovar_to_cart['tovar_alias'],true);
            if(!$_data){
                return false;
            }
            foreach($tovar_to_cart as $key => $param){
                if(strpos($key,"filter") !== false){
                    $tovar_to_cart['filters'][$param] = $param;
                }
            }
            $cart['data'][$cart_key] = array_merge($tovar_to_cart,$_data);
            $cart['data'][$cart_key]['count'] = 1;

        }
        $this->set_cart($cart);

        return true;
    }

    public function add_promo($promo_word){
        $cart = $this->get_cart();
        $cart['promo'] = $promo_word;
        $this->set_cart($cart);

        return true;
    }

    public function change_count($tovar_id,$count){

        $cart = $this->get_cart();
        if(isset($cart['data'][$tovar_id])){
            $cart['data'][$tovar_id]['count'] = $count;
        }
        $this->set_cart($cart);

        return true;

    }

    public function change_pack_postal($tovar_id,$pack,$postal){

        $cart = $this->get_cart();
        if(isset($cart['data'][$tovar_id])){
            $cart['data'][$tovar_id]['pack'] = $pack;
            $cart['data'][$tovar_id]['postal'] = $postal;
        }
        $this->set_cart($cart);

        return true;

    }

    public function del_tovar($tovar_id){

        $cart = $this->get_cart();
        unset($cart['data'][$tovar_id]);
        $this->set_cart($cart);

        return true;
    }

    public function get_actual_cart(){

        $_data = $this->get_cart();
        return isset($_data['data'])? $_data['data']:array();
    }

    public function get_delivery_cart(){

        $_data = $this->get_cart();
        return isset($_data['delivery'])? $_data['delivery']:array();
    }

    public function get_buyer_cart(){

        $_data = $this->get_cart();
        return isset($_data['buyer'])? $_data['buyer']:array();
    }

    public function get_status_cart(){

        $_data = $this->get_cart();

        $result = array(
            'cart_count'=>0,
            'cart_price'=>0,
            'delivery'=>'',
            'delivery_price'=>0,
            'present'=>'',
            'present_price'=>0,
            'promo_code'=>'',
            'promo_price'=>0,
            'promo_discount'=>0,
        );
        if(!isset($_data['data']) || count($_data['data']) == 0){
            return $result;
        }


        $promo_data = (isset($_data['promo']))?$this->promo_model->get_promo_by_word($_data['promo']): false;

        foreach($_data['data'] as $key => $item){
            $result['cart_count'] += $item['count'];
            $result['cart_price'] += $item['count']*$item['price'];
            if(isset($promo_data['categories'][$item['category_id']])){
                $_promo_discount = ($promo_data['type'] == 'percent')? ($item['price']* $promo_data['value'])/100 : $promo_data['value'];
                $result['promo_discount'] += $item['count']*$_promo_discount;
                $result['promo_tovars'][$key] = $_promo_discount;
            }
        }

        $result['promo_price'] = $result['cart_price'] - $result['promo_discount'];
        $result['promo_code'] = ($promo_data)? $_data['promo']:'';

        if(isset($_data['delivery'])){
            $result['delivery'] = $_data['delivery']['type'];
            $result['delivery_price'] = $_data['delivery']['price'];
            $result['promo_price'] += $result['delivery_price'];
        }

        return $result;
    }

    /**
     * Returns weight for all items in cart.
     *
     * @return int|float
     */
    public function get_cart_weight()
    {
        $data   = $this->get_cart();
        $alias  = get_templates_data_alias();
        $weight = 0;

        if (!$data) {
            return $weight;
        }

        foreach ($data['data'] as $item) {
            $tmpl       = $item['template'];
            $itemWeight = $alias[$tmpl]['delivery_weight'];
            $isMultiply = $alias[$tmpl]['delivery_multiplication'];

            if ($isMultiply) {
                $itemWeight = $itemWeight * $item['count'];
            }

            $weight += $itemWeight;
        }

        return $weight;
    }

    public function action_delivery($data){
        $cart = $this->get_cart();

        $cart['delivery'] = $data;
        $this->set_cart($cart);

        return true;
    }

     public function action_buyer($data){
        $cart = $this->get_cart();
        $cart['buyer'] = $data;
        $this->set_cart($cart);

        return true;
    }

    public function get_cart(){
        if(!isset($_SESSION['cart'])){
            $_SESSION['cart'] = array();
        }

        return $_SESSION['cart'];
    }

    private function set_cart($data){
        $_SESSION['cart'] = $data;
    }

    public function clear_cart(){
        $_SESSION['cart'] = array();
    }

}
