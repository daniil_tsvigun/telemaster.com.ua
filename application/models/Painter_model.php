<?php
class User_painter extends User {
    public function __construct($source = null) {
        parent::__construct($source);
        $this->vk_link        = isset($source['vk_link'])       ?$source['vk_link']        :'';
        $this->instagram_link = isset($source['instagram_link'])?$source['instagram_link'] :'';
        $this->behance_link   = isset($source['behance_link'])  ?$source['behance_link']   :'';
        $this->fb_link        = isset($source['fb_link'])       ?$source['fb_link']        :'';
        $this->tw_link        = isset($source['tw_link'])       ?$source['tw_link']        :'';
        $this->description    = isset($source['description'])   ?$source['description']    :'';
        $this->country        = isset($source['country'])       ?$source['country']        :'';
        $this->city           = isset($source['city'])          ?$source['city']           :'';
        $this->alias          = isset($source['alias'])         ?$source['alias']          :make_unic_alias(translit($this->name));
        $this->subs           = isset($source['subs'])          ?$source['subs']           :0;
        $this->role_id = 4;
    }
}

class Painter_model extends User_model {

    protected $table_name = 'painter_users';
    protected $save_path = 'painters';


    public function __construct() {
        parent::__construct();
    }


}