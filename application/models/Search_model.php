<?php

class Search_model extends MY_Model {

    protected $table_name = 'favorites';
    protected $t_tovars = 'tovars';
    protected $t_pictures = 'pictures';
    protected $t_tag_to_print = 'tag_to_print';
    protected $t_tags = 'tags';
    protected $fields = [];
    protected $date_fields = [];
    protected $date_format = 'd.m.y H:i';
    protected $obj = null;

    public function __construct() {
        parent::__construct();
    }

    public function get_search_tovars($category_id,$search){

        $tovar_ids = array();
        $categories_ids = get_all_child(array(0=>$category_id),'categories');
        $_print_ids = 'SELECT print_id FROM '. $this->t_tag_to_print .' WHERE tag_id IN(SELECT id FROM '. $this->t_tags .' WHERE name LIKE \'%'. $search .'%\')';
        $_tovars = $this->db->select('id')
                            ->from($this->t_tovars)
                            ->where('
                                (
                                    picture_id IN('. $_print_ids .') OR
                                    (
                                        title_ua LIKE \'%'. $search .'%\' OR
                                        title_ru LIKE \'%'. $search .'%\' OR
                                        alias LIKE \'%'. $search .'%\'
                                    )
                               )')
                            ->where('category_id IN('. implode(",",$categories_ids) .')')
                            ->order_by('category_id')
                            ->get()->result_array();
        if($_tovars){
            foreach($_tovars as $item){
                $tovar_ids[] = $item['id'];
            }
        }

        return $tovar_ids;
    }

}